using UnityEngine;
using System.Collections;

public class SlotNotificationContainer : MonoBehaviour {
	
	int sound_number_ = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void PlaySound()
	{
		Framework.Instance.audio_manager().Play("16_Combo Hit MIssile "+sound_number_+"v", transform.parent.transform, 1.0F, 1.0F, false);
	}
	
	public void SetSoundNumber(int sound_number)
	{
		sound_number_ = sound_number;
	}
}
