using UnityEngine;
using System.Collections;
using SimpleJSON;

public class AmmoBuyButtonPrefab : MonoBehaviour {
	
	private Store store_;
	private PlayerData player_data_;
	private JSONNode store_info_; //Static info for each item
	
	bool buy_enabled_ = true;
	
	// Use this for initialization
	void Awake ()
	{
		Messenger.AddListener("UpdateAmmoPrefab",Refresh);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Refresh()
	{
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		store_ = Framework.Instance.InstanceComponent<Store>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		int ammo_max = 1;
		if ((bool)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get ("IsMultiplayer") == false)
			ammo_max = store_info_["dd_ammo_level"]["upgrades"][player_data_.GetInt("dd_ammo_level")]["effect"].AsInt;
		
		int ammo_in_stock = 0;
		if ((bool)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get ("IsMultiplayer") == false)
			ammo_in_stock = player_data_.GetInt("dd_ammo");
		else
		{
			if ( player_data_.GetInt("dd_ammo") >= 1)
				ammo_in_stock = 1;
		} 
		
		//Debug.Log(player_data_.GetInt("dd_ammo"));
		transform.Find("AmmoNumTextMesh").GetComponent<tk2dTextMesh>().text = ammo_in_stock.ToString()+"/"+ammo_max.ToString();
		transform.Find("AmmoNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		string cost_text = "";
		
		if( ammo_in_stock == 0)
		{
			transform.Find("AmmoRedSprite").gameObject.SetActive(true);
			transform.Find("AmmoBlueSprite").gameObject.SetActive(false);
		}
		else
		{
			transform.Find("AmmoRedSprite").gameObject.SetActive(false);
			transform.Find("AmmoBlueSprite").gameObject.SetActive(true);
		}
		
		if ( ammo_in_stock >= ammo_max){
			transform.Find("AmmoBuyButton").gameObject.SetActive(false);
			//transform.Find("AmmoBuyButton").GetComponent<tk2dButton>().enabled = false;
			transform.Find("ButtonFullSprite").gameObject.SetActive(true);
			cost_text = "----";
		}
		else
		{
			transform.Find("AmmoBuyButton").gameObject.SetActive(true);
			//transform.Find("AmmoBuyButton").GetComponent<tk2dButton>().enabled = true;
			transform.Find("ButtonFullSprite").gameObject.SetActive(true);
			cost_text = FormattingWithZeros(store_info_["ammo"]["upgrades"][ammo_in_stock]["cost"].AsInt.ToString (),4);
		}
		
		transform.Find("AmmoCoinsTextMesh").GetComponent<tk2dTextMesh>().text = cost_text;
		transform.Find("AmmoCoinsTextMesh").GetComponent<tk2dTextMesh>().Commit();
	}
	
	
	void BuyPressed(){
		//Debug.Log("Buy");
		if (buy_enabled_ == true)
		{
			//Debug.Log("enabled");
			bool purchase = store_.BuyItem("ammo",1);
			if(purchase == true){
				Refresh();
				Messenger.Broadcast("RefreshCoins");
				Framework.Instance.audio_manager().Play("40_Menu Buy Upgrade", transform, 1.0F, 1.0F, false);
			}
			/*
			else{
				GameObject.Find("MainCamera").GetComponent<PopUpWindowCoinsStore>().Init();
			}
			*/
		}
	}
	
	
	public void SetBuyEnabled (bool is_enabled)
	{
		buy_enabled_ = is_enabled;
	}
	
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");
		
		return txt;
	}
	
	
	void OnDestroy(){
		Messenger.RemoveListener("UpdateAmmoPrefab",Refresh);
	}
}
