using UnityEngine;
using System.Collections;

public class BattleLogRowPrefab : MonoBehaviour {
	
	public GameObject revenge_button_container_;
	public GameObject enemy_name_text_mesh_;
	public GameObject enemy_ranking_text_mesh_;
	public GameObject you_won_sprite_;
	public GameObject you_lost_sprite_;
	public GameObject lost_score_text_mesh_;
	public GameObject won_score_text_mesh_;
	
	// Use this for initialization
	void Awake () {
		
	}
	
	
	public void Init (bool has_won, string enemy_name, int enemy_ranking, int score)
	{
		enemy_name_text_mesh_.GetComponent<tk2dTextMesh>().text = enemy_name;
		enemy_name_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		enemy_ranking_text_mesh_.GetComponent<tk2dTextMesh>().text = enemy_ranking.ToString();
		enemy_ranking_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		if (has_won == true)
		{
			you_won_sprite_.GetComponent<MeshRenderer>().enabled = true;
			you_lost_sprite_.GetComponent<MeshRenderer>().enabled = false;
			won_score_text_mesh_.GetComponent<MeshRenderer>().enabled = true;
			lost_score_text_mesh_.GetComponent<MeshRenderer>().enabled = false;
			//revenge_button_container_.SetActiveRecursively(false);
			
			won_score_text_mesh_.GetComponent<tk2dTextMesh>().text = score.ToString();
			won_score_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		}
		else if (has_won == false)
		{
			you_won_sprite_.GetComponent<MeshRenderer>().enabled = false;
			you_lost_sprite_.GetComponent<MeshRenderer>().enabled = true;
			won_score_text_mesh_.GetComponent<MeshRenderer>().enabled = false;
			lost_score_text_mesh_.GetComponent<MeshRenderer>().enabled = true;
			//revenge_button_container_.SetActiveRecursively(true);
			
			lost_score_text_mesh_.GetComponent<tk2dTextMesh>().text = score.ToString();
			lost_score_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		}
		
	}
	
	
	private string TrimString(string text){
		string new_string = text.TrimEnd('"');
		new_string = new_string.TrimStart('"');
	
		return new_string;
	}

}
