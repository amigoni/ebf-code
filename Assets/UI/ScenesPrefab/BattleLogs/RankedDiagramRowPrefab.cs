using UnityEngine;
using System.Collections;

public class RankedDiagramRowPrefab : MonoBehaviour {
	
	public GameObject sliced_sprite_;
	public GameObject pos_text_;
	public GameObject quantity_text_;
	
	// Use this for initialization
	void Start () {
	
	}
	
	
	public void Init (string pos, int quantity){
		float width = 185;
		
		sliced_sprite_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(width, 40);
		
		pos_text_.GetComponent<tk2dTextMesh>().text = pos +"'s";
		pos_text_.GetComponent<tk2dTextMesh>().Commit();
		
		
		quantity_text_.GetComponent<tk2dTextMesh>().text = quantity.ToString();
		quantity_text_.GetComponent<tk2dTextMesh>().Commit();
		
		float xPos = width-5;
		quantity_text_.transform.position = new Vector3(xPos,quantity_text_.transform.position.y,quantity_text_.transform.position.z);
	}
}
