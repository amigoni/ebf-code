using UnityEngine;
using System.Collections;

public class BossBattleLogRoundIconPrefab : MonoBehaviour {
	
	public GameObject icon_dead_round_sprite_;
	public GameObject icon_pass_round_sprite_;
	public GameObject icon_fail_round_sprite_;
	public GameObject icon_empty_round_sprite_;
	public GameObject icon_annihilated_round_sprite_;
	public GameObject icon_perfect_round_sprite_;
			
	// Use this for initialization
	void Awake () {
		Reset();
	}
	
	void Reset()
	{
		icon_dead_round_sprite_.SetActive(false);
		icon_pass_round_sprite_.SetActive(false);
		icon_fail_round_sprite_.SetActive(false);
		icon_empty_round_sprite_.SetActive(false);
		icon_annihilated_round_sprite_.SetActive(false);
		icon_perfect_round_sprite_.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init(string round_result){
		if(round_result == "won")
			icon_pass_round_sprite_.SetActive(true);	
		else if(round_result == "perfect")
			icon_perfect_round_sprite_.SetActive(true);	
		else if (round_result == "passed" || round_result == "survived")
			icon_fail_round_sprite_.SetActive(true);
		else if (round_result == "destroyed")
			icon_dead_round_sprite_.SetActive(true);
		else if (round_result == "annihilated")
			icon_annihilated_round_sprite_.SetActive(true);
		else
			icon_empty_round_sprite_.SetActive(true);	
			
	}
	
	public void SetType(Game.BossResultType round_result){
		Reset();
		switch(round_result)
		{
			case Game.BossResultType.won:
				icon_pass_round_sprite_.SetActive(true);	
			break;
			case Game.BossResultType.annihilated:
				icon_annihilated_round_sprite_.SetActive(true);	
			break;
			case Game.BossResultType.destroyed:
				icon_dead_round_sprite_.SetActive(true);	
			break;
			case Game.BossResultType.survived:
				icon_fail_round_sprite_.SetActive(true);	
			break;
			case Game.BossResultType.none:
				icon_empty_round_sprite_.SetActive(true);	
			break;
		}	
	}
}
