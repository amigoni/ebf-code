using UnityEngine;
using System.Collections;

public class GlobalStandingsRowPrefab : MonoBehaviour {
	
	
	public GameObject players_text_mesh_;
	public GameObject ranking_text_mesh_;
	
	public GameObject players_sprite_;
	
	public GameObject icon_dr_sprite_;
	public GameObject your_icon_dr_sprite_;
	public GameObject your_nd_sprite_;
	public GameObject your_player_position_text_mesh_;
	public GameObject your_pos_left_sprite_;
	public GameObject your_pos_right_sprite_;
	public GameObject your_ranking_text_mesh_;
	public GameObject your_rd_sprite_;
	public GameObject your_st_sprite_;
	public GameObject your_th_sprite_;
	public GameObject dd_indicator_sprite_;
	
	public GameObject icon_br_sprite_;
	public GameObject boss_your_st_sprite_;
	public GameObject boss_your_nd_sprite_;
	public GameObject boss_your_rd_sprite_;
	public GameObject boss_your_th_sprite_;
	public GameObject boss_your_pos_left_sprite_;
	public GameObject boss_your_pos_right_sprite_;
	public GameObject boss_your_icon_br_sprite_;
	public GameObject boss_indicator_sprite_;
	public GameObject pos_sprite_;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init (bool is_dd, string ranking_name, int players_quantity, bool are_you_in_it, int your_ranking, int your_position)
	{
		
		ranking_text_mesh_.GetComponent<tk2dTextMesh>().text = ranking_name;
		ranking_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		//players_text_mesh_.GetComponent<tk2dTextMesh>().text = players_quantity.ToString();

		players_text_mesh_.GetComponent<tk2dTextMesh>().text = "----";
		players_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
	
		your_player_position_text_mesh_.renderer.enabled = false;
		your_ranking_text_mesh_.renderer.enabled = false;
		
		icon_dr_sprite_.renderer.enabled = false;
		your_icon_dr_sprite_.renderer.enabled = false;
		your_pos_left_sprite_.renderer.enabled = false;
		your_pos_right_sprite_.renderer.enabled = false;
		your_st_sprite_.renderer.enabled = false;
		your_nd_sprite_.renderer.enabled = false;
		your_rd_sprite_.renderer.enabled = false;
		your_th_sprite_.renderer.enabled = false;
		
		icon_br_sprite_.renderer.enabled = false;
		boss_your_icon_br_sprite_.renderer.enabled = false;
		boss_your_pos_left_sprite_.renderer.enabled = false;
		boss_your_pos_right_sprite_.renderer.enabled = false;
		boss_your_st_sprite_.renderer.enabled = false;
		boss_your_nd_sprite_.renderer.enabled = false;
		boss_your_rd_sprite_.renderer.enabled = false;
		boss_your_th_sprite_.renderer.enabled = false;
		boss_indicator_sprite_.renderer.enabled = false;
		
		if (is_dd == true)
		{
			icon_dr_sprite_.renderer.enabled = true;
			icon_br_sprite_.renderer.enabled = false;
		}	
		else
		{
			icon_dr_sprite_.renderer.enabled = false;
			icon_br_sprite_.renderer.enabled = true;	
		}
		
		if (are_you_in_it == true){
			
			pos_sprite_.renderer.enabled = false;
				
			if (is_dd == true){
				icon_dr_sprite_.renderer.enabled = true;
				your_pos_left_sprite_.renderer.enabled = true;
				your_pos_right_sprite_.renderer.enabled = true;
				players_sprite_.renderer.enabled = false;
				your_icon_dr_sprite_.renderer.enabled = true;
				dd_indicator_sprite_.renderer.enabled = true;
				
				if (your_position == 1)
					your_st_sprite_.renderer.enabled = true;
				else if (your_position == 2)
					your_nd_sprite_.renderer.enabled = true;
				else if (your_position == 3)
					your_rd_sprite_.renderer.enabled = true;
				else 
					your_th_sprite_.renderer.enabled = true;
			}
			else
			{
				icon_br_sprite_.renderer.enabled = true;
				boss_your_pos_left_sprite_.renderer.enabled = true;
				boss_your_pos_right_sprite_.renderer.enabled = true;
				players_sprite_.renderer.enabled = false;
				boss_your_icon_br_sprite_.renderer.enabled = true;
				boss_indicator_sprite_.renderer.enabled = true;
				
				if (your_position == 1)
					boss_your_st_sprite_.renderer.enabled = true;
				else if (your_position == 2)
					boss_your_nd_sprite_.renderer.enabled = true;
				else if (your_position == 3)
					boss_your_rd_sprite_.renderer.enabled = true;
				else 
					boss_your_th_sprite_.renderer.enabled = true;
			}
			
			string your_position_string = "POS: "+your_position.ToString();
			if (your_position == -1)
				your_position_string = "100+";
			
			if (is_dd == false)
			{
				your_player_position_text_mesh_.GetComponent<tk2dTextMesh>().color = new Color(1.0f, 1.0f, 1.0f);
				your_ranking_text_mesh_.GetComponent<tk2dTextMesh>().color = new Color(1.0f, 1.0f, 1.0f);
			}
			
			your_player_position_text_mesh_.renderer.enabled = true;
			your_player_position_text_mesh_.GetComponent<tk2dTextMesh>().text = your_position_string;
			your_player_position_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			
			your_ranking_text_mesh_.renderer.enabled = true;
			
			string ranking_text = "";
			if (is_dd == true)
				ranking_text = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().PlayerRank.ToString();
			else
				ranking_text = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().Boss.Rank.ToString();
			
			your_ranking_text_mesh_.GetComponent<tk2dTextMesh>().text = ranking_text;
			your_ranking_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		}
		
	}
}
