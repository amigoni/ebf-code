using UnityEngine;
using System.Collections;
using EbFightAPI;

public class BossBattleLogRowPrefab : MonoBehaviour {
	
	public GameObject dr_text_mesh_;
	public GameObject enemy_name_text_mesh_;
	public GameObject lost_sprite_;
	public GameObject revenge_button_;
	public GameObject revenge_button_off_sprite_;
	public GameObject trophies_text_mesh_;
	public GameObject white_coins_text_mesh_;
	public GameObject won_sprite_;
	public GameObject xp_text_mesh_;
	public GameObject icon_container_;
	
	public GameObject boss_battle_log_round_icon_prefab_;
	
	private string unique_id_;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init (bool boss_has_won, string enemy_id, string enemy_name, int enemy_ranking, 
		int trophies_gained, int white_coins_gained , int xp_gained, string round1_result, 
		string round2_result, string round3_result, bool had_revenge, string unique_id)
	{
		unique_id_ = unique_id;
		
		revenge_button_.SetActive(false);
		revenge_button_off_sprite_.SetActive(false);
		won_sprite_.SetActive(false);
		lost_sprite_.SetActive(false);
		
		if (boss_has_won == false && had_revenge == false && unique_id_ != null)
		{
			lost_sprite_.SetActive(true);
			revenge_button_.SetActive(true);
			revenge_button_off_sprite_.SetActive(false);
		}
		else
		{
			revenge_button_.SetActive(false);
			revenge_button_off_sprite_.SetActive(true);
			won_sprite_.SetActive(true);
		}
	
		enemy_name_text_mesh_.GetComponent<tk2dTextMesh>().text = enemy_name;
		enemy_name_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		dr_text_mesh_.GetComponent<tk2dTextMesh>().text = enemy_ranking.ToString();
		dr_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		xp_text_mesh_.GetComponent<tk2dTextMesh>().text = xp_gained.ToString();
		xp_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		string trophies_gained_text = "";
		if(trophies_gained >=0)
			trophies_gained_text = "+"+trophies_gained.ToString();
		else 
			trophies_gained_text = trophies_gained.ToString();
		
		
		trophies_text_mesh_.GetComponent<tk2dTextMesh>().text = trophies_gained_text;
		trophies_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		/*
		string white_coins_gained_text = "";
		if(white_coins_gained >=0)
			white_coins_gained_text = "+"+white_coins_gained.ToString();
		else 
			white_coins_gained_text = white_coins_gained.ToString();
		
		white_coins_text_mesh_.GetComponent<tk2dTextMesh>().text = white_coins_gained_text;
		white_coins_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		*/
		
		string round_result = "";
		for (int i=0; i <3; i++)
		{
			if (i==0)
				round_result = round1_result;
			else if(i==1)
				round_result = round2_result;
			else if (i==2)
				round_result = round3_result;
			
			GameObject icon = (GameObject) Instantiate(boss_battle_log_round_icon_prefab_, new Vector3( 20 + 45*i + icon_container_.transform.position.x,icon_container_.transform.position.y, icon_container_.transform.position.z),Quaternion.identity);
			icon.transform.parent = icon_container_.transform;
			icon.GetComponent<BossBattleLogRoundIconPrefab>().Init(round_result);
		}
	}
	
	
	private void RevengePressed()
	{
		Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsMultiplayer", true);
		Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins(
			Framework.Instance.InstanceComponent<PlayerData>().GetInt("white_coins"),
			OnSync);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	

	private void OnSync(ResponseStatus status, JSONResult data)
	{
		Framework.Instance.InstanceComponent<ServerAPI>().Revenge(unique_id_, OnRevenge);
	}
	
	
	private void OnRevenge(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsRevenge", true);
			Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("MatchSession", data.As<MatchSession>());
			Application.LoadLevel("StartGame");
		}
	}
}
