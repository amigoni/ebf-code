using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using EbFightAPI;

public class BattleLogPopUpPrefab : MonoBehaviour {

	public GameObject left_scroll_;
	public GameObject left_scroll_area_;
	public GameObject right_scroll_;
	public GameObject right_scroll_area_;
	public GameObject battle_log_row_;
	public GameObject ranking_text_mesh_;
	public GameObject global_standings_row_prefab_;
	public bool is_dd_ = true;
	private string who = "dd";
	
	public GameObject tutorial_popup_;
	
	private List<GameObject> current_rows_ = new List<GameObject>();
	
	private JSONNode multiplayer_data_;
	
	private void Awake () 
	{
		//Init (); //Placed here to save Server calls. 
	}
	
	public void Init()
	{
		if (is_dd_ == false)
			who = "boss";
		
		multiplayer_data_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("MultiplayerData/MultiplayerData"));
		
		if(is_dd_)
			Framework.Instance.InstanceComponent<ServerAPI>().GetPlayerBattleLog( OnGetPlayerBattleLog );  
		else
			Framework.Instance.InstanceComponent<ServerAPI>().GetBossBattleLog( OnGetBossBattleLog );
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if(Game.FindObjectOfType( typeof(TutorialPopup) ) == null)
			if (Input.GetKeyDown(KeyCode.Escape))
				BackButtonPressed();
	}
#endif
	
	private void OnGetBossBattleLog(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			if (data.Data != "")
			{
				ranking_text_mesh_.GetComponent<tk2dTextMesh>().text = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().Boss.Rank.ToString();
				ranking_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
				
				BattleLogInfo battle_log = CreateBattleLogInfo(data.Data);
				RefreshBattleLog(battle_log.Log);
				RefreshGlobalStandings(battle_log.League, battle_log.Position);
				
				//Analytics
				Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
				analitycs_.AddEvent(KeenIOEventType.menu_opened);	
				analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "BossBattleLogScene");
				analitycs_.SendEvent(KeenIOEventType.menu_opened);
			}	
		}
		else
		{	
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("Can't Connect", "Please try again in a bit");
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
			gameObject.SetActiveRecursively(false);
			GameObject.Find("MainCamera").GetComponent<MainMenu>().multiplayerPrefab.GetComponent<MultiplayerUI>().Appear();
			Debug.Log("Error Connecting - OnGetBossBattleLog");
		}
	}
	
	
	private void OnGetPlayerBattleLog(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			if (data.Data != "")
			{
				ranking_text_mesh_.GetComponent<tk2dTextMesh>().text = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().PlayerRank.ToString();
				ranking_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
					
				BattleLogInfo battle_log_info = CreateBattleLogInfo(data.Data);
				RefreshBattleLog(battle_log_info.Log);
				RefreshGlobalStandings(battle_log_info.League, battle_log_info.Position);
				
				//Analytics
				Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
				analitycs_.AddEvent(KeenIOEventType.menu_opened);	
				analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "DDBattleLogScene");
				analitycs_.SendEvent(KeenIOEventType.menu_opened);
			}	
		}
		else
		{	
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("Can't Connect", "Please try again in a bit");
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
			gameObject.SetActiveRecursively(false);
			GameObject.Find("MainCamera").GetComponent<MainMenu>().multiplayerPrefab.GetComponent<MultiplayerUI>().Appear();
			Debug.Log("Error Connecting - OnGetPlayerBattleLog");
		}
	}
	
	
	public void RefreshBattleLog(List<EbFightAPI.BattleLog> battle_log_list_)
	{
		foreach(GameObject item in current_rows_)
		{
			Destroy(item);
			left_scroll_.GetComponent<Scrollview>().Reset();
			right_scroll_.GetComponent<Scrollview>().Reset();
		}
		
		float row_height = is_dd_ ? 80.0f: 116.0f;
		left_scroll_area_.GetComponent<tk2dUIScrollableArea>().ContentLength = battle_log_list_.Count*row_height;
		left_scroll_area_.GetComponent<tk2dUIScrollableArea>().VisibleAreaLength = left_scroll_area_.GetComponent<tk2dUIScrollableArea>().VisibleAreaLength/Framework.Instance.graphics_manager().pixel_to_point_ratio();
		
		//Battle Log
		for (int i = 0; i < battle_log_list_.Count; i++)
		{	
			GameObject row = (GameObject) Instantiate(
				battle_log_row_, 
				new Vector3(
					left_scroll_.transform.position.x, 
					-i * row_height + (is_dd_ ? (row_height / 2 + 125) : (row_height / 2 + 95)), 
					left_scroll_.transform.position.z),
				Quaternion.identity);
			
			//left_scroll_.GetComponent<Scrollview>().AddItem(row);
			row.transform.parent = left_scroll_.transform;
			
			//TO-DO fixs result
			if(is_dd_)
			{
				row.GetComponent<BattleLogRowPrefab>().Init(
					battle_log_list_[i].Modifier > 0, 
					battle_log_list_[i].OpponentName, 
					(int)battle_log_list_[i].OpponentRank, 
					(int)battle_log_list_[i].Modifier);
			}
			else
			{
				row.GetComponent<BossBattleLogRowPrefab>().Init(
					battle_log_list_[i].Modifier >= 0, 
					battle_log_list_[i].OpponentId,
					battle_log_list_[i].OpponentName, 
					(int)battle_log_list_[i].OpponentRank, 
					(int)battle_log_list_[i].Modifier,
					(int)battle_log_list_[i].WhiteCoins,
					(int)battle_log_list_[i].GainedXp,
					battle_log_list_[i].Results[0],
					battle_log_list_[i].Results[1],
					battle_log_list_[i].Results[2],
					battle_log_list_[i].HadRevenge,
					battle_log_list_[i].UniqueId);
			}
			current_rows_.Add(row);
		}
	}
	
	
	void RefreshGlobalStandings(long your_league_number, long your_standing_)
	{
		int your_ranking = -999;//multiplayer_data_[who]["ranking"].AsInt;
		int your_standing = (int) your_standing_;
		
		float row_height = 35F;
		float y_pos = -5*row_height + multiplayer_data_[who]["leagues"].AsArray.Count*row_height;
		right_scroll_area_.GetComponent<tk2dUIScrollableArea>().ContentLength = multiplayer_data_[who]["leagues"].AsArray.Count*(row_height+4);
		
		float scroll_position = 0;
		
		for (int i = multiplayer_data_[who]["leagues"].AsArray.Count-1; i >=0 ; i--)
		{
			bool are_you_in_group = false;
			if( multiplayer_data_[who]["leagues"][i]["league"].AsInt == (int) your_league_number )
				are_you_in_group = true;
			
			GameObject row = (GameObject) Instantiate(global_standings_row_prefab_, 
				new Vector3(right_scroll_area_.transform.position.x-22, y_pos,right_scroll_.transform.position.z),
				Quaternion.identity);
			
			y_pos -= row_height;
			
			if (are_you_in_group == true)
			{
				//Lower twice cause it's double height this row.
				y_pos -= row_height;
				if (i < 4)
					scroll_position = 180;
			}	
			
			row.transform.parent = right_scroll_.transform;		
			
			row.GetComponent<GlobalStandingsRowPrefab>().Init(is_dd_, multiplayer_data_[who]["leagues"][i]["name"].ToString() ,10, are_you_in_group, your_ranking, your_standing);
			current_rows_.Add(row);
		}
		
		Vector3 old_position =  right_scroll_area_.transform.Find("RightScroll").position;
		GameObject.Find("RightScroll").transform.position = new Vector3(old_position.x, right_scroll_area_.transform.position.y+scroll_position, old_position.z);
	}
	
	
	void BackButtonPressed ()
	{
		gameObject.SetActiveRecursively(false);
		GameObject.Find("MainCamera").GetComponent<MainMenu>().multiplayerPrefab.GetComponent<MultiplayerUI>().Appear();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void QuestionMarkPressed()
	{
		if (is_dd_ == true)
		{
			GameObject tutorial_popup = (GameObject) Instantiate(tutorial_popup_);
			tutorial_popup.GetComponent<TutorialPopup>().Init(TutorialPopUpType.dd_battle_log_, 2F);
		}
		else
		{
			GameObject tutorial_popup = (GameObject) Instantiate(tutorial_popup_);
			tutorial_popup.GetComponent<TutorialPopup>().Init(TutorialPopUpType.boss_battle_log_, 2F);
		}
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	private string TrimString(string text)
	{
		string new_string = text.TrimEnd('"');
		new_string = new_string.TrimStart('"');
	
		return new_string;
	}
	
	private BattleLogInfo CreateBattleLogInfo(string json)
	{
		BattleLogInfo battle_log_info = new BattleLogInfo();
		JSONNode data_result = JSON.Parse(json);
		JSONNode data = data_result["result"];
		battle_log_info.Position = data["position"].AsInt;
		battle_log_info.League = data["league"].AsInt;
		
		//Log
		battle_log_info.Log = new List<BattleLog>();
		BattleLog battle_log = null;
		int data_count = data["log"].Count;
		for(int i = 0; i < data_count; i++)
		{
			battle_log = new BattleLog();
			battle_log.OpponentName = data["log"][i]["opponent_name"];
			battle_log.GainedXp = data["log"][i]["gained_xp"].AsInt;
			battle_log.Created = data["log"][i]["created"].AsInt;
			battle_log.WhiteCoins = data["log"][i]["white_coins"].AsInt;
			battle_log.HadRevenge = data["log"][i]["had_revenge"].AsBool;
			battle_log.OpponentRank = data["log"][i]["opponent_rank"].AsInt;
			battle_log.OpponentId = data["log"][i]["opponent_id"];
			battle_log.Modifier = data["log"][i]["modifier"].AsInt;
			battle_log.UniqueId = data["log"][i]["unique_id"];
			
			battle_log.Results = new List<string>();
			int results_count = data["log"][i]["results"].Count;
			for(int j = 0; j < results_count; j++)
			{
				battle_log.Results.Add(data["log"][i]["results"][j]);
			}
			
			battle_log_info.Log.Add(battle_log);
		}
		
		return battle_log_info;
	/*		
			"position": 9, 
    "log": [
      {
        "opponent_name": "iPod5", 
        "gained_xp": 0, 
        "created": 1380870501, 
        "white_coins": 4, 
        "results": [
          "won", 
          "perfect", 
          "died"
        ], 
        "had_revenge": false, 
        "opponent_rank": 52, 
        "opponent_id": "1b922954-7558-49b6-b744-722860d305bf", 
        "modifier": -2, 
        "unique_id": "1e69f566-a07a-429a-b353-f895ff64afc8_1b922954-7558-49b6-b744-722860d305bf_2013-10-04 07:08:21.037130"
      }
    ], 
    "league": 0
    */
	}
}
