using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class UpgradeStatsItemPrefab : MonoBehaviour {
	
	private JSONNode store_info_; //Static info for each item
	private Store store_;
	
	private int bought_level_ = 3;
	private int unlocked_level_ = 20;
	private float next_purchase_price_ ;
	private string stat_name_;
	private bool has_buy_option_;
	private bool is_dd_;
	private bool upgrade_enabled_ = true;
	
	public GameObject slot_notification_container_;
	public GameObject slot_notification_background_;
	public GameObject slot_text_mesh_;
	
	public GameObject tacca_;
	public GameObject[] past_tacche;
	
	private EbFightAPI.Boss boss_data_;
	
	private Dictionary<int , int > tacche_positions_;
	

	// Use this for initialization
	void Awake () 
	{
		//Store Info
		store_ = Framework.Instance.InstanceComponent<Store>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		tacche_positions_ = new Dictionary<int, int>()
		{
		    {0, 114},
		    {1, 124},
			{2, 134},
		    {3, 144},
			{4, 154},
		    {5, 164},
			{6, 174},
		    {7, 184},
			{8, 194},
		    {9, 204},
			{10, 214},
		    {11, 224},
			{12, 234},
		    {13, 244},
			{14, 254},
		    {15, 264},
			{16, 274},
		    {17, 284},
			{18, 294},
		    {19, 304}
		};
		past_tacche = new GameObject[20];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void  Init(string stat_name, bool has_buy_option, bool is_dd)
	{
		Init(stat_name, has_buy_option, is_dd , null);
	}	
	
	
	
	public void Init(string stat_name, bool has_buy_option, bool is_dd , EbFightAPI.Boss boss_data)
	{
		stat_name_ = stat_name;
		has_buy_option_ = has_buy_option;
		is_dd_ = is_dd;
		boss_data_ = boss_data;
	
		if (is_dd == true)
		{
			bought_level_ = Framework.Instance.InstanceComponent<PlayerData>().GetInt(stat_name);
			unlocked_level_ = store_.GetMaxUnlockedLevelForItem(stat_name);
		}	
		else
		{
			if (stat_name == "boss_att")
				bought_level_ = (int) boss_data.Attack;
			else if (stat_name == "boss_def")
				bought_level_ = (int) boss_data.Defence;
			else if (stat_name == "boss_power_level")
				bought_level_ = (int) boss_data.Power;
			
			unlocked_level_ = store_.GetMaxUnlockedLevelForBossItem(stat_name, (int) boss_data.Level);
			
		}
		
		
		next_purchase_price_ = store_info_[stat_name]["upgrades"][bought_level_]["next_cost"].AsFloat;
		
		transform.Find("TitleTextMesh").GetComponent<tk2dTextMesh>().text =  store_info_[stat_name]["name"].ToString();
		transform.Find("TitleTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		transform.Find("NewLabelSprite").gameObject.SetActive(false);;
		
		//Set buy button
		if(bought_level_ >= 20)
		{
			transform.Find("UpgradeBuyBackgroundSprite").gameObject.SetActive(false);;
			transform.Find("PriceTextMesh").gameObject.SetActive(false);;
			transform.Find("UpgradeButtonSprite").gameObject.SetActive(false);;
			//transform.Find("UpgradeButtonSprite").GetComponent<tk2dButton>().enabled = false;
			transform.Find("LockedSprite").gameObject.SetActive(false);;
			//Show only maxed
			transform.Find("MaximumLabelSprite").gameObject.SetActive(true);;
		}
		else if (bought_level_ < 20 && bought_level_ >= unlocked_level_)
		{
			//Show Upgrade Button
			transform.Find("PriceTextMesh").GetComponent<tk2dTextMesh>().text = "------";
			transform.Find("PriceTextMesh").GetComponent<tk2dTextMesh>().Commit();
			transform.Find("UpgradeButtonSprite").gameObject.SetActive(false);;
			//transform.Find("UpgradeButtonSprite").GetComponent<tk2dButton>().enabled = false;
			transform.Find("LockedSprite").gameObject.SetActive(true);;
			transform.Find("MaximumLabelSprite").gameObject.SetActive(false);;
		}
		else if (bought_level_ < 20 && bought_level_ < unlocked_level_)
		{
			//Show Level Max Button
			transform.Find("PriceTextMesh").GetComponent<tk2dTextMesh>().text = FormattingWithZeros(next_purchase_price_.ToString(), 5);
			transform.Find("PriceTextMesh").GetComponent<tk2dTextMesh>().Commit();
			transform.Find("UpgradeButtonSprite").gameObject.SetActive(true);;
			//transform.Find("UpgradeButtonSprite").GetComponent<tk2dButton>().enabled = true;
			transform.Find("LockedSprite").gameObject.SetActive(false);;
			transform.Find("MaximumLabelSprite").gameObject.SetActive(false);;
		}
		
		if (has_buy_option == false)
		{
			transform.Find("PriceTextMesh").gameObject.SetActive(false);;
			transform.Find("UpgradeButtonSprite").gameObject.SetActive(false);;
			//transform.Find("UpgradeButtonSprite").GetComponent<tk2dButton>().enabled = false;
			transform.Find("LockedSprite").gameObject.SetActive(false);;
			transform.Find("MaximumLabelSprite").gameObject.SetActive(false);;
			transform.Find("UpgradeBuyBackgroundSprite").gameObject.SetActive(false);;
			transform.Find("NewLabelSprite").gameObject.SetActive(false);;
			transform.Find("PinkTextMesh").gameObject.SetActive(true);;
			transform.Find("PinkSlicedSprite").gameObject.SetActive(true);;
			slot_notification_container_.SetActive(true);
		}
		else
		{
			transform.Find("PinkTextMesh").gameObject.SetActive(false);;
			transform.Find("PinkSlicedSprite").gameObject.SetActive(false);;
		}
		
		
		//Destroy old Tacche
		for(int i = 0; i < 20; i++)
		{
			Destroy(past_tacche[i]);
			past_tacche[i] = null;
		}
		
	
		//Create New Tacche
		for(int i = 0; i < 20; i++)
		{
			
			float x_shift = 0F;
			if (has_buy_option_ == false)
				x_shift = 0F;
			
			GameObject tacca = (GameObject) Instantiate(tacca_, 
				new Vector3(transform.position.x + tacche_positions_[i] - 283 + x_shift,transform.position.y-33,transform.position.z), 
				Quaternion.identity);
			
			tacca.transform.parent = gameObject.transform;
			past_tacche[i] = tacca;
			
			//Leave as i+1 to make it work properly here
			if ( i+1 <= bought_level_){
				tacca.transform.Find("UpgradeFillSprite").gameObject.SetActive(true);;
				tacca.transform.Find("UpgradeEmptySprite").gameObject.SetActive(false);;
			}
			else if ( i+1 > bought_level_ && i+1 <= unlocked_level_){
				tacca.transform.Find("UpgradeFillSprite").gameObject.SetActive(false);;
				tacca.transform.Find("UpgradeEmptySprite").gameObject.SetActive(true);;
			}
			else if ( i+1 > unlocked_level_){
				tacca.transform.Find("UpgradeFillSprite").gameObject.SetActive(false);;
				tacca.transform.Find("UpgradeEmptySprite").gameObject.SetActive(false);;
			}
		}
	}
	
	
	public void UpgradePressed()
	{
		if (upgrade_enabled_ == true)
		{	
			if (is_dd_ == false)
			{
				bool was_bought = Framework.Instance.InstanceComponent<Store>().BuyItem(stat_name_, 1);
				if (was_bought == true)
				{
					Messenger.Broadcast("UpgradeSuccessful", stat_name_);
					Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
					Init(stat_name_,has_buy_option_,is_dd_, boss_data_);
				}
			}
			else if (is_dd_ == true)
			{
				Framework.Instance.InstanceComponent<Store>().BuyItemAsynch(stat_name_, 1, RefreshDD, OnFailedPurchase);
			}
		}	
	}
	
	public void RefreshDD()
	{
		Init(stat_name_,has_buy_option_,is_dd_);
	}
	
	public void OnFailedPurchase()
	{
		Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("Can't Connect", "Please try again in a bit");
		Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
	}
	
	
	public void SetUpgradeEnabled (bool is_enabled)
	{
		upgrade_enabled_ = is_enabled;
	}
	
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");
		
		return txt;
	}
	
	int sound_number_ = 1;
	public void PlayAnimation(float delay, int sound_number)
	{
		slot_notification_container_.GetComponent<SlotNotificationContainer>().SetSoundNumber(sound_number);
		Invoke("PlaySlotsAnimation", delay);
	}	
			
	private void PlaySlotsAnimation()
	{
		slot_notification_container_.transform.parent.gameObject.SetActive(true);
		slot_notification_container_.animation.Play();
	}
			
}
