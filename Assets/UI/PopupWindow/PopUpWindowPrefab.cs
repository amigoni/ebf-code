using UnityEngine;
using System.Collections;
using SimpleJSON;

//public delegate void Callback();

public class PopUpWindowPrefab : MonoBehaviour
{
	public GameObject ok_button_;
	public GameObject no_button_;
	public GameObject yes_button_;
	
	public GameObject title_text_mesh_;
	public GameObject description_text_mesh_;
		
	public delegate void PopUpWindowDelegate ();

	private PopUpWindowDelegate on_yes_callback_;
	private PopUpWindowDelegate on_no_callback_;
	private PopUpWindowDelegate on_ok_callback_;
	
	private bool destroy_on_closing_ = false;
	
	public void set_destroy_on_closing(bool destroy_on_closing)
	{
		destroy_on_closing_ = destroy_on_closing;
	}
	
	void Awake ()
	{

	}
	
	private void InitText(string title_text, string description_text)
	{
		title_text_mesh_.GetComponent<tk2dTextMesh>().text = title_text;
		title_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		description_text_mesh_.GetComponent<tk2dTextMesh>().text = description_text;
		description_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
	}
	
	public void Init (
		string title_text,
		string description_text, 
		PopUpWindowDelegate on_yes_callback,
		PopUpWindowDelegate on_no_callback )
	{
		Init();
		
		on_yes_callback_ = on_yes_callback;
		on_no_callback_ = on_no_callback;
		
		InitText(title_text,description_text);
		yes_button_.SetActive(true);
		no_button_.SetActive(true);
		ok_button_.SetActive (false);
	}
	
	public void Init (
		string title_text,
		string description_text, 
		PopUpWindowDelegate on_ok_callback )
	{
		Init();
		
		on_ok_callback_ = on_ok_callback;
		
		InitText(title_text,description_text);
		ok_button_.SetActive(true);
		no_button_.SetActive (false);
		yes_button_.SetActive (false);
	}
	
	public void Init (
		string title_text,
		string description_text)
	{
		Init();
		
		set_destroy_on_closing(false);
		
		InitText(title_text,description_text);
		ok_button_.SetActive(true);
		no_button_.SetActive (false);
		yes_button_.SetActive (false);
	}
	
	
	void YesPressed ()
	{
		if(destroy_on_closing_)
			Destroy (gameObject);
		else
			gameObject.SetActive(false);
		
		on_yes_callback_ ();
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void NoPressed ()
	{
		if(destroy_on_closing_)
			Destroy (gameObject);
		else
			gameObject.SetActive(false);
		
		on_no_callback_ ();
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void OKPressed ()
	{
		if(destroy_on_closing_)
			Destroy (gameObject);
		else
			gameObject.SetActive(false);
		
		if (on_no_callback_ != null)
			on_ok_callback_ ();
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	public void SetZPosition(float z_position)
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, z_position);
	}
	
	private void Init()
	{		
		tk2dButton[] buttons = GetComponentsInChildren<tk2dButton>();
		Camera current_camera = Framework.Instance.scene_manager().GetCurrentCamera();
		foreach(tk2dButton button in buttons)
		{
			button.viewCamera = current_camera;
		}
		
		/*if(GameObject.Find("NoButtonSprite") != null)
			GameObject.Find("NoButtonSprite").GetComponent<tk2dButton>().viewCamera = Framework.Instance.scene_manager().GetCurrentCamera();
		if(GameObject.Find("YesButtonSprite") != null)
			GameObject.Find("YesButtonSprite").GetComponent<tk2dButton>().viewCamera = Framework.Instance.scene_manager().GetCurrentCamera();
		if(GameObject.Find("OKButtonSprite") != null)
			GameObject.Find("OKButtonSprite").GetComponent<tk2dButton>().viewCamera = Framework.Instance.scene_manager().GetCurrentCamera();
		*/
	}
}
