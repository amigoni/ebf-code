using UnityEngine;
using System.Collections;
using SimpleJSON;
using EbFightAPI;

public class CoinStoreItemPrefab : MonoBehaviour {
	
	private string identifier_;
	private string key_;
	private string type_;
	private string title_;
	private string description_;
	private float price_;
	private string price_locale_;
	private float item_quantity_; //i.e. how many coins it buys
	
	public GameObject currency_background_;
	public GameObject currency_text_mesh_;
	public GameObject white_coins_cost_background_sprite_;
	public GameObject white_coins_cost_text_mesh_;
	public GameObject kreds_cost_text_mesh_;
	public GameObject kreds_background_sprite_;
	public GameObject quantity_text_mesh_;
	public GameObject up_icon_sprite_;
	public GameObject down_icon_sprite_;
	public GameObject icon_button_;
	private GameObject scroll_area_;
	
	private PlayerData player_data_;
	
	private Store store_; //Dynamic Data for each item
	private JSONNode store_info_;
	
	private int tier_number = 0;
	
	// Use this for initialization
	void Start () 
	{
		scroll_area_ = gameObject.transform.parent.gameObject.transform.parent.gameObject;
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		store_ = Framework.Instance.InstanceComponent<Store>();	
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		icon_button_.GetComponent<tk2dUIItem>().sendMessageTarget = gameObject;
		icon_button_.GetComponent<tk2dUIItem>().SendMessageOnReleaseMethodName = "BuyPressed";
		icon_button_.GetComponent<tk2dUIItem>().SendMessageOnDownMethodName = "OnDown";
	}
	
	
	
	public void Init( string type, string key, string name, string description, int quantity, float price, string price_locale)
	{
		type_ = type;
		key_ = key;
		price_locale_ = price_locale;
		
		//public void Init(JSONNode item){
		/*
		Debug.Log (item);
		identifier_ = item["apple_identifier"].ToString();
		title_ = item["title"].ToString();
		description_ = item["description"].ToString();
		price_ = item["price"].AsFloat;
		price_locale_ = item["price_locale"].ToString();
		*/
		

		
		string sprite_name = "";
			if(key == "white_milk_coins_pack01")
				sprite_name = "IconWhiteMilkPack01";
			else if(key == "white_milk_coins_pack02")
				sprite_name = "IconWhiteMilkPack02";
			else if(key == "white_milk_coins_pack03")			
				sprite_name = "IconWhiteMilkPack03";
			else if(key == "white_milk_coins_pack04")			
				sprite_name = "IconWhiteMilkPack04";
			else if(key == "white_milk_coins_pack05")			
				sprite_name = "IconWhiteMilkPack05";
			else if(key == "normal_coins_pack01")			
				sprite_name = "IconNormalCoinPack01";
			else if(key == "normal_coins_pack02")			
				sprite_name = "IconNormalCoinPack02";
			else if(key == "normal_coins_pack03")			
				sprite_name = "IconNormalCoinPack03";
			else if(key == "normal_coins_pack04")			
				sprite_name = "IconNormalCoinPack04";
			else if(key == "normal_coins_pack05")			
				sprite_name = "IconNormalCoinPack05";
		
		//Get the proper icon from identifier
		up_icon_sprite_.GetComponent<tk2dSprite>().spriteId = up_icon_sprite_.GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		down_icon_sprite_.GetComponent<tk2dSprite>().spriteId = down_icon_sprite_.GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
			
		
		if (type == "coins")
		{
			kreds_background_sprite_.SetActive(false);
			kreds_cost_text_mesh_.SetActive(false);
			currency_background_.SetActive(false);
			currency_text_mesh_.SetActive(false);
			
			white_coins_cost_background_sprite_.SetActive(true);
			white_coins_cost_text_mesh_.SetActive(true);
			
			white_coins_cost_text_mesh_.GetComponent<tk2dTextMesh>().text = price_locale;
			white_coins_cost_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		}
		else if (type == "white_coins")
		{
			white_coins_cost_background_sprite_.SetActive(false);
			white_coins_cost_text_mesh_.SetActive(false);
			
#if UNITY_IPHONE || UNITY_ANDROID
			kreds_background_sprite_.SetActive(false);
			kreds_cost_text_mesh_.SetActive(false);
			
			currency_background_.SetActive(true);
			currency_text_mesh_.SetActive(true);

			currency_text_mesh_.GetComponent<tk2dTextMesh>().text = price_locale;
			currency_text_mesh_.GetComponent<tk2dTextMesh>().Commit();	
#else
			currency_background_.SetActive(false);
			currency_text_mesh_.SetActive(false);
			
			kreds_background_sprite_.SetActive(true);
			kreds_cost_text_mesh_.SetActive(true);
			kreds_cost_text_mesh_.GetComponent<tk2dTextMesh>().text = price_locale;
			kreds_cost_text_mesh_.GetComponent<tk2dTextMesh>().Commit();	
#endif	
		}
		
		string quantity_text = quantity.ToString();
		if (quantity> 1000 && quantity < 1000000)
			quantity_text = quantity.ToString();
		else if (quantity > 1000000)
			quantity_text = (quantity/1000000).ToString() +"M";
		
		quantity_text_mesh_.GetComponent<tk2dTextMesh>().text = quantity_text;
		quantity_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
	}
	
	
	void BuyPressed()
	{
		Debug.Log(scroll_area_.GetComponent<ScrollHelper>().GetTouchEnabled());
		if (scroll_area_.GetComponent<ScrollHelper>().GetTouchEnabled() == true)
		{
			if (type_ == "coins")
			{
				store_.BuyItemAsynch(key_, 1, OnSuccessFullCoinPurchase, OnFailedCoinPurchase);
			}
			else if (type_ == "white_coins")
			{	
#if UNITY_WEBPLAYER		
				Framework.Instance.InstanceComponent<ServerAPI>().PurchaseItem(key_,OnFinalize);
#elif UNITY_IPHONE || UNITY_ANDROID
				Framework.Instance.InstanceComponent<ServerAPI>().PurchaseItemMobile(store_info_[key_]["apple_key"].ToString(), key_, gameObject);
				string tier_str = store_info_[key_]["apple_key"].ToString().Substring( 39 );
				Debug.Log("BuyPressed TIER STR: " + tier_str );
				if(!int.TryParse( tier_str, out tier_number))
					tier_number = 0;
#endif
			}
		}
	}
	
	void OnDown()
	{
		Debug.Log("Icon Down");
		scroll_area_.GetComponent<ScrollHelper>().OnPressedTouchScrollArea();
	}
	
	
	public void OnSuccessFullCoinPurchase()
	{
		GameObject popup_window_ = (GameObject) Instantiate(Resources.Load("Framework/PopUpWindowPrefab"));
		popup_window_.transform.position = new Vector3(popup_window_.transform.position.x, popup_window_.transform.position.y, transform.position.z-1F);
		popup_window_.GetComponent<PopUpWindowPrefab>().Init("Success","You got it");
		Messenger.Broadcast("RefreshCoins");
		Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);	
	}
	
	
	public void OnFailedCoinPurchase()
	{
		Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("Can't Connect", "Please try again in a bit");
		Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
	}
	
	
	public void OnFinalize(ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success)
		{
			GameObject popup_window_ = (GameObject) Instantiate(Resources.Load("Framework/PopUpWindowPrefab"));
			popup_window_.transform.position = new Vector3(
				popup_window_.transform.position.x, 
				popup_window_.transform.position.y, 
				transform.position.z - 0.5f);
			popup_window_.GetComponent<PopUpWindowPrefab>().Init("Success","Purchase Complete");
			
			EbFightAPI.Player old_data_player = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer();
			EbFightAPI.Player data_player= data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(data_player);
			
			player_data_.SetValue("white_coins", data_player.WhiteCoins.ToString());
			
			//##Analytics
			int wmc_bought = (int)(data_player.WhiteCoins - old_data_player.WhiteCoins);
			Framework.Instance.InstanceComponent<Analitycs>().AddCommonValue("hard_currency_bought", wmc_bought );
			int white_coins_balance = (int) data_player.WhiteCoins;
			Framework.Instance.InstanceComponent<Analitycs>().SetCommonValue("hard_currency", white_coins_balance);	
			
			Messenger.Broadcast("RefreshCoins");
			if (type_ == "white_coins")
			{
				//Analitycs
				Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
				analitycs_.AddEvent(KeenIOEventType.iap_currency_offer);
				analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "hard_currency_reward", wmc_bought);
				analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "type", key_);
#if UNITY_WEBPLAYER
				analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "currency_type", "Kreds");
				analitycs_.AddCommonValue("total_spent_in_usd", SafeParsePrice(price_locale_));
#elif UNITY_IPHONE || UNITY_ANDRIOD
				analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "currency_type", "USD");
#endif
				//TO-DO will be changed to IOS
				analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "currency_amount", SafeParsePrice(price_locale_));
				analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "context_of_offer", description_);
				analitycs_.SendEvent(KeenIOEventType.iap_currency_offer);
			}
		}
		else
		{
			GameObject popup_window_ = (GameObject) Instantiate(Resources.Load("Framework/PopUpWindowPrefab"));
			popup_window_.transform.position = new Vector3(
				popup_window_.transform.position.x, 
				popup_window_.transform.position.y, 
				transform.position.z - 0.5F);
			popup_window_.GetComponent<PopUpWindowPrefab>().Init("oh oh Error","There was an error processing your purchase.\nPlease try again.");
		}
	}
	
	private int SafeParsePrice(string price)
	{
		int int_price = 0;
		int.TryParse(price, out int_price);
		return int_price;
	}	

}
