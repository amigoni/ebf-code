using UnityEngine;
using System.Collections;

public class MissionPrefab : MonoBehaviour {
	
	private Transform check_sprite_;
	private Transform text_;
	private Color checked_color;
	
	// Use this for initialization
	void Awake () {
		check_sprite_ = transform.Find("CheckSprite"); 
		text_ = transform.Find("TextMesh");
		checked_color = text_.GetComponent<tk2dTextMesh>().color;
	}
	
	public void Init(bool animated, string text, bool is_complete, int number, bool is_in_one_session, int value_, int total_gotten_, int best_gotten_){
		string text_string = text;
		
		if(is_in_one_session == false){
			if (total_gotten_ < value_){
				//print ("Is in one session? "+is_in_one_session+ " Total Gotten: "+total_gotten_);
				text_string = text+ " ("+(value_-total_gotten_)+" left)";
			}	
		}
		else{
			if (best_gotten_ < value_){
				//print ("Is in one session? "+is_in_one_session+ " Best Gotten: "+best_gotten_);
				text_string = text+ " (best "+best_gotten_+")";
			}
		}
		
		SetText(text_string);
		
		if (number == 1){
			transform.Find("OneSprite").GetComponent<MeshRenderer>().enabled = true;
			transform.Find("TwoSprite").GetComponent<MeshRenderer>().enabled = false;
			transform.Find("ThreeSprite").GetComponent<MeshRenderer>().enabled = false;
		}
		else if (number == 2){
			transform.Find("OneSprite").GetComponent<MeshRenderer>().enabled = false;
			transform.Find("TwoSprite").GetComponent<MeshRenderer>().enabled = true;
			transform.Find("ThreeSprite").GetComponent<MeshRenderer>().enabled = false;
		}
		else if (number == 3){
			transform.Find("OneSprite").GetComponent<MeshRenderer>().enabled = false;
			transform.Find("TwoSprite").GetComponent<MeshRenderer>().enabled = false;
			transform.Find("ThreeSprite").GetComponent<MeshRenderer>().enabled = true;
		}
		
		if (is_complete == true)
		{
			MarkComplete();
		}
		else
			check_sprite_.renderer.enabled = false;
		
		if (animated == true){
			GetComponent<Animation>().Play();
		}
			
	}
	
	public void MarkComplete(){
		check_sprite_.renderer.enabled = true;
		transform.Find("OneSprite").GetComponent<MeshRenderer>().enabled = false;
		transform.Find("TwoSprite").GetComponent<MeshRenderer>().enabled = false;
		transform.Find("ThreeSprite").GetComponent<MeshRenderer>().enabled = false;
		checked_color.a = 0.6F;
		text_.GetComponent<tk2dTextMesh>().color = checked_color;
	}
	
	
	public void SetText(string text){
		text_.GetComponent<tk2dTextMesh>().text = text;
		text_.GetComponent<tk2dTextMesh>().Commit();
	}
	
}
