using UnityEngine;
using System.Collections;

public class MissionDisplayPrefab : MonoBehaviour {
	
	private MissionsManager mission_manager_;
	public GameObject missionPrefab_;
	
	public bool is_animated = true;
	public bool display_mission_sign = false;
	
	// Use this for initialization
	void Awake () {
		
		//Missions
		if (GameObject.Find("Game"))
			mission_manager_ = GameObject.Find("Game").GetComponent<MissionsManager>();
		else
		{
			mission_manager_ = GetComponent<MissionsManager>();
			mission_manager_.Init(true, false);
		}
		
		//Old Missions
		if (mission_manager_.current_mission_id_ > 1 && mission_manager_.initial_mission_has_been_completed == true){
			for (int i = 0; i < 3; ++i) {
				if (is_animated == true)
					StartCoroutine(InstantiateSubmission(i,false,(0.5F*i),true)); 
			}
		}

		//Current Missions
		for (int i = 0; i < 3; i++) {
			if (is_animated == true){
				float additional_delay_= 0.0F;
				if (mission_manager_.initial_mission_has_been_completed == true)
					additional_delay_ = 2.0F;
				
				StartCoroutine(InstantiateSubmission(i,true,(0.5F*i+additional_delay_),true)); 
			}
			else
				Spawn(i,true, false);
		}
		
		//Missions Count
		int current_mission_id_ = PlayerPrefs.GetInt ("current_mission_id");
		//transform.Find("MissionNumTextMesh").GetComponent<tk2dTextMesh>().text = current_mission_id_ + "/"+mission_manager_.mission_count();
		transform.Find("MissionNumTextMesh").GetComponent<tk2dTextMesh>().text = FormattingWithZeros(current_mission_id_.ToString(),2); 
		transform.Find("MissionNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		if (display_mission_sign == false)
		{
			transform.Find("MissionNumTextMesh").renderer.enabled = false;
			transform.Find("MissionNumLabelSprite").renderer.enabled = false;
		}
		
		
		//Mission Completed Animation
		if (mission_manager_.initial_mission_has_been_completed == true)
			Invoke("SpawnMissionCompletedAnimation",2.0F);
					
	}
	
	
	void SpawnMissionCompletedAnimation ()
	{
		GameObject.Find("MainCamera").GetComponent<EndGame>().PlayMissionCompletedAnimation(mission_manager_.current_mission_id_);
	}
	
	
	//Function to instantiate Submissions with delay.
	IEnumerator InstantiateSubmission(int i, bool isCurrentMission, float delay, bool make_animated)
	{
		yield return new WaitForSeconds(delay);
		Spawn(i,isCurrentMission, make_animated);
	}
	
	
	private void Spawn(int i, bool isCurrentMission, bool make_animated)
	{
		float z = 1;
		if (make_animated == false)
			z = 1;
		
		GameObject parent1 = GameObject.Find("MissionDisplayPrefab");
		GameObject mission_ = (GameObject) Instantiate(missionPrefab_, 
			new Vector3(parent1.transform.position.x+43, parent1.transform.position.y+80+34*(2-i),parent1.transform.position.z-1),Quaternion.identity);
		mission_.transform.parent = parent1.transform;
		
		bool is_completed = true; 
		string text  = "";
		bool session = false;
		int value_ = 0;
		int total_stats = 0;
		int best_stats = 0;
		
		
		if (isCurrentMission == true)
		{
			is_completed = mission_manager_.current_mission().sub_missions_[i].completed_;
			text = mission_manager_.current_mission().sub_missions_[i].text_;
			session =mission_manager_.current_mission().sub_missions_[i].session_;
			value_ = mission_manager_.current_mission().sub_missions_[i].value_;
			total_stats = mission_manager_.GetTotalStatsForType( mission_manager_.current_mission().sub_missions_[i].type_);
			best_stats = mission_manager_.GetBestStatsForType( mission_manager_.current_mission().sub_missions_[i].type_);
		}
		else
		{
			is_completed = mission_manager_.last_mission().sub_missions_[i].completed_;
			text = mission_manager_.last_mission().sub_missions_[i].text_;
			session =mission_manager_.last_mission().sub_missions_[i].session_;
			value_ = mission_manager_.last_mission().sub_missions_[i].value_;
			total_stats = mission_manager_.GetTotalStatsForType( mission_manager_.last_mission().sub_missions_[i].type_);
			best_stats = mission_manager_.GetBestStatsForType( mission_manager_.last_mission().sub_missions_[i].type_);
		}
		
		mission_.GetComponent<MissionPrefab>().Init(make_animated, text, is_completed, i+1, session, value_, total_stats, best_stats);
		
		if (isCurrentMission == false)
			Destroy(mission_, 2.0F);
		
	}
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");

		return txt;
	}
	
}