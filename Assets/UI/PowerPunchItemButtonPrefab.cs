using UnityEngine;
using System.Collections;

public class PowerPunchItemButtonPrefab : MonoBehaviour {

	private string key_;
	private bool is_selected_ = false;
	private bool is_locked_ = false;
	
	// Use this for initialization
	void Start () 
	{
		Messenger.AddListener<string>("PopUpWindowBuyItem",  SelectCB);
	}
	
	
	public void Init(string key, bool is_locked, bool is_selected)
	{
		key_ = key;
		is_locked_ = is_locked;
		is_selected_ = is_selected;
		
		transform.Find("LockedSprite").renderer.enabled = false;
		transform.Find("BackgroundSprite").renderer.enabled = true;
		transform.Find("SelectedSprite").renderer.enabled = false;
		transform.Find("Button/IconSprite").renderer.enabled = false;
		transform.Find("Button/IconSpriteDown").renderer.enabled = false;
		transform.Find("NewLabelSprite").renderer.enabled = false;
		transform.Find("LockSprite").renderer.enabled = false;
		transform.Find("CheckMarkSprite").renderer.enabled = false;
		
		string sprite_name = "";
		
		if(key == "great_ball_of_fire")
			sprite_name = "IconPP03_GreatBallOfFire";
		else if(key == "aaatatata")
			sprite_name = "IconPP02_Aaatatata";
		else if(key == "huge_punch")			
			sprite_name = "IconPP01_HugePunch";
		
		transform.Find("Button/IconSprite").GetComponent<tk2dSprite>().spriteId = transform.Find("Button/IconSprite").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		transform.Find("Button/IconSpriteDown").GetComponent<tk2dSprite>().spriteId = transform.Find("Button/IconSpriteDown").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);

		if (is_locked == false)
		{
			transform.Find("LockedSprite").renderer.enabled = false;
			//transform.Find("LockSprite").renderer.enabled = false;
			transform.Find("Button/IconSprite").renderer.enabled = true;
			transform.Find("Button/IconSpriteDown").renderer.enabled = true;
			transform.Find("Button").gameObject.SetActive(true);
		}
		else
		{
			transform.Find("LockedSprite").renderer.enabled = true;
			//transform.Find("LockSprite").renderer.enabled = true;
			transform.Find("Button/IconSprite").renderer.enabled = false;
			transform.Find("Button/IconSpriteDown").renderer.enabled = false;
			transform.Find("Button").gameObject.SetActive(false);
		}
		
		
		if (is_selected == true)
			Select();
		else
			Unselect();
		
	}
	
	
	void IconPressed ()
	{
		if ( is_selected_ == false && is_locked_== false)
		{
			Messenger.Broadcast("PowerPunchSelected", key_);
			Select();
		}
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
		
	void SelectCB(string key)
	{
		if (key == key_ && is_locked_ == false)
			Select();
	}	
	
	
	public void Select()
	{
		is_selected_ = true;
		
		transform.Find("SelectedSprite").GetComponent<MeshRenderer>().enabled = true;
		transform.Find("CheckMarkSprite").GetComponent<MeshRenderer>().enabled = true;
	}
	
	
	public void Unselect()
	{
		is_selected_ = false;
		
		transform.Find("SelectedSprite").GetComponent<MeshRenderer>().enabled = false;
		transform.Find("CheckMarkSprite").GetComponent<MeshRenderer>().enabled = false;
	}
	
	
	void OnDestroy () 
	{
		Messenger.RemoveListener<string>("PopUpWindowBuyItem",  SelectCB);
	}
}
