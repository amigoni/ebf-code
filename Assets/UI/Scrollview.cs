using UnityEngine;
using System.Collections;

public class Scrollview : MonoBehaviour {
	
	public GameObject row_prefab_;
	public string orientation_ = "horizontal";
	public string layout_ = "auto";
	public float width_ = 480;
	public float height_ = 320;
	public float vertical_row_space_ = 0;
	public float horizontal_row_space_ = 0;
	
	 // Internal variables for managing touches and drags
	private int selected = -1;
	private float scrollVelocity = 0f;
	private float timeTouchPhaseEnded = 0f;
	private const float inertiaDuration = 0.5f;
	
	
	private float scroll_top_;
	private float scroll_bottom_;
	private float content_top_;
	private float content_bottom_;
	private float start_x_;
	private float start_y_;
	
	private float next_object_x_ = 0;
	private float next_object_y_ = 0;
	private float content_width_ = 0;
	private float content_height_ = 0;
	
	
	// Use this for initialization
	void Start () {
		start_y_ = transform.position.y;
		start_x_ = transform.position.x;	
		
		scroll_top_ = transform.position.y;
		scroll_bottom_ = transform.position.y - height_;
		//next_object_y_ = -transform.position.y;
	}
	
	public void Init(float width, float height){
		width_ = width;
		height_ = height;
	}
	
	public void Reset(){
		next_object_x_ = 0;
		next_object_y_ = 0;
		content_width_ = 0;
		content_height_ = 0;
	}
	
	
	public void AddItem(GameObject item){
		item.transform.parent = transform;
		float item_width = item.GetComponent<ObjectSizeCheat>().width;
		float item_height = item.GetComponent<ObjectSizeCheat>().height;
		
		if (layout_ == "auto"){
			if (orientation_ == "vertical"){
				float xPos = 0;
				if (row_prefab_.GetComponent<ObjectSizeCheat>().align == "center")
					xPos = transform.position.x+width_/2;
				else if (row_prefab_.GetComponent<ObjectSizeCheat>().align == "left")
					xPos = transform.position.x+item_width/2;
				else if (row_prefab_.GetComponent<ObjectSizeCheat>().align == "right")
					xPos = transform.position.x+width_-item_width/2;
				
				item.transform.position = new Vector3(xPos,transform.position.y-item_height/2+next_object_y_,item.transform.parent.transform.position.z-1);
				next_object_y_ -= (item_height + vertical_row_space_);
				content_height_ += item_height + vertical_row_space_;
				content_bottom_ = scroll_top_ - content_height_;
				
				if(item_width > content_width_)
					content_width_ = item_width;
			}
		} 
	}
	
	
	private void ScrollX(float x){
		float xPos = XConv(x);
		transform.position = new Vector3(xPos,transform.position.y,transform.position.z);
	}
	
	private void ScrollTo(float x, float y){
		float xPos = XConv(x);
		float yPos = YConv(y);
		transform.position = new Vector3(xPos,yPos,transform.position.z);
	}
	
	private float XConv(float x){
		return x+width_/2;
	}
	
	private float YConv(float y){
		return y-height_/2;
	}
	
	// Update is called once per frame
	void Update () {
		float yPos = 0.0F;
		
		
		if (Input.touchCount != 1)
		{
			selected = -1;

			if ( scrollVelocity != 0.0f )
			{
				// slow down over time
				float t = (Time.time - timeTouchPhaseEnded) / inertiaDuration;
				float frameVelocity = Mathf.Lerp(scrollVelocity, 0, t);
				if (orientation_ == "vertical")
					yPos += frameVelocity * Time.deltaTime;
				
				// after N seconds, we've stopped
				if (t >= inertiaDuration) scrollVelocity = 0.0f;
			}
			return;
		}
		
		
		Touch touch = Input.touches[0];
		//Debug.Log("x: "+touch.position.x/Framework.Instance.graphics_manager().camera_scale()+" "+transform.position.x);
		//Debug.Log("y: "+touch.position.y/Framework.Instance.graphics_manager().camera_scale()+" "+transform.position.y);
		
		if (touch.position.x/Framework.Instance.graphics_manager().camera_scale() > transform.position.x && 
			touch.position.x/Framework.Instance.graphics_manager().camera_scale() < transform.position.x + width_ &&
			touch.position.y/Framework.Instance.graphics_manager().camera_scale() < transform.position.y && 
			touch.position.y/Framework.Instance.graphics_manager().camera_scale() > transform.position.y - height_
			)
		{
		
			/*
			if (touch.phase == TouchPhase.Began)
			{
				selected = TouchToRowIndex(touch.position);
				scrollVelocity = 0.0f;
			}
			else if (touch.phase == TouchPhase.Canceled)
			{
				selected = -1;
			}
			else if (touch.phase == TouchPhase.Ended)
			{
	            // Was it a tap, or a drag-release?
	            if ( selected > -1 )
	            {
		            Debug.Log("Player selected row " + selected);
	            }
				else
				{
					// impart momentum, using last delta as the starting velocity
					// ignore delta < 10; precision issues can cause ultra-high velocity
					if (Mathf.Abs(touch.deltaPosition.y) >= 10) 
						scrollVelocity = (int)(touch.deltaPosition.y / touch.deltaTime);
					timeTouchPhaseEnded = Time.time;
				}
			}
			
			else if(touch.phase == TouchPhase.Moved){
			*/
			if(touch.phase == TouchPhase.Moved){
				selected = -1;
				
				if(orientation_ == "horizontal"){
					float left_edge = transform.position.x + touch.deltaPosition.x;// transform.position.x - width_/2 + touch.deltaPosition.x;
					float right_edge = transform.position.x  + width_ + touch.deltaPosition.x;//transform.position.x + width_/2+ touch.deltaPosition.x; 
					float xPos= transform.position.x;
					
					if( left_edge < 0 && right_edge > 480){
						xPos += touch.deltaPosition.x;
						transform.position = new Vector3(xPos,transform.position.y,transform.position.z);
					}
						
					
					/*
					//Indicator
					Vector3 indicator_position_ = scrollView_indicator_.transform.position;
					indicator_position_.y = 20+0.5F*40-bottom*210/scrollView_height_;
					scrollView_indicator_.transform.position = indicator_position_;
					//print ("Inidicator y "+indicator_position_.y);
					*/
				}
				else if (orientation_ == "vertical"){
					content_top_ = transform.position.y ;
					content_bottom_ = transform.position.y - content_height_; 
					Debug.Log ("top: "+content_top_+ " bottom: "+content_bottom_+" scroll_top: "+scroll_top_+" scroll_bottom: "+scroll_bottom_);
						
					yPos = transform.position.y;
					
					if(content_bottom_ <= scroll_bottom_ && content_top_ >= scroll_top_ ){
						yPos += touch.deltaPosition.y;
					}
					
					if (yPos < scroll_top_)
						yPos = scroll_top_;
					
					if (yPos > scroll_bottom_ + content_height_)
						yPos = scroll_bottom_ + content_height_;
					
					transform.position = new Vector3(transform.position.x,yPos,transform.position.z);
					
					
					/*
					//Indicator
					Vector3 indicator_position_ = scrollView_indicator_.transform.position;
					indicator_position_.y = 20+0.5F*40-bottom*210/scrollView_height_;
					scrollView_indicator_.transform.position = indicator_position_;
					//print ("Inidicator y "+indicator_position_.y);
					*/
					
				}
			}
		}
	}
}