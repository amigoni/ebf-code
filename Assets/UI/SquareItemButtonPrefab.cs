using UnityEngine;
using System.Collections;

public class SquareItemButtonPrefab : MonoBehaviour {
	
	private string key_;
	private string category_;
	private bool is_selected_ = false;
	private bool is_locked_ = false;
	public GameObject popup_window_;
	
	// Use this for initialization
	void Start () {
		Messenger.AddListener<string>("PopUpWindowBuyItem",  SelectCB);
	}
	
	
	public void Init(string category, string key, bool is_locked, bool is_selected){
		category_ = category;
		key_ = key;
		is_locked_ = is_locked;
		is_selected_ = is_selected;
		
		transform.Find("LockedSprite").renderer.enabled = false;
		transform.Find("BackgroundSprite").renderer.enabled = false;
		transform.Find("SelectedSprite").renderer.enabled = false;
		transform.Find("Button/IconSprite").gameObject.SetActive(false);
		transform.Find("EquippedButtonSprite").renderer.enabled = false;
		transform.Find("NewLabelSprite").renderer.enabled = false;
		transform.Find("LockSprite").renderer.enabled = false;
		transform.Find("CheckMarkSprite").renderer.enabled = false;
		
		transform.Find("UtilitySelectedSprite").renderer.enabled = false;
		transform.Find("UtilityBackgroundSprite").renderer.enabled = false;
		transform.Find("UtilityIconSprite").renderer.enabled = false;
		transform.Find("UtilityCheckSmallSprite").renderer.enabled = false;
		
		if(category == "power_punch"){
			string sprite_name = "";
			if(key == "great_ball_of_fire")
				sprite_name = "IconPP03_GreatBallOfFire";
			else if(key == "aaatatata")
				sprite_name = "IconPP02_Aaatatata";
			else if(key == "huge_punch")			
				sprite_name = "IconPP01_HugePunch";
			
			transform.Find("Button/IconSprite").GetComponent<tk2dSprite>().spriteId = transform.Find("Button/IconSprite").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
			transform.Find("Button/IconSpriteDown").GetComponent<tk2dSprite>().spriteId = transform.Find("Button/IconSpriteDown").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);

			if (is_locked == false){
				transform.Find("LockedSprite").renderer.enabled = false;
				transform.Find("LockSprite").renderer.enabled = false;
				transform.Find("EquippedButtonSprite").renderer.enabled = false;
				transform.Find("BackgroundSprite").renderer.enabled = true;
				transform.Find("Button").gameObject.SetActive(true);
				transform.Find("Button/IconSprite").gameObject.SetActive(true);
				transform.Find("Button/IconSpriteDown").gameObject.SetActive(true);
			}
			else
			{
				transform.Find("LockedSprite").renderer.enabled = true;
				transform.Find("LockSprite").renderer.enabled = false;
				transform.Find("EquippedButtonSprite").renderer.enabled = false;
				transform.Find("BackgroundSprite").renderer.enabled = true;
				transform.Find("Button").gameObject.SetActive(false);
				transform.Find("Button/IconSprite").gameObject.SetActive(false);
				transform.Find("Button/IconSpriteDown").gameObject.SetActive(false);
			}
		}
		
		if(category == "utility"){
			transform.Find("UtilityIconSprite").GetComponent<tk2dSprite>().spriteId = transform.Find("UtilityIconSprite").GetComponent<tk2dSprite>().GetSpriteIdByName("IconUti01");
			if (is_locked == false){
				transform.Find("LockedSprite").renderer.enabled = false;
				transform.Find("LockSprite").renderer.enabled = false;
				transform.Find("UtilityIconSprite").renderer.enabled = true;
				//transform.Find("EquippedButtonSprite").renderer.enabled = false;
			}
			else
			{
				transform.Find("LockedSprite").renderer.enabled = false;
				transform.Find("LockSprite").renderer.enabled = true;
			}
		}
		
		
		
		if (is_selected == true){
			Select();
		}
		else{
			Unselect();
		}	
	}
	
	
	void SetIconImage(){
		
	}
	
	void IconPressed (){
		if ( is_selected_ == false && is_locked_== false){
			if (category_ == "power_punch"){
				Messenger.Broadcast("SquareItemSelected", key_,is_locked_);
				Select();
			}
			else if (category_ == "utility"){
				//GameObject popup_window = (GameObject) Instantiate(popup_window_,new Vector3(0,0,0), Quaternion.identity);
				//popup_window.GetComponent<PopUpWindowPrefab>().Init("buy","titleText","Description",key_);
				Select();
			}
		}
		else if ( is_selected_ == false && is_locked_ == true){
			//GameObject popup_window = (GameObject) Instantiate(popup_window_,new Vector3(0,0,0), Quaternion.identity);
			//popup_window.GetComponent<PopUpWindowPrefab>().Init("locked","titleText","Description",key_);
			if (category_ == "power_punch"){
				Messenger.Broadcast("SquareItemSelected", key_, is_locked_);
				Select();
			}
		}
		//Messenger.Broadcast("ButtonPressed");
	}
	
		
	void SelectCB(string key){
		if (key == key_ && is_locked_ == false)
			Select();
	}	
		
	public void Select(){
		is_selected_ = true;
		
		if (category_ == "power_punch")
		{
			transform.Find("SelectedSprite").GetComponent<MeshRenderer>().enabled = true;
			transform.Find("CheckMarkSprite").GetComponent<MeshRenderer>().enabled = false;
			transform.Find("EquippedButtonSprite").renderer.enabled = false;
		}
		else if (category_ == "utility")
		{
			transform.Find("UtilitySelectedSprite").renderer.enabled = true;
			transform.Find("UtilityCheckSmallSprite").renderer.enabled = true;
		}
	}
	
	public void Unselect(){
		is_selected_ = false;
		
		if (category_ == "power_punch")
		{
			transform.Find("SelectedSprite").GetComponent<MeshRenderer>().enabled = false;
			transform.Find("CheckMarkSprite").GetComponent<MeshRenderer>().enabled = false;
		}
		else if (category_ == "utility")
		{
			transform.Find("UtilitySelectedSprite").renderer.enabled = false;
			transform.Find("UtilityCheckSmallSprite").renderer.enabled = false;
		}
		
	}
	
	void OnDestroy () {
		Messenger.RemoveListener<string>("PopUpWindowBuyItem",  SelectCB);
	}
}
