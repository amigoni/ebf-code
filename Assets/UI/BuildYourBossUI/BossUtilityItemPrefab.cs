using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossUtilityItemPrefab : MonoBehaviour {
	
	private string key_;
	private bool is_selected_;
	private bool is_locked_;
	private double initial_time_left_;
	private double time_left_;
	private double expiration_time_;
	private int warning_time_limit_seconds_ = 60*60;
	
	private bool is_select_enabled_ = true;
	
	
	private Transform expires_text_mesh_;
	
	// Use this for initialization
	void Awake ()
	{
		GetComponent<tk2dCameraAnchor>().tk2dCamera = GameObject.Find("MainCamera").GetComponent<tk2dCamera>();
		expires_text_mesh_ = transform.Find("ExpiresTextMesh");
		InvokeRepeating("TimeUpdate", 0F,1.0F);
		
	}
	
	
	public void Init (string key, bool is_selected, bool is_locked)
	{
		key_ = key;
		is_selected_ = is_selected;
		is_locked_ = is_locked;
		
		//Set Icon
		transform.Find("UtilitySelectedSprite").renderer.enabled = is_selected;	
		string sprite_name = "";
		
		
		List<EbFightAPI.Utility> utilities = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().Boss.Utilities;
		
		for ( int i=0; i < utilities.Count; i++){
			if ( utilities[i].Kind == key)
				expiration_time_ =  utilities[i].Expires;	
		}
		
		
		if(key == "boss_utilities_att")
		{
			sprite_name = "IconUtiBoss01_Att";
			GetComponent<tk2dCameraAnchor>().offset = new Vector2 ( -80 ,35);
		}
			
		else if(key == "boss_utilities_def")
		{
			sprite_name = "IconUtiBoss02_Def";
			GetComponent<tk2dCameraAnchor>().offset = new Vector2 ( 0 ,35);
		}	
		else if(key == "boss_utilities_time")
		{
			sprite_name = "IconUtiBoss03_Time";
			GetComponent<tk2dCameraAnchor>().offset = new Vector2 ( 80 ,35);
		}	
			
		
		transform.Find("UtilityIconSprite").GetComponent<tk2dSprite>().spriteId = transform.Find("UtilityIconSprite").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
	}
	
	
	public void Refresh()
	{
		Init(key_,is_selected_,is_locked_);
	}
	
	
	public void IconPressed()
	{
		Select();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	public void Select(){
		if (is_select_enabled_ == true)
		{
			is_selected_ = true;
			Messenger.Broadcast("BossUtilitySelected",key_);
			transform.Find("UtilitySelectedSprite").renderer.enabled = true;
		}	
	}
	
	
	public void Unselect(){
		is_selected_ = false;
		transform.Find("UtilitySelectedSprite").renderer.enabled = false;	
	}
	
	
	private void TimeUpdate(){
		
		time_left_ = expiration_time_ - GetTimeStampUnix();
		
		if (time_left_ > warning_time_limit_seconds_)
		{
			transform.Find("ExpiresGreenSprite").renderer.enabled = true;
			transform.Find("ExpiresOrangeSprite").renderer.enabled = false;
			transform.Find("ExpiresRedSprite").renderer.enabled = false;
		}
		else if (time_left_ <= warning_time_limit_seconds_ && time_left_ > 0){
			transform.Find("ExpiresGreenSprite").renderer.enabled = false;
			transform.Find("ExpiresOrangeSprite").renderer.enabled = true;
			transform.Find("ExpiresRedSprite").renderer.enabled = true;
			
		}
		else if(time_left_ <= 0)
		{
			transform.Find("ExpiresGreenSprite").renderer.enabled = false;
			transform.Find("ExpiresOrangeSprite").renderer.enabled = false;
			transform.Find("ExpiresRedSprite").renderer.enabled = true;
			
			GameObject.Find("MainCamera").GetComponent<BossDetail>().ClearBossUtility(key_);
		}
		
		if (time_left_ >= 0){
			expires_text_mesh_.GetComponent<tk2dTextMesh>().text = ConvertTime(time_left_);
			expires_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		}
		
	}
	
	
	private string ConvertTime(double time){
		time = Mathf.Floor((float)time);
		float hours = Mathf.Floor((float)time / 3600F);
		float minutes = Mathf.Floor(((float)time-hours*3600)/60);
		float seconds = (float)time-hours*3600-minutes*60;
		
		return FormattingWithZeros(hours.ToString(),2)+":"+FormattingWithZeros(minutes.ToString(),2)+":"+FormattingWithZeros(seconds.ToString(),2);
	}
	
	
	public string GetKey(){
		return key_;
	}
	
	
	public bool IsPurchaseEnabled ()
	{
		bool return_value = false;
		if (time_left_ <= warning_time_limit_seconds_)
			return_value = true;
		return return_value;
	}
	
	
	string FormattingWithZeros(string txt, int digits){		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");
		
		return txt;
	}
	
	
	private double GetTimeStampUnix(){
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		var timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		
		return timestamp;
	}
	
	
	public void SetSelectEnabled (bool is_enabled)
	{
		is_select_enabled_ = is_enabled;
	}
	
	void OnDestroy()
	{
		CancelInvoke("TimeUpdate");
	}

}
