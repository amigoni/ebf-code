using UnityEngine;
using System.Collections;
using SimpleJSON;

public class WeaponIconPopUpPrefab : MonoBehaviour {
	
	private JSONNode store_info_; //Static info for each item
	public GameObject background_sliced_sprite_;
	public GameObject background_selected_sliced_sprite_;
	public GameObject quantity_text_mesh_;
	public GameObject cost_text_mesh_;
	public GameObject icon_sprite_;
	public GameObject lock_sprite_;
	public GameObject locked_sprite_;
	public GameObject sold_out_sprite_;
	public GameObject one_sprite_;
	public GameObject two_sprite_;
	public GameObject three_sprite_;
	public GameObject quantity_bubble_;
	public GameObject check_sprite_;
	public GameObject button_equip_green_sprite_;
	public GameObject equipped_sprite_;
	public GameObject equip_grey_sprite_;
	public GameObject icon_power_sprite_;
	public GameObject icon_no_power_sprite_;
	public GameObject name_text_mesh_;
	
	private BossPowerManager boss_power_manager_;
	
	bool button_enabled_ = true;
	
	string weapon_key_;
	int weapon_difficulty_;
	int weapon_cost_;
	int weapon_quantity_;
	
	private class WeaponData
	{
		public string key;
		public int difficulty;
		public int cost;
		public int quantity;
		public int quantity_used;
		public int unlock_level;
	}
	
	WeaponData weapon_data_ = new WeaponData();
	
	
	// Use this for initialization
	void Awake () 
	{
		boss_power_manager_ = GameObject.Find("MainCamera/BuildYourBossTabContainer").GetComponent<BossPowerManager>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
	}
	
	
	public void Init(string key, int difficulty )
	{
		weapon_key_ = key;
		weapon_difficulty_ = difficulty;
		
		weapon_data_ = GetWeaponData(weapon_key_, weapon_difficulty_);
		
		Config();
	}
	
	public void Refresh()
	{
		weapon_data_ = GetWeaponData(weapon_key_, weapon_difficulty_);
		
	//	Debug.Log("weapon_key_ : " +weapon_key_ + " weapon_difficulty_ : " +weapon_difficulty_ + 
	//			"\n quantity : " + weapon_data_.quantity + "\n quantity_used : " + weapon_data_.quantity_used);

		Config();
	}
			
			
	private WeaponData GetWeaponData(string item_name, int difficulty)
	{
		int level = (int) GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Level;
		JSONNode data_weapon = store_info_["boss_round_weapons"]["weapons"][level][item_name];
		
		WeaponData weapon_data = new WeaponData();
		weapon_data.key = "locked";
			
		//PORCATA MA IL RESTO NON FUNZA E DA STRANI ERRORI
		if(data_weapon["difficulty_"+difficulty]["key"].ToString().Length > 2)
		{
			weapon_data.key = data_weapon["difficulty_"+difficulty]["key"];
			weapon_data.difficulty = data_weapon["difficulty_"+difficulty]["difficulty"].AsInt;
			weapon_data.cost = data_weapon["difficulty_"+difficulty]["cost"].AsInt;
			weapon_data.quantity = data_weapon["difficulty_"+difficulty]["quantity"].AsInt;
			
			foreach (EbFightAPI.StageInfo stage in GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Stages)
			{
				foreach(EbFightAPI.BossWeaponInfo weapon in stage.Weapons)
				{
					if (weapon.Kind == item_name && weapon.Level == difficulty)
						weapon_data.quantity_used++;
				}
			}
		}
		
		weapon_data.unlock_level = GetWeaponUnlockedLevel(item_name, difficulty);
		
		return weapon_data;
	}	
	
	
	private int GetWeaponUnlockedLevel(string key, int difficulty)
	{
		int level = 99;
		
		JSONNode weapons_data = store_info_["boss_round_weapons"]["weapons"];
		
		//Debug.Log(key+" "+difficulty);
		for (int i = 0; i < weapons_data.Count; i++)
		{	
			//PORCATA MA NON SO COSA ALTRO FARE
			if (weapons_data[i][key].ToString().Length > 2)
			{
				for (int j = 0; j < 3; j++)
				{
				 	if(weapons_data[i][key]["difficulty_"+j]["difficulty"].AsInt == difficulty)
					{
						level = i+1;
						//Debug.Log(key+" diff: "+difficulty+" unlock Level is: "+level);
						break;
					}
				}
			}
			
			if (level != 99)
				break;
		}
		
		return level;
	}
	
	
	public void Config ()
	{	
		bool selected = boss_power_manager_.IsCurrentWeapon(weapon_data_.key,weapon_data_.difficulty);
		
		int current_weapon_cost = boss_power_manager_.GetCurrentWeaponCost();
		
		icon_sprite_.collider.enabled = false;
		
		if (selected == true)
		{	
			background_sliced_sprite_.renderer.enabled = false;
			background_selected_sliced_sprite_.renderer.enabled = true;
			check_sprite_.renderer.enabled = true;
			equipped_sprite_.renderer.enabled = true;
			button_equip_green_sprite_.SetActive(false);
			sold_out_sprite_.SetActive(false);
		}
		else
		{
			background_sliced_sprite_.renderer.enabled = true;
			background_selected_sliced_sprite_.renderer.enabled = false;
			check_sprite_.renderer.enabled = false;
			equipped_sprite_.renderer.enabled = false;
			button_equip_green_sprite_.SetActive(true);
		}
		
		
		//Can Affor or not
		if (weapon_data_.cost <= boss_power_manager_.GetAvailablePower() + current_weapon_cost)
		{
			if (weapon_data_.quantity - weapon_data_.quantity_used > 0)
			{
				if(!selected)
					button_equip_green_sprite_.SetActive(true);
			}
			else
			{
				button_equip_green_sprite_.SetActive(false);
			}
			
			equip_grey_sprite_.renderer.enabled = false;
			icon_power_sprite_.renderer.enabled = true;
			icon_no_power_sprite_.renderer.enabled = false;
		}
		else
		{	
			button_equip_green_sprite_.SetActive(false);
			
			if (selected == true)
				equip_grey_sprite_.renderer.enabled = false;
			else
				equip_grey_sprite_.renderer.enabled = true;
			
			icon_power_sprite_.renderer.enabled = false;
			icon_no_power_sprite_.renderer.enabled = true;
			sold_out_sprite_.renderer.enabled = false;
		}
			
		
		string sprite_name = "";
		string text = "";
		
		if(weapon_data_.key == "bouncing"){
            sprite_name = "IconWeap01_Bouncing";
            text = "Bouncing Missile";
        }
        else if(weapon_data_.key == "tap"){
            sprite_name = "IconWeap02_Tap";
            text = "Missile";
        }
        else if(weapon_data_.key == "avoid"){            
            sprite_name = "IconWeap03_Avoid";
            text = "Avoid Missile";
        }
        else if(weapon_data_.key == "fast"){            
            sprite_name = "IconWeap04_FastTap";
            text = "Fast Missile";
        }
        else if(weapon_data_.key == "laser"){            
            sprite_name = "IconWeap05_laser";
            text = "Laser";
        }
        else if(weapon_data_.key == "rocket"){            
            sprite_name = "IconWeap06_Rocket";
            text = "Rocket";
        }    
        else if(weapon_data_.key == "chasing"){            
            sprite_name = "IconWeap07_Chasing";
            text = "Chasing Missile";
        }
        else if(weapon_data_.key == "fast_rocket"){            
            sprite_name = "IconWeap08_FastRocket";
            text = "Fast Rocket";
        }
        else if(weapon_data_.key == "sin"){            
            sprite_name = "IconWeap09_Sin";
            text = "Sine Missile";
	    }
		
		name_text_mesh_.GetComponent<tk2dTextMesh>().text = text.ToUpper();
		name_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		transform.Find("IconSprite").GetComponent<tk2dSprite>().spriteId = transform.Find("IconSprite").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		
		
		if (weapon_data_.key != "locked")
		{
			quantity_text_mesh_.GetComponent<tk2dTextMesh>().text = (weapon_data_.quantity - weapon_data_.quantity_used).ToString()+"/"+weapon_data_.quantity.ToString();
			quantity_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			
			if (weapon_data_.quantity - weapon_data_.quantity_used > 0)
				sold_out_sprite_.SetActive(false);
				
			
			cost_text_mesh_.GetComponent<tk2dTextMesh>().text = weapon_data_.cost.ToString();
			cost_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			
			if(weapon_data_.difficulty == 0){
				two_sprite_.SetActive(false);
				three_sprite_.SetActive(false);
			}
			else if (weapon_data_.difficulty == 1){
				one_sprite_.SetActive(false);
				three_sprite_.SetActive(false);
			}
			else if (weapon_data_.difficulty == 2){
				one_sprite_.SetActive(false);
				two_sprite_.SetActive(false);
			}
			
			if(weapon_data_.quantity_used >= weapon_data_.quantity){
				if(selected == false)
					sold_out_sprite_.SetActive(true);
				button_enabled_ = false;
			}
			else{
				sold_out_sprite_.SetActive(false);
				button_enabled_ = true;
			}
			
			lock_sprite_.SetActive(false);
			locked_sprite_.SetActive(false);
		}
		else
		{
			quantity_text_mesh_.SetActive(false);
			cost_text_mesh_.SetActive(false);
			icon_sprite_.SetActive(false);
			lock_sprite_.SetActive(true);
			locked_sprite_.SetActive(true);
			one_sprite_.SetActive(false);
			two_sprite_.SetActive(false);
			three_sprite_.SetActive(false);
			quantity_bubble_.SetActive(false);
			button_equip_green_sprite_.SetActive(false);
			equipped_sprite_.SetActive(false);
			equip_grey_sprite_.SetActive(false);
			icon_power_sprite_.SetActive(false);
			icon_no_power_sprite_.SetActive(false);
			sold_out_sprite_.SetActive(false);
			
			button_enabled_ = false;
			
			name_text_mesh_.GetComponent<tk2dTextMesh>().text = ("Unlock at level "+weapon_data_.unlock_level).ToUpper();
			name_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		}
		
		
		//Need to set icon;
	}
	
	
	public void Pressed ()
	{
		if (button_enabled_ == true)
		{
			if (boss_power_manager_.CheckIfYouCanAffordWeapon(weapon_data_.cost) == true)
			{
				Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
				boss_power_manager_.BuyWeapon(weapon_data_.key, weapon_data_.difficulty, weapon_data_.cost, boss_power_manager_.GetCurrentRound());
				Messenger.Broadcast("WeaponSelected");
				GameObject.Find("WeaponPopUp(Clone)").GetComponent<WeaponPopUp>().ClosePopUp();
			}
		
//Achievement
#if UNITY_IPHONE && !UNITY_EDITOR		
			Framework.Instance.InstanceComponent<GameCenterAPI>().AddAchievementProgress("1",100F);
#endif
		}
	}
	
}
