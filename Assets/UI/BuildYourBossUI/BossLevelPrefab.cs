using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class BossLevelPrefab : MonoBehaviour {
	
	public GameObject weapon_icon_in_level_prefab_;
	
	public GameObject round1_sprite_;
	public GameObject round2_sprite_;
	public GameObject round3_sprite_;
	public GameObject background_sliced_sprite_;
	public GameObject select_weapon_grey_sprite_;
	public GameObject select_weapon_white_sprite_;
	public GameObject overlay_sliced_sprite_;
	public GameObject select_button_sprite_;
	public GameObject select_sprite_;
	public GameObject icon_container_;
	
	private int round_number_;
	
	bool touch_enabled_ = true;

	private List<GameObject> icon_list_ = new List<GameObject>();
		
	// Use this for initialization
	void Awake ()
	{
		round1_sprite_.SetActive(false);
		round2_sprite_.SetActive(false);
		round3_sprite_.SetActive(false);
		background_sliced_sprite_.SetActive(false);
		
		Messenger.AddListener<int>("UpdateBuildABossLevelRow", Refresh);
	}
	
	public void Refresh(int current_round)
	{
		if (round_number_ == current_round)
		{
			/*
			foreach (GameObject icon in icon_list_)
			{
				//Destroy(icon);
			}
			
			icon_list_ = new List<GameObject>();
			*/
			
			Init(round_number_);
		}
	}	
	
	
	public void Init (int round_number)
	{
		round_number_ = round_number;
		GetComponent<tk2dCameraAnchor>().offset.y = 156-63*round_number;
		
		EbFightAPI.StageInfo round_data = GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Stages[round_number];
		
		if (round_number == 0)
			round1_sprite_.SetActive(true);
		else if (round_number == 1)
			round2_sprite_.SetActive(true);
		else if (round_number == 2)
			round3_sprite_.SetActive(true);
		
		//Put Icons
		int i = 0;
		int icon_list_count = icon_list_.Count;
		foreach( EbFightAPI.BossWeaponInfo weapon in round_data.Weapons)
		{
			if (icon_list_count == 0)
			{
				GameObject icon = (GameObject) Instantiate(weapon_icon_in_level_prefab_,
				new Vector3 ( icon_container_.transform.position.x+55*i, icon_container_.transform.position.y, icon_container_.transform.position.z-5),Quaternion.identity);
				icon.transform.parent = icon_container_.transform;
				icon_list_.Add(icon);
				icon.GetComponent<WeaponIconInLevelPrefab>().Init(weapon.Kind, (int) weapon.Level, round_number, i);
			}
			else
				icon_list_[i].GetComponent<WeaponIconInLevelPrefab>().Init(weapon.Kind, (int) weapon.Level, round_number, i);
			
			i++;
		}
	}
	
	public void Pressed(){
		if (touch_enabled_ == true)
		{
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
			Selected();
		}
	}
	
	
	public void Selected ()
	{
		background_sliced_sprite_.SetActive(true);
		overlay_sliced_sprite_.SetActive(false);
		select_button_sprite_.SetActive(false);
		select_sprite_.SetActive(true);
		select_weapon_grey_sprite_.SetActive(true);
		select_weapon_white_sprite_.SetActive(false);
		
		Messenger.Broadcast("LevelSelected", round_number_);
	}
	
	
	public void Unselect ()
	{
		background_sliced_sprite_.SetActive(false);
		overlay_sliced_sprite_.SetActive(true);
		select_button_sprite_.SetActive(true);
		select_sprite_.SetActive(false);
		select_weapon_grey_sprite_.SetActive(false);
		select_weapon_white_sprite_.SetActive(true);
	}
	
	
	private void OnDestroy()
	{
		Messenger.RemoveListener<int>("UpdateBuildABossLevelRow", Refresh);
	}
	
	
	public int GetRound(){
		return round_number_;
	}
	
	
	public void ToggleTouchEnabled (bool enabled)
	{
		touch_enabled_ = enabled;
		
		foreach ( GameObject icon in icon_list_)
		{
			icon.GetComponent<WeaponIconInLevelPrefab>().ToggleTouchEnabled(enabled);
		}
	}

}
