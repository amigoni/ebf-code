using UnityEngine;
using System.Collections;
using SimpleJSON;
using MiniJSON;

public class BossRoundAdjustableParameterPrefab : MonoBehaviour {
	
	private JSONNode store_info_;
	private JSONNode this_data_;
	private int round_;
	private string key_;
	private BossPowerManager bonus_power_manager_;
	
	bool touch_enabled_ = true;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	
	public void Init ( int round, string key)
	{
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		bonus_power_manager_ = GameObject.Find("MainCamera/BuildYourBossTabContainer").GetComponent<BossPowerManager>();
		
		InitUI(round , key);
	}
	
	
	private void InitUI (int round, string key)
	{
		round_ = round;
		key_ = key;
		
		JSONNode [] data = bonus_power_manager_.GetCurrentDataForParameter(key);
		
		//Title Sprite
		if (key == "boss_round_shots_per_wave")
			transform.Find("Titles/ShotsPerWaveSprite").renderer.enabled = true;
		else if (key == "boss_round_reload_speed")
			transform.Find("Titles/ReloadSpeedSprite").renderer.enabled = true;
		else if (key == "boss_round_environment")
		{
			transform.Find("Titles/DesertSprite").renderer.enabled = false;	
			transform.Find("Titles/ForestSprite").renderer.enabled = false;	
			transform.Find("Titles/IceSprite").renderer.enabled = false;	
			
			if(data[1]["effect"].ToString() == "desert")
				transform.Find("Titles/DesertSprite").renderer.enabled = true;		
			else if (data[1]["effect"].ToString() == "forest")	
				transform.Find("Titles/ForestSprite").renderer.enabled = true;	
			else if (data[1]["effect"].ToString() == "ice")		
				transform.Find("Titles/IceSprite").renderer.enabled = true;
		}
		
		transform.Find("TextMesh").GetComponent<tk2dTextMesh>().text = data[1]["cost"].ToString();
		transform.Find("TextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		//Left Button
		if (data[0] != null)
		{
			transform.Find("PrevCostTextMesh").GetComponent<MeshRenderer>().enabled = true;
			transform.Find("PrevCostTextMesh").GetComponent<tk2dTextMesh>().text = data[0]["cost"].ToString();
			transform.Find("PrevCostTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			transform.Find("LeftButtons/MinusButtonSprite").gameObject.SetActive(false);
			//transform.Find("LeftButtons/MinusButtonSprite").GetComponent<tk2dButton>().enabled = false;
			
			transform.Find("LeftButtons/ButtonPrevGreenSprite").gameObject.SetActive(false);
			//transform.Find("LeftButtons/ButtonPrevGreenSprite").GetComponent<tk2dButton>().enabled = false;
			
			transform.Find("LeftButtons/PowerEmptyLeftSprite").GetComponent<MeshRenderer>().enabled = false;
			
			if (key == "boss_round_environment")
			{
				transform.Find("LeftButtons/ButtonPrevGreenSprite").gameObject.SetActive(true);
				//transform.Find("LeftButtons/ButtonPrevGreenSprite").GetComponent<tk2dButton>().enabled = true;
			}
			else{
				transform.Find("LeftButtons/MinusButtonSprite").gameObject.SetActive(true);
				//transform.Find("LeftButtons/MinusButtonSprite").GetComponent<tk2dButton>().enabled = true;
			}
		}
		else
		{
			transform.Find("PrevCostTextMesh").GetComponent<MeshRenderer>().enabled = false;
			
			transform.Find("LeftButtons/MinusButtonSprite").gameObject.SetActive(false);
			//transform.Find("LeftButtons/MinusButtonSprite").GetComponent<tk2dButton>().enabled = false;
			
			transform.Find("LeftButtons/PowerEmptyLeftSprite").GetComponent<MeshRenderer>().enabled = true;
			
			transform.Find("LeftButtons/ButtonPrevGreenSprite").gameObject.SetActive(false);
			//transform.Find("LeftButtons/ButtonPrevGreenSprite").GetComponent<tk2dButton>().enabled = false;
		}
		
		
		//Right Button
		if (data[2]!= null){
			transform.Find("NextCostTextMesh").GetComponent<MeshRenderer>().enabled = true;
			transform.Find("NextCostTextMesh").GetComponent<tk2dTextMesh>().text = data[2]["cost"].ToString();
			transform.Find("NextCostTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			if(data[2]["cost"].AsInt > bonus_power_manager_.GetAvailablePower()){
				if (key == "boss_round_environment"){
					transform.Find("RightButtons/PlusButtonSprite").gameObject.SetActive(false);
					//transform.Find("RightButtons/PlusButtonSprite").GetComponent<tk2dButton>().enabled = false;
					
					transform.Find("RightButtons/PlusRedSprite").GetComponent<MeshRenderer>().enabled = false;
					
					transform.Find("RightButtons/ButtonNextGreenSprite").gameObject.SetActive(false);
					//transform.Find("RightButtons/ButtonNextGreenSprite").GetComponent<tk2dButton>().enabled = false;
					transform.Find("RightButtons/NextRedSprite").GetComponent<MeshRenderer>().enabled = true;
				}
				else{
					transform.Find("RightButtons/PlusButtonSprite").gameObject.SetActive(false);
					//transform.Find("RightButtons/PlusButtonSprite").GetComponent<tk2dButton>().enabled = false;
					
					transform.Find("RightButtons/PlusRedSprite").GetComponent<MeshRenderer>().enabled = true;
					
					transform.Find("RightButtons/ButtonNextGreenSprite").gameObject.SetActive(false);
					//transform.Find("RightButtons/ButtonNextGreenSprite").GetComponent<tk2dButton>().enabled = false;
					transform.Find("RightButtons/NextRedSprite").GetComponent<MeshRenderer>().enabled = false;
				}
			}
			else
			{
				if (key == "boss_round_environment"){
					transform.Find("RightButtons/PlusButtonSprite").gameObject.SetActive(false);
					//transform.Find("RightButtons/PlusButtonSprite").GetComponent<tk2dButton>().enabled = false;
					
					transform.Find("RightButtons/PlusRedSprite").GetComponent<MeshRenderer>().enabled = false;
					
					transform.Find("RightButtons/ButtonNextGreenSprite").gameObject.SetActive(true);
					//transform.Find("RightButtons/ButtonNextGreenSprite").GetComponent<tk2dButton>().enabled = true;
					transform.Find("RightButtons/NextRedSprite").GetComponent<MeshRenderer>().enabled = false;
				}
				else{
					transform.Find("RightButtons/PlusButtonSprite").gameObject.SetActive(true);
					//transform.Find("RightButtons/PlusButtonSprite").GetComponent<tk2dButton>().enabled = true;
					
					transform.Find("RightButtons/PlusRedSprite").GetComponent<MeshRenderer>().enabled = false;
					
					transform.Find("RightButtons/ButtonNextGreenSprite").gameObject.SetActive(false);
					//transform.Find("RightButtons/ButtonNextGreenSprite").GetComponent<tk2dButton>().enabled = false;
					transform.Find("RightButtons/NextRedSprite").GetComponent<MeshRenderer>().enabled = false;
				}
			}
			
			transform.Find("RightButtons/PowerEmptyRightSprite").GetComponent<MeshRenderer>().enabled = false;
		}
		else
		{
			transform.Find("NextCostTextMesh").GetComponent<MeshRenderer>().enabled = false;
			
			transform.Find("RightButtons/PlusButtonSprite").gameObject.SetActive(false);
			//transform.Find("RightButtons/PlusButtonSprite").GetComponent<tk2dButton>().enabled = false;
			
			transform.Find("RightButtons/PowerEmptyRightSprite").GetComponent<MeshRenderer>().enabled = true;
		} 
	}
	
	
	public void Refresh(int round)
	{
		round_ = round;
		InitUI(round_,key_);
	}
	
	
	private void PlusPressed()
	{
		if (touch_enabled_ == true)
		{
			Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
			bonus_power_manager_.Upgrade (round_ , key_);
			Messenger.Broadcast("PressedAdjustableParameterChange", round_);	
			//Achievement
			#if UNITY_IPHONE && !UNITY_EDITOR		
			Framework.Instance.InstanceComponent<GameCenterAPI>().AddAchievementProgress("1",100F);
			#endif
		}
	}	
	
	
	private void MinusPressed()
	{
		if (touch_enabled_ == true)
		{
			Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
			bonus_power_manager_.Downgrade(round_, key_);
			Messenger.Broadcast("PressedAdjustableParameterChange", round_);
		}
	}	
	
	
	string TrimString(string text){
		text.TrimStart('"');
		text.TrimEnd('"');
		
		return text;
	}
	
	public void ToggleTouchEnabled (bool enabled)
	{
		touch_enabled_ = enabled;
	}
}