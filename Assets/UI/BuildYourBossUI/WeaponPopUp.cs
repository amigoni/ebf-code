using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class WeaponPopUp : MonoBehaviour {
	
	public GameObject button_block_collider_prefab_;
	public GameObject weapon_popup_icon_prefab_;
	public GameObject power_num_text_mesh_;
	public GameObject power_sliced_sprite_;
	public GameObject tutorial_popup_;
	public GameObject weapons_scroll_content_;
	public GameObject button_block_collider_;
	public GameObject scrollable_area_;
	
	private BossPowerManager boss_power_manager_;
	private JSONNode store_info_; //Static info for each item
	private int last_row = 0;
	private int current_round_number_;
	private bool close_button_enabled_ = true;
	
	float scroll_initial_y_position = 0;
	
	private List<GameObject> weapon_icons = new List<GameObject>(); 
	
	public void Init(int round_number)
	{
		boss_power_manager_ = GameObject.Find("MainCamera/BuildYourBossTabContainer").GetComponent<BossPowerManager>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		Messenger.AddListener<string,float>("UpdatePowerIndicator", UpdatePowerIndicator);	
		
		current_round_number_ = round_number;
		
		if (round_number == 0){
			transform.Find("Round1Sprite").renderer.enabled = true;
			transform.Find("Round2Sprite").renderer.enabled = false;
			transform.Find("Round3Sprite").renderer.enabled = false;
		}
		else if (round_number == 1){
			transform.Find("Round1Sprite").renderer.enabled = false;
			transform.Find("Round2Sprite").renderer.enabled = true;
			transform.Find("Round3Sprite").renderer.enabled = false;
		}
		else if (round_number == 2){
			transform.Find("Round1Sprite").renderer.enabled = false;
			transform.Find("Round2Sprite").renderer.enabled = false;
			transform.Find("Round3Sprite").renderer.enabled = true;
		}
		
		boss_power_manager_.RefreshPowerIndicatorValue();
		
		CreateRow("bouncing");
		CreateRow("tap");
		CreateRow("chasing");
		CreateRow("fast");
		CreateRow("avoid");
		CreateRow("fast_rocket");
		CreateRow("rocket");
		CreateRow("laser");
		
		scroll_initial_y_position = weapons_scroll_content_.transform.position.y;
	}
	
	
	private void CreateRow(string item_name)
	{
		for (int i = 0; i < 3; i++)
		{
			GameObject icon = (GameObject) Instantiate(weapon_popup_icon_prefab_,new Vector3( 97 +i*143,150 - last_row*107,transform.position.z-3),Quaternion.identity);
			icon.transform.parent = weapons_scroll_content_.transform;
			icon.GetComponent<WeaponIconPopUpPrefab>().Init(item_name, i);
			weapon_icons.Add(icon);
		}
		
		last_row++;
	}
	
	
	private void RefreshIcons()
	{
		foreach (GameObject icon in weapon_icons)
			icon.GetComponent<WeaponIconPopUpPrefab>().Refresh();
		
		weapons_scroll_content_.transform.position = new Vector3(weapons_scroll_content_.transform.position.x,scroll_initial_y_position, weapons_scroll_content_.transform.position.z );
	}
	
	
	private void UpdatePowerIndicator(string text, float portion)
	{
		power_num_text_mesh_.GetComponent<tk2dTextMesh>().text = text;
		power_num_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		power_sliced_sprite_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2( portion*57F, 9F);
	}	
	
	
	public void ClosePressed()
	{
		if(close_button_enabled_ == true)
		{
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
			ClosePopUp();
		}
	}
	
	
	public void ClosePopUp()
	{
		Messenger.Broadcast("LevelSelected", current_round_number_);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "BossDetailScene");
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "sub_menu_name", "BuildYourBossTab");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		button_block_collider_.SetActive(false);
		gameObject.SetActive(false);
		//Destroy(gameObject);
	}
	
	
	public void Show(int current_round_number)
	{
		current_round_number_ = current_round_number;
		gameObject.SetActive(true);
		button_block_collider_.SetActive(true);
		RefreshIcons();
	}
	
	
	private void OnDestroy()
	{
		scrollable_area_.GetComponent<tk2dUIScrollableArea>().PreDestroy();
		Messenger.RemoveListener<string,float>("UpdatePowerIndicator", UpdatePowerIndicator);
		Destroy(button_block_collider_);
	}
	
	public void SetCloseButtonEnabled(bool is_enabled)
	{
		close_button_enabled_ = is_enabled;
		if (is_enabled == false)
			GameObject.Find("WeaponPopUp(Clone)/tk2dScrollableArea").GetComponent<tk2dUIScrollableArea>().enabled = false;
	}
	
	void WeaponsQuestionMarkPressed()
	{
		GameObject tutorial_popup = (GameObject) Instantiate(tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup>().Init(TutorialPopUpType.boss_weapons_,1.0F);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
}
