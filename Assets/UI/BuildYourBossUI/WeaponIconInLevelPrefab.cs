using UnityEngine;
using System.Collections;
using SimpleJSON;

public class WeaponIconInLevelPrefab : MonoBehaviour {
	
	private BossPowerManager boss_power_manager_;
	private JSONNode store_info_; //Static info for each item
	private Store store_;
	private BossDetail boss_detail_;
	
	public GameObject cost_text_mesh_;
	public GameObject icon_sprite_;
	public GameObject icon_easy_sprite_;
	public GameObject icon_medium_sprite_;
	public GameObject icon_hard_sprite_;
		
	
	private string property_name_;
	private int property_difficulty_;
	private int boss_round_;
	private int array_position_;
	
	private bool touch_enabled_ = true;
	
	// Use this for initialization
	void Awake (){
		boss_power_manager_ = GameObject.Find("MainCamera/BuildYourBossTabContainer").GetComponent<BossPowerManager>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		store_ = Framework.Instance.InstanceComponent<Store>();
		boss_detail_ = GameObject.Find("MainCamera").GetComponent<BossDetail>();
	}
	
	
	public void Init(string property_name_, int property_difficulty, int boss_round, int array_position){
		Refresh(property_name_,property_difficulty, boss_round, array_position);
	}
	
	
	public void Refresh(string property_name, int property_difficulty, int boss_round, int array_position){
		property_name_ = property_name;
		property_difficulty_ = property_difficulty;
		boss_round_ = boss_round;
		array_position_ = array_position;
		
		//Set Image
		string sprite_name = "";
		if(property_name == "bouncing")
			sprite_name = "IconWeap01_Bouncing";
		else if(property_name == "tap")
			sprite_name = "IconWeap02_Tap";
		else if(property_name == "avoid")
			sprite_name = "IconWeap03_Avoid";
		else if(property_name == "fast")			
			sprite_name = "IconWeap04_FastTap";
		else if(property_name == "laser")			
			sprite_name = "IconWeap05_laser";
		else if(property_name == "rocket")			
			sprite_name = "IconWeap06_Rocket";
		else if(property_name == "chasing")			
			sprite_name = "IconWeap07_Chasing";
		else if(property_name == "fast_rocket")			
			sprite_name = "IconWeap08_FastRocket";
		else if(property_name == "sin")			
			sprite_name = "IconWeap09_Sin";
		
		transform.Find("IconSprite/Button").GetComponent<tk2dSprite>().spriteId = transform.Find("IconSprite/Button").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		
		
		//Set Difficulty Bubble
		if (property_difficulty == 0){
			icon_easy_sprite_.renderer.enabled = true;
			icon_medium_sprite_.renderer.enabled = false;
			icon_hard_sprite_.renderer.enabled = false;
		}
		else if (property_difficulty == 1){
			icon_easy_sprite_.renderer.enabled = false;
			icon_medium_sprite_.renderer.enabled = true;
			icon_hard_sprite_.renderer.enabled = false;
		}
		else if (property_difficulty == 2){
			icon_easy_sprite_.renderer.enabled = false;
			icon_medium_sprite_.renderer.enabled = false;
			icon_hard_sprite_.renderer.enabled = true;
		}
		
		//Set Cost
		cost_text_mesh_.GetComponent<tk2dTextMesh>().text = store_info_["boss_round_weapons"]["weapons"][(int)boss_detail_.GetBossData().Level][property_name]["difficulty_"+property_difficulty]["cost"].AsInt.ToString();
		cost_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
	}
	
	private void Pressed()
	{
		if (touch_enabled_ == true)
		{
			boss_power_manager_.SetCurrentWeapon(property_name_, property_difficulty_,boss_round_,array_position_);
			Messenger.Broadcast("LevelWeaponIconPressed");
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}
	}
	
	
	public void ToggleTouchEnabled(bool enabled)
	{
		touch_enabled_ = enabled;
	}
}
