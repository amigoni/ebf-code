using UnityEngine;
using System.Collections;
using SimpleJSON;

public class UtilityItemButtonPrefab : MonoBehaviour {

	private string key_;
	private bool is_selected_ = false;
	private bool is_bought_ = false;
	private bool is_locked_ = false;
	public GameObject popup_window_;
	
	private Store store_;
	private JSONNode store_info_;
	private PlayerData player_data_;
	
	// Use this for initialization
	void Awake () 
	{
		store_ = Framework.Instance.InstanceComponent<Store>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		
		Messenger.AddListener<string>("UtilityBuyButtonPressed", Buy);
	}
	
	
	public void Init(string key, bool is_locked, bool is_selected)
	{
		key_ = key;
		is_locked_ = is_locked;
		is_selected_ = is_selected;
		
		transform.Find("NewLabelSprite").renderer.enabled = false;
		
		transform.Find("UtilitySelectedSprite").renderer.enabled = false;
		transform.Find("UtilityBackgroundSprite").renderer.enabled = true;
		transform.Find("Button/UtilityIconSprite").renderer.enabled = false;
		transform.Find("UtilityCheckSmallSprite").renderer.enabled = false;
		transform.Find("UtilityLockSprite").renderer.enabled = false;
		
		string sprite_name = "";
		
		if(key == "double_bubble")
			sprite_name = "IconUtiDD01_DoubleBubble";
		else if(key == "double_magnet")
			sprite_name = "IconUtiDD02_DoubleMagnet";
		else if(key == "life_boost")			
			sprite_name = "IconUtiDD03_LifeBoost";
		else if(key == "att_boost")			
			sprite_name = "IconUtiDD04_AttBoost";
		else if(key == "ammo_boost")			
			sprite_name = "IconUtiDD05_AmmoBoost";
		
		transform.Find("Button/UtilityIconSprite").GetComponent<tk2dSprite>().spriteId = transform.Find("Button/UtilityIconSprite").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		transform.Find("Button/UtilityIconSpriteDown").GetComponent<tk2dSprite>().spriteId = transform.Find("Button/UtilityIconSpriteDown").GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		if (is_locked == false){
			transform.Find("UtilityLockSprite").renderer.enabled = false;
			transform.Find("Button/UtilityIconSprite").renderer.enabled = true;
			transform.Find("Button/UtilityIconSpriteDown").renderer.enabled = true;
			//transform.Find("EquippedButtonSprite").renderer.enabled = false;
		}
		else
		{
			transform.Find("UtilityLockSprite").renderer.enabled = true;
			transform.Find("Button").gameObject.SetActive(false);
		}
	
		
		if (is_selected == true)
			Select();
		else
			Unselect();
	}
	
	
	public void IconPressed ()
	{
		Select();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		/*
		if ( is_selected_ == false && is_locked_== false){
		
		}
		else if ( is_locked_ == true){
			Debug.Log("Locked Popup");
			//GameObject popup_window = (GameObject) Instantiate(popup_window_,new Vector3(0,0,0), Quaternion.identity);
			//popup_window.GetComponent<PopUpWindowPrefab>().Init("locked","titleText","Description",key_);
		}
		*/
	}
	
	
	public void Select()
	{
		Messenger.Broadcast("UtilitySelected", key_, is_bought_, is_locked_);
		is_selected_ = true;
		transform.Find("UtilitySelectedSprite").renderer.enabled = true;
		
	}
	
	
	public void Unselect()
	{
		is_selected_ = false;
		transform.Find("UtilitySelectedSprite").renderer.enabled = false;	
	}
	
	
	private void Buy(string selected_utility_key)
	{
		if (key_ == selected_utility_key && is_bought_ == false)
		{
			is_bought_ = store_.BuyItem(key_,1);
			if (is_bought_ == true){
				transform.Find("UtilityCheckSmallSprite").renderer.enabled = true;
				Select();
				Framework.Instance.audio_manager().Play("40_Menu Buy Upgrade", transform, 1.0F, 1.0F, false);
			}/*
			else{
				GameObject.Find("MainCamera").GetComponent<PopUpWindowCoinsStore>().Init();
			}
			*/
		}
	}		
	
	public string GetKey()
	{
		return key_;
	}
	
	
	void OnDestroy ()
	{
		Messenger.RemoveListener<string>("UtilityBuyButtonPressed", Buy);
	}
	
}
