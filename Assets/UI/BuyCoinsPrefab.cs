using UnityEngine;
using System.Collections;

public class BuyCoinsPrefab : MonoBehaviour {
	private string currency_;
	private int amount_;
	private Store store_;
	private PlayerData player_data_;
	
	public GameObject coins_text_mesh_;
	public GameObject white_coins_text_mesh;
	private bool use_temp_coins_;
	
	private GameObject coins_red_label_;
	private GameObject white_coins_red_label_;
	
	void Awake(){
		store_ = Framework.Instance.InstanceComponent<Store>();
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		
		Messenger.AddListener("RefreshCoins",Refresh);
		
		coins_red_label_ = GameObject.Find("CoinsRedLabel");
		white_coins_red_label_ = GameObject.Find("WhiteCoinsRedLabel");
		if(coins_red_label_ != null)
			coins_red_label_.SetActive(false);
		if(white_coins_red_label_ != null)
			white_coins_red_label_.SetActive(false);
	}
	
	// Use this for initialization
	void Start () {
		tk2dCamera camera = (tk2dCamera) Object.FindObjectOfType(typeof(tk2dCamera));
		GetComponent<tk2dCameraAnchor>().tk2dCamera = camera;
	}
	
	
	public void Init(float x_anchor, float y_anchor, int wmc_collected, int coins_collected)
	{
		if(wmc_collected > 0)
		{
			white_coins_red_label_.SetActive(true);
			white_coins_red_label_.GetComponentInChildren<tk2dTextMesh>().text = "+"+
				wmf.ui.Utility.FormattingWithZeros( wmc_collected, 2);
			white_coins_red_label_.GetComponentInChildren<tk2dTextMesh>().Commit();
		}
		else
			white_coins_red_label_.SetActive(false);

		if(coins_collected > 0)
		{
			coins_red_label_.SetActive(true);
			coins_red_label_.GetComponentInChildren<tk2dTextMesh>().text ="+"+
				wmf.ui.Utility.FormattingWithZeros( coins_collected, 4);
			coins_red_label_.GetComponentInChildren<tk2dTextMesh>().Commit();
		}
		else
			coins_red_label_.SetActive(false);
		
		Init (false, x_anchor, y_anchor);
	}
	
	public void Init(bool use_temp_coins, float x_anchor, float y_anchor)
	{
		GetComponent<tk2dCameraAnchor>().offset = new Vector2(x_anchor,y_anchor);
		
		use_temp_coins_ = use_temp_coins;
		
		if (use_temp_coins == true)
		{
			store_.SetTempWhiteCoins();
			transform.FindChild("BuyButton").gameObject.SetActive(false);
		}
		
		Refresh();
	}
	
	
	public void Refresh(){
		coins_text_mesh_.GetComponent<tk2dTextMesh>().text = FormattingWithZeros(player_data_.GetInt("coins").ToString(),8);
		coins_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		int white_coins = 0;
		if (use_temp_coins_ == true)
			white_coins = store_.GetTempWhiteCoins();
		else
			white_coins = player_data_.GetInt("white_coins");
		
		white_coins_text_mesh.GetComponent<tk2dTextMesh>().text = FormattingWithZeros(white_coins.ToString(),4);
		white_coins_text_mesh.GetComponent<tk2dTextMesh>().Commit();
	}
	
	
	public void BuyPressed()
	{
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		
		float z_position = 0;
		if (transform.position.z < 0)
			z_position = transform.position.z-60;
		
#if UNITY_IOS
		if (Framework.Instance.InstanceComponent<ServerAPI>().IsConnected() == true && (bool) Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("unibill_has_initialized") == true)
#else
		if (Framework.Instance.InstanceComponent<ServerAPI>().IsConnected())
#endif
		{
			Framework.Instance.InstanceComponent<LoadingScreenBlock>().Show();
			Framework.Instance.scene_manager().LoadScene("CoinsStoreScene", TransitionType.add_scene, z_position);
		}	
		else
		{
			Framework.Instance.InstanceComponent<ServerAPI>().NotifyError();
		}
	}
	
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");
		
		return txt;
	}
	
	
	public void ToggleEnabled (bool enabled){
		this.transform.Find("BuyButton").GetComponent<tk2dButton>().enabled = enabled;
	}
	
	
	void OnDestroy (){
		Messenger.RemoveListener("RefreshCoins",Refresh);
	}
}
