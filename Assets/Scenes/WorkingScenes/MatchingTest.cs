using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EbFightAPI;

public class MatchingTest : MonoBehaviour {
	
	public WWWnetwork www;
	
	private int call_count = 0;
	private const int max_call = 30;
	
	private int dd_attack = 0;
	
	private List<EbFightAPI.MatchSession> list_ = new List<EbFightAPI.MatchSession>();
	
	// Use this for initialization
	void Start () {
		EbFight.Setup(www);
		EbFight.UserId = "4792927";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnFindMatch(ResponseStatus status, JSONResult data)
	{
		call_count++;
		
		if(status == ResponseStatus.Success && call_count < max_call)
		{
			list_.Add(data.As<EbFightAPI.MatchSession>());
			EbFight.FindMatch(0, OnFindMatch);
		}
	}
	
	void OnGUI()
	{
		if(GUI.Button(new Rect(10.0f, 10.0f, 100.0f, 30.0f), "Run!"))
		{
			call_count = 0;
			list_.Clear();
			EbFight.FindMatch(9, OnFindMatch);
		}
		
		dd_attack = int.Parse(GUI.TextField(new Rect(110.0f, 10.0f, 100.0f, 30.0f), dd_attack.ToString()));
		
		GUI.Label(new Rect(10.0f, 50.0f, 100.0f, 30.0f), call_count.ToString() + " / " + max_call.ToString());
		
		if(call_count == max_call)
		{
			for(int i = 0; i < list_.Count; i++)
			{
				GUI.Label(new Rect(10.0f, 70.0f + i * 10, 100.0f, 30.0f), list_[i].Contender.Boss.Level.ToString());
				Debug.Log(list_[i].Contender.Boss.Level.ToString());
			}
			
			list_.Clear();
		}
	}
	
}
