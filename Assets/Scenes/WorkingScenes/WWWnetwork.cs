using System;
using System.Collections;
using System.Collections.Generic;
using EbFightAPI;
using UnityEngine;

public class WWWnetwork : MonoBehaviour, INetworkProvider
{
    // Kong App
	public string Server = "http://ebfight-kongregateplus.appspot.com/api";

    public float Timeout = 10.0f;
	
	void Start()
	{
	}
	
	void Update()
	{
	}
	
    public void SendRequest(string url, Dictionary<string, string> data, Action<ResponseStatus, string> action)
    {
        StartCoroutine(SendRequestWithWWW(url, data, action));
    }

    private IEnumerator SendRequestWithWWW(string url, Dictionary<string, string> data, Action<ResponseStatus, string> action)
    {
        var form = new WWWForm();
        foreach (var item in data)
            form.AddField(item.Key, item.Value);
        
        var www = new WWW(Server + url, form);
        var elapsedTime = 0.0f;

        while(!www.isDone)
        {
            elapsedTime += Time.deltaTime;

            if (elapsedTime >= Timeout)
                break;

            yield return null;
        }
	
        if(!www.isDone || !string.IsNullOrEmpty(www.error))
        {  
				action(ResponseStatus.Disconnected, MiniJSON.Json.Serialize(JSONResult.ToMiniJSON(
                new Dictionary<string, object>
                    {
                        {"error", true},
                        {"result", www.error}
                    })));
			
            yield break;
        }
		
        action(ResponseStatus.Success, www.text);
    }
}