using UnityEngine;
using System.Collections;

public class TutorialScene : MonoBehaviour {
	
	public GameObject button_text_mesh;
	int pause_count = 0;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	
	void PauseAnimation()
	{
		foreach ( AnimationState state in animation)
		{
			state.speed = 0;
		}
		
		pause_count++;
		button_text_mesh.GetComponent<tk2dTextMesh>().text = pause_count+"/6";
		button_text_mesh.GetComponent<tk2dTextMesh>().Commit();
	}
	
	void ContinueAnimation()
	{
		if (pause_count < 6)
		{
			foreach ( AnimationState state in animation)
			{
				state.speed = 1;
			}	
		}
		else
			Application.LoadLevel("StartGame");
	}
}
