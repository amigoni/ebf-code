using UnityEngine;
using System.Collections;

public class InGameTutorialManager : MonoBehaviour {
	
	public GameObject tutorial_pop_ups_;
	public GameObject tutorial_pop_ups_container_mobile_;
	
	public GameObject player_;
	public Boss boss_;
	
	private bool boss_hit_by_punch_ = false;
	private bool boss_hit_by_power_punch_ = false;
	
	int missiles_hit_ = 0;
	int punches_hit_ = 0;
	
	int move_frame_counter_ = 0;
	
	private int tutorial_step_ = 0;
	
	
	public enum InGameTutorialState
	{
		moving,
		sliding,
		punching,
		activate_power_punch,
		hitting_missiles,
		hitting_combos,
		count
	}
	
	// Use this for initialization
	void Start () {

#if UNITY_IPHONE || UNITY_ANDROID
		tutorial_pop_ups_ = tutorial_pop_ups_container_mobile_;
#endif		
		if (Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done") == false)
		{
			
			tutorial_pop_ups_.SetActive(true);
			GameObject.Find("GUI").GetComponent<PowerPunchHoldButton>().SetPowerPunchEnabled(false);
			GameObject.Find("GUI").GetComponent<DashController>().SetControllerEnabled(false);
		}
		
		player_ = GameObject.Find("Player");
		boss_ = GameObject.Find("Boss/MainBoss/BossPrefab").GetComponent<Boss>();
	}
	
	
	// Update is called once per frame
	void Update () {
		if (tutorial_step_ == 0)
		{
			//Check if Player is moving
			//Debug.Log("V Magnitude "+player_.GetComponent<PlayerMoveController>().GetCurrentDirection().magnitude+" move frame counter: "+move_frame_counter_);
			if (player_.GetComponent<PlayerMoveController>().GetCurrentDirection().magnitude != 0F && move_frame_counter_ <= 20)
			{
				move_frame_counter_ ++;
				
				if (move_frame_counter_ >= 20)
				{
					Continue();	
					Invoke("EnableDash",1.0F);
					
#if UNITY_WEBPLAYER
					tutorial_step_++;
#endif
				}		
			}	
		}
	}
	
	
	private void EnableDash()
	{
		GameObject.Find("GUI").GetComponent<DashController>().SetControllerEnabled(true);
	}
	
	public void PauseAnimation()
	{
		foreach ( AnimationState state in tutorial_pop_ups_.animation)
		{
			state.speed = 0;
		}
	}
	
	
	public void ContinueAnimation()
	{
		Debug.Log("Tutorial Step: "+tutorial_step_);
		
		if (tutorial_step_ <= 6)
		{
			foreach ( AnimationState state in tutorial_pop_ups_.animation)
			{
				state.speed = 1;
			}	
			
			if (tutorial_step_ == 6){
				boss_.ChangeState (IdleState.Instance);
				Invoke("EndTutorial",2.0F);
				//Framework.Instance.InstanceComponent<PlayerData>().SetValue("tutorial_in_game_done",true);
			}
		}
	}
	
	/*
	public void GoBackToActivatePowerPunch()
	{
		tutorial_step_ = 2;
		foreach ( AnimationState state in tutorial_pop_ups_.animation)
		{
			state.time = 1.5F;
		}
		
		Continue();
	}
	*/
	
	
	public void EndTutorial()
	{
		tutorial_pop_ups_.SetActive(false);
		GameObject.Find("Game").GetComponent<Game>().EndGame();
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("dd_ammo",1);
	}
	
	
	public void PerformDash()
	{
		if (tutorial_step_ == 1)
		{
			Continue();
		}
	}
	
	public void HitBoss(){
		
		if (tutorial_step_ == 2)
		{
			punches_hit_++;
			if (punches_hit_ == 4)
			{
				GameObject.Find("GUI").GetComponent<PowerPunchHoldButton>().SetPowerPunchEnabled(true);
				Continue();
			}
		}
	}
	
	
	public void HitWithPowerPunch(){
		if (tutorial_step_ == 3)
		{
			Continue();
			//boss_.ChangeState (IntroState.Instance);
		}	
	}
	
	public void HitMissile(){
		if (tutorial_step_ == 4)
		{
			missiles_hit_ ++;
			if (missiles_hit_ == 2)
			{
				Continue();
				boss_.ForceChangeSetup();
			}
		}
	}
	
	public void CompletedCombo(){
		if (tutorial_step_ == 5)
			Continue();
	}
	
	
	private void Continue(){
		tutorial_step_++;
		Invoke("ContinueAnimation", 0.75F);
	}
	
	
	//Getter / Setter
	public int tutorial_step()
	{
		return  tutorial_step_;
	}
	
}
