using UnityEngine;
using System.Collections;

public class GenericLoadingScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		PoolManager.ClearPool();
		System.GC.Collect();
		Invoke("NextScene", 0.1f);
	}
	
	void NextScene () {
		
		string next_scene = (string)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("next_scene");
		Framework.Instance.InstanceComponent<wmf.StaticParams>().Remove("next_scene");
		if(next_scene != "")
			Application.LoadLevel(next_scene);
		else
			Application.LoadLevel("MainMenu");
	}

}
