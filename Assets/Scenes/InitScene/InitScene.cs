using UnityEngine;
using System.Collections;


public class InitScene : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		Time.timeScale = 1.0f;
		
		Framework.Instance.Init();
	
		Framework.Instance.scene_manager().LoadScene("LandingPage", TransitionType.change_scene);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
