using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UtilityScene : MonoBehaviour {
	
	private int dd_won_passed_prize = 1;
	private int dd_perfect_prize = 3;
	private int dd_all_perfect_prize = 1;
	
	private int boss_not_destroyed_prize = 2;
	private int boss_all_not_destroyed_prize = 1;
	private int boss_won_prize = 4;
	
	private List<string[]> all_possible_results_ = new List<string[]>();
	
	private Dictionary<string, string> RESULTS_MAP = new Dictionary<string, string>();

	// Use this for initialization
	void Start () {
		RESULTS_MAP.Add("none", "none");
		RESULTS_MAP.Add("won", "destroyed");
		RESULTS_MAP.Add("passed", "survived");
		RESULTS_MAP.Add("died", "won");
		RESULTS_MAP.Add("perfect", "annihilated");
		
		all_possible_results_.Add( new string[]{ "perfect", "perfect", "perfect" } );
		all_possible_results_.Add( new string[]{ "perfect", "perfect", "won" } );
		all_possible_results_.Add( new string[]{ "perfect", "perfect", "died" } );
		all_possible_results_.Add( new string[]{ "perfect", "won", "won" } );
		all_possible_results_.Add( new string[]{ "perfect", "won", "passed" } );
		all_possible_results_.Add( new string[]{ "perfect", "won", "died" } );
		all_possible_results_.Add( new string[]{ "perfect", "died", "none" } );
		all_possible_results_.Add( new string[]{ "won", "won", "won" } );
		all_possible_results_.Add( new string[]{ "won", "won", "passed" } );
		all_possible_results_.Add( new string[]{ "won", "won", "died" } );
		all_possible_results_.Add( new string[]{ "won", "passed", "passed" } );
		all_possible_results_.Add( new string[]{ "won", "passed", "died" } );
		all_possible_results_.Add( new string[]{ "won", "died", "none" } );
		all_possible_results_.Add( new string[]{ "passed", "passed", "passed" } );
		all_possible_results_.Add( new string[]{ "passed", "passed", "died" } );
		all_possible_results_.Add( new string[]{ "passed", "died", "none" } );
		all_possible_results_.Add( new string[]{ "died", "none", "none" } );
		all_possible_results_.Add( new string[]{ "perfect", "passed", "died" } );
		all_possible_results_.Add( new string[]{ "perfect", "perfect", "passed" } );
		all_possible_results_.Add( new string[]{ "perfect", "passed", "passed" } );
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	int ComputeDDTrophies(List<string> results)
	{
		int passed_count = results.FindAll( r => (r == "won") || (r == "passed")).Count;
		int perfect_count = results.FindAll( r => (r == "perfect") ).Count; 
		int prize = passed_count * dd_won_passed_prize;
		prize += perfect_count * dd_perfect_prize;
		if (perfect_count == results.Count)
			prize += dd_all_perfect_prize;
		
		return prize;
	}	
	
	int ComputeBossTrophies(List<string> results)
	{
		int not_destroyed = results.FindAll( r => (r == "won") || (r == "none") || (r == "survived")).Count;
		int prize = not_destroyed * boss_not_destroyed_prize;
		if (not_destroyed == results.Count)
			prize += boss_all_not_destroyed_prize;
		if (results.Contains("won"))
			prize += boss_won_prize;
		return prize;
	}
	
	List<string> GetBossResults(List<string> dd_results)
	{
		List<string> boss_results = new List<string>();
		foreach(string dd_result in dd_results)
		{
			boss_results.Add(RESULTS_MAP[dd_result]);
		}
		
		return boss_results;
	}
	
	void OnGUI()
	{
		for(int i = 0; i < all_possible_results_.Count; i++)
		{
			List<string> results = new List<string>( all_possible_results_[i] );
			int dd_rank_prize = ComputeDDTrophies( results );
			List<string> boss_results = GetBossResults( new List<string>( all_possible_results_[i] ) );
			int boss_rank_prize = ComputeBossTrophies( boss_results );
			int rank_diff = dd_rank_prize - boss_rank_prize;
			
			int j = 0;
			for(j = 0; j < results.Count; j++)
			{
				GUI.Button(new Rect(1 + (j * 130), 10 + (i * 30), 130, 30), results[j] + "/" + boss_results[j]);
			}
			
			GUI.Button(
				new Rect(1 + (j * 130), 10 + (i * 30), 300, 30), 
				"dd_prize: " + dd_rank_prize + " boss_prize: " + boss_rank_prize + " diff:" + rank_diff + (rank_diff > 0 ? " Player won" : " BOSS WON") 
			);
			
			GUI.Button(
				new Rect(1 + (j * 130) + 300, 10 + (i * 30), 100, 30), 
				"BOSS XP: " + boss_rank_prize
			);
			
			/*
			rank_diff = -rank_diff;
			
			if(rank_diff < 0)
				rank_diff = 0;
			GUI.Button(
				new Rect(1 + (j * 130) + 300 + 100, 10 + (i * 30), 100, 30), 
				"BOSS XP: " + rank_diff
			);
			*/
		}
		
	}	
	
}
