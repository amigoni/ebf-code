using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
	
	private float[] time_array_;
	
	// Use this for initialization
	void Start () {
		time_array_ = new float[3];
		time_array_[0] = 30;
		time_array_[1] = 22;
		time_array_[2] = 17;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private const float WIDTH1 = 45;
	void OnGUI()
	{
		Store store = Framework.Instance.InstanceComponent<Store> ();
		
		GUI.Button(new Rect(5, 0, WIDTH1, 30), "P att");
		GUI.Button(new Rect(5 + WIDTH1 * 1, 0, WIDTH1, 30), time_array_[0].ToString() + "s");	
		GUI.Button(new Rect(5 + WIDTH1 * 2, 0, WIDTH1, 30), time_array_[1].ToString() + "s");	
		GUI.Button(new Rect(5 + WIDTH1 * 3, 0, WIDTH1, 30), time_array_[2].ToString() + "s");	
		GUI.Button(new Rect(5 + WIDTH1 * 4, 0, WIDTH1, 30), "B hp");
		GUI.Button(new Rect(5 + WIDTH1 * 5, 0, WIDTH1, 30), "B def");
		GUI.Button(new Rect(5 + WIDTH1 * 6, 0, WIDTH1, 30), "shot t");
		
		GUI.Button(new Rect(5 + WIDTH1 * 7, 0, WIDTH1, 30), "45N");
		
		for(int i = 0; i < 10; i++)
		{
			float damage = store.GetUpgradablePropertyEffectByNameAsFloat("dd_att_level", i * 2);	
			float boss_hp = store.GetUpgradablePropertyEffectByNameAsFloat("boss_level_upgrade", i);
			float def = store.GetUpgradablePropertyEffectByNameAsFloat("boss_def", i * 2);
			
			GUI.Button(new Rect(5, 30 + (i * 30), WIDTH1, 30), damage.ToString());
			GUI.Button(new Rect(5 + WIDTH1 * 1, 30 + (i * 30), WIDTH1, 30), (damage * time_array_[0] / def).ToString("0.0"));	
			GUI.Button(new Rect(5 + WIDTH1 * 2, 30 + (i * 30), WIDTH1, 30), (damage * time_array_[1] / def).ToString("0.0"));	
			GUI.Button(new Rect(5 + WIDTH1 * 3, 30 + (i * 30), WIDTH1, 30), (damage * time_array_[2] / def).ToString("0.0"));	
			
			GUI.Button(new Rect(5 + WIDTH1 * 4, 30 + (i * 30), WIDTH1, 30), boss_hp.ToString());	
			GUI.Button(new Rect(5 + WIDTH1 * 5, 30 + (i * 30), WIDTH1, 30), def.ToString());
			GUI.Button(new Rect(5 + WIDTH1 * 6, 30 + (i * 30), WIDTH1, 30), (0.3 * 6 + 3).ToString());
			
			//GUI.Button(new Rect(5 + WIDTH1 * 5, 30 + (i * 30), WIDTH1, 30), def.ToString());
		}
	}
}
