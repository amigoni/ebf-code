using UnityEngine;
using System.Collections;
using EbFightAPI;

public class MultiplayerUI : MonoBehaviour {
	
	public GameObject boss_battle_prefab_;
	public GameObject dd_battle_log_;
	public GameObject welcome_multiplayer_screen_;
	public GameObject invite_button_;
	
	public GameObject dd_battle_log_button_;
	public GameObject boss_battle_log_button_;
	public GameObject back_button_;
	
	public GameObject multiplayer_bubble_container_;
	
	//Tutorial
	public GameObject multiplayer_tutorial_;
	public GameObject tutorial_popup_;
	int tutorial_step_ = 9;
	
	private PlayerData player_data_;
	
	
	// Use this for initialization
	void Start () 
	{
		//Appear();
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if(transform.position != new Vector3(transform.position.x, transform.position.y, -100.0f) &&
		   Game.FindObjectOfType( typeof(TutorialPopup) ) == null)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
				CloseButtonPressed();
		}
	}
#endif
	
	public void Appear()
	{
		//gameObject.SetActiveRecursively(true);
		gameObject.SetActive(true);
		transform.position = new Vector3(transform.position.x, transform.position.y,0F);
#if UNITY_WEBPLAYER
		invite_button_.SetActive(false);
#endif
		multiplayer_bubble_container_.SetActiveRecursively(false);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "MultiplayerUIScene");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		//Open tutorial the first time
		if (player_data_.GetBool("tutorial_multiplayer_done") == false)
		{
			QuestionMarkPressed();
			player_data_.SetValue("tutorial_multiplayer_done",true);
		}
		
		GameObject.Find("MyNameTextMesh").GetComponent<tk2dTextMesh>().text = player_data_.GetString("user_name");
		GameObject.Find("MyNameTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		//DD
		GameObject.Find("DDDRContainer/AnimationContainer/TextMesh0").GetComponent<tk2dTextMesh>().text = 
			Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().PlayerRank.ToString();
		GameObject.Find("DDDRContainer/AnimationContainer/TextMesh0").GetComponent<tk2dTextMesh>().Commit();
		GameObject.Find("DDLeagueTextMesh").GetComponent<tk2dTextMesh>().text = 
			Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().PlayerLeague.ToString();
		GameObject.Find("DDLeagueTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		//BOSS
		GameObject.Find("BossBRContainer/AnimationContainer/TextMesh0").GetComponent<tk2dTextMesh>().text = 
			Framework.Instance.InstanceComponent<ServerAPI>().GetBoss().Rank.ToString();
		GameObject.Find("BossBRContainer/AnimationContainer/TextMesh0").GetComponent<tk2dTextMesh>().Commit();
		GameObject.Find("BossLeagueTextMesh").GetComponent<tk2dTextMesh>().text = 
			Framework.Instance.InstanceComponent<ServerAPI>().GetBoss().League.ToString();
		GameObject.Find("BossLeagueTextMesh").GetComponent<tk2dTextMesh>().Commit();
	}
	
	public void Init()
	{
		SetMultiplayerBubble(0);
		
		//Tutorial
		/*
		if (Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_multiplayer_done") == false)
		{
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("tutorial_multiplayer_done", true);
			multiplayer_tutorial_.SetActive(true);
			multiplayer_tutorial_.animation.Play();
			//dd_battle_log_button_.GetComponent<tk2dButton>().enabled = false;
			//boss_battle_log_button_.GetComponent<tk2dButton>().enabled = false;
			//back_button_.GetComponent<tk2dButton>().enabled = false;
		}
		*/
	}
	
	
	private void FightRankedPressed ()
	{
		Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsMultiplayer", true);
		Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins(
			Framework.Instance.InstanceComponent<PlayerData>().GetInt("white_coins"),
			OnSyncWhiteCoins);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	private void OnSyncWhiteCoins(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			Framework.Instance.InstanceComponent<ServerAPI>().BeginMatch(OnBeginMatch);
		}
		else
		{
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("Can't Connect", "Please try again in a bit");
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
			Debug.Log("Error Connecting - OnSyncWhiteCoins");
		}
	}
	
	private void OnBeginMatch(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("MatchSession", data.As<MatchSession>());
			Application.LoadLevel("StartGame");
		}
		else
		{
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("Can't Connect", "Please try again in a bit");
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
			Debug.Log("Error Connecting - OnBeginMatch");
		}
	}
	
	
	private void DDBattleLogPressed ()
	{
		dd_battle_log_.SetActiveRecursively(true);
		dd_battle_log_.GetComponent<BattleLogPopUpPrefab>().Init();
		transform.position = new Vector3(transform.position.x, transform.position.y,-100F);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	private void BossBattleLogPressed ()
	{
		boss_battle_prefab_.SetActiveRecursively(true);
		boss_battle_prefab_.GetComponent<BattleLogPopUpPrefab>().Init();
		transform.position = new Vector3(transform.position.x, transform.position.y,-100F);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	private void InviteAFriendPressed ()
	{
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	private void CloseButtonPressed ()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y,-100F);
		GameObject.Find("MainCamera").GetComponent<MainMenu>().ToggleButtons(true);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "MainMenu");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
	
	private void QuestionMarkPressed ()
	{
		//welcome_multiplayer_screen_.SetActive(true);
		//gameObject.SetActive(false);
		GameObject tutorial_popup = (GameObject) Instantiate(tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup>().Init(TutorialPopUpType.multiplayer_,2F);
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "MultiplayerScene");
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "sub_menu_name", "WelcomeMultiplayer");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
	
	public void ToggleWelcomeScreen (bool is_visible_){
		
		welcome_multiplayer_screen_.SetActiveRecursively(is_visible_);
		//GameObject.Find("MainCamera").GetComponent<MainMenu>().ToggleButtons(true);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);	
		
		if (is_visible_ == false){
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			analitycs_.AddEvent(KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "MultiplayerScene");
			analitycs_.SendEvent(KeenIOEventType.menu_opened);
		}
	}
	
	
	public void SetMultiplayerBubble (int number){
		if (number == 0){
			multiplayer_bubble_container_.SetActiveRecursively(false);
		}
		else {
			multiplayer_bubble_container_.SetActiveRecursively(true);
			multiplayer_bubble_container_.transform.Find ("BubbleTextMesh").GetComponent<tk2dTextMesh>().text = number.ToString();
			multiplayer_bubble_container_.transform.Find ("BubbleTextMesh").GetComponent<tk2dTextMesh>().Commit();
		}
	}
	
	//Tutorial	
	/*
	public void IncreaseTutorialStep(){
		tutorial_step_++;
		
		if (tutorial_step_ == 1)
		{
			
		}	
		else if (tutorial_step_ == 2)
		{
			
		}	
		else if (tutorial_step_ == 3)
		{
			
		}	
		else
		{
		}	
		
			
		foreach ( AnimationState state in multiplayer_tutorial_.animation)
		{
			state.speed = 1;
		}	
	}
	*/
	
}
