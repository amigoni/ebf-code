using UnityEngine;
using System.Collections;

public class RefillTimer : MonoBehaviour {
	
	private float timer_;
	
	void Awake (){
		timer_ = 9999;
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void Init(float start_time){
		timer_ = start_time;
		//if (timer_ >0)
			//InvokeRepeating("TimeUpdate", 0.0F, 1.0F);
	}
	
	public void TimeUpdate (){		
		timer_ --;
		if (timer_ <= 0){
			TimerDisappear();
		}
			
		
		float seconds = timer_ % 60;
		float minutes = Mathf.Floor( timer_ / 60) % 60;
		float hours = Mathf.Floor( timer_ / 3600) % 24;
		float days = Mathf.Floor( timer_ / (3600*24));
		
		string text = seconds+"s";
		
		if (days > 0){
			text = 	days +"d "+ hours+"h";
		}		
		else if (hours > 0){	
			text = 	hours +"h "+ minutes+"m";	
		}		
		else if (minutes > 0){
			text = minutes +"m "+ seconds+"s";
		};								
		
		GetComponent<tk2dTextMesh>().text = text;
		GetComponent<tk2dTextMesh>().Commit();
	}
	
	
	void TimerDisappear(){
		transform.parent.Find("MoneyTextMesh").GetComponent<MeshRenderer>().enabled = false;
		transform.parent.Find("RefillButtonSprite").GetComponent<MeshRenderer>().enabled = false;
		transform.parent.Find("RefillLabelSprite").GetComponent<MeshRenderer>().enabled = false;
		transform.parent.Find("TimeTextMesh").GetComponent<MeshRenderer>().enabled = false;
	}
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");
		
		return txt;
	}
}
