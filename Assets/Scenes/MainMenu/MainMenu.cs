using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using MiniJSON;
using EbFightAPI;

public class MainMenu : MonoBehaviour {
	
	public GameObject multiplayerPrefab;
	public GameObject reward_container_;
	public GameObject reward_full_sprite_;
	public GameObject reward_text_mesh_;
	
	public static string currentScene_ = "MainMenu"; //Leo. Keep track of the last scene for buttons
	private GameObject background_music_instance_;
	public GameObject buy_coins_prefab_;
	
	private int current_mission_id_;
	private float dd_level_;
	private float dd_ammo_;
	private float dd_ammo_max_;
	private float dd_ammo_refill_rate_;
	private bool click_enabled_ = true;

	private Store store_;
	private PlayerData player_data_;
	private EbFightAPI.BattleReport battle_report_;
	
	//Tutorial
	public GameObject main_menu_tutorial_;
	
	public GameObject tutorial_popup_;
	
	//Battle Report Update Time
	private const float BATTLE_REPORT_UPDATE_TIME = 60.0f;
	
	// Use this for initialization
	void Start () 
	{	
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetEnabled();
		PoolManager.ClearPool();
		
		//Save score
		string saved_best_score_txt = PlayerPrefs.GetString("best_score");
		if(saved_best_score_txt == "")
		{
			saved_best_score_txt = "0";
			PlayerPrefs.SetString("best_score", saved_best_score_txt);
		}
		
		store_ = Framework.Instance.InstanceComponent<Store>();
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		
		//Init Ui
		InitBuyCoinsButton(280, 290);
		
		//Missions 
		int current_mission_id_ = PlayerPrefs.GetInt ("current_mission_id")-1;
		if (current_mission_id_ < 0) 
			current_mission_id_ = 0;
	
		Framework.Instance.audio_manager().PlayBackgroundMusic("Endless Boss Menu");
		
		//DD bubble Notification
		//GameObject.Find("MainCamera/ButtonMyDDContainer/NotifySprite").renderer.enabled = false;
		/*
		if(store_.AreThereUnpurchasedUpgrades("dd") == true)
			GameObject.Find("MainCamera/ButtonMyDDContainer/NotifySprite").renderer.enabled = true;
		else
			GameObject.Find("MainCamera/ButtonMyDDContainer/NotifySprite").renderer.enabled = false;
		*/
		
		//Boss bubble Notification
		//GameObject.Find("MainCamera/ButtonMyBossContainer/NotifySprite").renderer.enabled = false;
		
		SetMultiplayerBubble(0);
		//ToggleButtons(false);
		
		//for sure
		Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsMultiplayer", false);
		
		//Last Time Battle Report Opened
		if (Framework.Instance.InstanceComponent<wmf.StaticParams>().ContainsKey("last_battle_report_time_unix") == false)
			Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("last_battle_report_time_unix", 0D);
		
		double last_battle_report_time_unix = (double) Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("last_battle_report_time_unix");
		
		if (player_data_.GetString("first_play_time") != "none")
		{
			if ((last_battle_report_time_unix + BATTLE_REPORT_UPDATE_TIME) < GetTimeStampUnix())
			{
				Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("last_battle_report_time_unix", GetTimeStampUnix());
				Framework.Instance.InstanceComponent<ServerAPI>().GetBattleReport(OnGetBattleReport);
			}
			else
			{
				if (Framework.Instance.InstanceComponent<wmf.StaticParams>().ContainsKey("last_battle_report_") == true)
					LoadBattleReport();
			}
		}
		
			
		if ( player_data_.GetString("first_play_time") == "none")
		{
#if UNITY_WEBPLAYER			
			FeedbackPressed();
#endif			
		}
		
		
		//Tutorial 
		if (player_data_.GetBool("tutorial_in_game_done") == false)
		{
			GameObject.Find ("MainCamera/ButtonFightNowContainer").SetActive(true);
			GameObject.Find ("MainCamera/MultiplayerButtonContainer").SetActive(false);
			GameObject.Find ("MainCamera/ButtonMyDDContainer").SetActive(false);
			GameObject.Find ("MainCamera/ButtonMyBossContainer").SetActive(false);
			GameObject.Find ("MainCamera/MissionsButtonContainer").SetActive(false);
			
			//GameObject.Find ("MainCamera/OptionsButtonContainer").SetActive(false);
			//GameObject.Find ("MainCamera/BuyCoinsPrefab(Clone)").SetActive(false);
			player_data_.SetValue("dd_xp", (player_data_.GetInt("dd_xp")+1));
		}
		/*
		else if (player_data_.GetInt("tutorial_current_step") == 1)
		{
			GameObject.Find ("MainCamera/ButtonFightNowContainer").SetActive(false);
			GameObject.Find ("MainCamera/MultiplayerButtonContainer").SetActive(true);
			GameObject.Find ("MainCamera/ButtonMyDDContainer").SetActive(false);
			GameObject.Find ("MainCamera/ButtonMyBossContainer").SetActive(false);
			GameObject.Find ("MainCamera/MissionsButtonContainer").SetActive(false);
			
			//GameObject.Find ("MainCamera/OptionsButtonContainer").SetActive(false);
			GameObject.Find ("MainCamera/BuyCoinsPrefab(Clone)").SetActive(false);
		}
		else if (player_data_.GetInt("tutorial_current_step") == 2)
		{
			GameObject.Find ("MainCamera/ButtonFightNowContainer").SetActive(false);
			GameObject.Find ("MainCamera/MultiplayerButtonContainer").SetActive(false);
			GameObject.Find ("MainCamera/ButtonMyDDContainer").SetActive(false);
			GameObject.Find ("MainCamera/ButtonMyBossContainer").SetActive(true);
			GameObject.Find ("MainCamera/MissionsButtonContainer").SetActive(false);
			
			//GameObject.Find ("MainCamera/OptionsButtonContainer").SetActive(false);
			GameObject.Find ("MainCamera/BuyCoinsPrefab(Clone)").SetActive(false);
			
			
		}	
		*/
	
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "main_menu");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		//Platform Button
#if UNITY_WEBPLAYER
		GameObject.Find("GameCenter").SetActive(false);
		GameObject.Find("GooglePlay").SetActive(false);
#elif UNITY_IPHONE
		//GameObject.Find("FeedbackButtonSprite").SetActive(false);
		GameObject.Find("GooglePlay").SetActive(false);
#elif UNITY_ANDROID
		//GameObject.Find("FeedbackButtonSprite").SetActive(false);
		GameObject.Find("GameCenter").SetActive(false);
		GameObject.Find("GooglePlay").SetActive(false);
#endif
		
#if UNITY_IPHONE
		if(Framework.Instance.InstanceComponent<ServerAPI>().GetBoss() != null)
			KongregateAPI.GetAPI().Stats.Submit("bosslevel", Framework.Instance.InstanceComponent<ServerAPI>().GetBoss().Level);
		KongregateAPI.GetAPI().Stats.Submit("ddlevel", player_data_.GetInt("dd_level") );
		KongregateAPI.GetAPI().Stats.Submit("missions", PlayerPrefs.GetInt ("current_mission_id") );
#elif UNITY_WEBPLAYER
		if(Framework.Instance.InstanceComponent<ServerAPI>().GetBoss() != null)
			Framework.Instance.InstanceComponent<WebKongregateAPI>().SendBadge("bosslevel", Framework.Instance.InstanceComponent<ServerAPI>().GetBoss().Level);
		Framework.Instance.InstanceComponent<WebKongregateAPI>().SendBadge("ddlevel", player_data_.GetInt("dd_level"));
		Framework.Instance.InstanceComponent<WebKongregateAPI>().SendBadge("missions", PlayerPrefs.GetInt ("current_mission_id") - 1);
#endif
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(Game.FindObjectOfType( typeof(SettingsScene) ) == null &&
		 	   Game.FindObjectOfType( typeof(CoinsStoreScene) ) == null &&
			   Game.FindObjectOfType( typeof(MultiplayerUI) ) == null)
			{
				Application.Quit();
			}
		}
	}
#endif
	
	//PlayGame
	void PlayPressed()
	{
		if (player_data_.GetString("first_play_time") == "none")
			player_data_.SetValue("first_play_time", GetTimeStampUnix());
		
		Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsMultiplayer", false);
		if (click_enabled_ == true)
		{	
			if (player_data_.GetBool("tutorial_in_game_done") == false)
			{
				Application.LoadLevel("LoadingScene");
				Framework.Instance.audio_manager().StopBackgroundMusic();	
				Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
				
				//Analitycs
				Framework.Instance.InstanceComponent<Analitycs>().NewRun(null);
				Framework.Instance.InstanceComponent<Analitycs>().IncreaseCommonValue("num_pve_played");
			}
			else
			{
				Framework.Instance.InstanceComponent<LoadingScreenBlock>().Show();
				Framework.Instance.InstanceComponent<UpdateDataManager>().SetDisabled();
				Application.LoadLevel("StartGame");
			}
					
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}
	}
	
	
	void UpgradesPressed()
	{
		if (click_enabled_ == true){
			Application.LoadLevel("UpgradesLobby");
		}	
	}
	
	
	void MyDDPressed()
	{
		if (click_enabled_ == true){
			Framework.Instance.InstanceComponent<LoadingScreenBlock>().Show();
			Application.LoadLevel("DDDetail");
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	
	void MyBossPressed()
	{
		if (click_enabled_ == true)
		{
			int white_coins = Framework.Instance.InstanceComponent<PlayerData>().GetInt("white_coins");
			Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins(white_coins, OnSyncWhiteCoins);
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}
	}
	
	
	void KongregatePressed()
	{
		GameObject.Find("Kongregate").GetComponent<KongregateGameObject>().OpenKongregateWindow();
	}
	
	
	void FeedbackPressed()
	{
		GameObject tutorial_popup = (GameObject) Instantiate(tutorial_popup_);	
#if UNITY_WEBPLAYER		
		tutorial_popup.GetComponent<TutorialPopup>().Init(TutorialPopUpType.ebf_kong_plus_,2F);
#elif UNITY_IPHONE || UNITY_ANDROID
		tutorial_popup.GetComponent<TutorialPopup>().Init(TutorialPopUpType.feedback_mobile_,2F);
#endif	
	}
	
	
	void GameCenterPressed()
	{
#if UNITY_WEBPLAYER && UNITY_EDITOR
				
#elif UNITY_WEBPLAYER 
				
#elif UNITY_IPHONE 
		GameCenterAPI game_center_ = Framework.Instance.InstanceComponent<GameCenterAPI>();
		if (game_center_.IsUserAuthenticated() == true)
			game_center_.ShowLeaderboardUI();
		else
			game_center_.Initialize();
				
#elif UNITY_ANDROID
		
#endif
	}
			
	
	void OnSyncWhiteCoins(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			long sync_white_coins = data.As<long>();
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", sync_white_coins);
			Application.LoadLevel("BossDetail");
		}
		else
		{
			//Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().GetComponent<PopUpWindowPrefab>().Init("We can't connect", "Please try again in a bit");
			Debug.Log("Error Connecting to our server");
		}
	}
	
	
	void OkCallBack()
	{
		
	}
	
	
	void PlayExitAnimation ()
	{
		gameObject.animation["MainMenuAnimation"].speed = -2;
		gameObject.animation["MainMenuAnimation"].time = gameObject.animation["MainMenuAnimation"].length;
		gameObject.animation.Play();
	}
	
	
	void MultiplayerPressed()
	{	
		if (click_enabled_ == true)
		{
#if UNITY_IPHONE
			if (Framework.Instance.InstanceComponent<ServerAPI>().IsConnected() == true)
				OpenMultiplayer();
			else
				Framework.Instance.InstanceComponent<ServerAPI>().NotifyError();
#else				
			OpenMultiplayer();
#endif			
		}
	}
	
	void OpenMultiplayer()
	{
		ToggleButtons(false);
		//multiplayerPrefab.SetActiveRecursively(true);
		multiplayerPrefab.GetComponent<MultiplayerUI>().Init();
		multiplayerPrefab.GetComponent<MultiplayerUI>().Appear();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	public void SetMultiplayerBubble (int number)
	{	
		if (number == 0){
			GameObject.Find ("MainCamera/MultiplayerButtonContainer/MultiplayerBubbleContainer").SetActiveRecursively(false);
		}
		else 
		{
			GameObject.Find ("MainCamera/MultiplayerButtonContainer/MultiplayerBubbleContainer").SetActiveRecursively(true);
			GameObject.Find ("MainCamera/MultiplayerButtonContainer/MultiplayerBubbleContainer/BubbleTextMesh").GetComponent<tk2dTextMesh>().text = number.ToString();
			GameObject.Find ("MainCamera/MultiplayerButtonContainer/MultiplayerBubbleContainer/BubbleTextMesh").GetComponent<tk2dTextMesh>().Commit();
		}
	}
	
	
	void MissionsPressed()
	{
		Framework.Instance.InstanceComponent<LoadingScreenBlock>().Show();
		Application.LoadLevel("MissionsScene");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void SettingsPressed()
	{
		//Framework.Instance.scene_manager().LoadScene("SettingsScene", TransitionType.add_scene);
		Application.LoadLevel("SettingsScene");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void RewardPressed()
	{
		if(click_enabled_ == true)
		{
			Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("last_battle_report_time_unix", GetTimeStampUnix() - BATTLE_REPORT_UPDATE_TIME - 10);
			Framework.Instance.scene_manager().LoadScene("BattleReportScene", TransitionType.add_scene, 0);
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	
	void InitBuyCoinsButton(float x, float y)
	{
		GameObject button = (GameObject) Instantiate(buy_coins_prefab_, new Vector3(x,y,this.transform.position.z+20),Quaternion.identity);
		button.transform.parent = this.transform;
		button.GetComponent<BuyCoinsPrefab>().Init(false, 29F,-32F);
		//buy_coins_prefab_.transform.FindChild("BuyButtonSprite").GetComponent<tk2dUIItem>().sendMessageTarget = this.gameObject;
	}

#if UNITY_EDITOR
	void OnGUI()
	{
		if(GUI.Button(new Rect(400, 5, 110, 30), "DebugScene"))
		{
			Application.LoadLevel("DebugScene");
		}
	}
#endif
	
	public void ToggleButtons(bool enabled){
		click_enabled_ = enabled;
		foreach( tk2dUIItem button in gameObject.GetComponentsInChildren<tk2dUIItem>())
		{
			button.enabled = enabled;
		}
		
		foreach( tk2dButton button in gameObject.GetComponentsInChildren<tk2dButton>())
		{
			button.enabled = enabled;
		}
	}
	
	private void OnGetBattleReport(ResponseStatus status, JSONResult data)	
	{
		if(status == ResponseStatus.Success)
		{
			if (Framework.Instance.InstanceComponent<wmf.StaticParams>().ContainsKey("last_battle_report_") == false)
				Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("last_battle_report_", data);
			else
				Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("last_battle_report_", data);
			
			LoadBattleReport();
		}
		else if (status == ResponseStatus.Error || status == ResponseStatus.Disconnected)
		{
			Debug.Log("Battle Report: Error in getting battle Report - Don't block interface");
		}
	}
	
	
	public void LoadBattleReport()
	{
		JSONResult result = (JSONResult) Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("last_battle_report_");
		battle_report_ = Framework.Instance.InstanceComponent<ServerAPI>().GetBattleReport( result.Data );
			
		if(battle_report_.WhiteCoinsEarned > 0 || battle_report_.HasBossLeveledUp == true)
		{
			reward_container_.SetActive(true);
			reward_text_mesh_.GetComponent<tk2dTextMesh>().text = battle_report_.WhiteCoinsEarned.ToString();
			reward_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			
			if( battle_report_.WhiteCoinsEarned >= battle_report_.WhiteCoinsChestCap)
				reward_full_sprite_.SetActive(true);	
		}
		
		if(battle_report_.HasBossLeveledUp == true)
		{
			Framework.Instance.InstanceComponent<UpdateDataManager>().AlreadySpawned(LoadBattleReportScene);	
		}
	}
	
	public void LoadBattleReportScene()
	{
		Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("last_battle_report_time_unix", GetTimeStampUnix() - BATTLE_REPORT_UPDATE_TIME - 10);
		Framework.Instance.scene_manager().LoadScene("BattleReportScene", TransitionType.add_scene, 0);
	}
	
	public void CloseBattleReport()
	{
		reward_container_.SetActive(false);
	}
	
	public EbFightAPI.BattleReport GetBattleReportData()
	{
		return battle_report_;
	}
	
	private void BattleReportRefresh(BattleReport battle_report)
	{
		
	}
	
	double GetTimeStampUnix()
	{
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		var timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		
		return timestamp;
	}
	
	
	
	/*
	private void LockInterface(){
		ToggleButtons(false);
	}
	
	private void UnlockInterface(){
		ToggleButtons(true);
	}
	
	
	public void ToggleButtons(bool enabled)
	{
		GameObject.Find("BuyCoinsPrefab(Clone)").GetComponent<BuyCoinsPrefab>().ToggleEnabled(enabled);
		GameObject.Find("ButtonFightNow").GetComponent<tk2dUIItem>().enabled = enabled;
		GameObject.Find("ButtonMyDD").GetComponent<tk2dUIItem>().enabled = enabled;
		GameObject.Find("ButtonMyBoss").GetComponent<tk2dUIItem>().enabled = enabled;
		GameObject.Find("ButtonMultiplayer").GetComponent<tk2dUIItem>().enabled = enabled;
		GameObject.Find("MissionsButtonSprite").GetComponent<tk2dButton>().enabled = enabled;
	}
	*/
}
