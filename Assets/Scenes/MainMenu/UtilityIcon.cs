using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UtilityIcon : MonoBehaviour {
	
	public GameObject active_container_;
	public GameObject inactive_container_;
	public string utility_key_;
	
	double expiration_time_; 
	double time_left_;
	int warning_time_limit_seconds_ = 60*60;
	
	
	
	// Use this for initialization
	void Awake () 
	{
		List<EbFightAPI.Utility> utilities = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().Boss.Utilities;
			
		for ( int i=0; i < utilities.Count; i++){
			if ( utilities[i].Kind == utility_key_)
				expiration_time_ =  utilities[i].Expires;	
		}
		
		time_left_ = expiration_time_ - GetTimeStampUnix();
		
		active_container_.SetActive(false);
		inactive_container_.SetActive(false);
		
		UpdateIcon();
	}
	
	double GetTimeStampUnix()
	{
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		var timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		
		return timestamp;
	}
	
	
	void UpdateIcon()
	{	
		if ( time_left_ > warning_time_limit_seconds_)
		{
			active_container_.SetActive(true);
			inactive_container_.SetActive(false);
		}	
		else if (time_left_ <= warning_time_limit_seconds_ && time_left_ > 0)
		{
			active_container_.SetActive(true);
			inactive_container_.SetActive(false);
		}
		else if(time_left_ <= 0)
		{
			active_container_.SetActive(false);
			inactive_container_.SetActive(true);
		}
	}
	
}
