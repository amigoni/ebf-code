using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SimpleJSON;

public class CoinsPage : MonoBehaviour 
{
	
	public GameObject scroll_area_;
	public GameObject scrollview_;
	public GameObject icon_prefab_;
	public GameObject root_;
	private JSONNode store_info_;
	
	
	// Use this for initialization
	void Awake () 
	{
		
	}
	
	
	public void InitScrollView(GameObject tab, string page_type)
	{
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		float screen_width = Screen.width;
		
		if (tk2dSystem.CurrentPlatform.Contains("2") == true)
			screen_width = screen_width/2;
		
		//Takes care of special cases like iPod4 and iPad Retina where grafics are non retina.
		scroll_area_.GetComponent<tk2dUIScrollableArea>().VisibleAreaLength = screen_width/Framework.Instance.graphics_manager().pixel_to_point_ratio();
		
		
		float item_width = 99F;
		
		float width = 0.0F;
		int i = 0;
		
		if (page_type == "coins")
		{
			List<JSONNode>  items = new List<JSONNode>();
			
			for (int j=0; j <store_info_.Count; j++)
			{
				if (store_info_[j]["category"].ToString().Contains("normal_coins") && store_info_[j]["key"].ToString() != "normal_coins_rate_me" ){
					items.Add(store_info_[j]);
				}
			}
				
			float left_border = 35;// item_width/2F-240;
			width = (screen_width - items.Count*item_width)/2F + item_width/2F;
			
			foreach( JSONNode item in items)
			{
				GameObject icon = (GameObject) Instantiate(
					icon_prefab_, 
					new Vector3(
						i *	105 + left_border, 
					    scrollview_.transform.position.y + 35,
					   	scrollview_.transform.position.z - 2),
					Quaternion.identity);
				icon.transform.parent = scrollview_.transform;
				icon.GetComponent<CoinStoreItemPrefab>().Init("coins", item["key"], item["name"], item["description"], item["quantity"].AsInt, item["cost"].AsFloat, item["cost"].ToString());
				
				width += item_width;
				
				i++;
			}
		}
		else if (page_type == "white_coins")
		{
			
			List<JSONNode>  items = new List<JSONNode>();
			
			for (int j=0; j <store_info_.Count; j++)
			{
				if (store_info_[j]["category"].ToString().Contains("white_milk_coins")){
					items.Add(store_info_[j]);
				}
			}
			
			float left_border = item_width/2F-240;
			width = (screen_width - items.Count*item_width)/2F + item_width/2F;
			
			foreach( JSONNode item in items)
			{
				GameObject icon = (GameObject) Instantiate(icon_prefab_, new Vector3(i*105+left_border, scrollview_.transform.position.y+35,scrollview_.transform.position.z-3),Quaternion.identity);
				icon.transform.parent = scrollview_.transform;
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR 				
				icon.GetComponent<CoinStoreItemPrefab>().Init("white_coins", item["key"], item["name"], item["description"],  item["quantity"].AsInt, item["cost"].AsFloat, GetUnibillerKeyForItem(item["key"].ToString()));
#else
				icon.GetComponent<CoinStoreItemPrefab>().Init("white_coins", item["key"], item["name"], item["description"],  item["quantity"].AsInt, item["cost"].AsFloat, item["cost"].ToString());
#endif				
				width += item_width;
				i++;
			}
		}	

		scroll_area_.GetComponent<tk2dUIScrollableArea>().ContentLength = width;
	}
	
	string GetUnibillerKeyForItem(string key)
	{
		string return_string = "";
		
		if (key == "white_milk_coins_pack01")	
		{
			return_string = Unibiller.GetPurchasableItemById("com.kongregate.mobile.endlessbossfight.t05_hard").localizedPriceString;
		}
		else if (key == "white_milk_coins_pack02")	
		{
			return_string = Unibiller.GetPurchasableItemById("com.kongregate.mobile.endlessbossfight.t10_hard").localizedPriceString;
		}	
		else if (key == "white_milk_coins_pack03")
		{
			return_string = Unibiller.GetPurchasableItemById("com.kongregate.mobile.endlessbossfight.t20_hard").localizedPriceString;
		}
		else if (key == "white_milk_coins_pack04")	
		{
			return_string = Unibiller.GetPurchasableItemById("com.kongregate.mobile.endlessbossfight.t40_hard").localizedPriceString;
		}	
		return	return_string;			
	}
	
	
	void OnDestroy()
	{
		scroll_area_.GetComponent<tk2dUIScrollableArea>().PreDestroy();
	}
}
