using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;


public class CoinsStoreScene : MonoBehaviour {
	
	
	public GameObject lobby_container_;
	public GameObject coins_container_;
	public GameObject white_coins_container_;
	
	private string current_tab_ = "lobby";
	private PlayerData player_data_;
	private JSONNode store_info_;
	
	public GameObject icon_prefab_;
	
	void Awake()
	{
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		Messenger.AddListener("RefreshCoins", RefreshCoins);
		RefreshCoins();
	}
	
	
	// Use this for initialization
	void Start ()
	{
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetDisabled();
		
		Framework.Instance.InstanceComponent<LoadingScreenBlock>().Hide();
		
		coins_container_.GetComponent<CoinsPage>().InitScrollView(coins_container_ , "coins");
		white_coins_container_.GetComponent<CoinsPage>().InitScrollView(white_coins_container_ ,"white_coins");
		
		ShowLobby();
		RefreshCoins();
		
#if UNITY_IPHONE
		Framework.Instance.InstanceComponent<InAppPurchasesUnibillManager>().RedeemUnconfirmedReceipt();
#endif		
			
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "CoinsStoreScene");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(coins_container_.activeSelf || white_coins_container_.activeSelf)
				ShowLobby();
			else
				CloseScene();
		}
	}
#endif
	
	void CoinsPressed()
	{
		lobby_container_.SetActive(false);
		coins_container_.SetActive(true);
		
		Framework.Instance.scene_manager().AssignsCameraToAnchors("CoinsStoreScene");
			
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "CoinsStoreScene");
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "sub_menu_name", "NormalCoinsPage");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void WhiteCoinsPressed()
	{		
		lobby_container_.SetActive(false);
		white_coins_container_.SetActive(true);
		
		Framework.Instance.scene_manager().AssignsCameraToAnchors("CoinsStoreScene");
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "CoinsStoreScene");
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "sub_menu_name", "WhiteCoinsPage");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void ShowLobby ()
	{
		lobby_container_.SetActive(true);
		white_coins_container_.SetActive(false);
		coins_container_.SetActive(false);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "CoinsStoreScene");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
	
	void CloseScene()
	{
		Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins(player_data_.GetInt("white_coins"), OnSyncWhiteCoins);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void OnSyncWhiteCoins(EbFightAPI.ResponseStatus status, EbFightAPI.JSONResult data)
	{
		if (status == EbFightAPI.ResponseStatus.Success)
		{
			long sync_white_coins = data.As<long>();
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", sync_white_coins);
		}
		else
		{
			Debug.Log("Error Connecting - OnSynchWhiteCoins");	
		}
			
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetEnabled();
		Framework.Instance.scene_manager().BackFromCurrentScene();
	}
	
	public void BackPressed()
	{
		ShowLobby();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	private void RefreshCoins()
	{
		Debug.Log("RefreshCoins");
		string coins_str = wmf.ui.Utility.FormattingWithZeros(player_data_.GetInt("coins").ToString(),8);
		GameObject.Find("Root/CoinsLabelContainer/CoinsTextMesh").GetComponent<tk2dTextMesh>().text = coins_str;
		GameObject.Find("Root/CoinsLabelContainer/CoinsTextMesh").GetComponent<tk2dTextMesh>().Commit(); 
		
		string white_coins_str = wmf.ui.Utility.FormattingWithZeros(player_data_.GetInt("white_coins").ToString(),4);
		GameObject.Find("WhiteCoinsLabelContainer/CoinsTextMesh").GetComponent<tk2dTextMesh>().text = white_coins_str;
		GameObject.Find("WhiteCoinsLabelContainer/CoinsTextMesh").GetComponent<tk2dTextMesh>().Commit(); 
	}
	
	
	void OnDestroy ()
	{
		Messenger.RemoveListener("RefreshCoins", RefreshCoins);
		//Framework.Instance.InstanceComponent<wmf.StaticParams>().Remove("current_scene");
		//Framework.Instance.InstanceComponent<wmf.StaticParams>().Remove("current_tab");
	}
}
