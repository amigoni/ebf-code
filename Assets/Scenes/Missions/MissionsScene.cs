using UnityEngine;
using System.Collections;

public class MissionsScene : MonoBehaviour {
	
	private MissionsManager mission_manager_;
	
	// Use this for initialization
	void Start () {
		Framework.Instance.InstanceComponent<LoadingScreenBlock>().Hide();
		mission_manager_ = GameObject.Find("MissionDisplayPrefab").GetComponent<MissionsManager>();
		MissionsManager.Mission mission_ = mission_manager_.current_mission();
		
		//Missions Count
		int current_mission_id_ = PlayerPrefs.GetInt ("current_mission_id");
		transform.Find("MissionCompletedContainer/MissionNumTextMesh").GetComponent<tk2dTextMesh>().text = current_mission_id_.ToString();
		transform.Find("MissionCompletedContainer/MissionNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		transform.Find("MissionCompletedContainer/MissionTotalNumTextMesh").GetComponent<tk2dTextMesh>().text = mission_manager_.mission_count().ToString();
		transform.Find("MissionCompletedContainer/MissionTotalNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
	
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "MissionsScene");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			BackPressed();
		}
	}
#endif
	
	//Button Callback
	void BackPressed()
	{
		Application.LoadLevel("MainMenu");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
}
