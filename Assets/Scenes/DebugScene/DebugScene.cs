using UnityEngine;
using System.Collections;

public class DebugScene : MonoBehaviour {
	
	private const float BUTTON_SIZE_X = 43;
	private const float BUTTON_SIZE_Y = 27;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		float camera_scale = Framework.Instance.graphics_manager().camera_scale();
		Vector2 pivotPoint = new Vector2(0.0f, 0.0f);
        GUIUtility.ScaleAroundPivot(new Vector2( camera_scale, camera_scale), pivotPoint);
		
		//bool custom = GUI.Toggle(new Rect(350, 0, 100, 30),
		//PlayerPrefs.GetInt("Custom_params") == 0 ? false : true, "Custom");
		//PlayerPrefs.SetInt("Custom_params", custom == false ? 0 : 1);
		
		GUI.Button(new Rect(220, 300, 120, 30), "Build Version " + Framework.Instance.InstanceComponent<PlayerData>().GetFloat("client_build_version").ToString());
		GUI.Button(new Rect(350, 300, 120, 30), "Data Version " + Framework.Instance.InstanceComponent<PlayerData>().GetFloat("client_data_version").ToString());
			
		//if(custom)
		{
			Store store = Framework.Instance.InstanceComponent<Store>();
			GUI.Button(new Rect(0, 0, 100, 30), "Level: " + PlayerPrefs.GetInt("player_level").ToString());
			GUI.Button(new Rect(120, 0, 100, 30), "Xp: " + PlayerPrefs.GetInt("player_xp").ToString());
			
			for(int i = 0; i < 11; i++)
			{
				if(GUI.Button(new Rect(BUTTON_SIZE_X * i, 30, BUTTON_SIZE_X, BUTTON_SIZE_Y), "level" + i))
				{
					PlayerPrefs.SetInt("player_level", i);
					Framework.Instance.InstanceComponent<Store>().UpdateDDLevel(i);
				}
			}
			
			GUI.Button(new Rect(0, 60, 100, 30), "Att: " + store.GetCurrentPropertyEffectValueByName("dd_att_level"));
			
			for(int i = 0; i < 11; i++)
			{
				if(GUI.Button(new Rect(BUTTON_SIZE_X * i, 90, BUTTON_SIZE_X, BUTTON_SIZE_Y), "att" + i))
				{
					Framework.Instance.InstanceComponent<Store>().DebugSetAttack(i * 2);
				}
			}
			
			GUI.Button(new Rect(0, 120, 100, 30), "Def: " + store.GetCurrentPropertyEffectValueByName("dd_def_level"));
			
			for(int i = 0; i < 11; i++)
			{
				if(GUI.Button(new Rect(BUTTON_SIZE_X * i, 150, BUTTON_SIZE_X, BUTTON_SIZE_Y), "def" + i))
				{
					Framework.Instance.InstanceComponent<Store>().DebugSetDefense(i * 2);
				}
			}
			/*
		    PlayerPrefs.SetString("Force Number Level",
				GUI.TextField (
					new Rect (220, 5, 30, 20), 
					PlayerPrefs.GetString("Force Number Level"), 25)
				);
			
			bool force_level = GUI.Toggle(new Rect(270, 5, 100, 30), 
				PlayerPrefs.GetInt("Force Level") == 0 ? false : true, 
				"Force Level");
			PlayerPrefs.SetInt("Force Level", force_level == false ? 0 : 1);
			*/
		}
		
		if(GUI.Button(new Rect(10, 300, 110, 30), "Back!"))
		{
			Application.LoadLevel("MainMenu");
		}
		
		int coins = Framework.Instance.InstanceComponent<PlayerData>().GetInt("coins");	
		string coins_str = GUI.TextField(new Rect (0, 200, 100, 20), coins.ToString(), 9);
		if(coins_str == "")
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("coins", 0);
		else
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("coins", int.Parse(coins_str));
		
		/*
		if(GUI.Button(new Rect(0, 300, 100, 30), "Reset!"))
		{
			Framework.Instance.InstanceComponent<Store>().Reset();
		}
		*/
	}
}
