using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using SimpleJSON;

public class DDDetail : MonoBehaviour
{
	
	public GameObject upgrade_item_;
	public GameObject buy_coins_prefab_;
	public GameObject square_icon_button_prefab_;
	public GameObject dd_detail_tutorial_;
	public GameObject tutorial_popup_;
	private GameObject ammo_row_;
	private GameObject att_row_;
	private GameObject def_row_;
	private PlayerData player_data_;
	private JSONNode dd_info_;
	private Store store_; //Dynamic Data for each item
	private JSONNode store_info_; //Static info for each item
	
	private string current_power_punch_key_;
	public List<GameObject> power_punch_icons_;
	public List<GameObject> stats_rows_;
	private GameObject character_stats_button_tab_container_;
	private GameObject character_stats_tab_;
	private GameObject power_punches_button_tab_container_;
	private GameObject power_punches_tab_;
	
	private GameObject power_punches_tab_container_;
	
	void Awake ()
	{
		Framework.Instance.InstanceComponent<LoadingScreenBlock> ().Hide ();
		player_data_ = Framework.Instance.InstanceComponent<PlayerData> ();
		dd_info_ = JSON.Parse (Framework.Instance.resouces_manager ().GetStringResource ("Config/DDInfo"));
		store_info_ = JSON.Parse (Framework.Instance.resouces_manager ().GetStringResource ("Config/StoreInfo"));
		store_ = Framework.Instance.InstanceComponent<Store> ();
		
		character_stats_button_tab_container_ = GameObject.Find ("CharacterStatsButtonContainer/CharacterStatsButtonTabContainer");
		character_stats_tab_ = GameObject.Find ("CharacterStatsButtonContainer/CharacterStatsTabContainer");
		power_punches_button_tab_container_ = GameObject.Find ("PowerPunchesButtonContainer/PowerPunchesButtonTabContainer");
		power_punches_tab_ = GameObject.Find ("PowerPunchesButtonContainer/PowerPunchesTabContainer");
		
		power_punches_tab_container_ = GameObject.Find ("MainCamera/PowerPunchesTabContainer");
	}
	
	// Use this for initialization
	void Start ()
	{		
		Framework.Instance.InstanceComponent<LoadingScreenBlock> ().Hide ();
		
		InitBuyCoinsButton (375, 290);

		GameObject.Find ("AmmoBuyButtonPrefab").GetComponent<AmmoBuyButtonPrefab> ().Refresh ();
		
		//Stats
		GameObject.Find ("LevelNumTextMesh").GetComponent<tk2dTextMesh> ().text = FormattingWithZeros ((player_data_.GetInt ("dd_level")).ToString (), 2);
		GameObject.Find ("LevelNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		GameObject.Find ("DRNumTextMesh").GetComponent<tk2dTextMesh> ().text = Framework.Instance.InstanceComponent<ServerAPI> ().GetPlayer ().PlayerRank.ToString ();
		GameObject.Find ("DRNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		if (player_data_.GetInt ("dd_level") <= 9) {
			GameObject.Find ("XPSlicedSprite").GetComponent<tk2dSlicedSprite> ().dimensions = new Vector2 ((player_data_.GetInt ("dd_xp") / dd_info_ [player_data_.GetInt ("dd_level")] ["xp_to_next"].AsFloat) * 193.0F, 10F);
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().text = player_data_.GetString ("dd_xp") + "/" + dd_info_ [player_data_.GetInt ("dd_level")] ["xp_to_next"];
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
			GameObject.Find ("HPNumTextMesh").GetComponent<tk2dTextMesh> ().text = dd_info_ [player_data_.GetInt ("dd_level")] ["hp_max"].AsInt.ToString ();
			GameObject.Find ("HPNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		} else {
			GameObject.Find ("XPSlicedSprite").GetComponent<tk2dSlicedSprite> ().dimensions = new Vector2 (193.0F, 10F);
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().text = "Max";
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		}
		
		
		//Upgradable Rows
		int y_base = -20;
		int offset = 40;
		ammo_row_ = CreateStatsRow ("dd_ammo_level", y_base);
		att_row_ = CreateStatsRow ("dd_att_level", y_base - offset);
		def_row_ = CreateStatsRow ("dd_def_level", y_base - offset * 2);
		
		InitPowerPunchesTab ();
		Messenger.AddListener<string,bool> ("SquareItemSelected", SelectPowerPunch);
		//Messenger.AddListener<string>("UpgradeSuccessful",UpgradeTutorialForward);
		DisplayThisTab ("CharacterStatsTabContainer");	
	}

#if UNITY_ANDROID
	void Update()
	{
		if(Game.FindObjectOfType( typeof(TutorialPopup) ) == null &&
		   Game.FindObjectOfType( typeof(CoinsStoreScene) ) == null)
			if (Input.GetKeyDown(KeyCode.Escape))
				BackPressed();
	}
#endif
	
	void ResetButton ()
	{
		character_stats_button_tab_container_.SetActive (true);
		character_stats_tab_.SetActive (false);
		power_punches_button_tab_container_.SetActive (true);
		power_punches_tab_.SetActive (false);
	}
	
	//Tutorial	
	void StatsQuestionMarkPressed ()
	{
		GameObject tutorial_popup = (GameObject)Instantiate (tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup> ().Init (TutorialPopUpType.dd_stats_, 2F);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void PowerPunchQuestionMarkPressed ()
	{
		GameObject tutorial_popup = (GameObject)Instantiate (tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup> ().Init (TutorialPopUpType.dd_power_punches_, 2F);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	GameObject CreateStatsRow (string stat_name, float y)
	{
		GameObject upgrades_container = GameObject.Find ("UpgradesContainer");
		GameObject stat_row = (GameObject)Instantiate (
			upgrade_item_, 
			new Vector3 (0, 0, GameObject.Find ("CharacterStatsTabContainer").transform.position.z), 
			Quaternion.identity);
		stat_row.GetComponent<tk2dCameraAnchor> ().tk2dCamera = GameObject.Find ("MainCamera").GetComponent<tk2dCamera> ();
		stat_row.GetComponent<tk2dCameraAnchor> ().offset = new Vector2 (45, y);
		stat_row.GetComponent<UpgradeStatsItemPrefab> ().Init (stat_name, true, true);
		stat_row.transform.parent = GameObject.Find ("MainCamera/CharacterStatsTabContainer").transform;
		stats_rows_ .Add (stat_row);
		
		return stat_row;
	}
	
	public void RefreshStats ()
	{
		foreach (GameObject stat_row in stats_rows_) {
			stat_row.GetComponent<UpgradeStatsItemPrefab> ().RefreshDD ();
		}
	}
	
	void InitBuyCoinsButton (float x, float y)
	{
		//if(tutorial_step_ >= 6)
		{	
			GameObject button = (GameObject)Instantiate (buy_coins_prefab_, new Vector3 (x, y, 25), Quaternion.identity);
			button.transform.parent = GameObject.Find ("MainCamera").transform;
			button.GetComponent<BuyCoinsPrefab> ().Init (false, 124F, -32F);
		}	
	}
	
	void BackPressed ()
	{
		//if(tutorial_step_ >= 6)
		{
			Application.LoadLevel ("MainMenu");
			Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
		}
	}
	
	void CharacterStatsTabPressed ()
	{
		DisplayThisTab ("CharacterStatsTabContainer");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void PowerPunchesTabPressed ()
	{
		DisplayThisTab ("PowerPunchesTabContainer");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	public void DisplayTab (string tab, bool setting)
	{
		int zPos = 20;
		if (setting == false)
			zPos = -100;
		GameObject.Find (tab).transform.position = new Vector3 (GameObject.Find (tab).transform.position.x, GameObject.Find (tab).transform.position.y, zPos);
	}
	
	private void DisplayThisTab (string tab)
	{
		if (tab == "CharacterStatsTabContainer") {
			ResetButton ();
			character_stats_button_tab_container_.SetActive (false);
			character_stats_tab_.SetActive (true);
			
			DisplayTab ("CharacterStatsTabContainer", true);
			DisplayTab ("PowerPunchesTabContainer", false);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
			analitycs_.AddEvent (KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "DDDetailScene");
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "DDStatsTab");
			analitycs_.SendEvent (KeenIOEventType.menu_opened);
		} else if (tab == "PowerPunchesTabContainer") {
			ResetButton ();
			power_punches_button_tab_container_.SetActive (false);
			power_punches_tab_.SetActive (true);
			
			DisplayTab ("CharacterStatsTabContainer", false);
			DisplayTab ("PowerPunchesTabContainer", true);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
			analitycs_.AddEvent (KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "DDDetailScene");
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "DDPowerPunchesTab");
			analitycs_.SendEvent (KeenIOEventType.menu_opened);
		}
	}
	
	void InitPowerPunchesTab ()
	{
		CreateSquareIcon ("huge_punch", -80, true);
		CreateSquareIcon ("aaatatata", 0, false);
		CreateSquareIcon ("great_ball_of_fire", 80, false);
		
		SetPowerPunchesTabPowerPunch ("huge_punch", false);
	}
	
	void CreateSquareIcon (string item_key, int x, bool selected)
	{
		GameObject icon = (GameObject)Instantiate (square_icon_button_prefab_, new Vector3 (x, 190, -100), Quaternion.identity);
		icon.GetComponent<tk2dCameraAnchor> ().enabled = true;
		icon.GetComponent<tk2dCameraAnchor> ().tk2dCamera = GameObject.Find ("MainCamera").GetComponent<tk2dCamera> ();
		icon.GetComponent<tk2dCameraAnchor> ().offset = new Vector2 (x, 30);
		
		icon.transform.parent = power_punches_tab_container_.transform;
		icon.GetComponent<SquareItemButtonPrefab> ().Init ("power_punch", item_key, store_.IsItemLocked (item_key), selected);	
		power_punch_icons_.Add (icon);
	}
	
	void SetPowerPunchesTabPowerPunch (string item_key, bool is_locked)
	{
		//if (tutorial_step_ <=9)
		{
			current_power_punch_key_ = item_key;
		
			//Title
			if (item_key == "huge_punch") {
				power_punches_tab_container_.transform.Find ("GreatBallOfFireSprite").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("AaatatataSprite").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("HugePunchSprite").gameObject.SetActive(true);
			} else if (item_key == "aaatatata") {
				power_punches_tab_container_.transform.Find ("GreatBallOfFireSprite").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("AaatatataSprite").gameObject.SetActive(true);
				power_punches_tab_container_.transform.Find ("HugePunchSprite").gameObject.SetActive(false);
			} else if (item_key == "great_ball_of_fire") {
				power_punches_tab_container_.transform.Find ("GreatBallOfFireSprite").gameObject.SetActive(true);
				power_punches_tab_container_.transform.Find ("AaatatataSprite").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("HugePunchSprite").gameObject.SetActive(false);
			}	
			
			
			//Description
			power_punches_tab_container_.transform.Find ("DescriptionTextMesh").GetComponent<tk2dTextMesh> ().text = store_info_ [item_key] ["description"];
			power_punches_tab_container_.transform.Find ("DescriptionTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
			
			
			//Upgrade display
			int upgrade_level = player_data_.GetInt (item_key);
			
			if (upgrade_level < 5) {
				power_punches_tab_container_.transform.Find ("MaximumSprite").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("CoinsTextMesh").gameObject.SetActive(true);
				power_punches_tab_container_.transform.Find ("WhiteCoinsSprite").gameObject.SetActive(true);
				
				if (is_locked == false) {
					power_punches_tab_container_.transform.Find ("UpgradePowerPunchesButtonSprite").gameObject.SetActive(true);
					//power_punches_tab_container_.transform.Find ("UpgradePowerPunchesButtonSprite").GetComponent<tk2dButton> ().enabled = true;
					power_punches_tab_container_.transform.Find ("LockedButtonSprite").renderer.enabled = false;
				} else {
					power_punches_tab_container_.transform.Find ("UpgradePowerPunchesButtonSprite").renderer.enabled = false;
					//power_punches_tab_container_.transform.Find ("UpgradePowerPunchesButtonSprite").GetComponent<tk2dButton> ().enabled = false;
					power_punches_tab_container_.transform.Find ("LockedButtonSprite").renderer.enabled = true;
				}	
			} else {
				power_punches_tab_container_.transform.Find ("MaximumSprite").gameObject.SetActive(true);
				power_punches_tab_container_.transform.Find ("CoinsTextMesh").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("WhiteCoinsSprite").gameObject.SetActive(false);
				power_punches_tab_container_.transform.Find ("UpgradePowerPunchesButtonSprite").renderer.enabled = false;
				//power_punches_tab_container_.transform.Find ("UpgradePowerPunchesButtonSprite").GetComponent<tk2dButton> ().enabled = false;
				power_punches_tab_container_.transform.Find ("LockedButtonSprite").renderer.enabled = false;
			}
			
			
			//Tacche
			for (int i =0; i<5; i++) {
				bool is_visible = false;
				
				if (i < upgrade_level)
					is_visible = true;
				power_punches_tab_container_.transform.Find ("UpgradeTacca" + (i + 1)).GetComponent<MeshRenderer> ().enabled = is_visible;
			}
			
			
			//Cost Display
			string cost_text = "";
				
			if (is_locked == true)
				cost_text = "-----";
			else
				cost_text = FormattingWithZeros (store_info_ [item_key] ["upgrades"] [upgrade_level] ["next_cost"].ToString (), 5);
			
			power_punches_tab_container_.transform.Find ("CoinsTextMesh").GetComponent<tk2dTextMesh> ().text = cost_text;
			power_punches_tab_container_.transform.Find ("CoinsTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		}
	}
	
	void BuyPowerPunchUpgrade ()
	{
		store_.BuyItemAsynch (current_power_punch_key_, 1, OnSuccessfullPowerPunchBuy, OnFailedPowerPunchBuy);
	}
	
	public void OnSuccessfullPowerPunchBuy ()
	{
		RefreshPowerPunchesTab ();
		RefreshStats ();	
		Framework.Instance.audio_manager ().PlayUISound ("40_Menu Buy Upgrade", 1.0F, 1.0F, false);

	}
	
	public void OnFailedPowerPunchBuy ()
	{
		Framework.Instance.InstanceComponent<ServerAPI> ().pop_up_window ().Init ("Can't Connect", "Please try again in a bit");
		Framework.Instance.InstanceComponent<ServerAPI> ().pop_up_window ().set_destroy_on_closing (false);
	}
	
	public void RefreshPowerPunchesTab ()
	{
		SetPowerPunchesTabPowerPunch (current_power_punch_key_, false);
	}
	
	public void SelectPowerPunch (string key, bool is_locked)
	{
		foreach (GameObject item in power_punch_icons_) {
			item.GetComponent<SquareItemButtonPrefab> ().Unselect ();
		}
		
		Vector3 position = power_punches_tab_container_.transform.Find ("ArrowSprite").transform.position;
		
		float arrow_x_position = -81F;
		if (key == "huge_punch") {
			//power_punches_tab_container_.transform.Find("ArrowSprite").transform.position = new Vector3(160, position.y, position.z);
			arrow_x_position = -81F;
		} else if (key == "aaatatata") {
			//power_punches_tab_container_.transform.Find("ArrowSprite").transform.position = new Vector3(240, position.y, position.z);
			arrow_x_position = 0F;
		} else if (key == "great_ball_of_fire") {
			//power_punches_tab_container_.transform.Find("ArrowSprite").transform.position = new Vector3(320, position.y, position.z);
			arrow_x_position = 81F;
		}
		power_punches_tab_container_.transform.Find ("ArrowSprite").gameObject.GetComponent<tk2dCameraAnchor> ().offset.x = arrow_x_position;
		
		SetPowerPunchesTabPowerPunch (key, is_locked);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	string FormattingWithZeros (string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert (0, "0");
		
		return txt;
	}
	
	void OnDestroy ()
	{
		Messenger.RemoveListener<string,bool> ("SquareItemSelected", SelectPowerPunch);
		//Messenger.RemoveListener<string>("UpgradeSuccessful",UpgradeTutorialForward);
	}
}
