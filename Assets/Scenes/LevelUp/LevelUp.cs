using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using EbFightAPI;

public class LevelUp : MonoBehaviour {
	
	public GameObject upgrade_item_;
	public GameObject item_unlocked_container_;
	private GameObject continue_button_;
	public GameObject loading_page_prefab_;
	public GameObject dd_level_up_sprite_container_;
	public GameObject boss_level_up_sprite_container_;
	public GameObject tutorial_prefab_;
	
	public GameObject sliced_sprite_;
	public GameObject xp_text_mesh_;
	
	private bool continue_is_displayed_ = true;
	private PlayerData player_data_;
	private EbFightAPI.Boss boss_data_; 
	private JSONNode dd_info_;
	private Store store_; //Dynamic Data for each item
	private JSONNode store_info_; //Static info for each item
	
	int boss_level_ = -1;
	bool is_dd_ = true;
	
	float xp_ = 0;
	float xp_to_next_ = 0;
	bool xp_animation_enabled_ = false;
	
	private List<JSONNode> unlocked_items_data_ = new List<JSONNode>();
	private JSONNode current_unlocked_item_;
	int current_unlocked_item_place_ = 0;
	
	
	void Start()
	{
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		dd_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/DDInfo"));
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		store_ = Framework.Instance.InstanceComponent<Store>();
		
		Messenger.AddListener("ItemUnlockedClosed", CloseUnlockedItemPopUp);

		Framework.Instance.audio_manager().Play("41_Level Up", transform, 1.0F, 1.0F, false);
		
		if ((bool)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get("IsLevelUpDD") == true)
		{
			Init (true, player_data_.GetInt("dd_level"), player_data_.GetInt("dd_xp"));
			//Analitycs
			SendAnalitycs(player_data_.GetInt("dd_level"), false);
		}
		else
			Framework.Instance.InstanceComponent<ServerAPI>().GetBossConfiguration(OnGetBossConfiguration);
		
		Framework.Instance.InstanceComponent<wmf.StaticParams> ().Remove("IsLevelUpDD");
		
		
		//Tutorial
		/*
		if (player_data_.GetInt("tutorial_current_step") == 0)
		{
			tutorial_prefab_.SetActive(true);
			tutorial_prefab_.animation.Play();	
		}	
		*/
	}
	
	float time_delay_ = 0.5F;
	float time_counter_ = 0F;
	
	void Update()
	{
		if (xp_animation_enabled_ == true)
		{
			if (time_counter_ > time_delay_)
			{
				float delta_x = Mathf.Ceil((time_counter_-time_delay_)*(xp_to_next_ - xp_)/60);
				//Debug.Log(delta_x);
				if ( delta_x/xp_to_next_ <= xp_/xp_to_next_)	
					sliced_sprite_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(delta_x/xp_to_next_*192,10);
				else
					delta_x = xp_;
				
				xp_text_mesh_.GetComponent<tk2dTextMesh>().text = delta_x+"/"+xp_to_next_;
				xp_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			}
			
			time_counter_ += 1.0F;
		}
	}
	
	
	private void OnGetBossConfiguration(ResponseStatus status, JSONResult data)
	{
		boss_data_ = data.As<EbFightAPI.Boss>();
		
		GameObject loading_page_prefab = (GameObject) Instantiate(loading_page_prefab_, new Vector3(0,0,1),Quaternion.identity);
		float boss_min_xp = store_info_["boss_level_upgrade"]["upgrades"][(int) boss_data_.Level]["xp_min"].AsInt -1;
		
		Init(false, (int) boss_data_.Level, (int) boss_data_.Xp);
		//Init(false, boss_level_, (int) boss_data_.Xp);
		Destroy(loading_page_prefab);
		//Analitycs
		SendAnalitycs((int) boss_data_.Level, true);
	}
	
	
	
	// Use this for initialization
	void Init (bool is_dd, int new_level, int new_xp) 
	{
		is_dd_ = is_dd;
		
		int prev_level = 0;
		if (new_level > 0)
			prev_level = new_level-1;
		float xp = 0;
		float xp_to_next = 0;
		float hp_max = 0;
		float prev_level_hp_max = 0;
		
		if (is_dd == true)
		{	
			xp = new_xp;
			xp_to_next = dd_info_[new_level]["xp_to_next"].AsFloat;
			hp_max = dd_info_[new_level]["hp_max"].AsFloat;
			prev_level_hp_max =  dd_info_[prev_level]["hp_max"].AsFloat;
			dd_level_up_sprite_container_.SetActive(true);
#if UNITY_IPHONE && !UNITY_EDITOR		
		if ( new_level == 2)
			Framework.Instance.InstanceComponent<GameCenterAPI>().AddAchievementProgress("3",100F);
#endif			
		}
		else
		{
			boss_level_ = new_level;
			if (boss_level_ > 0)
				prev_level = boss_level_-1;
			xp = new_xp - (store_info_["boss_level_upgrade"]["upgrades"][boss_level_]["xp_min"].AsInt-1);	
			xp_to_next = store_info_["boss_level_upgrade"]["upgrades"][boss_level_]["xp_max"].AsInt - (store_info_["boss_level_upgrade"]["upgrades"][boss_level_]["xp_min"].AsInt-1)+1;
			hp_max = store_info_["boss_level_upgrade"]["upgrades"][boss_level_]["effect"].AsFloat;
			prev_level_hp_max = store_info_["boss_level_upgrade"]["upgrades"][prev_level]["effect"].AsFloat;	
			boss_level_up_sprite_container_.SetActive(true);
#if UNITY_IPHONE && !UNITY_EDITOR		
		if ( new_level == 2)
			Framework.Instance.InstanceComponent<GameCenterAPI>().AddAchievementProgress("2",100F);
#endif	
		}
		
		xp_ = xp;
		xp_to_next_= xp_to_next;
		
		float difference_hp = hp_max - prev_level_hp_max;
		
		//Get list of unlocked items
		if (is_dd == true)
		{
			for ( int i = 0; i < store_info_.Count; i++)
			{
				if (store_info_[i]["unlocked_at_player_level"].AsInt == new_level && 
					store_info_[i].ToString().Contains("boss") == false && 
					(store_info_[i]["category"].ToString() == "utility" || store_info_[i]["category"].ToString() == "power_punch" ))	
						unlocked_items_data_.Add(store_info_[i]);
			}
		}
		else if (is_dd == false)
			AddBossUnlockedWeapons();
		
		
		if (unlocked_items_data_.Count > 0)
			current_unlocked_item_ = unlocked_items_data_[0];
		
		
		continue_button_ = GameObject.Find("ContinueButtonSprite");
		ToggleContinueButton();
		
		
		GameObject.Find("LevelNumTextMesh").GetComponent<tk2dTextMesh>().text = FormattingWithZeros((new_level).ToString(),2);
		GameObject.Find("LevelNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		GameObject.Find("HPNumTextMesh").GetComponent<tk2dTextMesh>().text = hp_max.ToString();
		GameObject.Find("HPNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		GameObject.Find("HpNumPinkTextMesh").GetComponent<tk2dTextMesh>().text = "+ "+difference_hp.ToString();
		GameObject.Find("HpNumPinkTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		//Upgradable Rows
		if (is_dd_ == true)
		{
			GameObject row1 =  CreateStatsRow("dd_ammo_level", 40);
			row1.GetComponent<UpgradeStatsItemPrefab>().PlayAnimation(1.5F,2);
			GameObject row2 = CreateStatsRow("dd_att_level",0);
			row2.GetComponent<UpgradeStatsItemPrefab>().PlayAnimation(2.0F,3);
			GameObject row3 = CreateStatsRow("dd_def_level", -40);
			row3.GetComponent<UpgradeStatsItemPrefab>().PlayAnimation(2.5F,4);
		}
		else
		{
			GameObject row1 = CreateStatsRow("boss_power_level", 40);
			row1.GetComponent<UpgradeStatsItemPrefab>().PlayAnimation(1.5F,2);
			GameObject row2 = CreateStatsRow("boss_def",0);
			row2.GetComponent<UpgradeStatsItemPrefab>().PlayAnimation(2.0F,3);
			GameObject row3 = CreateStatsRow("boss_att", -40);
			row3.GetComponent<UpgradeStatsItemPrefab>().PlayAnimation(2.5F,4);
		}
		
		
		//Unlocked Items
		if (unlocked_items_data_.Count > 0)
			Invoke("OpenUnlockedItemPopUp", 3.3F);
		else
			Invoke("ToggleContinueButton", 3.3F);
		
		xp_animation_enabled_ = true;
	}
	
	
	private GameObject CreateStatsRow(string stat_name, float y)
	{
		GameObject stat_row = (GameObject) Instantiate(
			upgrade_item_, 
			new Vector3(0,0,GameObject.Find("MainCamera").transform.position.z+10), 
			Quaternion.identity);
		stat_row.GetComponent<tk2dCameraAnchor>().tk2dCamera = GameObject.Find("MainCamera").GetComponent<tk2dCamera>();
		stat_row.GetComponent<tk2dCameraAnchor>().offset = new Vector2(73,y);
		
		if(is_dd_ == true)
			stat_row.GetComponent<UpgradeStatsItemPrefab>().Init(stat_name, false, is_dd_);
		else
			stat_row.GetComponent<UpgradeStatsItemPrefab>().Init(stat_name, false, is_dd_, boss_data_);
		
		stat_row.transform.parent = GameObject.Find("MainCamera").transform;
		
		return stat_row;
	}
	
	
	void ContinuePressed(){
		if (is_dd_ == true)
			Application.LoadLevel("EndGame");		
		else	
			Application.LoadLevel("BossDetail");
		
		Framework.Instance.audio_manager().Play("37_Menu Button Select", transform, 1.0F, 1.0F, false);
	}
	
	
	void ToggleContinueButton()
	{
		if (continue_is_displayed_ == true)
			continue_button_.SetActive(false);
		else
			continue_button_.SetActive(true);
		
		continue_is_displayed_ = !continue_is_displayed_;
		PlayContinueSound();
	}
	
	
	void OpenUnlockedItemPopUp()
	{
		item_unlocked_container_.GetComponent<ItemUnlocked>().Init(current_unlocked_item_);
	}
	
	
	void CloseUnlockedItemPopUp()
	{
		current_unlocked_item_place_++;
		
		
		if (current_unlocked_item_place_ < unlocked_items_data_.Count)
		{
			current_unlocked_item_ = unlocked_items_data_[current_unlocked_item_place_];
			OpenUnlockedItemPopUp();
		}		
		else
			ToggleContinueButton();
	}
	
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");
		
		return txt;
	}
	
	
	void AddBossUnlockedWeapons()
	{
		AddUnlockedWeapon("bouncing");
		AddUnlockedWeapon("tap");
		AddUnlockedWeapon("avoid");
		AddUnlockedWeapon("fast");
		AddUnlockedWeapon("rocket");
		AddUnlockedWeapon("chasing");
		AddUnlockedWeapon("laser");
		AddUnlockedWeapon("fast_rocket");
	}
	
	
	void AddUnlockedWeapon(string key)
	{
		for (int i =0; i < 3; i++)
		{
			int level = GetWeaponUnlockedLevel(key, i);
			if ( level == boss_level_)
			{
				JSONNode item_data = FindWeaponsFromWeaponKey(key);
				item_data["difficulty"].Value = i.ToString();
				//Porcata to find if it's a real item or empty
				if(item_data.ToString().Length > 2)
					unlocked_items_data_.Add(item_data);
			}		
		}
	}
	
	
	JSONNode FindWeaponsFromWeaponKey(string key)
	{
		JSONNode return_node = new JSONNode();
		
		for (int i =0; i < store_info_.Count; i++)
		{
			if (store_info_[i]["weapon_key"].ToString() == key)
			{
				return_node = store_info_[i];
			}
				 
		}
		return return_node;
	}				
	
	
	private int GetWeaponUnlockedLevel(string key, int difficulty)
	{
		int level = 99;
		
		JSONNode weapons_data = store_info_["boss_round_weapons"]["weapons"];
		
		for (int i = 0; i < weapons_data.Count; i++){
			
			//PORCATA MA NON SO COSA ALTRO FARE
			if (weapons_data[i][key].ToString().Length > 2){
				for (int j = 0; j < 3; j++){
				 	if(weapons_data[i][key]["difficulty_"+j]["difficulty"].AsInt == difficulty){
						level = i;
						break;
					}
				}
			}
			
			if (level != 99)
				break;
		}
		
		return level;
	}
	
	
	public void PlayContinueSound()
	{
		Framework.Instance.audio_manager().Play("16_Combo Hit MIssile 5v", transform, 1.0F, 1.0F, false);
	}
	
	public void PlayHPSlotContainer()
	{
		Framework.Instance.audio_manager().Play("16_Combo Hit MIssile 1v", transform, 1.0F, 1.0F, false);
	}
	
	
	void OnDestroy ()
	{
		Messenger.RemoveListener("ItemUnlockedClosed", CloseUnlockedItemPopUp);
	}
	
	//Analitycs
	private void SendAnalitycs(int level, bool is_boss)
	{
		//Analitycs
		Framework.Instance.InstanceComponent<Analitycs>().AddEvent(KeenIOEventType.player_level_up);
		Framework.Instance.InstanceComponent<Analitycs>().AddNewKey(KeenIOEventType.player_level_up, "new_level", level);
		Framework.Instance.InstanceComponent<Analitycs>().AddNewKey(KeenIOEventType.player_level_up, "is_boss", is_boss);
		Framework.Instance.InstanceComponent<Analitycs>().SendEvent(KeenIOEventType.player_level_up);
	}
}