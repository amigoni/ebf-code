using UnityEngine;
using System.Collections;
using SimpleJSON;

public class ItemUnlocked : MonoBehaviour {
	
	public GameObject icon_sprite_;
	public GameObject title_text_mesh_;
	public GameObject description_text_mesh_;
	public GameObject icon_easy_sprite_;
	public GameObject icon_medium_sprite_;
	public GameObject icon_hard_sprite_;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	
	public void Init (JSONNode data)
	{
		//Debug.Log(data);
		
		string sprite_name = "";
		if(data["key"].ToString() == "aaatatata")
			sprite_name = "IconPP02_Aaatatata";
		else if(data["key"].ToString() == "great_ball_of_fire")
			sprite_name = "IconPP03_GreatBallOfFire";
		else if(data["key"].ToString() == "double_bubble")
			sprite_name = "IconUtiDD01_DoubleBubble";
		else if(data["key"].ToString() == "double_Magnet")
			sprite_name = "IconUtiDD02_DoubleMagnet";
		else if(data["key"].ToString() == "life_boost")
			sprite_name = "IconUtiDD03_LifeBoost";
		else if(data["key"].ToString() == "att_boost")
			sprite_name = "IconUtiDD04_AttBoost";
		else if(data["key"].ToString() == "ammo_boost")
			sprite_name = "IconUtiDD05_AmmoBoost";
		else if(data["key"].ToString() == "boss_weapons_bouncing")
			sprite_name = "IconWeap01_Bouncing";
		else if(data["key"].ToString() == "boss_weapons_tap")
			sprite_name = "IconWeap02_Tap";
		else if(data["key"].ToString() == "boss_weapons_avoid")
			sprite_name = "IconWeap03_Avoid";
		else if(data["key"].ToString() == "boss_weapons_fast")
			sprite_name = "IconWeap04_FastTap";
		else if(data["key"].ToString() == "boss_weapons_rocket")
			sprite_name = "IconWeap06_Rocket";
		else if(data["key"].ToString() == "boss_weapons_chasing")
			sprite_name = "IconWeap07_Chasing";
		else if(data["key"].ToString() == "boss_weapons_laser")
			sprite_name = "IconWeap05_laser";
		else if(data["key"].ToString() == "boss_weapons_fast_rocket")
			sprite_name = "IconWeap08_FastRocket";
		else if(data["key"].ToString() == "boss_weapons_sin")
			sprite_name = "IconWeap09_Sin";
		
		if(data["difficulty"].AsInt == 0)
			icon_easy_sprite_.SetActive(true);
		else if (data["difficulty"].AsInt == 1)
			icon_medium_sprite_.SetActive(true);
		else if (data["difficulty"].AsInt == 2)
			icon_hard_sprite_.SetActive(true);
			
		
		icon_sprite_.GetComponent<tk2dSprite>().spriteId = icon_sprite_.GetComponent<tk2dSprite>().GetSpriteIdByName(sprite_name);
		
		title_text_mesh_.GetComponent<tk2dTextMesh>().text = data["name"].ToString();
		title_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		description_text_mesh_.GetComponent<tk2dTextMesh>().text = data["description"].ToString();
		description_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		gameObject.SetActive(true);
		
		Framework.Instance.audio_manager().Play("18_Get Crate", transform, 1.0F, 1.0F, false);
	}
	
	
	
		
	
	
	void ClosePressed ()
	{
		gameObject.SetActive(false);
		Messenger.Broadcast("ItemUnlockedClosed");
	}
}
