using UnityEngine;
using System.Collections;
using EbFightAPI;
using System.Text.RegularExpressions;

public class InsertName : MonoBehaviour {
	
	//TO-DO ask manu to UI max name size
	public const int MIN_NAME_LENGTH = 3;
	public const int MAX_NAME_LENGTH = 12;
	
	public GameObject name_input_obj_;
	public GameObject next_button_;
	public GameObject disabled_button_;
	
	// Use this for initialization
	void Start () {
		name_input_obj_.GetComponent<tk2dUITextInput>().maxCharacterLength = MAX_NAME_LENGTH;
		GameObject.Find("MainCamera/NameInput").GetComponent<tk2dUITextInput>().SetFocus();
		tk2dUITextInput text_input = name_input_obj_.GetComponent<tk2dUITextInput>();
		text_input.OnTextChange = OnTextChange;
	}
	
	
	void Update (){
		if(name_input_obj_.GetComponent<tk2dUITextInput>().Text.Length < MIN_NAME_LENGTH)
		{
			next_button_.SetActive(false);
			disabled_button_.SetActive(true);
		}
		else
		{
			next_button_.SetActive(true);
			disabled_button_.SetActive(false);
		}		
	}
	
	public void OnTextChange(tk2dUITextInput text)
	{
		Regex rgx = new Regex("[^a-zA-Z0-9]");
		if(rgx.IsMatch(text.Text))
		{
			text.Text = text.Text.Remove(text.Text.Length - 1);
		}
		if(text.Text.Length > MAX_NAME_LENGTH)
		{
			text.Text = text.Text.Remove(text.Text.Length - 1);
		}
		
		//Debug.Log( text.Text.Length );
	}
	
	public void NextPressed ()
	{
		string user_name = name_input_obj_.GetComponent<tk2dUITextInput>().Text;
		Framework.Instance.InstanceComponent<ServerAPI>().RegisterPlayer(user_name, "mobile", OnRegisterPlayer);
	}
	
	void OnRegisterPlayer(ResponseStatus status, JSONResult data)
	{
		//Debug.Log("OnRegisterPlayer: " + data.Data);
		if(status == ResponseStatus.Success)
		{
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(player);
			long white_coins = player.WhiteCoins;
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", white_coins);
					
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_id", player.UserId);
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_name", player.Username);
			
			Framework.Instance.InstanceComponent<Analitycs>().SendGamePlaysEvent();
			Application.LoadLevel("MainMenu");
		} 
		else 
		{
			if(data.ErrorMessage == "Username is not available")
				Framework.Instance.InstanceComponent<ServerAPI>().NotifyError("Username not available", "Please choose a different username");
			else
				Framework.Instance.scene_manager().LoadScene("NeedConnectionScene", TransitionType.add_scene);
		}
	}
}
