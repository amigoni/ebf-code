using UnityEngine;
using System.Collections;
using SimpleJSON;

public class SettingsScene : MonoBehaviour {
	
	public bool is_settings_scene = true;
	
	public GameObject background_black_sliced_;
	public GameObject background_divisory_sliced_;
	
	public GameObject background_popup_sliced_sprite_;
	public GameObject background_popup_sliced_sprite_web_;
	public GameObject x_close_button_;
	public GameObject x_close_button_web_;
	
	public GameObject reset_popup_prefab_;
	
	public GameObject dpad_sprite_;
	public GameObject vpad_sprite_;
	public GameObject swipe_motion_sprite_;
	
	public GameObject sfx_on_sprite_;
	public GameObject sfx_off_sprite_;
	public GameObject sfx_on_sprite_web_;
	public GameObject sfx_off_sprite_web_;
	
	public GameObject music_on_sprite_;
	public GameObject music_off_sprite_;
	public GameObject music_on_sprite_web_;
	public GameObject music_off_sprite_web_;
	
	public GameObject sfx_button_web_container_;
	public GameObject music_button_web_container_;
	
	bool is_non_touch_layout = false;
	
	private PlayerData player_data_;
	private Options options_; 
	private OptionsParams options_params_;
	
	private bool click_enabled_ = true;
	
	void Awake()
	{
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		options_ = Framework.Instance.InstanceComponent<Options> ();
		options_params_ = options_.options_params();
	}
	
	// Use this for initialization
	void Start () 
	{
		
		sfx_on_sprite_.SetActive(false);
		sfx_off_sprite_.SetActive(false);
		music_on_sprite_.SetActive(false);
		music_off_sprite_.SetActive(false);
		sfx_on_sprite_web_.SetActive(false);
		sfx_off_sprite_web_.SetActive(false);
		music_on_sprite_web_ .SetActive(false);
		music_off_sprite_web_.SetActive(false);
		
		if (is_settings_scene == false)
		{
			background_popup_sliced_sprite_.SetActive(true);
			background_popup_sliced_sprite_web_.SetActive(false);
			x_close_button_.SetActive(true);
			x_close_button_web_.SetActive(false);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			analitycs_.AddEvent(KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "GameScene");
			analitycs_.AddNewKey(KeenIOEventType.menu_opened, "sub_menu_name", "PauseSettings");
			analitycs_.SendEvent(KeenIOEventType.menu_opened);
		}
		else
		{
			string text = GameObject.Find("VersionNumberTextMesh").GetComponent<tk2dTextMesh>().text;
			GameObject.Find("VersionNumberTextMesh").GetComponent<tk2dTextMesh>().text = text.Replace("%d", "050214X1236W");
			GameObject.Find("VersionNumberTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			analitycs_.AddEvent(KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "SettingsScene");
			analitycs_.SendEvent(KeenIOEventType.menu_opened);
		}
		
#if UNITY_WEBPLAYER
		is_non_touch_layout = true;
		if (is_settings_scene == true)
			NonTouchLayout(0F);
		else
			NonTouchLayout(0F);
#endif
		
		SFXRefresh();
		MusicRefresh();
		ControllerSelected(options_.options_params().input_controller_type);
		
#if! UNITY_EDITOR
		GameObject.Find("ResetGameButtonContainer").SetActive(false);
#endif
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			if (is_settings_scene)
				MenuPressed();
			else
				CloseSettingsPressed();
		
	}
#endif
	
	void NonTouchLayout(float y_position)
	{
		
		sfx_button_web_container_.GetComponent<tk2dCameraAnchor>().offset = new Vector2(-80,y_position);
		music_button_web_container_.GetComponent<tk2dCameraAnchor>().offset = new Vector2(80,y_position);
		
		dpad_sprite_.transform.parent.gameObject.SetActive(false);
		vpad_sprite_.transform.parent.gameObject.SetActive(false);
		swipe_motion_sprite_.transform.parent.gameObject.SetActive(false);
		
		if (is_settings_scene == true)
		{
			background_black_sliced_.SetActive(false);
			background_divisory_sliced_.SetActive(false);
			GameObject.Find("Root2/Background").GetComponent<tk2dCameraAnchor>().offset.y = 160F;
		}
		else
		{
			background_popup_sliced_sprite_.SetActive(false);
			background_popup_sliced_sprite_web_.SetActive(true);
			x_close_button_.SetActive(false);
			x_close_button_web_.SetActive(true);
		}
		
	}
	
	
	void ResetGamePressed()
	{
		if(click_enabled_ == true)
		{
			reset_popup_prefab_.SetActive(true);
			click_enabled_ = false;
			
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}
			
	}
	
	void ResetGame()
	{
		//if(click_enabled_ == true)
		//{
			MissionsManager mission_manager = GetComponent<MissionsManager>();
			//missions
			mission_manager.Init(false, false);
			mission_manager.Reset();
			//player data
			Framework.Instance.InstanceComponent<PlayerData>().Reset();
			//options
			Framework.Instance.InstanceComponent<Options>().Reset();
			
			Debug.Log("Should reset PlayerDetails");
			
			reset_popup_prefab_.SetActive(false);
			click_enabled_ = true;
		//}
		Framework.Instance.audio_manager().Play("37_Menu Button Select", transform, 1.0F, 1.0F, false);
	}
	
	void CancelReset()
	{
		reset_popup_prefab_.SetActive(false);
		click_enabled_ = true;
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}	
	
	void SFXRefresh(){
		//Debug.Log("SFX :"+player_data_.GetBool("sfx_on"));
		
		if (is_non_touch_layout == false)
		{
			if (options_params_.sfx_on == true)
			{
				sfx_on_sprite_.SetActive(true);
				sfx_off_sprite_.SetActive(false);
			}
			else if(options_params_.sfx_on == false)
			{
				sfx_on_sprite_.SetActive(false);
				sfx_off_sprite_.SetActive(true);
			}
		}
		else
		{
			if (options_params_.sfx_on == true)
			{
				sfx_on_sprite_web_.SetActive(true);
				sfx_off_sprite_web_.SetActive(false);
			}
			else if(options_params_.sfx_on == false)
			{
				sfx_on_sprite_web_.SetActive(false);
				sfx_off_sprite_web_.SetActive(true);
			}
		}
		
	}
	
	
	void MenuPressed(){
		if(click_enabled_ == true){
			Application.LoadLevel("MainMenu");
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	
	void SFXPressed(){
		if(click_enabled_ == true){
			if (options_params_.sfx_on == true)
			{
				options_params_.sfx_on = false;
				options_.ChangeOptionsParams(options_params_);
				//player_data_.SetValue("sfx_on", false);
			}		
			else if (options_params_.sfx_on == false)
			{
				options_params_.sfx_on = true;
				options_.ChangeOptionsParams(options_params_);
				//player_data_.SetValue("sfx_on", true);	
			}	
					
			SFXRefresh();	
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}
		
	}
	
	
	void MusicRefresh(){
		if (is_non_touch_layout == false)
		{
			if (options_params_.music_on == true)
			{
				music_on_sprite_.SetActive(true);
				music_off_sprite_.SetActive(false);
			}
			else if(options_params_.music_on == false)
			{
				music_on_sprite_.SetActive(false);
				music_off_sprite_.SetActive(true);
			}
		}	
		else
		{
			if (options_params_.music_on == true)
			{
				music_on_sprite_web_.SetActive(true);
				music_off_sprite_web_.SetActive(false);
			}
			else if(options_params_.music_on == false)
			{
				music_on_sprite_web_.SetActive(false);
				music_off_sprite_web_.SetActive(true);
			}
		}
	}
	
	
	void MusicPressed(){
		if(click_enabled_ == true)
		{
			if (options_params_.music_on == true)
			{
				options_params_.music_on = false;
				options_.ChangeOptionsParams(options_params_);
				//player_data_.SetValue("music_on", false);
			}	
			else if (options_params_.music_on == false)
			{
				options_params_.music_on = true;
				options_.ChangeOptionsParams(options_params_);
				//player_data_.SetValue("music_on", true);
			}	
				
			MusicRefresh();	
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}
	}
	
	
	void CreditsPressed(){
		if(click_enabled_ == true){
			Application.LoadLevel("CreditsScene");
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	
	void DPadPressed()
	{
		if(click_enabled_ == true)
		{
			ControllerSelected(InputControllerType.d_pad);
			options_params_.input_controller_type = InputControllerType.d_pad;
			options_.ChangeOptionsParams(options_params_);
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}		
	}
	
	void VPadPressed()
	{
		if(click_enabled_ == true)
		{
			ControllerSelected(InputControllerType.virtual_pad);
			options_params_.input_controller_type = InputControllerType.virtual_pad;
			options_.ChangeOptionsParams(options_params_);
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	void SwipePressed()
	{
		
		if(click_enabled_ == true)
		{
			ControllerSelected(InputControllerType.swipe_motion);
			options_params_.input_controller_type = InputControllerType.swipe_motion;
			options_.ChangeOptionsParams(options_params_);	
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	private void ControllerSelected(InputControllerType controller_type)
	{
		
		if(controller_type == InputControllerType.d_pad)
		{
			dpad_sprite_.renderer.enabled = true;
			vpad_sprite_ .renderer.enabled = false;
			swipe_motion_sprite_.renderer.enabled = false;
		}
		else if(controller_type == InputControllerType.swipe_motion)
		{
			dpad_sprite_.renderer.enabled = false;
			vpad_sprite_.renderer.enabled = false;
			swipe_motion_sprite_.renderer.enabled = true;
		}
		else if(controller_type == InputControllerType.virtual_pad)
		{
			dpad_sprite_.renderer.enabled = false;
			vpad_sprite_.renderer.enabled = true;
			swipe_motion_sprite_.renderer.enabled = false;
		}
			
	}
	
	void CloseSettingsPressed()
	{
		//If I call it here before it becomes active it won't paly
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		
		
		gameObject.SetActive(false);
		Messenger.Broadcast("ClosedPauseSettings");
		
	}
}
