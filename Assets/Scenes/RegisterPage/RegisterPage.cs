using UnityEngine;
using System.Collections;

public class RegisterPage : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	void PlayPressed()
	{
#if UNITY_WEBPLAYER
		WebKongregateAPI kongregate_api = Framework.Instance.InstanceComponent<WebKongregateAPI>();
		kongregate_api.UpdateGuest();
		if(!kongregate_api.is_guest())
			Framework.Instance.scene_manager().LoadScene("MainMenu", TransitionType.change_scene);
		else
			kongregate_api.ShowRegistrationBox();
#endif
	}
}
