using UnityEngine;
using System.Collections;

public enum CardsPrizeType
{
	white_coins,
	coins,
	ammo,
	count
}

public class CardsScene : MonoBehaviour
{
	public CardsPrizeType prize_type_ = CardsPrizeType.white_coins;
	public string prize_value_ = "10";
	
	private Animation animation_;
	private tk2dUIItem ui_item_;
	private GameObject card_ = null;
	private GameObject card_prize_ = null;
	private Animation card_animation_ = null;
	private GameObject card_label_sprite_ = null;
	private tk2dTextMesh num_text_mesh_ = null;
	
	// Use this for initialization
	void Awake ()
	{ 
		animation_ = GetComponent<Animation> ();
		//animation_.Stop();
		
		SetEnableUIItems (false);
	}
	
	
	void Start ()
	{
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "CardsScene");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		if(card_animation_ != null && 
		   !card_animation_.isPlaying &&
		   card_prize_.GetComponent<MeshRenderer> ().enabled )
		{
			BackToEndGame();
		}
	}
	
	
	public void EnableUIItems ()
	{
		SetEnableUIItems (true);
	}
	
	
	private void SetEnableUIItems (bool enable)
	{
		tk2dUIItem[] ui_items = GetComponentsInChildren<tk2dUIItem> ();
		for (int i = 0; i < ui_items.Length; i++) {
			ui_items [i].enabled = enable;
		}
	}
		
	private void OnPressedCard1 ()
	{
		animation_.Play ("Cards1Animation");
		GetObjects ("1");
		SetEnableUIItems (false);
	}
	
	
	private void OnPressedCard2 ()
	{
		animation_.Play ("Cards2Animation");
		GetObjects ("2");
		SetEnableUIItems (false);
	}
	
	
	private void OnPressedCard3 ()
	{
		animation_.Play ("Cards3Animation");
		GetObjects ("3");
		SetEnableUIItems (false);
	}
	
	
	private void GetObjects (string card_value)
	{
		string parent_name = "Card" + card_value + "Container";
		card_ = GameObject.Find (parent_name + "/CardsPrefab/CardFrontContainer/CardFrontSprite");
		card_animation_ = GameObject.Find (parent_name + "/CardsPrefab").GetComponent<Animation> ();
		card_label_sprite_ = GameObject.Find (parent_name + "/CardsPrefab/CardLabelSprite");
		num_text_mesh_ = GameObject.Find (parent_name + "/CardsPrefab/CardLabelSprite/NumTextMesh").GetComponent<tk2dTextMesh> ();
		
		if (prize_type_ == CardsPrizeType.white_coins)
			card_prize_ = GameObject.Find (parent_name + "/CardsPrefab/CardRearWhiteCoinsSprite");
		else if (prize_type_ == CardsPrizeType.coins)
			card_prize_ = GameObject.Find (parent_name + "/CardsPrefab/CardRearCoinsSprite");
		else if (prize_type_ == CardsPrizeType.ammo)
			card_prize_ = GameObject.Find (parent_name + "/CardsPrefab/CardRearAmmoSprite");
		
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	public void PlayShowCardAnimation ()
	{
		card_animation_.Play ("CardsAnimation");
		card_.GetComponent<MeshRenderer> ().enabled = false;
		card_prize_.GetComponent<MeshRenderer> ().enabled = true;
		
		//if (prize_type_ != CardsPrizeType.ammo) 
		//{
			card_label_sprite_.GetComponent<MeshRenderer> ().enabled = true;
			num_text_mesh_.GetComponent<MeshRenderer> ().enabled = true;
			num_text_mesh_.text = prize_value_;
			num_text_mesh_.Commit ();
		//}
		Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
	}
	
	
	public void BackToEndGame()
	{
		Framework.Instance.scene_manager().BackFromCurrentScene();
		GameObject.Find("FakeRoot").GetComponent<EndGameMultiplayer>().PlayIntroAnimation();
	}
	
	
	public void SetParams(CardsPrizeType prize_type, string prize_value)
	{
		animation_.Play ("CardEnteringAnimation");
		prize_type_ = prize_type;
		prize_value_ = prize_value;
	}
	
	
	public void SetParams(CardsPrizeType prize_type, int prize_value)
	{
		animation_.Play ("CardEnteringAnimation");
		prize_type_ = prize_type;
		prize_value_ = prize_value.ToString();
	}
	
	
	private void PlaySwooshSound()
	{
		Framework.Instance.audio_manager().PlayUISound("20_Get Hero Punch Misses 1v", 1.0F, 1.0F, false);
	}
	
	
	private void PlayEnterSound()
	{
		Framework.Instance.audio_manager().PlayUISound("13_Combo Success", 1.0F, 1.0F, false);
	}

}
