using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using EbFightAPI;

public class EndGame : MonoBehaviour {

	public GameObject buy_coins_prefab_;
	public GameObject mission_completed_;
	public GameObject multiplier_text_mesh_;
	public GameObject mission_number_text_mesh_;
	
	private PlayerData player_data_;
	private JSONNode dd_info_;
	
	private tk2dTextMesh end_score_text_;
	private tk2dTextMesh high_score_text_;
	private tk2dTextMesh coins_text_;
	private long score_;
	
	public GameObject loading_page_;

	public GameObject black_overlay_tutorial_;
	
	private StaticParams static_param;
	
	void Awake()
	{	
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		dd_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/DDInfo"));
		MainMenu.currentScene_ = "EndGame";//Leo
		static_param = GameObject.Find("StaticParams").GetComponent<StaticParams>();
		score_ = static_param.score_;
		
		Framework.Instance.audio_manager().PlayBackgroundMusic("Endless Boss Menu");
		
		Destroy(GameObject.Find("StaticParams"));
#if UNITY_WEBPLAYER
		Framework.Instance.InstanceComponent<WebKongregateAPI>().SendScore( score_ );
		
		Framework.Instance.InstanceComponent<WebKongregateAPI>().SendBadge("bosslevel", Framework.Instance.InstanceComponent<ServerAPI>().GetBoss().Level);
		Framework.Instance.InstanceComponent<WebKongregateAPI>().SendBadge("ddlevel", player_data_.GetInt("dd_level"));
		Framework.Instance.InstanceComponent<WebKongregateAPI>().SendBadge("missions", PlayerPrefs.GetInt ("current_mission_id") - 1);
#elif UNITY_IPHONE
		KongregateAPI.GetAPI().Stats.Submit("Endless", score_);
		
		KongregateAPI.GetAPI().Stats.Submit("bosslevel", Framework.Instance.InstanceComponent<ServerAPI>().GetBoss().Level);
		KongregateAPI.GetAPI().Stats.Submit("ddlevel", player_data_.GetInt("dd_level"));
		KongregateAPI.GetAPI().Stats.Submit("missions", PlayerPrefs.GetInt ("current_mission_id") - 1);
#endif
		
	}

	public void SendEndRunParams()
	{
		Analitycs analitycs = Framework.Instance.InstanceComponent<Analitycs>();
		//DD in multiplayer does not gain xp
		analitycs.AddNewKey(KeenIOEventType.run_end, "xp_gained", static_param.xp_ - static_param.starting_xp_);
		analitycs.AddNewKey(KeenIOEventType.run_end, "run_score", score_);
		
		Framework.Instance.InstanceComponent<Analitycs>().EndRun();
	}
	
	// Use this for initialization
	void Start () 
	{
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetEnabled();
		
		PoolManager.ClearPool();
		
		//Analitycs
		SendEndRunParams();
		
		//Score
		end_score_text_ = GameObject.Find("ScoreTextMesh").GetComponent<tk2dTextMesh>();
		end_score_text_.text = FormattingWithZeros( score_.ToString(), 9);
		end_score_text_.Commit();
		
		//High score
		if(player_data_.GetInt("single_player_high_score") < score_)
		{
			player_data_.SetValue("single_player_high_score", score_);
			GameObject.Find("NewRecordLabelSprite").active = true;
#if UNITY_IPHONE && !UNITY_EDITOR
			GameCenterAPI game_center_ = Framework.Instance.InstanceComponent<GameCenterAPI>();
			if (game_center_.IsUserAuthenticated() == true)
				game_center_.PostHighScore((long) score_);
#endif	
			
#if UNITY_IPHONE || UNITY_ANDROID
			//Need to do this only once every 3 or 5 times
			
			if (Framework.Instance.InstanceComponent<ServerAPI>().IsConnected() == true){
				player_data_.SetValue("number_of_times_set_single_player_high_score", player_data_.GetInt("number_of_times_set_single_player_high_score")+1);
			
				if (player_data_.GetInt("number_of_times_set_single_player_high_score") % 3 == 0 && 
					player_data_.GetInt("number_of_times_set_single_player_high_score") !=0 && 
					player_data_.GetInt("number_of_times_set_single_player_high_score") <= 9 &&
					player_data_.GetBool("should_show_rate_pop_up") == true)
						Framework.Instance.InstanceComponent<RateMePopUp>().Init();		
			}
#endif			
		}
		else
			GameObject.Find("NewRecordLabelSprite").active = false;

		//Coins
		InitBuyCoinsButton(375,290);
		
		//XP
		GameObject.Find("LevelTextMesh").GetComponent<tk2dTextMesh>().text = "LEVEL: "+FormattingWithZeros((player_data_.GetInt("dd_level")).ToString(),2);
		GameObject.Find("LevelTextMesh").GetComponent<tk2dTextMesh>().Commit();
		GameObject.Find("XPSlicedSprite").GetComponent<tk2dSlicedSprite>().dimensions = new Vector2((player_data_.GetInt("dd_xp")/dd_info_[player_data_.GetInt("dd_level")]["xp_to_next"].AsFloat)*110.0F,10F);
		GameObject.Find("XPTextMesh").GetComponent<tk2dTextMesh>().text = player_data_.GetString("dd_xp")+"/"+dd_info_[player_data_.GetInt("dd_level")]["xp_to_next"];
		GameObject.Find("XPTextMesh").GetComponent<tk2dTextMesh>().Commit();
		GameObject.Find("GameXpNumTextMesh").GetComponent<tk2dTextMesh>().text = "+" + 
			wmf.ui.Utility.FormattingWithZeros( static_param.xp_ - static_param.starting_xp_, 4 );
		GameObject.Find("GameXpNumTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "EndGameSinglePlayer");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		//Tutorial
		black_overlay_tutorial_.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	public void FightPressed()
	{	
		Application.LoadLevel ("StartGame");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	public void BackPressed()
	{
		Application.LoadLevel ("MainMenu");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}

	public void OpenShop()
	{
		Application.LoadLevel ("PowerStoreScene");
	}

	
	void InitBuyCoinsButton(float x, float y)
	{
		GameObject button = (GameObject) Instantiate(buy_coins_prefab_, new Vector3(x,y,10),Quaternion.identity);
		button.transform.parent = GameObject.Find("MainCamera").transform;
		button.GetComponent<BuyCoinsPrefab>().Init(124F, -32F, static_param.white_coins_, static_param.coins_);
	}
	
	public void PlayMissionCompletedAnimation(int mission_number)
	{
		mission_completed_.SetActive(true);
		mission_completed_.animation.Play();
		multiplier_text_mesh_.GetComponent<tk2dTextMesh>().text = mission_number.ToString();
		multiplier_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		mission_number_text_mesh_.GetComponent<tk2dTextMesh>().text = (mission_number - 1).ToString();
		mission_number_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		Framework.Instance.audio_manager().PlayUISound("38_Menu Mission Complete", 1.0F, 1.0F, false);
	}
	

	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");

		return txt;
	}

}