using UnityEngine;
using System.Collections;
using EbFightAPI;

public class EndGameTraining : MonoBehaviour
{
	public GameObject round_result_prefab_;
	
	private GameObject boss_level_up_container_;
	private GameObject continue_training_button_container_;
	
	private StaticParams static_param;
	
	// Use this for initialization
	void Start ()
	{
		MainMenu.currentScene_ = "EndGame";//Leo
		
		static_param = GameObject.Find ("StaticParams").GetComponent<StaticParams> ();
		Destroy (GameObject.Find ("StaticParams"));
		
		Framework.Instance.InstanceComponent<ServerAPI> ().EndMatch (
				static_param.session_id_,
				//TO-DO Added a real result
				new string[]{static_param.dd_results_ [0].ToString (), 
							 static_param.dd_results_ [1].ToString (), 
				             static_param.dd_results_ [2].ToString ()},
				0,
				OnEndMatch);
		
		boss_level_up_container_ = GameObject.Find("BossLevelUpContainer");
		continue_training_button_container_ = GameObject.Find("ContinueTrainingButtonContainer");
		boss_level_up_container_.SetActive( false );
		continue_training_button_container_.SetActive( false );
		
		Framework.Instance.audio_manager().PlayBackgroundMusic("Endless Boss Menu");
		
		Round1Trigger ();
		Round2Trigger ();
		Round3Trigger ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	public void BackPressed ()
	{
		Application.LoadLevel ("BossDetail");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	public void TrainAgainPressed ()
	{
		Framework.Instance.InstanceComponent<ServerAPI> ().BeginTraining (OnBeginTraining);
	}
	
	public void ButtonBossLevelUpPressed ()
	{
		if (Framework.Instance.InstanceComponent<wmf.StaticParams>().ContainsKey("IsLevelUpDD") == false)
			Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("IsLevelUpDD", false);
		else
			Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsLevelUpDD", false);
				
		Application.LoadLevel("LevelUp");
	}
	
	void OnBeginTraining (ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success) {
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("MatchSession", data.As<MatchSession> ());
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().ChangeValue ("IsTraining", true);
			Application.LoadLevel ("LoadingScene");
		}
	}
	
	void OnEndMatch (ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success) {
			EbFightAPI.Boss old_boss = Framework.Instance.InstanceComponent<ServerAPI>().GetBoss();
			EbFightAPI.Boss new_boss = data.As<EbFightAPI.Player>().Boss;
			
			if(new_boss.Level > old_boss.Level)
			{
				boss_level_up_container_.SetActive( true );
				GameObject.Find("MenuButtonContainer").SetActive(false);
			}
			else
			{
				continue_training_button_container_.SetActive( true );
			}
			
		} else {
			continue_training_button_container_.SetActive( true );
		}
	}
	
	void Round1Trigger ()
	{
		CreateRoundPrefab (0);
	}
	
	void Round2Trigger ()
	{
		CreateRoundPrefab (1);
	}
	
	void Round3Trigger ()
	{
		CreateRoundPrefab (2);
	}
	
	void CreateRoundPrefab (int round_number)
	{
		GameObject round_result_prefab = (GameObject)Instantiate (round_result_prefab_);
		round_result_prefab.transform.parent = GameObject.Find ("FakeRoot/RoundsResultsContainer").transform;
		round_result_prefab.transform.localPosition = new Vector3 (
			537.0f,
			108.0f - 30.0f * round_number, 
			0.0f);
		round_result_prefab.GetComponent<RoundResultPrefab> ().Init (static_param.dd_results_ [round_number]);
		
		Framework.Instance.audio_manager ().Play ("16_Combo Hit MIssile " + (round_number + 1) + "v", transform, 1.0F, 1.0F, false);
		
		round_result_prefab.GetComponentInChildren<Animation>().Play("RoundsResultsTrainingAnimation");
	}
}
