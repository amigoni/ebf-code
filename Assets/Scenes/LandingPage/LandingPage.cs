using UnityEngine;
using System.Collections;
using EbFightAPI;

public class LandingPage : MonoBehaviour {
	
	public GameObject logo_sprite_;

	private string user_id_ = "TestUser2";
	private string user_name_ = "TestUserTestUser2";
	private string token_ = "TestUser2";
	
	public Animation logo_animation_;
	
#if UNITY_IPHONE
	private string gamecenter_id_ = "";
	GameCenterAPI game_center_;
#endif
	
	private bool auth_fired_ = false;
	// Use this for initialization
	void Start () 
	{
		logo_animation_.Play();
		
		Framework.Instance.InstanceComponent<ServerAPI>();

#if UNITY_IPHONE
		game_center_ = Framework.Instance.InstanceComponent<GameCenterAPI>();
		game_center_.Initialize();
#endif
		
#if UNITY_IPHONE || UNITY_ANDROID
		Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("unibill_has_initialized", false);
		Framework.Instance.InstanceComponent<InAppPurchasesUnibillManager>();
#endif
		
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
		//SendAdXAnalitics();
		Framework.Instance.InstanceComponent<LocalNotificationsManager>();
#endif
		
	}
	
	// Update is called once per frame
	void Update () {
		ServerAPI server_api = Framework.Instance.InstanceComponent<ServerAPI>();
		if(!logo_sprite_.GetComponent<Animation>().isPlaying)
		{
			if(!auth_fired_)
			{
				auth_fired_ = true;

#if UNITY_WEBPLAYER && !UNITY_EDITOR
				Framework.Instance.InstanceComponent<WebKongregateAPI>().Init(GetKongragateParams);
#elif UNITY_IPHONE || UNITY_ANDROID

				user_id_ = Framework.Instance.InstanceComponent<PlayerData>().GetString("user_id");
				Framework.Instance.InstanceComponent<Analitycs>();
				bool is_connected = server_api.IsConnected();
				if(is_connected && user_id_ != "false")
				{
					Framework.Instance.InstanceComponent<ServerAPI>().AuthenticatePlayer(user_id_, token_, OnAuthenticate);
				}
				else if(user_id_ == "false")
				{
					//Application.LoadLevel("InsertNameGUI");
					Application.LoadLevel("InsertName");
				}
				else 
				{
					Framework.Instance.InstanceComponent<UpdateDataManager>();
					Application.LoadLevel("MainMenu");
				}
				
#elif UNITY_EDITOR && !UNITY_IPHONE 
				Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_id", user_id_);
				Framework.Instance.InstanceComponent<Analitycs>();
				
				Framework.Instance.InstanceComponent<ServerAPI>().AuthenticatePlayer(user_id_, token_, OnAuthenticate);
#endif

			}
		}
	}
	
	public void GetKongragateParams(string user_id, string user_name, string token)
	{
		user_id_ = user_id;
		user_name_ = user_name;
		token_ = token;
		Framework.Instance.InstanceComponent<ServerAPI>().AuthenticatePlayer(user_id, token, OnAuthenticate);
	}
	
	private void OnAuthenticate(ResponseStatus status, JSONResult data)
	{
#if UNITY_WEBPLAYER
		if(Framework.Instance.InstanceComponent<WebKongregateAPI>().is_guest())
			return;
		
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_id", user_id_);
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_name", user_name_);
		
		Framework.Instance.InstanceComponent<Analitycs>();
#endif
		ServerAPI server_api = Framework.Instance.InstanceComponent<ServerAPI>();
		
		if(status == ResponseStatus.Error)
		{
			server_api.ClosePopUpWindow();
			//TO-DO check
			Application.LoadLevel("MainMenu");
#if UNITY_WEBPLAYER			
			server_api.RegisterPlayer(user_id_, user_name_, token_, OnRegisterPlayer);
#endif
		} 
#if UNITY_IPHONE || UNITY_ANDROID		
		else if(status == ResponseStatus.Disconnected)
		{
			Framework.Instance.InstanceComponent<UpdateDataManager>();
			Application.LoadLevel("MainMenu");
		}
#endif
		else {	
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
#if UNITY_WEBPLAYER	
			if(!string.IsNullOrEmpty(player.Data))
				Framework.Instance.InstanceComponent<PlayerData>().player_data_ = SimpleJSON.JSON.Parse(player.Data);
#endif					
			long max_wmc = (long) Framework.Instance.InstanceComponent<PlayerData>().GetInt("white_coins");
			if(max_wmc < player.WhiteCoins)
				max_wmc = player.WhiteCoins;
			
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", max_wmc);
			server_api.SavePlayer(player);

			Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins( Framework.Instance.InstanceComponent<PlayerData>().GetInt("white_coins"), OnSynchWhiteCoins);
		}
	}
	
	private void OnSynchWhiteCoins(ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success)
		{
			//Debug.Log("Synching WMC's in Landing");
			long sync_white_coins = data.As<long>();
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", sync_white_coins);	
		}
		else
		{
			Debug.Log("Error Synching WMC "+data.ErrorMessage);
		}
#if UNITY_IPHONE || UNITY_ANDROID	
		Framework.Instance.InstanceComponent<UpdateDataManager>();
		Application.LoadLevel("MainMenu");
#else
		Application.LoadLevel("MainMenu");
#endif	
	}			
				
				
	private void OnRegisterPlayer(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(player);
			long white_coins = player.WhiteCoins;
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", white_coins);
			
#if UNITY_IPHONE || UNITY_ANDROID	
			Framework.Instance.InstanceComponent<UpdateDataManager>();
			Application.LoadLevel("MainMenu");
#else
			Application.LoadLevel("MainMenu");
#endif	
		}
#if UNITY_IPHONE || UNITY_ANDROID
		else
		{
			Framework.Instance.InstanceComponent<UpdateDataManager>();
			Application.LoadLevel("MainMenu");
		}
#endif
	}
	
#if UNITY_IPHONE && !UNITY_EDITOR
	/*
	void SendAdXAnalitics()
	{
		//Lauch event
		AdX.SendLaunch();
		
		//Session event
		int session = Framework.Instance.InstanceComponent<PlayerData>().GetInt("session");
		session++;
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("session", session);
		AdX.SendSession(session);
	
		//Day Event
		long install_date_time_ticks = Framework.Instance.InstanceComponent<PlayerData>().GetInt("install_date");
		if(install_date_time_ticks == 0)
		{
			install_date_time_ticks = System.DateTime.UtcNow.Ticks;
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("install_date", install_date_time_ticks);
		}
		System.DateTime install_date_time = new System.DateTime(install_date_time_ticks);
		System.DateTime current_date = System.DateTime.UtcNow;
		
		int diff_days = (current_date - install_date_time).Days;
		if( diff_days > 0 )
		{
			AdX.SendDay(diff_days);
		}
	}
	*/
#endif
	
	
}
