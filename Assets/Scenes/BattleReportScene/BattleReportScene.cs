using UnityEngine;
using System.Collections;
using SimpleJSON;
using EbFightAPI;

public class BattleReportScene : MonoBehaviour {
	
	public GameObject battles_text_mesh_;
	public GameObject won_text_mesh_;
	public GameObject lost_text_mesh_;
	public GameObject br_absolute_text_mesh_;
	public GameObject br_partial_text_mesh_;
	public GameObject white_coins_text_mesh_;
	public GameObject level_num_text_mesh_;
	public GameObject xp_text_mesh_;
	public GameObject xp_partial_text_mesh_;
	public GameObject xp_sliced_sprite_;
	public GameObject your_boss_leveled_up_sprite_;
	
	public GameObject loading_page_prefab_;
	
	private EbFightAPI.Boss boss_data_;
	
	public JSONNode store_info_;
	
	private EbFightAPI.BattleReport report_;
	
	// Use this for initialization
	void Start ()
	{
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetDisabled();
			
		Framework.Instance.InstanceComponent<ServerAPI>().GetBossConfiguration(OnGetBossConfiguration);
		
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	
	private void OnGetBossConfiguration(ResponseStatus status, JSONResult data)
	{
		boss_data_ = data.As<EbFightAPI.Boss>();
		Framework.Instance.InstanceComponent<ServerAPI>().SaveBoss(boss_data_);
	
		//GameObject loading_page_prefab = (GameObject) Instantiate(loading_page_prefab_, new Vector3(0,0,1),Quaternion.identity);
		//Destroy(loading_page_prefab);
	
		Init(GameObject.Find("MainCamera").GetComponent<MainMenu>().GetBattleReportData(), Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer());
	}
	
	public void Init (EbFightAPI.BattleReport report, EbFightAPI.Player old_player)
	{
		report_ = report;
		
		battles_text_mesh_.GetComponent<tk2dTextMesh>().text = report.TotalMatches.ToString();	
		battles_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		won_text_mesh_.GetComponent<tk2dTextMesh>().text = report.MatchesWon.ToString();	
		won_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		lost_text_mesh_.GetComponent<tk2dTextMesh>().text = report.MatchesLost.ToString();	
		lost_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		br_absolute_text_mesh_.GetComponent<tk2dTextMesh>().text = ((int) boss_data_.Rank).ToString();
		br_absolute_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		string br_partial_text = "";
		if (report.TotalPointsModifier >= 0)
			br_partial_text = "+"+report.TotalPointsModifier.ToString();
		else
			br_partial_text = report.TotalPointsModifier.ToString();
		
		br_partial_text_mesh_.GetComponent<tk2dTextMesh>().text = br_partial_text;	
		br_partial_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
		
		white_coins_text_mesh_.GetComponent<tk2dTextMesh>().text = report.WhiteCoinsEarned.ToString();	
		white_coins_text_mesh_.GetComponent<tk2dTextMesh>().Commit(); 
		
		float xp = boss_data_.Xp - (store_info_["boss_level_upgrade"]["upgrades"][(int) report.NewLevel]["xp_min"].AsFloat - 1);
		float xp_max = store_info_["boss_level_upgrade"]["upgrades"][(int) report.NewLevel]["xp_max"].AsFloat - (store_info_["boss_level_upgrade"]["upgrades"][(int) report.NewLevel]["xp_min"].AsFloat-1)+1;
		long level = report.NewLevel;
		
		//Debug.Log(xp);
		
		level_num_text_mesh_.GetComponent<tk2dTextMesh>().text = level.ToString();	
		level_num_text_mesh_.GetComponent<tk2dTextMesh>().Commit(); 
		
		xp_text_mesh_.GetComponent<tk2dTextMesh>().text = xp.ToString()+"/"+xp_max.ToString();	
		xp_text_mesh_.GetComponent<tk2dTextMesh>().Commit(); 
		
		xp_partial_text_mesh_.GetComponent<tk2dTextMesh>().text = "+"+report.TotalGainedXp.ToString();	
		xp_partial_text_mesh_.GetComponent<tk2dTextMesh>().Commit(); 
		
		xp_sliced_sprite_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2((xp/xp_max)*101F,10);
		
		if(report.HasBossLeveledUp == true)
			your_boss_leveled_up_sprite_.SetActive(true);
		else
			your_boss_leveled_up_sprite_.SetActive(false);
	}
	
	
	void GetCoinsPressed()
	{
		Framework.Instance.InstanceComponent<ServerAPI>().CollectWhiteCoins(OnCollectWhiteCoins);
		Framework.Instance.audio_manager().PlayUISound("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
	}
	
	void OnCollectWhiteCoins(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(player);
			
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", player.WhiteCoins);
			GameObject.Find("MainCamera").GetComponent<MainMenu>().CloseBattleReport();
			Messenger.Broadcast("RefreshCoins");
		
			Framework.Instance.scene_manager().BackFromCurrentScene();
			
			if (report_.HasBossLeveledUp == true)
			{	
				if (Framework.Instance.InstanceComponent<wmf.StaticParams>().ContainsKey("IsLevelUpDD") == false)
					Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("IsLevelUpDD", false);
				else
					Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("IsLevelUpDD", false);
				
				Application.LoadLevel("LevelUp");
			}
			//Analitycs
			Framework.Instance.InstanceComponent<Analitycs>().AddCommonValue("num_boss_played", (int)report_.TotalMatches);
			Framework.Instance.InstanceComponent<Analitycs>().AddCommonValue("num_boss_won", (int)report_.MatchesWon);
		}
		else
		{
			Framework.Instance.scene_manager().BackFromCurrentScene();
		}
	}
}
