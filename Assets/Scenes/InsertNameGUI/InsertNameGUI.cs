using UnityEngine;
using System.Collections;
using EbFightAPI;
using System.Text.RegularExpressions;

public class InsertNameGUI : MonoBehaviour {
	
	//TO-DO ask manu to UI max name size
	public const int MIN_NAME_LENGTH = 3;
	public const int MAX_NAME_LENGTH = 12;
	
	public GameObject next_button_;
	public GameObject disabled_button_;
	
	string user_name_ = "";
	// Use this for initialization
	void Start () {
		
	}
	
	
	void Update (){
		if(user_name_.Length < MIN_NAME_LENGTH)
		{
			next_button_.SetActive(false);
			disabled_button_.SetActive(true);
		}
		else
		{
			next_button_.SetActive(true);
			disabled_button_.SetActive(false);
		}	
	}
	
	public void NextPressed ()
	{
		Framework.Instance.InstanceComponent<ServerAPI>().RegisterPlayer(user_name_, "mobile", OnRegisterPlayer);
	}
	
	void OnRegisterPlayer(ResponseStatus status, JSONResult data)
	{
		//Debug.Log("OnRegisterPlayer: " + data.Data);
		if(status == ResponseStatus.Success)
		{
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(player);
			long white_coins = player.WhiteCoins;
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", white_coins);
					
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_id", player.UserId);
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("user_name", player.Username);
			
			Framework.Instance.InstanceComponent<Analitycs>().SendGamePlaysEvent();
			Application.LoadLevel("MainMenu");
		} 
		else 
		{
			if(data.ErrorMessage == "Username is not available")
				Framework.Instance.InstanceComponent<ServerAPI>().NotifyError("Username not available", "Please choose a different username");
			else
				Framework.Instance.scene_manager().LoadScene("NeedConnectionScene", TransitionType.add_scene);
		}
	}
	
	string stringToEdit = "";
	void OnGUI()
	{
		//set up scaling
	    float rx = Screen.width / 480.0f;
	    float ry = Screen.height / 320.0f;
	    GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3 (rx, ry, 1)); 
 
		user_name_ = GUI.TextField(new Rect(50, 80, 200, 20), user_name_, MAX_NAME_LENGTH);
		
		//Regex rgx = new Regex("[^a-zA-Z0-9]");
		//if(rgx.IsMatch(user_name_))
		//{
		//	user_name_ = user_name_.Remove(user_name_.Length - 1);
		//}
	}
}
