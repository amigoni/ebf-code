using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using SimpleJSON;
using EbFightAPI;
using MiniJSON;

public class StartGame : MonoBehaviour {
	
	List<string> configuration_ = new List<string>();
	
	public GameObject power_punch_item_button_prefab_;
	public GameObject utility_item_button_prefab_;
	public GameObject buy_coins_prefab_;
	public GameObject best_score_container_;
	public GameObject boss_info_container_;
	
	public GameObject pp_great_ball_container_;
	public GameObject pp_atatata__container_;
	public GameObject pp_huge_punch__container_;
	
	public GameObject tutorial_object_;
	public GameObject black_overlay_tutorial_;
	
	private EbFightAPI.Boss contender_boss_data_;
	
	public string selected_utility_key_;
	
	private Store store_;
	private PlayerData player_data_;
	private JSONNode store_info_;
	
	private List<GameObject> power_punch_icons_ = new List<GameObject>();
	private List<GameObject> utilities_icons_ = new List<GameObject>();
	
	void Awake()
	{
		Framework.Instance.InstanceComponent<LoadingScreenBlock>().Hide();
		
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetDisabled();
		
		store_ = Framework.Instance.InstanceComponent<Store>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		
		//Reset utilities Data(for sure)
		player_data_.SetArrayValue("utilities_equipped", "double_bubble", false );
		player_data_.SetArrayValue("utilities_equipped", "double_magnet", false );
		player_data_.SetArrayValue("utilities_equipped", "life_boost", false );
		player_data_.SetArrayValue("utilities_equipped", "att_boost", false );
		player_data_.SetArrayValue("utilities_equipped", "ammo_boost", false );
		
		//Messenger.AddListener("ButtonPressed", DummyButtonPressed);
		Messenger.AddListener("PopUpOpen", LockInterface);
		Messenger.AddListener("PopUpClosed", UnlockInterface);
		Messenger.AddListener<string>("PowerPunchSelected", SelectPowerPunch);
		Messenger.AddListener<string,bool,bool>("UtilitySelected", SelectUtility);
		
		// TO-DO check the connection
		string last_white_coins_call = player_data_.GetString("last_white_coins_call");
		last_white_coins_call = last_white_coins_call.Replace('#', ':');
		last_white_coins_call = last_white_coins_call.Replace('_', ' ');
		
		bool first_time = false;
		if(last_white_coins_call == "0")
		{
			first_time = true;
			last_white_coins_call = System.DateTime.Now.ToString();
			
			string save_string = last_white_coins_call.Replace(':', '#');
			save_string = save_string.Replace(' ', '_');
			player_data_.SetValue("last_white_coins_call", save_string);
		}
		
		System.DateTime current_date = System.DateTime.Now;
		System.DateTime last_date_call;
		System.TimeSpan diff_date = new System.TimeSpan(0);
		if( System.DateTime.TryParse(last_white_coins_call, out last_date_call))
		{
			diff_date = current_date.Subtract( last_date_call );
		}
					
		//if(diff_date.Days != 0)
		if(diff_date.Seconds > 3600 || first_time == true || player_data_.GetInt("free_white_coins_per_day") == 0)
		{
#if UNITY_IPHONE || UNITY_ANDROID
			if (Framework.Instance.InstanceComponent<ServerAPI>().IsConnected() == true)
				Framework.Instance.InstanceComponent<ServerAPI>().FreeWhiteCoinsLeftToday(
					player_data_.GetInt("dd_level"),
					OnFreeWhiteCoinsLeftToday);
#else
			Framework.Instance.InstanceComponent<ServerAPI>().FreeWhiteCoinsLeftToday(
					player_data_.GetInt("dd_level"),
					OnFreeWhiteCoinsLeftToday);
#endif			
		}
		//else
			//Debug.Log("free_white_coins_per_day: " + player_data_.GetInt("free_white_coins_per_day"));

		}
	
	// Use this for initialization
	void Start ()
	{
		GameObject.Find("BackgroundBossInfoSlicedSprite").GetComponent<tk2dSlicedSprite>().BorderOnly = false; 
		GameObject.Find("AmmoBuyButtonPrefab").GetComponent<AmmoBuyButtonPrefab>().Refresh();
		InitBuyCoinsButton(375,290);

		InitPowerPunches();
		InitUtilities();
		
		if((bool)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get ("IsMultiplayer") == false)
		{
			GetComponent<Animation>().Play();
			
			player_data_.SetValue("single_player_games_played", player_data_.GetInt("single_player_games_played")+1);
			
			best_score_container_.SetActive(true);
			boss_info_container_.SetActive(false);
			GameObject.Find("MainCamera/BestScoreContainer/TextMesh").GetComponent<tk2dTextMesh>().text = FormattingWithZeros( player_data_.GetInt("single_player_high_score").ToString(), 9);
			GameObject.Find("MainCamera/BestScoreContainer/TextMesh").GetComponent<tk2dTextMesh>().Commit();
			//Analitycs
			Framework.Instance.InstanceComponent<Analitycs>().NewRun(null);
			Framework.Instance.InstanceComponent<Analitycs>().IncreaseCommonValue("num_pve_played");
			
			GameObject.Find("BossUtilitiesContainer").SetActive(false);
			GameObject.Find("ChallangerFound").SetActive(false);
		}
		else if((bool)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get ("IsMultiplayer") == true)
		{				
			boss_info_container_.SetActive(true);
			best_score_container_.SetActive(false);
			
			EbFightAPI.MatchSession match_session = ((EbFightAPI.MatchSession) Framework.Instance.InstanceComponent<wmf.StaticParams>().Get ("MatchSession"));
			
			//Analitycs
			Framework.Instance.InstanceComponent<Analitycs>().NewRun(match_session);
			Framework.Instance.InstanceComponent<Analitycs>().IncreaseCommonValue("num_pvp_played");
			
			contender_boss_data_ = match_session.Contender.Boss;
			
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/AttackTextMesh").GetComponent<tk2dTextMesh>().text = contender_boss_data_.Attack.ToString();
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/AttackTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/DefenceTextMesh").GetComponent<tk2dTextMesh>().text = contender_boss_data_.Defence.ToString();
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/DefenceTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/LevelTextMesh").GetComponent<tk2dTextMesh>().text = contender_boss_data_.Level.ToString();
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/LevelTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/DefenceTextMesh").GetComponent<tk2dTextMesh>().text = contender_boss_data_.Defence.ToString();
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/DefenceTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/BRTextMesh").GetComponent<tk2dTextMesh>().text = contender_boss_data_.Rank.ToString();
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/BRTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/EnemyNameTextMesh").GetComponent<tk2dTextMesh>().text = match_session.Contender.Username;
			GameObject.Find("MainCamera/BossInfoContainer/BossInfoContainerAnim/EnemyNameTextMesh").GetComponent<tk2dTextMesh>().Commit();
			
			//Utilities
			GameObject.Find ("IconAttActive").renderer.enabled = false;
			GameObject.Find ("IconAttBackgroundActive").renderer.enabled = false;
			GameObject.Find ("IconDefActive").renderer.enabled = false;
			GameObject.Find ("IconDefBackgroundActive").renderer.enabled = false;
			GameObject.Find ("IconTimeActive").renderer.enabled = false;
			GameObject.Find ("IconTimeBackgroundActive").renderer.enabled = false;
			
			for(int i = 0; i < contender_boss_data_.Utilities.Count; i++)
			{
				if(contender_boss_data_.Utilities[i].Kind == "boss_utilities_att")
				{
					GameObject.Find ("IconAttActive").renderer.enabled = true;
					GameObject.Find ("IconAttBackgroundActive").renderer.enabled = false;
				}
				else if(contender_boss_data_.Utilities[i].Kind == "boss_utilities_def")
				{
					GameObject.Find ("IconDefActive").renderer.enabled = true;
					GameObject.Find ("IconDefBackgroundActive").renderer.enabled = false;
				}
				else if(contender_boss_data_.Utilities[i].Kind == "boss_utilities_time")
				{
					GameObject.Find ("IconTimeActive").renderer.enabled = true;
					GameObject.Find ("IconTimeBackgroundActive").renderer.enabled = false;
				}
			}	
			GameObject.Find("BackgroundUtilitiesContainer").GetComponent<tk2dCameraAnchor>().offset = new Vector2(-402.0f, -160.0f);
			GameObject.Find("UtilitiesContainer").GetComponent<tk2dCameraAnchor>().offset = new Vector2(-316.0f, -19.0f);	
			
			if( (bool)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("IsRevenge") == true)
			{
				GetComponent<Animation>().Play();
				GameObject.Find("ChallangerFound").SetActive(false);
			}
			else
			{
				GameObject.Find("ChallangerFound").GetComponent<ChallengerFound>().Init(
				match_session.Contender.Username,
				match_session.Contender.Boss.Level.ToString());
			}
			
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().ChangeValue ("IsRevenge", false);
		}	
	
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "StartGameScene");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		analitycs_.AddEvent(KeenIOEventType.player_configuration);
		analitycs_.AddNewKey(KeenIOEventType.player_configuration, "run_id", analitycs_.GetCurrentRunEvent().run_id);
		analitycs_.AddNewKey(KeenIOEventType.player_configuration, "is_attacker_configuration",true);
		
		
		//Tutorial
		if (player_data_.GetBool("tutorial_in_game_done") == false)
		{
			black_overlay_tutorial_.SetActive(true);
			FightButtonPressed();
		}	
		else
		{
			black_overlay_tutorial_.SetActive(false);
			
			if (player_data_.GetBool("tutorial_startgame_done") == false)
			{
				tutorial_object_.SetActive(true);
				player_data_.SetValue("tutorial_startgame_done", true);
			}
		}	
	}
	
	
	public void FightButtonPressed()
	{
		Application.LoadLevel("LoadingScene");
		Framework.Instance.audio_manager().StopBackgroundMusic();	
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		
		//Analytics
		Dictionary<string, string> dict = new Dictionary<string, string>();
  		dict.Add("resource_type", "ammo");
		dict.Add("resource_subtype", "none");
		dict.Add("resource_quantity", player_data_.GetInt("dd_ammo").ToString());
		configuration_.Add(Json.Serialize(dict));
		
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddNewKey(KeenIOEventType.player_configuration, "resources",Json.Serialize(configuration_));
		analitycs_.SendEvent(KeenIOEventType.player_configuration);
	}
	
	
	public void MenuButtonPressed()
	{
		Application.LoadLevel("MainMenu");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void InitBuyCoinsButton(float x, float y)
	{
		GameObject buy_coins_obj = (GameObject) Instantiate(buy_coins_prefab_);
		buy_coins_obj.transform.parent = GameObject.Find("MainCamera").transform;
		buy_coins_obj.GetComponent<BuyCoinsPrefab>().Init(false,124F,-32F);
		buy_coins_obj.name = "BuyCoinsObject";
	}
	
	
	private void InitPowerPunches()
	{
		List<JSONNode> items_info_list = store_.GetStoreItemsInfoForCategory("power_punch");
			
		int xPos = 0;
		int xSpace = 60;
		GameObject selected_power_punch = new GameObject();
		GameObject parent1 = GameObject.Find("PowerPunchesContainer");
		
		foreach (JSONNode item_info in items_info_list){
			xPos += xSpace;
			GameObject item = (GameObject) Instantiate(power_punch_item_button_prefab_, 
				new Vector3(parent1.transform.position.x+xPos,parent1.transform.position.y,parent1.transform.position.z+1),
				Quaternion.identity);
			
			item.transform.parent = parent1.transform;
			power_punch_icons_.Add(item);
			
			bool is_selected = false;
			bool is_locked = store_.IsItemLocked(item_info["key"].ToString());
			//is_locked = false;
			if (item_info["key"].ToString() == player_data_.GetString("power_punch_equipped"))
			{
				item.GetComponent<PowerPunchItemButtonPrefab>().Init(item_info["key"],is_locked,true);
				selected_power_punch = item;
			}	
			else
				item.GetComponent<PowerPunchItemButtonPrefab>().Init(item_info["key"],is_locked,false);
		};
		
		//Select Saved PP
		SelectPowerPunch(player_data_.GetString("power_punch_equipped"));
		selected_power_punch.GetComponent<PowerPunchItemButtonPrefab>().Select();
	}
	
	
	public void SelectPowerPunch(string key)
	{
		foreach( GameObject item in power_punch_icons_){
			item.GetComponent<PowerPunchItemButtonPrefab>().Unselect();
		}
		
		player_data_.SetValue("power_punch_equipped",key);
		PlayDDAnimation(key);
		
		//Analytics
		int count = configuration_.Count;
		string to_remove = "";
		for( int i = 0; i < count; i++)
		{
			if (configuration_[i].Contains("power_punch") == true)
				to_remove = configuration_[i];
		}
		
		if (to_remove != "")
			configuration_.Remove(to_remove);
		
		Dictionary<string, string> dict = new Dictionary<string, string>();
  		dict.Add("resource_type", "power_punch");
		dict.Add("resource_subtype", key);
		dict.Add("resource_quantity", "1");
		
		configuration_.Add(Json.Serialize(dict));
		//
	}
	
	
	private void PlayDDAnimation(string power_punch_name){
		return;
		if (power_punch_name == "great_ball_of_fire"){
			pp_great_ball_container_.SetActive(true);
			pp_atatata__container_.SetActive(false);
			pp_huge_punch__container_.SetActive(false);
			//GameObject.Find("PPGreatBallOfFireContainer/PPGreatBallLoadingAnimatedSprite").animation.Play();
			
		}
		else if (power_punch_name == "aaatatata"){
			pp_great_ball_container_.SetActive(false);
			pp_atatata__container_.SetActive(true);
			pp_huge_punch__container_.SetActive(false);
			//GameObject.Find("PPAtatataContainer/PPAtatataLoadingAnimatedSprite").animation.Play();
		}	
		else if (power_punch_name == "huge_punch"){
			pp_great_ball_container_.SetActive(false);
			pp_atatata__container_.SetActive(false);
			pp_huge_punch__container_.SetActive(true);
			//GameObject.Find("PPHugePunchContainer/PPHugePunchLoadingAnimatedSprite").animation.Play();
		}		
	}
	
	
	private void InitUtilities()
	{
		List<JSONNode> items_info_list = store_.GetStoreItemsInfoForCategory("utility");
		
		int xPos = 0;
		int xSpace = 44;
		
		int i = 0;
		
		GameObject parent1 = GameObject.Find("UtilitiesContainer");
		
		foreach (JSONNode item_info in items_info_list){
			if ( (bool) Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsMultiplayer") == false || 
				((bool) Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsMultiplayer") == true && (item_info["key"].ToString() == "life_boost" || item_info["key"].ToString() == "att_boost")))
			{	
				xPos += xSpace;
				GameObject item = (GameObject) Instantiate(utility_item_button_prefab_, 
					new Vector3(parent1.transform.position.x + xPos,parent1.transform.position.y,parent1.transform.position.z+1),
					Quaternion.identity);
				
				item.transform.parent = parent1.transform;
				utilities_icons_.Add(item);
				
				bool is_locked = false;
				if ( (bool) Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsMultiplayer") == false)
					is_locked = store_.IsItemLocked(item_info["key"].ToString());
				
				item.GetComponent<UtilityItemButtonPrefab>().Init(item_info["key"], is_locked,false);
				//item.GetComponent<UtilityItemButtonPrefab>().Init(item_info["key"],false,false);
				i++;
			}	
		};
		
		utilities_icons_[0].GetComponent<UtilityItemButtonPrefab>().Select();
	}
	
	
	public void SelectUtility(string key, bool is_bought, bool is_locked){
		selected_utility_key_ = key;
		
		foreach( GameObject item in utilities_icons_){
			item.GetComponent<UtilityItemButtonPrefab>().Unselect();
		}
		
		string cost_text = "-----";
		if (is_locked == false)
			cost_text = FormattingWithZeros(store_info_[key]["cost"].AsInt.ToString(),4);
		
		if (is_bought == true)
			cost_text = "-----";
		
		GameObject.Find("MainCamera/UtilitiesBuyButtonContainer/TextMesh").GetComponent<tk2dTextMesh>().text = cost_text;
		GameObject.Find("MainCamera/UtilitiesBuyButtonContainer/TextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		
		if(is_bought == false)
		{
			GameObject.Find("MainCamera/UtilitiesBuyButtonContainer/DisabledBuyButtonSprite").renderer.enabled = false;
			GameObject.Find("MainCamera/UtilitiesBuyButtonContainer/BuyButtonSprite").SetActive(true);
		}
		
		
		if (is_bought == true ||  is_locked == true)
		{
			GameObject.Find("MainCamera/UtilitiesBuyButtonContainer/DisabledBuyButtonSprite").renderer.enabled = true;
			GameObject.Find("MainCamera/UtilitiesBuyButtonContainer/BuyButtonSprite").SetActive(false);
		}
		
		
		if (is_locked == false){
			GameObject.Find("MainCamera/BackgroundUtilitiesContainer/DescriptionTextMesh").GetComponent<tk2dTextMesh>().text = store_info_[key]["description"].ToString().ToUpper();
			GameObject.Find("MainCamera/BackgroundUtilitiesContainer/DescriptionTextMesh").GetComponent<tk2dTextMesh>().Commit();
		}
		else{
			GameObject.Find("MainCamera/BackgroundUtilitiesContainer/DescriptionTextMesh").GetComponent<tk2dTextMesh>().text = store_info_[key]["name"].ToString().ToUpper();
			GameObject.Find("MainCamera/BackgroundUtilitiesContainer/DescriptionTextMesh").GetComponent<tk2dTextMesh>().Commit();
		}
			
	}
	
	
	private void BuyUtility(){
		Messenger.Broadcast("UtilityBuyButtonPressed", selected_utility_key_);
		
		//Analytics
		Dictionary<string, string> dict = new Dictionary<string, string>();
  		dict.Add("resource_type", "utility");
		dict.Add("resource_subtype", selected_utility_key_);
		dict.Add("resource_quantity", "1");
 
		configuration_.Add(Json.Serialize(dict));
	}
	
	
	
	public void ToggleButtons(bool enabled){
		foreach( tk2dButton button in gameObject.GetComponentsInChildren<tk2dButton>())
		{
			button.enabled = enabled;
		}
	}
	
	private void LockInterface(){
		ToggleButtons(false);
	}
	
	private void UnlockInterface(){
		ToggleButtons(true);
	}
	
	/*
	private void DummyButtonPressed(){
		//Here because required to have a listener.
	}
	*/
	
	private void OnDestroy()
	{
		Messenger.RemoveListener("PopUpOpen", LockInterface);
		Messenger.RemoveListener("PopUpClosed", UnlockInterface);
		Messenger.RemoveListener<string>("PowerPunchSelected", SelectPowerPunch);
		Messenger.RemoveListener<string,bool,bool>("UtilitySelected", SelectUtility);
	}
	
	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");

		return txt;
	}
	
	private void OnFreeWhiteCoinsLeftToday(ResponseStatus status, JSONResult data)	
	{
		if(status == ResponseStatus.Success)
		{			
			string date_time = System.DateTime.Now.ToString().Replace(':', '#');
			date_time = date_time.Replace(' ', '_');
			
			player_data_.SetValue("last_white_coins_call", date_time);

			player_data_.SetValue("free_white_coins_per_day", data.As<long>());
			Debug.Log("Daily White Milk Coins"+data.As<long>());
		}
		else
		{
			Debug.Log("Error Connecting - OnFreeWhiteCoinsLeftToday - Not blocking UI "+data.ErrorMessage);
		}
	}
	
	public void PlayIntroAnimation()
	{
		GetComponent<Animation>().Play();
	}
}
