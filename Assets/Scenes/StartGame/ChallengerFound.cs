using UnityEngine;
using System.Collections;

public class ChallengerFound : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void Init(string contender_name, string contender_level)
	{
		GameObject.Find("MyNameTextMesh").GetComponent<tk2dTextMesh>().text = contender_name;
		GameObject.Find("MyNameTextMesh").GetComponent<tk2dTextMesh>().Commit();
		GameObject.Find("LevelNumberTextMesh").GetComponent<tk2dTextMesh>().text = contender_level;
		GameObject.Find("LevelNumberTextMesh").GetComponent<tk2dTextMesh>().Commit();
	}
	
	public void EndAnimationCallback()
	{
		GameObject.Find("MainCamera").GetComponent<StartGame>().PlayIntroAnimation();
		gameObject.SetActive(false);
	}
}
