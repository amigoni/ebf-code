using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using SimpleJSON;
using EbFightAPI;
using MiniJSON;
using wmf.KeenIOEvents;

public class BossDetail : MonoBehaviour
{
	
	public GameObject upgrade_item_;
	public GameObject buy_coins_prefab_;
	public GameObject square_icon_button_prefab_;
	public GameObject loading_page_prefab_;
	public GameObject boss_detail_tutorial_;
	private GameObject weapon_pop_up_;
	public GameObject tutorial_popup_;
	private PlayerData player_data_;
	private BossPowerManager boss_power_manager_;
	private JSONNode dd_info_;
	private Store store_; //Dynamic Data for each item
	private JSONNode store_info_; //Static info for each item
	private EbFightAPI.Boss boss_data_;
	private int current_round_number_ = 0;
	private List<GameObject> stats_row_ = new List<GameObject> (); 
	
	//***BUILD A BOSS
	public GameObject boss_level_prefab_;
	public GameObject boss_round_adjustable_parameter_prefab_;
	public GameObject weapon_popup_prefab_;
	private GameObject build_your_boss_tab_container_;
	private GameObject boss_stats_tab_container_;
	private List<GameObject> right_panel_rows_ = new List<GameObject> ();
	private List<GameObject> rounds_rows_ = new List<GameObject> ();
	private GameObject power_row_;
	private GameObject att_row_;
	private GameObject def_row_;
	
	//***UTILITIES
	public GameObject boss_utilities_tab_container_;
	public GameObject boss_utility_item_prefab_;
	private List<GameObject> utilities_icons_ = new List<GameObject> ();
	private string current_utility_key_;
	private GameObject boss_stats_container_;
	private GameObject boss_stats_tab_button_container_;
	private GameObject boss_training_container;
	private GameObject boss_training_tab_button_container;
	private GameObject boss_utilities_container;
	private GameObject boss_utilities_tab_button_container;
	private GameObject build_your_boss_container;
	private GameObject build_your_boss_tab_button_container;
	
	void Awake ()
	{
		build_your_boss_tab_container_ = GameObject.Find ("BuildYourBossTabContainer");
		boss_stats_tab_container_ = GameObject.Find ("BossStatsTabContainer");
		
		player_data_ = Framework.Instance.InstanceComponent<PlayerData> ();
		boss_power_manager_ = GameObject.Find ("MainCamera/BuildYourBossTabContainer").GetComponent<BossPowerManager> ();
		dd_info_ = JSON.Parse (Framework.Instance.resouces_manager ().GetStringResource ("Config/DDInfo"));
		store_info_ = JSON.Parse (Framework.Instance.resouces_manager ().GetStringResource ("Config/StoreInfo"));
		store_ = Framework.Instance.InstanceComponent<Store> ();
		store_.SetTempWhiteCoins ();
		
		Messenger.AddListener<int> ("LevelSelected", UpdateRightPanel);
		Messenger.AddListener<string,float> ("UpdatePowerIndicator", UpdatePowerIndicator);
		Messenger.AddListener ("LevelWeaponIconPressed", OpenWeaponPopUp);
		Messenger.AddListener<int> ("PressedAdjustableParameterChange", UpdateRightPanel);
		Messenger.AddListener<string> ("BossUtilitySelected", SelectUtility);
		Messenger.AddListener<string> ("UpgradeSuccessful", RefreshPowerIndicator);
		//Messenger.AddListener("WeaponSelected", IncreaseTutorialStep);
		
		boss_stats_container_ = GameObject.Find ("BossStatsButtonContainer/BossStatsContainer");
		boss_stats_tab_button_container_ = GameObject.Find ("BossStatsButtonContainer/BossStatsTabButtonContainer");
		boss_training_container = GameObject.Find ("BossTrainingButtonContainer/BossTrainingContainer");
		boss_training_tab_button_container = GameObject.Find ("BossTrainingButtonContainer/BossTrainingTabButtonContainer");
		boss_utilities_container = GameObject.Find ("BossUtilitiesButtonContainer/BossUtilitiesContainer");
		boss_utilities_tab_button_container = GameObject.Find ("BossUtilitiesButtonContainer/BossUtilitiesTabButtonContainer");
		build_your_boss_container = GameObject.Find ("BuildYourBossButtonContainer/BuildYourBossContainer");
		build_your_boss_tab_button_container = GameObject.Find ("BuildYourBossButtonContainer/BuildYourBossTabButtonContainer");
		
		DisplayThisTab ("BossStatsTabContainer");
	}
	
	void Start ()
	{
		Framework.Instance.InstanceComponent<ServerAPI> ().GetBossConfiguration (OnGetBossConfiguration);
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if(Game.FindObjectOfType( typeof(TutorialPopup) ) == null)
			if (Input.GetKeyDown(KeyCode.Escape))
				BackPressed();
	}
#endif
	
	private void OnGetBossConfiguration (ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success) {
			boss_data_ = data.As<EbFightAPI.Boss> ();
			Framework.Instance.InstanceComponent<ServerAPI> ().SaveBoss (boss_data_);
			GetComponentInChildren<BossPowerManager> ().InitBoss (boss_data_);
			GameObject loading_page_prefab = (GameObject)Instantiate (loading_page_prefab_, new Vector3 (0, 0, 1), Quaternion.identity);
			Init ();
			Destroy (loading_page_prefab);
		} else {
			Debug.Log ("Error Connecting - OnGetBossConfiguration");
			Application.LoadLevel ("MainMenu");
		}	
		
	}
	
	private void OnUpdateBossConfiguration (ResponseStatus status, JSONResult data)
	{
		EbFightAPI.Boss boss_data;
		
		if (status == ResponseStatus.Success) {
			store_.ReconcileWhiteCoinsToTemp ();
			Framework.Instance.InstanceComponent<ServerAPI> ().SaveBoss (data.As<EbFightAPI.Boss> ());
			boss_data = data.As<EbFightAPI.Boss> ();
			
			Destroy (weapon_pop_up_);
			
			Application.LoadLevel ("MainMenu");
		} else {
			//TODO warnig
			boss_data = boss_data_;
			Debug.Log ("Error Connecting - OnUpdateBossConfiguration");
			Framework.Instance.InstanceComponent<ServerAPI> ().pop_up_window ().GetComponent<PopUpWindowPrefab> ().Init ("We can't connect", "Would you like to try again?",
				BackPressed,
				OnNoCallback);
			Framework.Instance.InstanceComponent<ServerAPI> ().pop_up_window ().GetComponent<PopUpWindowPrefab> ().set_destroy_on_closing (false);
		}
		
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
		wmf.KeenIOEvents.BossConfiguration boss_configuration = new wmf.KeenIOEvents.BossConfiguration ();
		
		analitycs_.AddEvent (KeenIOEventType.boss_configuration);
		analitycs_.AddNewKey (KeenIOEventType.boss_configuration, "round_number", null);
		analitycs_.AddNewKey (KeenIOEventType.boss_configuration, "resources", boss_configuration.SerializeBossConfiguration (boss_data));
		analitycs_.AddNewKey (KeenIOEventType.boss_configuration, "change_id", boss_configuration.GetUniqueRunId ());
		analitycs_.SendEvent (KeenIOEventType.boss_configuration);			
	}
	
	void OnNoCallback ()
	{
		Application.LoadLevel ("MainMenu");
	}
	
	//Tutorial
	void StatsQuestionMarkPressed ()
	{
		GameObject tutorial_popup = (GameObject)Instantiate (tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup> ().Init (TutorialPopUpType.boss_stats_, 2F);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void BuildQuestionMarkPressed ()
	{
		GameObject tutorial_popup = (GameObject)Instantiate (tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup> ().Init (TutorialPopUpType.boss_build_, 2F);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void UtilitiesQuestionMarkPressed ()
	{
		GameObject tutorial_popup = (GameObject)Instantiate (tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup> ().Init (TutorialPopUpType.boss_utilities_, 2F);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void TrainingQuestionMarkPressed ()
	{
		GameObject tutorial_popup = (GameObject)Instantiate (tutorial_popup_);
		tutorial_popup.GetComponent<TutorialPopup> ().Init (TutorialPopUpType.boss_training_, 2F);
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void Init ()
	{		
		InitBuyCoinsButton (375, 290);
		
		//Stats
		UpdateStatsText ();
		
		//Upgradable Rows
		CreateStatsRows ();
		
		//Build a Boss
		CreateRightPanelParameter (0, "boss_round_reload_speed");
		CreateRightPanelParameter (1, "boss_round_shots_per_wave");
		CreateRightPanelParameter (2, "boss_round_environment");
		
		int offset_j = 0;
		foreach (EbFightAPI.StageInfo round in boss_data_.Stages) {
			GameObject level_row = (GameObject)Instantiate (boss_level_prefab_, new Vector3 (142, 156 - offset_j * 63, build_your_boss_tab_container_.transform.position.z - 2), Quaternion.identity);
			level_row.transform.parent = GameObject.Find ("MainCamera/BuildYourBossTabContainer").transform;
			level_row.GetComponent<BossLevelPrefab> ().Init (offset_j);
			
			rounds_rows_.Add (level_row);
			
			if (offset_j == 0)
				level_row.GetComponent<BossLevelPrefab> ().Selected ();
			
			offset_j++;
		}
		
		InitUtilitiesTab ();
		
		DisplayThisTab ("BossStatsTabContainer");
		
		weapon_pop_up_ = (GameObject)Instantiate (weapon_popup_prefab_, new Vector3 (0, 0, 5), Quaternion.identity);
		weapon_pop_up_.GetComponent<WeaponPopUp> ().Init (current_round_number_);
		weapon_pop_up_.GetComponent<WeaponPopUp> ().ClosePopUp ();
	}
	
	void UpdateStatsText ()
	{
		
		GameObject.Find ("HPNumTextMesh").GetComponent<tk2dTextMesh> ().text = store_info_ ["boss_level_upgrade"] ["upgrades"] [(int)boss_data_.Level] ["effect"].AsInt.ToString ().ToString ();
		GameObject.Find ("HPNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		float boss_max_xp = store_info_ ["boss_level_upgrade"] ["upgrades"] [(int)boss_data_.Level] ["xp_max"].AsFloat;
		float boss_min_xp = store_info_ ["boss_level_upgrade"] ["upgrades"] [(int)boss_data_.Level] ["xp_min"].AsFloat - 1;
		if (boss_min_xp < 0)
			boss_min_xp = 0;
		
		float xp_range = boss_max_xp - boss_min_xp + 1;
		float xp_relative = boss_data_.Xp - boss_min_xp;
		
		if (boss_data_.Level <= 9) {
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().text = (xp_relative.ToString () + "/" + xp_range.ToString ());
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
			GameObject.Find ("XPSlicedSprite").GetComponent<tk2dSlicedSprite> ().dimensions = new Vector2 (xp_relative / xp_range * 193F, 10);
		} else {
			GameObject.Find ("XPSlicedSprite").GetComponent<tk2dSlicedSprite> ().dimensions = new Vector2 (193.0F, 10F);
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().text = "Max";
			GameObject.Find ("XPTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		}
		
		string boss_data_level_str = wmf.ui.Utility.FormattingWithZeros (((int)boss_data_.Level).ToString (), 2);
		GameObject.Find ("LevelNumTextMesh").GetComponent<tk2dTextMesh> ().text = boss_data_level_str;
		GameObject.Find ("LevelNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		GameObject.Find ("BRNumTextMesh").GetComponent<tk2dTextMesh> ().text = boss_data_.Rank.ToString ();
		GameObject.Find ("BRNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
	}
	
	void CreateStatsRows ()
	{
		
		foreach (GameObject row in stats_row_)
			Destroy (row);
		
		stats_row_ = new List<GameObject> ();
		
		power_row_ = CreateStatsRow ("boss_power_level", -20);
		def_row_ = CreateStatsRow ("boss_def", -60);
		att_row_ = CreateStatsRow ("boss_att", -100);
	}
	
	GameObject CreateStatsRow (string stat_name, float y)
	{
		GameObject stat_row = (GameObject)Instantiate (upgrade_item_, 
			new Vector3 (0, 0, GameObject.Find ("BossStatsTabContainer").transform.position.z),
			Quaternion.identity);
		stat_row.GetComponent<tk2dCameraAnchor> ().tk2dCamera = GameObject.Find ("MainCamera").GetComponent<tk2dCamera> ();
		stat_row.GetComponent<tk2dCameraAnchor> ().offset = new Vector2 (43, y);
		stat_row.GetComponent<UpgradeStatsItemPrefab> ().Init (stat_name, true, false, boss_data_);
		stat_row.transform.parent = GameObject.Find ("BossStatsTabContainer").transform;
		stats_row_.Add (stat_row);
		
		return stat_row;
	}
	
	void UpdatePowerIndicator (string text, float portion)
	{
		GameObject.Find ("MainCamera/BossStatsTabContainer/BossPowerContainer/PowerNumTextMesh").GetComponent<tk2dTextMesh> ().text = text;
		GameObject.Find ("MainCamera/BossStatsTabContainer/BossPowerContainer/PowerNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		GameObject.Find ("MainCamera/BossStatsTabContainer/BossPowerContainer/PowerSlicedSprite").GetComponent<tk2dSlicedSprite> ().dimensions = new Vector2 (portion * 57F, 9F);

			
		GameObject.Find ("MainCamera/BuildYourBossTabContainer/BossPowerContainer/PowerNumTextMesh").GetComponent<tk2dTextMesh> ().text = text;
		GameObject.Find ("MainCamera/BuildYourBossTabContainer/BossPowerContainer/PowerNumTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		GameObject.Find ("MainCamera/BuildYourBossTabContainer/BossPowerContainer/PowerSlicedSprite").GetComponent<tk2dSlicedSprite> ().dimensions = new Vector2 (portion * 57F, 9F);
	}
	
	void RefreshPowerIndicator (string key)
	{
		if (key == "boss_power_level") {
			boss_power_manager_.UpdateMaxPower ();
			UpdateRightPanel (current_round_number_);
		}
	}
	
	void OpenWeaponPopUp ()
	{
		weapon_pop_up_.GetComponent<WeaponPopUp> ().Show (current_round_number_);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
		analitycs_.AddEvent (KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "BossDetailScene");
		analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "WeaponPopUp");
		analitycs_.SendEvent (KeenIOEventType.menu_opened);
	}
	
	void InitBuyCoinsButton (float x, float y)
	{
		GameObject button = (GameObject)Instantiate (buy_coins_prefab_, new Vector3 (x, y, 25), Quaternion.identity);
		button.transform.parent = GameObject.Find ("MainCamera").transform;
		button.GetComponent<BuyCoinsPrefab> ().Init (true, 124F, -32F);
	}
	
	void BackPressed ()
	{
		Framework.Instance.InstanceComponent<ServerAPI> ().UpdateBossConfiguration (boss_data_, OnUpdateBossConfiguration); 
		Debug.Log ("BackPressed post call");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void CharacterStatsTabPressed ()
	{	
		DisplayThisTab ("BossStatsTabContainer");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void ResetBuilYourBossAllRounds ()
	{
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
		boss_power_manager_.ResetAllRounds ();
		UpdateRightPanel (current_round_number_);
	}
	
	void OnBeginTraining (ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success) {
			weapon_pop_up_.GetComponent<WeaponPopUp> ().ClosePopUp ();
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add("MatchSession", data.As<MatchSession>());
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().ChangeValue ("IsTraining", true);
			
			//Analitycs
			Framework.Instance.InstanceComponent<Analitycs>().NewTrainingRun();
			Framework.Instance.InstanceComponent<Analitycs>().IncreaseCommonValue("num_pve_played");
			
			Application.LoadLevel ("LoadingScene");
		}
	}
	
	void BuildYourBossTabPressed ()
	{

		DisplayThisTab ("BuildYourBossTabContainer");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void BossUtilitiesTabPressed ()
	{
		DisplayThisTab ("BossUtilitiesTabContainer");	
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
			
		foreach (GameObject round_row in rounds_rows_) {
			round_row.GetComponent<BossLevelPrefab> ().ToggleTouchEnabled (true);
		}
			
		foreach (GameObject right_row in right_panel_rows_) {
			right_row.GetComponent<BossRoundAdjustableParameterPrefab> ().ToggleTouchEnabled (true);	
		}	
	}
	
	void BossTrainingTabPressed ()
	{
		DisplayThisTab ("BossTrainingTabContainer");
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void StartTrainingPressed()
	{
		Framework.Instance.InstanceComponent<ServerAPI> ().UpdateBossConfiguration (boss_data_, OnUpdateBossConfigurationPreTraining); 	
	}
	
	private void OnUpdateBossConfigurationPreTraining (ResponseStatus status, JSONResult data)
	{
		EbFightAPI.Boss boss_data;
		
		if (status == ResponseStatus.Success) {
			store_.ReconcileWhiteCoinsToTemp ();
			Framework.Instance.InstanceComponent<ServerAPI> ().SaveBoss (data.As<EbFightAPI.Boss> ());
			boss_data = data.As<EbFightAPI.Boss> ();
			
			Framework.Instance.InstanceComponent<ServerAPI>().BeginTraining(OnBeginTraining);
		} else {
			//TODO warnig
			boss_data = boss_data_;
			Debug.Log ("Error Connecting - OnUpdateBossConfiguration");
			Framework.Instance.InstanceComponent<ServerAPI> ().pop_up_window ().GetComponent<PopUpWindowPrefab> ().Init ("We can't connect", "Would you like to try again?",
				BackPressed,
				OnNoCallback);
			Framework.Instance.InstanceComponent<ServerAPI> ().pop_up_window ().GetComponent<PopUpWindowPrefab> ().set_destroy_on_closing (false);
		}
	}
	
	public void DisplayTab (string tab, bool setting)
	{
		int zPos = 20;
		if (setting == false)
			zPos = -100;
		GameObject.Find (tab).transform.position = new Vector3 (GameObject.Find (tab).transform.position.x, GameObject.Find (tab).transform.position.y, zPos);
	}
	
	private void ResetButtons()
	{
		boss_stats_container_.SetActive (false);
		boss_stats_tab_button_container_.SetActive (true);
		build_your_boss_container.SetActive (false);
		build_your_boss_tab_button_container.SetActive (true);
		boss_utilities_container.SetActive (false);
		boss_utilities_tab_button_container.SetActive (true);
		boss_training_container.SetActive (false);
		boss_training_tab_button_container.SetActive (true);
	}
	
	private void DisplayThisTab (string tab)
	{
		if (tab == "BossStatsTabContainer") {
			ResetButtons();
			boss_stats_container_.SetActive (true);
			boss_stats_tab_button_container_.SetActive (false);
			
			DisplayTab ("BossStatsTabContainer", true);
			DisplayTab ("BuildYourBossTabContainer", false);
			DisplayTab ("BossUtilitiesTabContainer", false);
			DisplayTab ("BossTrainingTabContainer", false);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
			analitycs_.AddEvent (KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "BossDetailScene");
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "BossStatsTab");
			analitycs_.SendEvent (KeenIOEventType.menu_opened);
		} else if (tab == "BuildYourBossTabContainer") {
			ResetButtons();
			build_your_boss_container.SetActive (true);
			build_your_boss_tab_button_container.SetActive (false);
			
			DisplayTab ("BossStatsTabContainer", false);
			DisplayTab ("BuildYourBossTabContainer", true);
			DisplayTab ("BossUtilitiesTabContainer", false);
			DisplayTab ("BossTrainingTabContainer", false);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
			analitycs_.AddEvent (KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "BossDetailScene");
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "BuildYourBossTab");
			analitycs_.SendEvent (KeenIOEventType.menu_opened);
		} else if (tab == "BossUtilitiesTabContainer") {
			/*
			GameObject.Find("BossStatsButtonSprite").GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("BossStatsSprite").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("BuildYourBossButtonSprite").GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("BuildYourBossSprite").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("BossUtilitiesButtonSprite").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("BossUtilitiesSprite").GetComponent<MeshRenderer>().enabled = true;
			*/
			ResetButtons();
			boss_utilities_container.SetActive (true);
			boss_utilities_tab_button_container.SetActive (false);
			
			DisplayTab ("BossStatsTabContainer", false);
			DisplayTab ("BuildYourBossTabContainer", false);
			DisplayTab ("BossUtilitiesTabContainer", true);
			DisplayTab ("BossTrainingTabContainer", false);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
			analitycs_.AddEvent (KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "BossDetailScene");
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "BossUtilitiesTab");
			analitycs_.SendEvent (KeenIOEventType.menu_opened);
		} else if (tab == "BossTrainingTabContainer") {
			/*
			GameObject.Find("BossStatsButtonSprite").GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("BossStatsSprite").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("BuildYourBossButtonSprite").GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("BuildYourBossSprite").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("BossUtilitiesButtonSprite").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("BossUtilitiesSprite").GetComponent<MeshRenderer>().enabled = true;
			*/
			ResetButtons();
			boss_training_container.SetActive (true);
			boss_training_tab_button_container.SetActive (false);

			DisplayTab ("BossStatsTabContainer", false);
			DisplayTab ("BuildYourBossTabContainer", false);
			DisplayTab ("BossUtilitiesTabContainer", false);
			DisplayTab ("BossTrainingTabContainer", true);
			
			//Analytics
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
			analitycs_.AddEvent (KeenIOEventType.menu_opened);	
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "BossDetailScene");
			analitycs_.AddNewKey (KeenIOEventType.menu_opened, "sub_menu_name", "BossTrainingTab");
			analitycs_.SendEvent (KeenIOEventType.menu_opened);
		}
	}
	
	void BuyBossUpgradePressed ()
	{
		if (boss_data_.Level < store_info_ ["boss_level_upgrade"] ["upgrades"].Count - 1) {
			bool purchase = Framework.Instance.InstanceComponent<Store> ().BuyItem ("boss_level_upgrade", 1);
			
			if (purchase == true) {
				UpdateStatsText ();
				CreateStatsRows ();
				boss_power_manager_.RefreshPowerIndicatorValue ();
				
				Framework.Instance.audio_manager ().PlayUISound ("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
			}
		}	
	}
	
	void CreateRightPanelParameter (int position, string key)
	{
		GameObject parameter = (GameObject)Instantiate (boss_round_adjustable_parameter_prefab_, new Vector3 (404, 156 - position * 62, build_your_boss_tab_container_.transform.position.z - 2), Quaternion.identity);
		parameter.transform.parent = GameObject.Find ("MainCamera/BuildYourBossTabContainer").transform;
		parameter.GetComponent<tk2dCameraAnchor> ().offset.y = 156 - position * 63;
		parameter.GetComponent<BossRoundAdjustableParameterPrefab> ().Init (current_round_number_, key);
		right_panel_rows_.Add (parameter);	
	}
	
	void UpdateRightPanel (int round_number)
	{
		foreach (GameObject row in rounds_rows_) {
			if (row.GetComponent<BossLevelPrefab> ().GetRound () != round_number)
				row.GetComponent<BossLevelPrefab> ().Unselect ();
		}
		
		current_round_number_ = round_number;
		boss_power_manager_.ChangeRound (current_round_number_);
		
		foreach (GameObject item in right_panel_rows_) {
			item.GetComponent<BossRoundAdjustableParameterPrefab> ().Refresh (round_number);
		}
	}
	
	void RefreshBossData ()
	{
		//UpdateRightPanel(current_round_number_);
	}
	
	
	
	//*****UTILITIES TAB*****
	void InitUtilitiesTab ()
	{
		CreateUtilityIcon ("boss_utilities_att", 159, true);
		CreateUtilityIcon ("boss_utilities_def", 240, false);
		CreateUtilityIcon ("boss_utilities_time", 321, false);
		
		current_utility_key_ = "boss_utilities_att";
		utilities_icons_ [0].GetComponent<BossUtilityItemPrefab> ().Select ();
		
		InvokeRepeating ("CheckIfUtilityIsPurchasable", 0F, 1.0F);
	}
	
	void CreateUtilityIcon (string item_key, int x, bool selected)
	{
		GameObject icon = (GameObject)Instantiate (boss_utility_item_prefab_, new Vector3 (x, 195, boss_utilities_tab_container_.transform.position.z + 1), Quaternion.identity);
		icon.transform.parent = boss_utilities_tab_container_.transform;
		icon.GetComponent<BossUtilityItemPrefab> ().Init (item_key, selected, false);	
		utilities_icons_.Add (icon);
	}
	
	void SelectUtility (string item_key)
	{
		current_utility_key_ = item_key;
		
		//Unselect all Icons
		foreach (GameObject icon in utilities_icons_) {
			if (icon.GetComponent<BossUtilityItemPrefab> ().GetKey () != current_utility_key_)
				icon.GetComponent<BossUtilityItemPrefab> ().Unselect ();
		}
		
		//Change Title & move Arrow
		Transform arrow = boss_utilities_tab_container_.transform.Find ("ArrowSprite");
		
		float arrow_x_position = -81;
		if (item_key == "boss_utilities_att") {
			arrow_x_position = -81;
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle1Sprite").gameObject.SetActive(true);
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle2Sprite").gameObject.SetActive(false);
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle3Sprite").gameObject.SetActive(false);
		} else if (item_key == "boss_utilities_def") {
			arrow_x_position = 0;
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle1Sprite").gameObject.SetActive(false);
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle2Sprite").gameObject.SetActive(true);
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle3Sprite").gameObject.SetActive(false);
		} else if (item_key == "boss_utilities_time") {
			arrow_x_position = 81;
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle1Sprite").gameObject.SetActive(false);
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle2Sprite").gameObject.SetActive(false);
			boss_utilities_tab_container_.transform.Find ("UtilitiesTitleContainer/UtilityTitle3Sprite").gameObject.SetActive(true);
		}
		
		//Move Arrow
		boss_utilities_tab_container_.transform.Find ("ArrowSprite").gameObject.GetComponent<tk2dCameraAnchor> ().offset.x = arrow_x_position;		
		
		//Change Description
		boss_utilities_tab_container_.transform.Find ("DescriptionTextMesh").GetComponent<tk2dTextMesh> ().text = store_info_ [item_key] ["description"];
		boss_utilities_tab_container_.transform.Find ("DescriptionTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		//Update Price
		string cost_str = wmf.ui.Utility.FormattingWithZeros (store_info_ [item_key] ["cost"].ToString (), 8);
		boss_utilities_tab_container_.transform.Find ("LabelCoinsContainer/UtilitiesCoinsTextMesh").GetComponent<tk2dTextMesh> ().text = cost_str;
		boss_utilities_tab_container_.transform.Find ("LabelCoinsContainer/UtilitiesCoinsTextMesh").GetComponent<tk2dTextMesh> ().Commit ();
		
		//Update info for buy button
		CheckIfUtilityIsPurchasable ();
	}
	
	void CheckIfUtilityIsPurchasable ()
	{
		foreach (GameObject icon in utilities_icons_) {
			if (icon.GetComponent<BossUtilityItemPrefab> ().GetKey () == current_utility_key_) {
				if (icon.GetComponent<BossUtilityItemPrefab> ().IsPurchaseEnabled () == true) {
					boss_utilities_tab_container_.transform.Find ("BuyButtonExpiresContainer/BuyButtonSprite").gameObject.SetActive(true);
					//boss_utilities_tab_container_.transform.Find ("BuyButtonExpiresContainer/BuyButtonSprite").GetComponent<tk2dButton> ().enabled = true;
					boss_utilities_tab_container_.transform.Find ("BuyButtonExpiresContainer/LockedSprite").gameObject.SetActive(false);
				} else {
					boss_utilities_tab_container_.transform.Find ("BuyButtonExpiresContainer/BuyButtonSprite").gameObject.SetActive(false);
					//boss_utilities_tab_container_.transform.Find ("BuyButtonExpiresContainer/BuyButtonSprite").GetComponent<tk2dButton> ().enabled = false;
					boss_utilities_tab_container_.transform.Find ("BuyButtonExpiresContainer/LockedSprite").gameObject.SetActive(true);
				}	
			}	
		}
	}
	
	void BuyCurrentUtility ()
	{
		bool purchase = store_.BuyItem (current_utility_key_, 1);
		
		if (purchase == true) {
			foreach (GameObject icon in utilities_icons_) {
				icon.GetComponent<BossUtilityItemPrefab> ().Refresh ();
			}
			SelectUtility (current_utility_key_);
			Framework.Instance.audio_manager ().PlayUISound ("40_Menu Buy Upgrade", 1.0F, 1.0F, false);
		}/*
		else
			GameObject.Find("MainCamera").GetComponent<PopUpWindowCoinsStore>().Init();
		*/	
	
	}
	
	public void ClearBossUtility (string key)
	{		
		EbFightAPI.Utility utility_to_remove = new EbFightAPI.Utility ();
		
		for (int i=0; i < boss_data_.Utilities.Count; i++) {
			if (boss_data_.Utilities [i].Kind == key)
				utility_to_remove = boss_data_.Utilities [i];	
		}
		
		boss_data_.Utilities.Remove (utility_to_remove);
	}
	
	
	//***********************
	
	
	public int GetCurrentSelectedLevelNumber ()
	{
		return current_round_number_;
	}
	
	public EbFightAPI.Boss GetBossData ()
	{
		return boss_data_;
	}
	
	void OnDestroy ()
	{
		Messenger.RemoveListener<int> ("LevelSelected", UpdateRightPanel);
		//Messenger.AddListener("LevelParameterChanged", RefreshBossData);
		Messenger.RemoveListener<string,float> ("UpdatePowerIndicator", UpdatePowerIndicator);
		Messenger.RemoveListener ("LevelWeaponIconPressed", OpenWeaponPopUp);
		Messenger.RemoveListener<int> ("PressedAdjustableParameterChange", UpdateRightPanel);
		Messenger.RemoveListener<string> ("BossUtilitySelected", SelectUtility);
		Messenger.RemoveListener<string> ("UpgradeSuccessful", RefreshPowerIndicator);
		//Messenger.RemoveListener("WeaponSelected", IncreaseTutorialStep);
	}
	
#if UNITY_EDITOR
	void OnGUI()
	{
		if(GUI.Button(new Rect(200, 0, 100, 20), "Reset Utilities!"))
			boss_data_.Utilities.Clear();
		
	}
#endif
}
