using UnityEngine;
using System.Collections;
using System;
using SimpleJSON;
using EbFightAPI;

public class EndGameMultiplayer : MonoBehaviour {

	public GameObject round_result_prefab_;
	public GameObject you_won_container_;
	public GameObject you_lost_container_;
	public GameObject dr_text_mesh_;
	public GameObject white_coins_text_mesh_;
	public GameObject white_coins_container_;

	public GameObject button_new_battle_container_;
	
	private CardsScene cards_scene_;
	
	private PlayerData player_data_;
	private JSONNode dd_info_;
	
	private tk2dTextMesh end_score_text_;
	private tk2dTextMesh high_score_text_;
	private tk2dTextMesh coins_text_;
	private long score_;
	
	private int current_coins_prize_ = 0;

	private GameObject background_music_instance_;
	
	private StaticParams static_param;
	private EbFightAPI.Player old_player_;
	private int starting_white_coins_; 
	
	void Awake()
	{	
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		dd_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/DDInfo"));
		MainMenu.currentScene_ = "EndGame";//Leo
		static_param = GameObject.Find("StaticParams").GetComponent<StaticParams>();
		score_ = static_param.score_;
		old_player_ = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer();
		
		starting_white_coins_ = player_data_.GetInt("white_coins");
		
		Framework.Instance.audio_manager().PlayBackgroundMusic("Endless Boss Menu");
		
		if((bool)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsMultiplayer"))
		{
			Framework.Instance.InstanceComponent<ServerAPI>().EndMatch(
				static_param.session_id_,
				//TO-DO Added a real result
				new string[]{static_param.dd_results_[0].ToString(), 
							 static_param.dd_results_[1].ToString(), 
				             static_param.dd_results_[2].ToString()},
				score_,
				OnEndMatch);
		}
		
		Framework.Instance.scene_manager().LoadScene("CardsScene", TransitionType.add_scene, -20);
		
		GameObject.Find("EnemyNameTextMesh").GetComponent<tk2dTextMesh>().text = GameObject.Find("StaticParams").GetComponent<StaticParams>().contender_name_;
		GameObject.Find("EnemyNameTextMesh").GetComponent<tk2dTextMesh>().Commit();
				
		Destroy(GameObject.Find("StaticParams"));
	}


	// Use this for initialization
	void Start () 
	{
		PoolManager.ClearPool();
		//Coins
		//GameObject.Find("FakeRoot/BuyCoinsPrefab").GetComponent<BuyCoinsPrefab>().Init(false, 124F, -32F);
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "EndGameMultiplayer");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
		
		//Tutorial
		//if (player_data_.GetInt("tutorial_current_step") == 1)
		/*
		if (player_data_.GetBool("tutorial_endgame_multiplayer_detail_done") == false)
		{
			button_new_battle_container_.SetActive(false);
			player_data_.SetValue("tutorial_endgame_multiplayer_detail_done",true);
			tutorial_object_.SetActive(true);
			tutorial_object_.animation.Play();
		}
		*/
		
	}

	private void OnEndMatch(ResponseStatus status, JSONResult data)	
	{
		if(status == ResponseStatus.Success)
		{
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(player);
			
			//Debug.Log("New Player Rank "+player.PlayerRank+" Old Player Rank "+old_player_.PlayerRank+" Difference :"+(player.PlayerRank - old_player_.PlayerRank));
			
			//Won/Lost
			if ( player.PlayerRank > old_player_.PlayerRank)
			{
				you_won_container_.SetActive(true);
				you_lost_container_.SetActive(false);
				//Analitycs
				Framework.Instance.InstanceComponent<Analitycs>().IncreaseCommonValue("num_pvp_won");
			}		
			else
			{
				you_won_container_.SetActive(false);
				you_lost_container_.SetActive(true);
			}	
			
			
			dr_text_mesh_.GetComponent<tk2dTextMesh>().text = FormattingWithZeros(player.PlayerRank.ToString(),4);
			dr_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			
			white_coins_text_mesh_.GetComponent<tk2dTextMesh>().text = "+"+FormattingWithZeros((player.WhiteCoins - starting_white_coins_).ToString(),4);
			white_coins_text_mesh_.GetComponent<tk2dTextMesh>().Commit();
			
			if (player.WhiteCoins - starting_white_coins_ <= 0)
				white_coins_container_.SetActive(false);
			
			player_data_.SetValue("white_coins", player.WhiteCoins);
	
			//CardsScene
			cards_scene_ = Framework.Instance.scene_manager().GetRootNode("CardsScene").GetComponent<CardsScene>();
			
			if(player.WhiteCoins - starting_white_coins_ > 0)
				cards_scene_.SetParams(CardsPrizeType.white_coins, (int)player.WhiteCoins - starting_white_coins_);
			else
			{
				if( UnityEngine.Random.value < 0.5f)
				{
					JSONNode store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
					if ( player_data_.GetInt("dd_ammo") < store_info_["dd_ammo_level"]["upgrades"][player_data_.GetInt("dd_ammo_level")]["effect"].AsInt)
						GiveAmmo();
					else
						GiveCoins();
					
				}
				else
					GiveCoins();
			}
			
			GameObject.Find("FakeRoot/BuyCoinsPrefab").GetComponent<BuyCoinsPrefab>().Init(
				124F, -32F, Convert.ToInt32(player.WhiteCoins - starting_white_coins_), static_param.coins_ + current_coins_prize_);
			
			//Analitycs
			SetEndRunParams();
			
			//Analitycs
			Framework.Instance.InstanceComponent<Analitycs>().EndRun();
		}
		else
		{
			//In Case of error for connection when battle is over
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init(
				"We can't Connect", "We can't seem to connect to the server.\nNo worries we'll send the result next time.\n Wanna try again?",
				OnYesCallback, 
				OnNoCallback);
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
		}
	}
	
	void OnYesCallback()
	{
		Framework.Instance.InstanceComponent<ServerAPI>().EndMatch(
				static_param.session_id_,
				//TO-DO Added a real result
				new string[]{static_param.dd_results_[0].ToString(), 
							 static_param.dd_results_[1].ToString(), 
				             static_param.dd_results_[2].ToString()},
				score_,
				OnEndMatch);
	}

	void OnNoCallback()
	{
		Framework.Instance.scene_manager().BackFromCurrentScene();
		Application.LoadLevel ("MainMenu");
	}
	
	
	private void GiveCoins()
	{
		current_coins_prize_ = 100 + UnityEngine.Random.Range(0,81)*5;
		player_data_.SetValue("coins", player_data_.GetInt("coins") + current_coins_prize_);
		cards_scene_.SetParams(CardsPrizeType.coins, current_coins_prize_);
		Messenger.Broadcast("RefreshCoins");
	}
	
	private void GiveAmmo()
	{
		int prize = UnityEngine.Random.Range(1,3);
		player_data_.SetValue("dd_ammo", player_data_.GetInt("dd_ammo") + prize);
		cards_scene_.SetParams(CardsPrizeType.ammo, prize);
		
		//Analytics
		wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet("ammo", prize);
		reward_set.SendEvent();
	}
	
	public void FightPressed()
	{	
		if((bool)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsMultiplayer"))
			Framework.Instance.InstanceComponent<ServerAPI>().BeginMatch(OnBeginMatch);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	private void OnBeginMatch(ResponseStatus status, JSONResult data)	
	{
		if(status == ResponseStatus.Success)
		{
			MatchSession match_session = data.As<MatchSession>();
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add("MatchSession", match_session);
			Application.LoadLevel ("StartGame");
		}
		else
		{
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("We can't Connect", "Please try again in a bit");
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().set_destroy_on_closing(false);
			Debug.Log("Error Connection - OnBeginMatch");
		}
	}
	
	
	void Round1Trigger(){
		CreateRoundPrefab(0);
	}
	
	
	void Round2Trigger(){
		CreateRoundPrefab(1);
	}
	
	
	void Round3Trigger(){
		CreateRoundPrefab(2);
	}
	
	
	void CreateRoundPrefab(int round_number)
	{
		GameObject round_result_prefab = (GameObject) Instantiate( round_result_prefab_);
		round_result_prefab.transform.parent = GameObject.Find("FakeRoot/RoundsResultsContainer").transform;
		round_result_prefab.transform.localPosition = new Vector3(
			400.0f,
			160.0f-30.0f*round_number, 
			0.0f);
		round_result_prefab.GetComponent<RoundResultPrefab>().Init(static_param.dd_results_[round_number]);
		
		round_result_prefab.GetComponentInChildren<Animation>().Play("RoundsResultsAnimation");
		
		Framework.Instance.audio_manager().Play("16_Combo Hit MIssile "+(round_number+1)+"v", transform, 1.0F, 1.0F, false);
	}
	
	
	public void BackPressed()
	{
		Application.LoadLevel ("MainMenu");
		//Framework.Instance.scene_manager().AssignsCameraToAnchors("EndGameMultiplayer");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}


	string FormattingWithZeros(string txt, int digits)
	{		
		int zeros = digits - txt.Length;
		for (int i = 0; i < zeros; i++)
			txt = txt.Insert(0, "0");

		return txt;
	}
	
	public void PlayIntroAnimation()
	{
		Camera right_camera = Framework.Instance.scene_manager().GetCurrentCamera();
		tk2dButton[] buttons = GameObject.Find("/FakeRoot").GetComponentsInChildren<tk2dButton>();
		for(int i = 0; i < buttons.Length; i++)
		{
			buttons[i].viewCamera = right_camera;
		}
		
		GetComponent<Animation>().Play("EndGameMultiplayer");
		Framework.Instance.InstanceComponent<UpdateDataManager>().SetEnabled();
	}
	
	public void PlayWonLostSound()
	{
		Framework.Instance.audio_manager().Play("16_Combo Hit MIssile 4v", transform, 1.0F, 1.0F, false);
	}
	
	//Analitycs
	public void SetEndRunParams()
	{
		EbFightAPI.Player new_player = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer();
		
		Analitycs analitycs = Framework.Instance.InstanceComponent<Analitycs>();
		
		analitycs.AddNewKey(KeenIOEventType.run_end, "attacker_leaderboard_rating_change", (new_player.PlayerRank - old_player_.PlayerRank));  //Clarification needed
		analitycs.AddNewKey(KeenIOEventType.run_end, "attacker_battle_rating_change", 0);  //Clarification needed
		analitycs.AddNewKey(KeenIOEventType.run_end, "attacker_hard_currency_change", (new_player.WhiteCoins - old_player_.WhiteCoins));
		analitycs.AddNewKey(KeenIOEventType.run_end, "attacker_soft_currency_1_change", player_data_.GetInt("coins") - static_param.starting_coins_ );
		analitycs.AddNewKey(KeenIOEventType.run_end, "attacker_other_resource_change_set_id", 0); //Clarification needed
		
		analitycs.AddNewKey(KeenIOEventType.run_end, "defender_leaderboard_rating_change", 0);  //Clarification needed, better to get it from Server!!
		analitycs.AddNewKey(KeenIOEventType.run_end, "defender_battle_rating_change", 0); //Clarification needed
		analitycs.AddNewKey(KeenIOEventType.run_end, "defender_hard_currency_change", 0); //To implement need to calculate locally, but better to get from server
		analitycs.AddNewKey(KeenIOEventType.run_end, "defender_soft_currency_1_change", 0); // Will always be 0;
		analitycs.AddNewKey(KeenIOEventType.run_end, "defender_other_resource_change_set_id", 0); //Clarification needed
		
		//DD in multiplayer does not gain xp
		analitycs.AddNewKey(KeenIOEventType.run_end, "xp_gained", 0);
		analitycs.AddNewKey(KeenIOEventType.run_end, "run_score", score_);
	}
#if UNITY_EDITOR
	/*
	void OnGUI()
	{
		
		GUI.Button(new Rect(10, 165, 200, 30), "Boss Result");
		for(int i = 0; i < static_param.boss_results_.Count; i++)
			GUI.Button(new Rect(10, 200 + 35 * i, 200, 30), static_param.boss_results_[i].ToString());
		
		GUI.Button(new Rect(300, 165, 200, 30), "DD Result");
		for(int i = 0; i < static_param.dd_results_.Count; i++)
			GUI.Button(new Rect(300, 200 + 35 * i, 200, 30), static_param.dd_results_[i].ToString());
		
	}
	 */
#endif

}