using UnityEngine;
using System.Collections;

public class RoundResultPrefab : MonoBehaviour {
	
	public GameObject ko_sprite_;
	public GameObject none_sprite_;
	public GameObject lost_sprite_;
	public GameObject won_sprite_;
	public GameObject perfect_sprite_;
	
	
	// Xleo by manu:
	// i have added the game object perfect_sprite 
	// setted as false  in Void Awake ()
	// reordered the SetActive for every condition in public void, it's work fine
	
	// Use this for initialization
	void Awake () {
		ko_sprite_.SetActive(false);
		none_sprite_.SetActive(false);
		lost_sprite_.SetActive(false);
		won_sprite_.SetActive(false);
		perfect_sprite_.SetActive(false);
	}
	
	
	public void Init(Game.DDResultType result){
		//Debug.Log(result);
		if (result == Game.DDResultType.died){
			ko_sprite_.SetActive(true);
		} 
		else if (result == Game.DDResultType.none){
			none_sprite_.SetActive(true);
		}
		else if (result == Game.DDResultType.passed){
			lost_sprite_.SetActive(true);
		}
		else if (result == Game.DDResultType.perfect){
			perfect_sprite_.SetActive(true);
		}
		else if (result == Game.DDResultType.won){
			won_sprite_.SetActive(true);
		}	
		
		transform.Find("AnimationContainer").animation.Play();
	}
	
}
