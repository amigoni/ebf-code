 using UnityEngine;
using System.Collections;

public class CreditsScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			BackPressed();
		}
	}
#endif
	
	//Button callbacks
	public void BackPressed()
	{
		Application.LoadLevel ("SettingsScene");
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
}
