using UnityEngine;
using System.Collections;

public class BossBaloonContainer : MonoBehaviour {

	private tk2dTextMesh boss_baloon_text_mesh_;
	
	private const float duration_ = 4.0f;
	
	private GameObject boss_object_;
	private Animation animation_;
	
	// Use this for initialization
	void Awake()
	{
		boss_baloon_text_mesh_  = GameObject.Find("BossBaloonTextMesh").GetComponent<tk2dTextMesh>();
		
		boss_object_ = GameObject.Find("BossPrefab");
		animation_ = GetComponent<Animation>();
		
	}
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3( 
			boss_object_.transform.position.x + 100.0f, 
			boss_object_.transform.position.y + 200.0f, 
			transform.position.z);
	}
	
	public void PlayIntro(string text)
	{
		gameObject.SetActive(true);

		animation_.Play();
		
		boss_baloon_text_mesh_.text = text;
		boss_baloon_text_mesh_.Commit();
		
		Invoke("PlayExit", duration_);
	}
			
	public void PlayExit()
	{
		gameObject.SetActive(false);
	}
}
