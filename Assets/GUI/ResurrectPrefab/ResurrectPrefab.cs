using UnityEngine;
using System.Collections;
using SimpleJSON;

public class ResurrectPrefab : MonoBehaviour {
	
	private JSONNode store_info_; //Static info for each item
	private Store store_; //Dynamic Data for each item
	
	public GameObject not_enough_money_popup_;
	
	
	void Awake ()
	{
		store_ = Framework.Instance.InstanceComponent<Store>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		Messenger.AddListener("Resurrect", Resurrect);
		Messenger.AddListener("NotEnoughMoney",OpenNotEnoughMoneyPopUp);
	}
	
	// Use this for initialization
	void Start ()
	{
		int times_resurrected = GameObject.Find("Game").GetComponent<Game>().number_of_resurrections();
		int cost = store_info_["resurrection"]["upgrades"][times_resurrected]["next_cost"].AsInt;
		
		string cost_string = wmf.ui.Utility.FormattingWithZeros( cost.ToString(), 3);
		transform.Find("CoinContainer/CoinsTextMesh").GetComponent<tk2dTextMesh>().text = cost_string;
		transform.Find("CoinContainer/CoinsTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		transform.Find("ResurrectLabelContainer/DescriptionTextMesh").GetComponent<tk2dTextMesh>().text = store_info_["resurrection"]["description"].ToString();
		transform.Find("ResurrectLabelContainer/DescriptionTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		transform.Find("WhiteMilkGameIndicator/WhiteCoinsTextMesh").GetComponent<tk2dTextMesh>().text = wmf.ui.Utility.FormattingWithZeros((Framework.Instance.InstanceComponent<PlayerData>().GetInt("white_coins")+ GameObject.Find("Game").GetComponent<Game>().GetWhiteMilkCoins()).ToString(),5);
		transform.Find("WhiteMilkGameIndicator/WhiteCoinsTextMesh").GetComponent<tk2dTextMesh>().Commit();
		
		//transform.Find("BuyCoinsPrefab").GetComponent<BuyCoinsPrefab>().Init(false);
		//Messenger.Broadcast("RefreshCoins");
	}
	
	void BuyPressed()
	{
		bool purchase = store_.BuyItem("resurrection",1);
		if (purchase == true)
		{
			PurchaseSuccessful();
		}
		else
		{
			Framework.Instance.scene_manager().LoadScene("CoinsStoreScene", TransitionType.add_scene, -1200.0f);
			Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		}	
	}
	
	public void PurchaseSuccessful()
	{
		Destroy(gameObject);
		Messenger.Broadcast("Resurrect");
		Framework.Instance.audio_manager().Play("40_Menu Buy Upgrade", GameObject.Find("Game").transform, 1.0F, 1.0F, false);
		//Analitycs
		Framework.Instance.InstanceComponent<Analitycs>().IncreaseValue(KeenIOEventType.run_end, "num_resurrections");
		Framework.Instance.InstanceComponent<Analitycs>().GetCurrentRound().is_resurrect = true;
	}
	
	void NoPressed()
	{
		//Analitycs
		Framework.Instance.InstanceComponent<Analitycs>().AddNewKey(KeenIOEventType.run_end, "end_type", "Died");
		
		GameObject.Find("Game").GetComponent<Game>().EndGame();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	void Resurrect()
	{
		GameObject.Find("Game").GetComponent<Game>().Resurrect();
	}
	
	
	void OpenNotEnoughMoneyPopUp()
	{
		
	}
	
	void OnDestroy ()
	{
		Messenger.RemoveListener("Resurrect", Resurrect);
		Messenger.AddListener("NotEnoughMoney",OpenNotEnoughMoneyPopUp);
	}
}
