using UnityEngine;
using System.Collections;

public enum InputControllerType
{
	d_pad,
	virtual_pad,
	swipe_motion,
	count
};

public abstract class InputController : MonoBehaviour {
	
	protected string prefab_name_;
	
	virtual public void Init()
	{
		if(this.GetComponent<tk2dCameraAnchor>() != null)
			this.GetComponent<tk2dCameraAnchor>().tk2dCamera = tk2dCamera.inst;
	}
	
	abstract public InputControllerType type();
	
	abstract public Vector2 GetDirection ();
	
	//Getter / Setter
	public string prefab_name()
	{
		return  prefab_name_;
	}
}
