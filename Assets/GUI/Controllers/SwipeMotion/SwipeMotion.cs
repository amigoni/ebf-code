using UnityEngine;
using System.Collections;

public class SwipeMotion : InputController
{
	//public tk2dSprite foreground_;
	//public tk2dSprite background_;
	private const int pad_ray_ = 20;
	private Vector2 initial_position_ = new Vector2(80,80);
	private Vector2 last_position_;
	private Vector2 move_direction_;
	private float dead_zone = 7F;
	private float stop_zone = 7F;
	private Vector2 max_dimension = new Vector2(30, 30);
	private int current_id_;
	private bool touched_ = false;
	private int frame_counter_ = 0;	

	
	
	//Base class implementation
	public override void Init ()
	{
		Input.multiTouchEnabled = true;
		current_id_ = -1;
		touched_ = false;
	}
	
	
	// Use this for initialization
	void Start ()
	{
		Init ();
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		bool valid = false;
		
		foreach (Touch touch in Input.touches) {
			
			valid = true;
			if (touch.position.x < Screen.width * 0.5F) {
				//last_position_ = touch.position;
				last_position_ = Framework.Instance.graphics_manager().ScreenToInput(touch.position);
      			
				if (touch.phase == TouchPhase.Began) 
				{	
					//initial_position_ = touch.position;
					initial_position_ = Framework.Instance.graphics_manager().ScreenToInput(touch.position);
					
					current_id_ = touch.fingerId;
					touched_ = true;
				}
				else if (current_id_ != -1) 
				{
					/*
					if (touch.phase == TouchPhase.Stationary){
						initial_position_ = touch.position;
					}
					*/
					if (touch.phase == TouchPhase.Moved){
						
						//Vector2 v_diff = touch.position - initial_position_;
						Vector2 v_diff = Framework.Instance.graphics_manager().ScreenToInput(touch.position) - initial_position_;
						
	          			Vector2 max_limit = initial_position_ + max_dimension;
						
						//Debug.Log(VectorMagnitude(v_diff));
					
						if(VectorMagnitude(v_diff) <= stop_zone){
							move_direction_ = new Vector2(0,0);
						}
						//If greater then deadzone
						if(VectorMagnitude(v_diff) > dead_zone * Framework.Instance.graphics_manager().camera_scale()){
							//move_direction_ = touch.position - initial_position_;
							move_direction_ = Framework.Instance.graphics_manager().ScreenToInput(touch.position) - initial_position_;
							/*
							move_direction_ = move_direction_.normalized;
							move_direction_.x = Mathf.Round( move_direction_.x *2);
							move_direction_.y = Mathf.Round( move_direction_.y *2);
							move_direction_ = move_direction_.normalized;
							*/
							//initial_position_ = touch.position;	
						}
							
						if (VectorMagnitude(Framework.Instance.graphics_manager().ScreenToInput(touch.position)) >= 
							VectorMagnitude(max_limit * Framework.Instance.graphics_manager().camera_scale()))
			            	initial_position_ = Framework.Instance.graphics_manager().ScreenToInput(touch.position);  
						
					}
					else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
						current_id_ = -1;
						touched_ = false;
						move_direction_ = new Vector2(0,0);
					} 
				}
			}
		}
		
		if (!valid) {
			current_id_ = -1;
			touched_ = false;
			move_direction_ = new Vector2(0,0);
		}
		
		//print("x "+move_direction_.x +"y "+move_direction_.y);
	}
	
	
	public override Vector2 GetDirection ()
	{
		return move_direction_.normalized;
	}
	
	public void ResetPosition( string type){
		if (type == "vertical")
			initial_position_.y = last_position_.y;
		else if (type == "horizontal")
			initial_position_.x = last_position_.x;
		else if (type == "both")
			initial_position_ = last_position_;
	}
	
	public override InputControllerType type()
	{
		return InputControllerType.swipe_motion;
	}
	
	private float VectorMagnitude(Vector2 v)
	{
		return Mathf.Sqrt( (v.x*v.x) + (v.y*v.y));
	}
	/*
	void OnGUI()
	{
		GUI.Button (new Rect (10, 20, 150, 30), current_id_.ToString());  
	}*/
}