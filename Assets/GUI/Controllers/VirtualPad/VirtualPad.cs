using UnityEngine;
using System.Collections;

public class VirtualPad : InputController
{
	public tk2dSprite foreground_;
	public tk2dSprite background_;
	private Vector2 initial_position_;
	private int current_id_;
	private bool touched_ = false;
	Rect right_rect;
	
	private float pad_ray_ = 15.0f;
	private float dead_point_ = 10.0f;
	private bool floating_ = true;
	
	private float last_move_time_ = 0.0f;
	
	private const float hide_timer_ = 0.25f;
	private float dt_hide_ = 0;
		
	//Base class implementation
	public override void Init ()
	{
		Input.multiTouchEnabled = true;
			
		initial_position_ = foreground_.transform.position;	
		
		current_id_ = -1;
		
		touched_ = false;
		
		right_rect = GetRect();
		
		prefab_name_ = "VirtualPad";
			
		if(Framework.Instance.device_type() == Framework.WMF_DeviceType.ipad)
		{
			pad_ray_ = 8.0f;
			dead_point_ = 5.0f;
		}
		else
		{
			pad_ray_ = 15.0f;
			dead_point_ = 10.0f;
		}
		
		dt_hide_ = hide_timer_;
	}
	
	public override Vector2 GetDirection ()
	{
		Vector2 foreground_pos_2D = new Vector2 (foreground_.transform.position.x, foreground_.transform.position.y);
		Vector2 v_diff = foreground_pos_2D - initial_position_;
		
		float magnitude = VectorMagnitude(v_diff);
		if(magnitude <= dead_point_)
		{
			v_diff = v_diff.normalized;
			v_diff.x = 0.0F;
			v_diff.y = 0.0F;
			//Reset position
			
		}
		else
			v_diff = v_diff.normalized;
		
		return v_diff;
	}
	
	public override InputControllerType type()
	{
		return InputControllerType.virtual_pad;
	}
	
	// Use this for initialization
	void Start ()
	{
		Init ();
	}
	
	bool moved_ = false;
	
	// Update is called once per frame
	void Update ()
	{
		bool valid = false;
		
		foreach (Touch touch in Input.touches) {
			valid = true;
			if (touch.position.x < Screen.width * 0.5F) {
				dt_hide_ = hide_timer_;
				
				Vector2 converted_position = Framework.Instance.graphics_manager().ScreenToInput(touch.position);
				if (touch.phase == TouchPhase.Began) {	
					
					foreground_.GetComponent<MeshRenderer>().enabled = true;
					background_.GetComponent<MeshRenderer>().enabled = true;
					
					if(floating_)
					{
						background_.transform.position = converted_position;
						initial_position_ = converted_position;
						foreground_.transform.position = initial_position_;
						right_rect = GetRect();
					}
					if (right_rect.Contains (converted_position)) {
						current_id_ = touch.fingerId;
						touched_ = true;
						initial_position_ = converted_position;
						foreground_.transform.position = initial_position_;
					}
				} else if (current_id_ != -1) {
					if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
						current_id_ = -1;
						foreground_.transform.position = initial_position_;
						touched_ = false;
					} else if (touch.phase == TouchPhase.Moved) {
						foreground_.transform.position = converted_position;
						Vector2 foreground_pos_2D = new Vector2 (foreground_.transform.position.x, 
														 foreground_.transform.position.y);
						Vector2 v_diff = foreground_pos_2D - initial_position_;
						
						if(v_diff.magnitude > pad_ray_)
						{
							Vector2 clamped_v_diff = Vector2.ClampMagnitude (v_diff, pad_ray_);
							background_.transform.position = new Vector3(
								background_.transform.position.x + (v_diff.x - clamped_v_diff.x),
								background_.transform.position.y + (v_diff.y - clamped_v_diff.y),
								-700.0f);
							initial_position_ = background_.transform.position;
						}
						
						v_diff = Vector2.ClampMagnitude (v_diff, pad_ray_);
						//foreground_.transform.position = initial_position_ + v_diff;
							
						moved_ = true;
					}
				}
			}
		}
		
		if (!valid) {
			current_id_ = -1;
			foreground_.transform.position = initial_position_;
			touched_ = false;
		}
		
		foreground_.transform.position = new Vector3 (foreground_.transform.position.x,
													foreground_.transform.position.y,
													-700.0F);
		
		if(dt_hide_ < 0)
		{
			foreground_.GetComponent<MeshRenderer>().enabled = false;
			background_.GetComponent<MeshRenderer>().enabled = false;
		}
		else
		{
			dt_hide_ -= Time.deltaTime;
		}
	}
	
	public float VectorMagnitude(Vector2 v)
	{
		return Mathf.Sqrt( (v.x*v.x) + (v.y*v.y));
	}
	
	private Rect GetRect()
	{
		Vector2 size = new Vector2(86.0F, 86.0F);//background_.GetComponent<BoxCollider> ().size;
		Vector2 position = background_.transform.position;
		
		Rect rect = new Rect (position.x - size.x * 0.5F,
							position.y - size.y * 0.5F,
							size.x,
							size.y);
		
		return rect;
	}
	
	public void set_floating(bool floating)
	{
		floating_ = floating;
	}
	
	/*
	void OnGUI ()
	{	
	//	Vector2 foreground_pos_2D = new Vector2 (foreground_.transform.position.x, foreground_.transform.position.y);
	//	Vector2 v_diff = foreground_pos_2D - initial_position_;
	
	//	GUI.Button (new Rect (100, 20, 150, 30), moved_.ToString());  
	//	GUI.Button (new Rect (100, 110, 150, 30), GetDirection().ToString() ); 
		
	//	GUI.Button (new Rect (100, 20, 150, 30), VectorMagnitude(v_diff).ToString());  
		
	//	GUI.Button (new Rect (100, 20, 150, 30), initial_position_.ToString() ); 
	//	GUI.Button (new Rect (100, 50, 150, 30), foreground_pos_2D.ToString() ); 
		
		/*
		GUI.Button (new Rect (50, 0, 100, 20), "PAD RAY:" ); 
		pad_ray_ = int.Parse( GUI.TextField(new Rect (150, 0, 30, 20), pad_ray_.ToString()) );
		GUI.Button (new Rect (200, 0, 100, 20), "DEAD POINT:" ); 
		dead_point_ = int.Parse( GUI.TextField(new Rect (300, 0, 30, 20), dead_point_.ToString()) ); 
		*/
	//}
}

/*
using UnityEngine;
using System.Collections;

public class VirtualPad : InputController
{
	public tk2dSprite foreground_;
	public tk2dSprite background_;
	private Vector2 initial_position_;
	private int current_id_;
	private bool touched_ = false;
	Rect right_rect;
	
	private float pad_ray_ = 5.0f;
	private float dead_point_ = 1.0f;
	private bool floating_ = true;
		
	//Base class implementation
	public override void Init ()
	{
		Input.multiTouchEnabled = true;
			
		initial_position_ = foreground_.transform.position;	
		
		current_id_ = -1;
		
		touched_ = false;
		
		right_rect = GetRect();
		
		prefab_name_ = "VirtualPad";
	}
	
	public override Vector2 GetDirection ()
	{
		Vector2 foreground_pos_2D = new Vector2 (foreground_.transform.position.x, foreground_.transform.position.y);
		Vector2 v_diff = foreground_pos_2D - initial_position_;
		
		float magnitude = VectorMagnitude(v_diff);
		if(magnitude <= dead_point_)
		{
			v_diff = v_diff.normalized;
			v_diff.x = 0.0F;
			v_diff.y = 0.0F;
		}
		else
			v_diff = v_diff.normalized;
		
		return v_diff;
	}
	
	public override InputControllerType type()
	{
		return InputControllerType.virtual_pad;
	}
	
	// Use this for initialization
	void Start ()
	{
		Init ();
	}
	
	bool moved_ = false;
	
	// Update is called once per frame
	void Update ()
	{
		bool valid = false;
		
		foreach (Touch touch in Input.touches) {
			valid = true;
			if (touch.position.x < Screen.width * 0.5F) {
				Vector2 converted_position = Framework.Instance.graphics_manager().ScreenToInput(touch.position);
				if (touch.phase == TouchPhase.Began) {	
					if(floating_)
					{
						background_.transform.position = converted_position;
						initial_position_ = converted_position;
						foreground_.transform.position = initial_position_;
						right_rect = GetRect();
					}
					if (right_rect.Contains (converted_position)) {
						current_id_ = touch.fingerId;
						touched_ = true;
						initial_position_ = converted_position;
						foreground_.transform.position = initial_position_;
					}
				} else if (current_id_ != -1) {
					if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
						current_id_ = -1;
						foreground_.transform.position = initial_position_;
						touched_ = false;
					} else if (touch.phase == TouchPhase.Moved) {
						foreground_.transform.position = converted_position;
						Vector2 foreground_pos_2D = new Vector2 (foreground_.transform.position.x, 
														 foreground_.transform.position.y);
						Vector2 v_diff = foreground_pos_2D - initial_position_;
		
						v_diff = Vector2.ClampMagnitude (v_diff, pad_ray_);
						foreground_.transform.position = initial_position_ + v_diff;
						
						moved_ = true;
					}
				}
			}
		}
		
		if (!valid) {
			current_id_ = -1;
			foreground_.transform.position = initial_position_;
			touched_ = false;
		}
		
		foreground_.transform.position = new Vector3 (foreground_.transform.position.x,
													foreground_.transform.position.y,
													-700.0F);
	}
	
	public float VectorMagnitude(Vector2 v)
	{
		return Mathf.Sqrt( (v.x*v.x) + (v.y*v.y));
	}
	
	private Rect GetRect()
	{
		Vector2 size = new Vector2(86.0F, 86.0F);//background_.GetComponent<BoxCollider> ().size;
		Vector2 position = background_.transform.position;
		
		Rect rect = new Rect (position.x - size.x * 0.5F,
							position.y - size.y * 0.5F,
							size.x,
							size.y);
		
		return rect;
	}
	
	public void set_floating(bool floating)
	{
		floating_ = floating;
	}
	
	void OnGUI ()
	{	
	//	Vector2 foreground_pos_2D = new Vector2 (foreground_.transform.position.x, foreground_.transform.position.y);
	//	Vector2 v_diff = foreground_pos_2D - initial_position_;
	
	//	GUI.Button (new Rect (100, 20, 150, 30), moved_.ToString());  
	//	GUI.Button (new Rect (100, 110, 150, 30), GetDirection().ToString() ); 
		
	//	GUI.Button (new Rect (100, 20, 150, 30), VectorMagnitude(v_diff).ToString());  
		
	//	GUI.Button (new Rect (100, 20, 150, 30), initial_position_.ToString() ); 
	//	GUI.Button (new Rect (100, 50, 150, 30), foreground_pos_2D.ToString() ); 
		
		//GUI.Button (new Rect (50, 0, 100, 20), "PAD RAY:" ); 
		//pad_ray_ = int.Parse( GUI.TextField(new Rect (150, 0, 30, 20), pad_ray_.ToString()) );
		//GUI.Button (new Rect (200, 0, 100, 20), "DEAD POINT:" ); 
		//dead_point_ = int.Parse( GUI.TextField(new Rect (300, 0, 30, 20), dead_point_.ToString()) ); 
		
	}
}
*/