using UnityEngine;
using System.Collections;

public class GestureController : MonoBehaviour
{
	
	private bool controller_enabled_ = true;
	private tk2dSprite foreground_;
	private tk2dSprite background_;
	private Vector2 initial_position_;
	private float initial_touch_y_position_;
	private int current_finger_id_ = -1;
	public bool clicked_ = false;
	public bool moved_ = false;
	private float tap_time_ = 0.1F;
	private float tap_dt_ = 0.0F;
	private float y_swipe_tollerance_ = 20F;
	private Vector2 DEBUG_position_;
	private bool already_fired_ = false;
	
	private PlayerAttackController player_controller_;
	
	Rect right_rect;
	
	// Use this for initialization
	void Start ()
	{
		foreground_ = GameObject.Find ("ForegroundController").GetComponent<tk2dSprite> ();
		initial_position_ = foreground_.transform.position;
		
		background_ = GameObject.Find ("BackgroundController").GetComponent<tk2dSprite> ();

		player_controller_ = GameObject.Find("Player").GetComponent<PlayerAttackController>();
		
		transform.position = new Vector3(
			(Screen.width / Framework.Instance.graphics_manager().camera_scale()) - 75.0f,
			transform.position.y, 
			transform.position.z);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (controller_enabled_ == false)
			return;
		
		bool valid = false;
		
		if (current_finger_id_ == -1) {
			foreach (Touch touch in Input.touches)
			{
				if (touch.position.x > Screen.width * 0.5F)
				{
					Vector2 converted_position = Framework.Instance.graphics_manager().ScreenToInput(touch.position);
					//Debug.Log(touch.position.ToString());
					//Debug.Log(converted_position.ToString());
					if (touch.phase == TouchPhase.Began) 
					{
						player_controller_.DoPowerPunchIfNeeded();
						
						//auto layout
						if ((converted_position.x > 300) && (converted_position.y < 250))
						{
							foreground_.transform.position = new Vector3(
								converted_position.x,
								converted_position.y,
								-700.0f);//foreground_.transform.position.z);
							background_.transform.position = new Vector3(
								converted_position.x,
								converted_position.y,
								background_.transform.position.z);
							initial_position_ = foreground_.transform.position;
						}
						
						Vector2 size = new Vector2(32,32);//foreground_.GetComponent<BoxCollider> ().size;
						Vector2 position = background_.transform.position;
						 right_rect = new Rect (position.x - size.x * 0.5F,
								position.y - size.y * 0.5F,
								size.x,
								size.y);
		
						
						if (right_rect.Contains (converted_position))
						{
							initial_touch_y_position_ = foreground_.transform.position.y - converted_position.y;
							
							current_finger_id_ = touch.fingerId;
							clicked_ = true;
							moved_ = false;
							tap_dt_ = tap_time_;
						}
					}
					else if (touch.phase == TouchPhase.Ended )
					{
						if(current_finger_id_ == touch.fingerId)
							player_controller_.TouchEnded();
					}
				}
			}
		}

		
		foreach (Touch touch in Input.touches) {
			if (touch.position.x > Screen.width * 0.5F)
			{
				if (touch.fingerId == current_finger_id_) {
					valid = true;
				}
			}
		}
		
		
		//fake touch
		if (!valid && current_finger_id_ != -1) {
			player_controller_.PlayAnimation (AttackType.tap);
			player_controller_.TouchEnded();
			Reset ();
		}
		
		
		if (valid) {
		
			if (current_finger_id_ != -1) {
				Touch touch = Input.GetTouch (current_finger_id_);
				Vector2 converted_position = Framework.Instance.graphics_manager().ScreenToInput(touch.position);
				
				bool moved = Mathf.Abs(initial_position_.y - converted_position.y) > 5 ? true : false;
				
				if (moved) 
				{
					moved_ = true;
					
					CheckGesture (Input.GetTouch (current_finger_id_).position.y);
					
					foreground_.transform.position = new Vector2(foreground_.transform.position.x,
																converted_position.y + initial_touch_y_position_);
				} 
				
				if (touch.phase == TouchPhase.Canceled) {
					Reset ();
					player_controller_.TouchEnded();
				} else if (touch.phase == TouchPhase.Stationary ) {
					tap_dt_ -= Time.deltaTime;
					if (tap_dt_ < 0.0F && !already_fired_) {
						player_controller_.PlayAnimation (AttackType.tap);
						already_fired_ = true;
					}
				} else if (touch.phase == TouchPhase.Ended) {
					if(!already_fired_)
						player_controller_.PlayAnimation (AttackType.tap);
					
					player_controller_.TouchEnded();
					Reset ();
				}
				
			} else {
				//Reset ();
			}
		}
		
		//reseting Z
		foreground_.transform.position = new Vector3(foreground_.transform.position.x,
													 foreground_.transform.position.y,
													-700.0f);
		
	}

	void CheckGesture (float y)
	{
		
		float scale = Framework.Instance.graphics_manager().camera_scale();
		if (y-foreground_.transform.position.y*scale > y_swipe_tollerance_)
		{
			player_controller_.PlayAnimation ( AttackType.swipe_up );
			player_controller_.TouchEnded();
			Reset ();
		}
		else if ( y-foreground_.transform.position.y*scale < -y_swipe_tollerance_)
		{
			player_controller_.PlayAnimation ( AttackType.swipe_down );
			player_controller_.TouchEnded();
			Reset ();
		}
	}
	
	void Reset ()
	{
		moved_ = false;
		clicked_ = false;
		current_finger_id_ = -1;
		already_fired_ = false;
		
		foreground_.transform.position = new Vector3(initial_position_.x, initial_position_.y, -1);
		//background_.transform.position = new Vector3(foreground_.transform.position.x,foreground_.transform.position.y,background_.transform.position.z);
	}
	/*	
	void OnGUI ()
	{	
		GUI.Button (new Rect (100, 10, 50, 30), current_finger_id_.ToString ());  
		//GUI.Button (new Rect (200, 10, 150, 30), old_phase_.ToString ());		
	}
	*/
	public void SetControllerEnabled ( bool enabled)
	{
		controller_enabled_ = enabled;
	}
}
