using UnityEngine;
using System.Collections;

public class DashController: MonoBehaviour {
	
	private float first_press_time_ = 0.0F;
	private int current_id_ = -1;
	private bool touched_ = false;
	private bool dash_enabled_ = true;
	private float swipe_tollerance_ = 60.0f; //Pixels
	private float dashed_time_;
	private Vector2 dash_position_;
	private float previous_touch_position_x_;
	private float previous_time_;
	private float swipe_velocity_x_ = 0.0F;
	private float min_swipe_velocity_x_ = 400F;//pixels/sec
	private bool dash_back_enabled_ = true;
	
	private bool controller_enabled_ = true;
	
	// Use this for initialization
	void Start () 
	{
		min_swipe_velocity_x_ *= Framework.Instance.graphics_manager().camera_scale();
		if(Framework.Instance.device_type() == Framework.WMF_DeviceType.ipad)
			swipe_tollerance_ = 80;
		else if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone)
			swipe_tollerance_ = 60;
		else
			swipe_tollerance_ = 120;
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		if (controller_enabled_ == true)
		{
			bool valid = false;
			
			if( Time.time - dashed_time_ > 1.0F)
				ToggleDash(true);
			
			foreach (Touch touch in Input.touches) 
			{
				valid = true;
				
				if (touch.position.x < Screen.width * 0.5F) {
					if (touch.phase == TouchPhase.Began) 
					{	
						current_id_ = touch.fingerId;
						touched_ = true;
						dash_position_ = touch.position;
						previous_touch_position_x_ = touch.position.x;
						previous_time_ = Time.time;
						ToggleDash(true);
					}
					else if (current_id_ != -1) 
					{
						if (touch.phase == TouchPhase.Moved)
							CheckTollerancesForDashing(touch.position.x);	
						else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
						{
							CheckTollerancesForDashing(touch.position.x);
							
							current_id_ = -1;
							touched_ = false;
							ToggleDash(true);
						} 
					}
				}
			}
			
			if (!valid) {
				current_id_ = -1;
				touched_ = false;
			}
	
	//Only for testing
#if UNITY_EDITOR
			if(Input.GetKeyDown(KeyCode.F))
			{
				GameObject.Find("Player").GetComponent<Player>().Dash("forward");
			}
			
			if(Input.GetKeyDown(KeyCode.V))
			{
				GameObject.Find("Player").GetComponent<Player>().Dash("back");
			}
#endif
		}	
	}
	
	
	private void CheckTollerancesForDashing(float x_position)
	{
		swipe_velocity_x_ = (x_position - previous_touch_position_x_)/(Time.time - previous_time_);
		
		if (dash_enabled_ == true && Mathf.Abs(swipe_velocity_x_) > min_swipe_velocity_x_ )
		{
			if ((x_position -dash_position_.x) > swipe_tollerance_){
				ToggleDash(false);
				GameObject.Find("Player").GetComponent<Player>().Dash("forward");
			}
			else if (((x_position -  dash_position_.x) < - swipe_tollerance_) && dash_back_enabled_ == true)
			{
				ToggleDash(false);
				GameObject.Find("Player").GetComponent<Player>().Dash("back");
			}		
		}			
	}

	
	private void ToggleDash(bool enabled)
	{
		dash_enabled_ = enabled;
		if (enabled == false)
			dashed_time_ = Time.time;
	}
	
	
	public void SetDashBackEnabled (bool enabled)
	{
		dash_back_enabled_ = enabled;
	}
	
	
	public void Pressed(){
		//print (Time.time - first_press_time_);
		if(Time.time - first_press_time_ < 0.25F){
			//Debug.Log ("Dash!!!");
			GameObject.Find("Player").GetComponent<Player>().Dash("auto");
		}
		first_press_time_ = Time.time;
	}
	
	public void SetControllerEnabled(bool enabled)
	{
		controller_enabled_ = enabled;
		//Debug.Log("Controller set to: "+enabled);
	}
}
