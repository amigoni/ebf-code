using UnityEngine;
using System.Collections;

public class DPad : InputController {
	private Vector2 direction_;
	private const float direction_value_ = 1.0F;
	private const float diagonal_direction_value_ = 0.75F;
	
	private string txt_ = "";
	
	//Base class implementation
	public override void Init ()
	{
		Input.multiTouchEnabled = true;
		
		direction_ = new Vector2(0.0F, 0.0F);
		
		prefab_name_ = "DPad";
		
		ScaleCollider("ColliderUp");
		ScaleCollider("ColliderRight");
		ScaleCollider("ColliderDown");
		ScaleCollider("ColliderLeft");
		
		ScaleCollider("ColliderRightUp");
		ScaleCollider("ColliderRightDown");
		ScaleCollider("ColliderLeftUp");
		ScaleCollider("ColliderLeftDown");
		
		base.Init();
		//
		//GameObject.Find("ColliderUp").transform.localScale = new Vector3(2.0F, 2.0F, GameObject.Find("ColliderUp").transform.localScale.z);
	}
	
	void ScaleCollider(string collider_name)
	{
		Vector3 box_collider_position = GameObject.Find(this.name + "/" + collider_name).GetComponent<BoxCollider>().transform.position;
			
	 	Vector2 native_resolution = Framework.Instance.graphics_manager().native_resolution();
		box_collider_position = new Vector2( 
			box_collider_position.x * (Screen.width / native_resolution.x ), 
			box_collider_position.y * (Screen.height / native_resolution.y) );
				
		GameObject.Find(this.name + "/" + collider_name).GetComponent<BoxCollider>().transform.position = box_collider_position;
		GameObject.Find(this.name + "/" + collider_name).GetComponent<BoxCollider>().size *= Framework.Instance.graphics_manager().camera_scale();
	}
	
	public override Vector2 GetDirection (){	
		return direction_;
	}	
	
	public override InputControllerType type()
	{
		return InputControllerType.d_pad;
	}
	
	// Use this for initialization
	void Start () {
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		//ray test
		foreach (Touch touch in Input.touches) {
			if (touch.position.x < Screen.width * 0.5F)
			{
				if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved)
				{	
					RaycastHit hit = new RaycastHit();
					Vector3 position = new Vector3(touch.position.x, touch.position.y, -400.0f);
					//8 is PadCollider
					int layer_mask = 1 << LayerMask.NameToLayer("PadCollider");
					layer_mask = ~layer_mask;
					Physics.Raycast(position, transform.forward, out hit);
					
					if(hit.transform != null)
						txt_ = hit.transform.name;
					
					if(hit.transform != null && hit.transform.name == "ColliderUp")
						SetY(direction_value_);
					else if(hit.transform != null && hit.transform.name == "ColliderDown")
						SetY(-direction_value_);
					else
						SetY(0.0F);
					
					if(hit.transform != null && hit.transform.name == "ColliderRight")
						SetX(direction_value_);
					else if(hit.transform != null && hit.transform.name == "ColliderLeft")
						SetX(-direction_value_);
					else
						SetX(0.0F);
					
					if(hit.transform != null && hit.transform.name == "ColliderLeftUp")
					{
						SetX(-diagonal_direction_value_);
						SetY(diagonal_direction_value_);
					}
					if(hit.transform != null && hit.transform.name == "ColliderLeftDown")
					{
						SetX(-diagonal_direction_value_);
						SetY(-diagonal_direction_value_);
					}
					if(hit.transform != null && hit.transform.name == "ColliderRightUp")
					{
						SetX(diagonal_direction_value_);
						SetY(diagonal_direction_value_);
					}
					if(hit.transform != null && hit.transform.name == "ColliderRightDown")
					{
						SetX(diagonal_direction_value_);
						SetY(-diagonal_direction_value_);
					}
				}
				else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
				{
					SetY(0.0F);
					SetX(0.0F);
				}
			}
		}
	}
	
	private void SetX(float x)
	{
		direction_ = new Vector2(x, direction_.y);
	}
	
	private void SetY(float y)
	{
		direction_ = new Vector2(direction_.x, y);
	}
	
	//void OnGUI ()
	//{
		//GUI.Button (new Rect (10, 0, 200, 30), GameObject.Find(this.name).transform.position.ToString()); 
		//GUI.Button (new Rect (10, 40, 200, 30), GameObject.Find(this.name + "/ColliderUp").transform.position.ToString());  
	//}
}