using UnityEngine;
using System.Collections;

public class TimeLeft : MonoBehaviour {
	
	private Animation animation_;
	private DigitsWidget digits_widget_;
	private tk2dAnimatedSprite digit_2_;
	private int timer_ = 0;
	private Game game_;
	private bool time_down_enabled_ = false;
	
	private Animation warning_animation_;
	
	void Awake()
	{
		animation_ = GetComponent<Animation>();
		
		digits_widget_ = GameObject.Find ("TimeLeftDigitsWidget").GetComponent<DigitsWidget>();
		
		game_ = GameObject.Find("Game").GetComponent<Game>();
		
		warning_animation_ = GameObject.Find("Time_left_red_label_sprite").GetComponent<Animation>();
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void TimeUpdate()
	{
		timer_--;
		digits_widget_.UpdateValue(timer_);
		
		if(timer_ <= 0 && time_down_enabled_ == false)
		{
			game_.TimeFinished();
		} 
		else if(timer_ < 15 && !warning_animation_.Play())
		{
			warning_animation_.Play();
			game_.BossBaloon("The End of Time is Coming");
		}
	}
	
	public void SetDigits(int digits)
	{
		warning_animation_.GetComponent<MeshRenderer>().enabled = false;
		warning_animation_.Stop();
		time_down_enabled_ = false;
		timer_ = digits;
		TimeUpdate();
	}
	
	public void AddDigitis(int digits)
	{
		timer_ += digits;
	}
	
	public void Go()
	{
		if(!IsInvoking("TimeUpdate"))
			InvokeRepeating("TimeUpdate", 1.0F, 1.0F);
	}
	
	public void Stop()
	{
		CancelInvoke("TimeUpdate");
	}
	
	public void TimeDown(){
		time_down_enabled_ = true;
		timer_--;
		if(timer_ > 0)
			TimeUpdate();
		else
			CancelInvoke("TimeDown");
	}
	
	public int StartTimeDown(){
		//InvokeRepeating("TimeDown", 0.0F, 0.005F);
		return timer_;
	}
	
	public void PlayExit()
	{
		animation_.Play("TimeLeftExitAnimation");
	}
	
	public void DestroyAll()
	{
		GameObject.Destroy(gameObject);
	}
}
