using UnityEngine;
using System.Collections;

public class OptionsScene : MonoBehaviour {
	Options options_;
	OptionsParams options_params_;
		
	string[] controller_str_ = new string[3];
	string[] move_on_shot_str_ = new string[3];
	int selection_controller_ = 0;
	int selection_move_ = 0;
	
	// Use this for initialization
	void Start () {
		options_ = Framework.Instance.InstanceComponent<Options>();
		options_params_ = options_.options_params();
		
		if(options_params_.input_controller_type == InputControllerType.d_pad )
			selection_controller_ = 0;
		else if(options_params_.input_controller_type == InputControllerType.virtual_pad )
			selection_controller_ = 1;
		else if(options_params_.input_controller_type == InputControllerType.swipe_motion )
			selection_controller_ = 2;
		
		controller_str_[0] = "DPAD";
		controller_str_[1] = "VIRTUAL PAD";
		controller_str_[2] = "SWIPE MOTION";
		
		
		if(options_params_.move_on_shot == MoveOnShotType.stop )
			selection_move_ = 0;
		else if(options_params_.move_on_shot == MoveOnShotType.slow )
			selection_move_ = 1;
		else if(options_params_.move_on_shot == MoveOnShotType.move )
			selection_move_ = 2;
		
		move_on_shot_str_[0] = "STOP";
		move_on_shot_str_[1] = "SLOW";
		move_on_shot_str_[2] = "MOVE";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		selection_move_ = GUI.SelectionGrid(new Rect(5, 5, 460, 40), 
													selection_move_, 
													move_on_shot_str_,
													3);

		selection_controller_ = GUI.SelectionGrid( new Rect (5, 45, 460, 40), 
												  	selection_controller_, 
													controller_str_, 
													3);
		
		if (GUI.Button(new Rect(10,85,100,40),"Save & Exit"))
		{
			if(selection_controller_ == 0)
				options_params_.input_controller_type = InputControllerType.d_pad;
			else if(selection_controller_ == 1)
				options_params_.input_controller_type = InputControllerType.virtual_pad;
			else if(selection_controller_ == 2)
				options_params_.input_controller_type = InputControllerType.swipe_motion;
			
			if(selection_move_ == 0)
				options_params_.move_on_shot = MoveOnShotType.stop;
			else if(selection_move_ == 1)
				options_params_.move_on_shot = MoveOnShotType.slow;
			else if(selection_move_ == 2)
				options_params_.move_on_shot = MoveOnShotType.move;
			
			options_.ChangeOptionsParams( options_params_ );
			//options_.Save();
			print ("Luca had to comment the Save on OptionsScene as the serialization crashes on device");
			
			GameObject.Find("Game").GetComponent<Game>().set_options(options_);
			GameObject.Find("Game").GetComponent<Game>().ResumeGame();
		}
	}
}
