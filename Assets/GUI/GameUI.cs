using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class GameUI: MonoBehaviour {
	
	private DigitsWidget score_digits_;
	private DigitsWidget coins_digits_;
	private DigitsWidget ammo_digits_;
	private DigitsWidget multipler_digits_;
	private DigitsWidget level_digits_;
	
	private GameObject life_dd_sprite_;
	private GameObject boss_life_bar_;
	private Animation score_animation_;
	private List<BossBattleLogRoundIconPrefab> rounds_ = new List<BossBattleLogRoundIconPrefab>();
	
	private Animation hit_animation_;
	private Animation multiplier_animation_;
	
	private long high_score_ = 0;
	
	private Game game_;
	
	private TimeLeft time_left_;
	
	private PowerPunchType current_power_punch_;
	
	private HideOnEndAnimation next_round_ = null;
	
	//Ammo
	private PlayerData player_data_;
	private float max_ammo_ = 0;
	private tk2dSlicedSprite dd_ammo_bar_;
	private JSONNode store_info_;
		
	// Use this for initialization
	void Awake () {
		score_digits_ = GameObject.Find("ScoreDigitsWidget").GetComponent<DigitsWidget>();
		coins_digits_ = GameObject.Find("CoinsDigitsWidget").GetComponent<DigitsWidget>();
		ammo_digits_ = GameObject.Find("AmmoDigitsWidget").GetComponent<DigitsWidget>();
		multipler_digits_ = GameObject.Find("MultiplierDigitsWidget").GetComponent<DigitsWidget>();
		level_digits_ = GameObject.Find("LevelDigitsWidget").GetComponent<DigitsWidget>();
		score_animation_ = GameObject.Find("ScoreContainer").GetComponent<Animation>(); 
		
		life_dd_sprite_ = GameObject.Find("Life_dd_sliced_sprite");
		boss_life_bar_ = GameObject.Find("Life_boss_sliced_sprite");
		hit_animation_ = GameObject.Find("RocketHit").GetComponent<Animation>();
		if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)
	 		hit_animation_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(568.0f, 320.0f);
		if(Framework.Instance.device_type() == Framework.WMF_DeviceType.ipad)
	 		hit_animation_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(512.0f, 384.0f);
		multiplier_animation_ = GameObject.Find("MultiplierContainer").GetComponent<Animation>();
		
		high_score_ = PlayerPrefs.GetInt("best_score");
		if(high_score_ == 0)
			high_score_ = long.MaxValue;
		
		game_ = GameObject.Find("Game").GetComponent<Game>();
	
		time_left_ = GetComponentInChildren<TimeLeft>();
		
		next_round_ = GetComponentInChildren<HideOnEndAnimation>();
		
		//Ammo
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		Framework.Instance.InstanceComponent<Store>();
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		max_ammo_ = store_info_["dd_ammo_level"]["upgrades"][player_data_.GetInt("dd_ammo_level")]["effect"].AsFloat;
		dd_ammo_bar_ = GameObject.Find("Ammo_sliced_sprite").GetComponent<tk2dSlicedSprite>();
		UpdateAmmo();
		
		string power_punch_equipped_str = player_data_.GetString("power_punch_equipped");
		if(power_punch_equipped_str == "great_ball_of_fire")
			current_power_punch_ = PowerPunchType.great_ball_of_fire;
		else if(power_punch_equipped_str == "aaatatata")
			current_power_punch_ = PowerPunchType.a_ta_ta_ta;
		else if(power_punch_equipped_str == "huge_punch")
			current_power_punch_ = PowerPunchType.huge_punch;
		else
		{
			current_power_punch_ = PowerPunchType.count;
			//GameObject.Find("PowerPunchButton").active = false;
		}
		
		level_digits_.UpdateValue(1);
		
		rounds_.Add( GameObject.Find("ResultsContainer/Round1").GetComponent<BossBattleLogRoundIconPrefab>() );
		rounds_.Add( GameObject.Find("ResultsContainer/Round2").GetComponent<BossBattleLogRoundIconPrefab>() );
		rounds_.Add( GameObject.Find("ResultsContainer/Round3").GetComponent<BossBattleLogRoundIconPrefab>() );
		
		for(int i = 0; i < rounds_.Count; i++)
			rounds_[i].SetType(Game.BossResultType.none);
		
		if(game_.game_mode() == Game.GameMode.multiplayer || game_.game_mode() == Game.GameMode.boss_training)
		{
			GameObject.Find("Fight_sprite").GetComponent<tk2dSprite>().SetSprite("InGameRound1");
			score_digits_.gameObject.SetActive( false );
			GameObject.Find("ScoreLabelSprite").SetActive( false );
		}
		else
		{
			GameObject.Find("ResultsContainer").SetActive( false );
		}
		
		//Set Rocket Hit size
		GameObject.Find("RocketHit").GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(
			Screen.width / Framework.Instance.graphics_manager().camera_scale(),
			Screen.height / Framework.Instance.graphics_manager().camera_scale());
			
#if UNITY_WEBPLAYER
		//Controller
		GameObject.Find("GestureController").SetActive(false);
#endif
	}
	
	void Start () {

	}
	// Update is called once per frame
	void Update () {
	/*
#if UNITY_WEBPLAYER
		if( Input.GetKeyDown(KeyCode.Space) )
		{
			SpawnPowerPunch();
		}
#endif
	*/
	}
	
	//setter ui
	public void UpdateScore(long score_value)
	{
		score_digits_.UpdateValue(score_value);
	}
	
	public void UpdateCoins (int coins_value)
	{
		coins_digits_.UpdateValue(coins_value);
	}	
	
	public void UpdateMultiplier(int multiplier_value, bool do_animation)
	{
		if(do_animation)
			multiplier_animation_.Play();
		multipler_digits_.UpdateValue(multiplier_value);
	}
	
	public void UpdateAmmo()
	{
		if (player_data_.GetBool("tutorial_in_game_done") == false)
			max_ammo_ = 10;
		
		if ((bool)Framework.Instance.InstanceComponent<wmf.StaticParams>().Get ("IsMultiplayer") == true)
			max_ammo_ = 1;
		
		float dd_ammo_value_ = ((float)GetCurrentAmmo()) / max_ammo_*58.0F;
		
		if (dd_ammo_value_ <= 0){
			dd_ammo_value_ = 0;
			dd_ammo_bar_.GetComponent<MeshRenderer>().enabled = false;
		}
		else
			dd_ammo_bar_.GetComponent<MeshRenderer>().enabled = true;

		Vector2 dd_ammo_vector = new Vector2(dd_ammo_value_,6.0F);
		dd_ammo_bar_.GetComponent<tk2dSlicedSprite>().dimensions = dd_ammo_vector;
		
		ammo_digits_.UpdateValue( GetCurrentAmmo() );
	}
	
	public int GetCurrentAmmo(){
		
		if (player_data_.GetBool("tutorial_in_game_done") == false)
			return 10;
		
		if (game_.game_mode() == Game.GameMode.singleplayer)
			return player_data_.GetInt("dd_ammo");
		else //mulitplayer or training
			return game_.GetMultiplayerAmmmo();
		
	}
	
	
	public void SetLifes(float n)
	{		
		float full_life_value_ = GameObject.Find("Player").GetComponent<Player>().GetInitialHealth();
		float dd_life_value_ = n/full_life_value_;
		
		if (dd_life_value_ <= 0){
			dd_life_value_ = 0;
			life_dd_sprite_.GetComponent<MeshRenderer>().enabled = false;
		}
		else
			life_dd_sprite_.GetComponent<MeshRenderer>().enabled = true;
			
		
		if (dd_life_value_ <= 0.20F)
			life_dd_sprite_.animation.Play();
		else
		{
			life_dd_sprite_.GetComponent<tk2dSlicedSprite>().color = new Color(1.0F, 1.0F, 1.0F, 1.0F);
			life_dd_sprite_.animation.Stop();	
		}
		
		life_dd_sprite_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(dd_life_value_*100.0F,8F);
		
		
	} 
	
	
	public void SetBossLife(float ratio){
		
		if (ratio <= 0){
			boss_life_bar_.GetComponent<MeshRenderer>().enabled = false;
		}
		else
			boss_life_bar_.GetComponent<MeshRenderer>().enabled = true;
		
		
		if (ratio <= 0.20F)
			boss_life_bar_.animation.Play();
		else
		{
			boss_life_bar_.GetComponent<tk2dSlicedSprite>().color = new Color(1.0F, 1.0F, 1.0F, 1.0F);
			boss_life_bar_.animation.Stop();
		}
		
		 boss_life_bar_.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(ratio*100.0F,8F);
	}
	
	public void SetLevel(int level_number, bool play_audio, bool show_round)
	{
		level_digits_.UpdateValue( level_number );
		
		if(show_round)
		{
			string sprite_name = "";
			switch(level_number)
			{
				case 2:
						{
							sprite_name = "InGameRound2";
								if(play_audio)
									Framework.Instance.audio_manager().Play("42_Next Round Final", transform, 1.0f, false);
						}
				break;
				case 3:
					sprite_name = "FinalRound";
				break;
				default:
						{
							sprite_name = "Popup10";
								if(play_audio)
									Framework.Instance.audio_manager().Play("42_Next Round Final", transform, 1.0f, false);
						}
				break;
			}
			next_round_.GetComponent<tk2dSprite>().SetSprite(sprite_name);
			next_round_.Play();
		}
		else
		{
			next_round_.Play();
			if(play_audio)
				Framework.Instance.audio_manager().Play("42_Next Round Final", transform, 1.0f, false);
		}
			
	}		
	
	public void SetRoundResult(int level_number, Game.BossResultType round_result)
	{
		rounds_[level_number].SetType(round_result);
	}
	
	public void ShowFightOver()
	{
		next_round_.GetComponent<tk2dSprite>().SetSprite("FightOver");
		next_round_.Play();
	}
	
	
	public void TimeIsUp()
	{
		next_round_.GetComponent<tk2dSprite>().SetSprite("TimesUp");
		next_round_.Play();
	}
	void DisableHeart()
	{
	}
	
	public void PlayHitAnimation()
	{
		hit_animation_.Play("RocketHitAnimation");
	}
	
	//PowerPunch
	public void SpawnPowerPunch()
	{
		//GameObject.Find("Game").GetComponent<Game>().CancelPowerPunchHasBeenUsed();
		game_.CancelPowerPunchHasBeenUsed();
		
		SpawnPowerPunch( current_power_punch_ );
	}
	
	public void SpawnPowerPunch(PowerPunchType type)
	{
		bool spawned = game_.SpawnPowerPunch( type );
		if(spawned)
			UpdateAmmo();
	}
	
	//score container
	public void PlayEnteringScore()
	{
		score_animation_.Play("EnteringScore");
	}
	
	public void PlayExitingScore()
	{
		score_animation_.Play("ExitingScore");
	}
	
	//TimeLeft
	public void SetTimeLeft(int time)
	{			
		time_left_.SetDigits(time);
	}
	
	public void AddTimeLeft(int time)
	{
		time_left_.AddDigitis(time);
	}
	
	public void GoTimeLeft()
	{
		time_left_.Go ();
	}
	
	public void StopTimeLeft()
	{
		time_left_.Stop ();
		//time_left_.SetDigits(99);
	}
	
	public int StartTimeDown(){
		return time_left_.StartTimeDown();
	}
	
	public void SetContenderName(string contender_name)
	{
		if(contender_name  != "")
		{
			GameObject.Find("GUI/ContenderName/TextMesh").GetComponent<tk2dTextMesh>().text = contender_name;
			GameObject.Find("GUI/ContenderName/TextMesh").GetComponent<tk2dTextMesh>().Commit();
		}
		else
		{
			GameObject.Find("GUI/ContenderName/LabelName").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("GUI/ContenderName/TextMesh").SetActive(false);
		}
	}
}
