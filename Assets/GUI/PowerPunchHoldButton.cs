using UnityEngine;
using System.Collections;

public class PowerPunchHoldButton : MonoBehaviour {
	
	private int current_id_ = -1;
	private bool touched_ = false;
	private float pressed_time_ = 0;
	private bool has_been_pressed = false;
	private bool has_been_called = false;
	
	private tk2dSpriteAnimator charge_animation_;
	private AudioManager audio_manager_;
	private AudioSource audio_source_;
	
	private bool power_punch_enabled_ = true;
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
	private const float power_punch_time_ = 1.0F;
	private const float power_punch_start_charging_ = 0.4F;
#else
	private const float power_punch_time_ = 0.7F;
	private const float power_punch_start_charging_ = 0.3F;
#endif
	
	private bool start_charging_ = false;
	
	// Use this for initialization
	void Start () {
		audio_manager_ = Framework.Instance.audio_manager();
		charge_animation_ = GameObject.Find("Player/PowerPunchChargeAnimation").GetComponent<tk2dSpriteAnimator>();
		charge_animation_.Stop();
	}
	
	// Update is called once per frame
	void Update () {
		
		
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
		if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetButtonDown("Button A"))
		{
			start_charging_ = true;
		}
		else if(Input.GetKeyUp(KeyCode.RightArrow) || Input.GetButtonUp("Button A"))
		{	
			pressed_time_ = 0.0f;
			StopChargeAnim();
			start_charging_ = false;
		}
#elif UNITY_IPHONE || UNITY_ANDROID
		bool valid = false;
		
		foreach (Touch touch in Input.touches) {
			valid = true;
			
			if (touch.position.x > Screen.width * 0.5F) {
				if (touch.phase == TouchPhase.Began) 
				{	
					current_id_ = touch.fingerId;
					touched_ = true;
					
					start_charging_ = true;
				}
				else if (current_id_ != -1) 
				{
					if (touch.phase == TouchPhase.Moved)
					{
						
					}
					else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) 
					{
						current_id_ = -1;
						touched_ = false;
						
						pressed_time_ = 0.0f;
						StopChargeAnim();
						start_charging_ = false;
					} 
				}
			}
		}
		
		if (!valid) {
			current_id_ = -1;
			touched_ = false;
			
			pressed_time_ = 0.0f;
			StopChargeAnim();
			start_charging_ = false;
		}
#endif
		
		if(charge_animation_.Playing)
		{
			pressed_time_ += Time.deltaTime;
			if(pressed_time_ > power_punch_time_)
			{
				StopChargeAnim();
				GameObject.Find("GUI").GetComponent<GameUI>().SpawnPowerPunch();
			}
		}
		
		if(start_charging_ == true && power_punch_enabled_ == true)
		{
			pressed_time_ += Time.deltaTime;
			if(pressed_time_ > power_punch_start_charging_)
			{
				start_charging_ = false;
				PlayChargeAnim();
			}
		}

	}
	
	public void Pressed (){
#if UNITY_IPHONE || UNITY_ANDROID
		start_charging_ = true;
#endif
	}

	public void Released (){
#if UNITY_IPHONE || UNITY_ANDROID
		pressed_time_ = 0.0f;
		StopChargeAnim();
		start_charging_ = false;
#endif
	}


	public void PlayChargeAnim()
	{
		if(!charge_animation_.Playing && GameObject.Find("Game").GetComponent<PowerPunchManager>().current_power_punch() == null)
		{
			charge_animation_.GetComponent<MeshRenderer>().enabled = true;
			charge_animation_.Play();	
		}
	}
	
	public void StopChargeAnim()
	{
		charge_animation_.Stop();	
		charge_animation_.GetComponent<MeshRenderer>().enabled = false;
	}
	
	
	public void SetPowerPunchEnabled (bool enabled)
	{
		power_punch_enabled_ = enabled;
	}
	
}
