using UnityEngine;
using System.Collections;

public class ComboPopUpContainer : MonoBehaviour {
	private AudioManager audio_manager_;
	
	// Use this for initialization
	void Start () {
		audio_manager_= Framework.Instance.audio_manager();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void PlayEnterSound(){
		audio_manager_.Play("11_Combo Appear", transform, 1.0F, false);
	}
}
