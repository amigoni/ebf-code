using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class PauseScene : MonoBehaviour {
	
	public GameObject settings_container_;
	
	public bool click_enabled_ = true;
	
	//Analitycs
	private Stopwatch pause_time_ms_;
	
	// Use this for initialization
	void Awake ()
	{
		Messenger.AddListener("ClosedPauseSettings", EnableButtons);
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if(Game.FindObjectOfType( typeof(SettingsScene) ) == null)
			if (Input.GetKeyDown(KeyCode.Escape))
				ResumePressed();
	}
#endif
	
	public void Init(long score, Game.GameMode game_mode)
	{
		// NumTextMesh
		if(game_mode == Game.GameMode.multiplayer)
		{
			//reposition object
			GameObject.Find("PlateContainer/ScoreSprite").transform.position = new Vector3(
				148.0f,
				228.0f,
				GameObject.Find("PlateContainer/ScoreSprite").transform.position.z);
			GameObject.Find("PlateContainer/PausePlateSprite").transform.position = new Vector3(
				240.0f,
				189.0f,
				GameObject.Find("PlateContainer/PausePlateSprite").transform.position.z);
			GameObject.Find("PlateContainer/ScoreDigitsWidget").transform.position = new Vector3(
				240.0f,
				190.0f,
				GameObject.Find("PlateContainer/ScoreDigitsWidget").transform.position.z);
			
			GameObject.Find("MissionDisplayPrefab").SetActive(false);
		}
		
		if(game_mode == Game.GameMode.singleplayer)
			GameObject.Find("PlateContainer/ScoreDigitsWidget").GetComponent<DigitsWidget>().UpdateValue(score);
		else
			GameObject.Find("PlateContainer").SetActive(false);
		
		if(game_mode == Game.GameMode.boss_training)
		{
			GameObject.Find("MissionDisplayPrefab").SetActive(false);
		}
		
		//Analitycs
		pause_time_ms_ = new Stopwatch();
		pause_time_ms_.Start();
		
		//Analytics
		Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		analitycs_.AddEvent(KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "menu_name", "GameScene");
		analitycs_.AddNewKey(KeenIOEventType.menu_opened, "sub_menu_name", "PausePopUp");
		analitycs_.SendEvent(KeenIOEventType.menu_opened);
	}
	
	void ResumePressed(){
		GameObject.Find("Game").GetComponent<Game>().ResumeGame();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		
		//Analitycs
		SetAnalitycs();
	}
	
	
	void SettingsPressed(){
		settings_container_.SetActive(true);
		ToggleButtons(false);
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	void MenuPressed(){
		//Analitycs
		SetAnalitycs();
		
		GameObject.Find("Game").GetComponent<Game>().ExitGame();
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	
	public void ToggleButtons(bool enabled){
		click_enabled_ = enabled;
		foreach( tk2dUIItem button in gameObject.GetComponentsInChildren<tk2dUIItem>())
		{
			if (button.name.Contains("Settings") || button.name.Contains("Resume") || button.name.Contains("MainMenu"))
				button.enabled = enabled;
		}
		
		foreach( tk2dButton button in gameObject.GetComponentsInChildren<tk2dButton>())
		{
			if (button.name.Contains("Settings") || button.name.Contains("Resume") || button.name.Contains("MainMenu"))
				button.enabled = enabled;
		}
	}
	
	
	private void EnableButtons()
	{
		ToggleButtons(true);
	}

	
	void OnDestroy()
	{
		Messenger.RemoveListener("ClosedPauseSettings", EnableButtons);	
	}
	
	//Analitycs
	private void SetAnalitycs()
	{
		pause_time_ms_.Stop();
		Framework.Instance.InstanceComponent<Analitycs>().AddValue(KeenIOEventType.run_end, "pause_time_ms", (float)pause_time_ms_.Elapsed.TotalMilliseconds);
	}
}
