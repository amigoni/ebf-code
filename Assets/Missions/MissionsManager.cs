using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization; 
using System.IO; 
using System;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

public class MissionsManager : MonoBehaviour
{
	public GameObject mission_pop_up_;
	
	public enum MissionType
	{
		hit_boss,
		hit_boss_bouncing,
		hit_boss_pp1,
		hit_boss_pp2,
		hit_boss_pp3,
		hit_up_enemy,
		hit_tap_enemy,
		hit_down_enemy,
		hit_bouncing_enemy,
		hit_chasing_enemy,
		hit_fast_enemy,
		be_hit_by_avoid,
		be_hit_by_chainsaw,
		be_hit_by_fast_enemy,
		be_hit_by_fast_rockets,
		collect_coin,
		collect_white_coin,
		collect_heart,
		collect_multiplier,
		collect_invulnerability,
		collect_big_shoot,
		collect_restore,
		collect_cleaner_explosion,
		collect_magnetic,
		collect_freeze,
		max_point,
		stun_boss,
		combo_complete,
		level_perfect,
		mega_bomb_destroy,
		time_finished,
		no_hits_to_level,
		chainsaw_no_hit,
		no_power_punch_to_level,
		mission_type_count
	
	}
	
	public struct SubMission
	{
		public MissionType type_;
		public int value_;
		public string text_;
		public bool session_;
		public bool completed_;
		
		public void Reset ()
		{
			value_ = 0;
			completed_ = false;
		}
		
	};
	
	public struct Mission
	{
		public int id_;
		public SubMission[] sub_missions_;

		public bool IsCompleted ()
		{
			bool completed = true;
			for (int i = 0; i< sub_missions_.Length; i++)
				completed &= sub_missions_ [i].completed_;
			
			return completed;
		}
		
		public void Reset ()
		{	
			for (int i = 0; i< sub_missions_.Length; i++)
				sub_missions_ [i].Reset ();
		}
	};
	
	private Mission current_mission_;
	public int current_mission_id_;
	private Mission last_mission_;
	private int mission_count_ = 0;
	private const int mission_max_ = 21;
	public bool initial_mission_has_been_completed = false;
	
	private bool enabled_ = true;
	private bool initialized_ = false;
	
	//Analitycs
	private Analitycs analitycs_;
	
	//mission abs stats
	private Dictionary<MissionType, int> absolute_stats_;

	public Dictionary<MissionType, int>  absolute_stats {
		get { return absolute_stats_; } 	
	}
	//mission total stats
	private Dictionary<MissionType, int> total_stats_;

	public Dictionary<MissionType, int>  total_stats {
		get { return total_stats_; } 	
	}
	//mission session stats
	private Dictionary<MissionType, int> session_stats_;

	public Dictionary<MissionType, int>  session_stats {
		get { return session_stats_; } 	
	}
	
	//mission best stats
	private Dictionary<MissionType, int> best_stats_;

	public Dictionary<MissionType, int>  best_stats {
		get { return best_stats_; } 	
	}
	
	//Analitycs
	private struct AnalitycsParams
	{
		public object quest_id;
		public object progress_status;
		public object run_id;
		public object quest_progress;
		public object quest_name;
	}
	
	private List<AnalitycsParams> analitycs_params_ = new List<AnalitycsParams>();
	
	// Use this for initialization
	void Start ()
	{	
		
	}
	
	public void Init (bool check_new_mission, bool send_analitycs)
	{		
		//Analitycs
		analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			
		initialized_ = true;
		enabled_ = true;
		//Init absolute stats
		InitAbsoluteStats ();
		//Init total stats
		InitTotalStats ();	
		//Init session stats
		InitSessionStats ();
		//Init Best stats
		InitBestStats ();
		
	
		//first time
		current_mission_id_ = PlayerPrefs.GetInt ("current_mission_id");
		
		if (current_mission_id_ == 0) {
			current_mission_id_ = 1;
			PlayerPrefs.SetInt ("current_mission_id", current_mission_id_);
		} 
		
		current_mission_ = LoadMissionFromXml (current_mission_id_);
		
		if (current_mission_id_ > mission_max_) {
			current_mission_ = new Mission ();
			current_mission_.id_ = mission_max_ + 1;
			current_mission_.sub_missions_ = new SubMission[3];
			
			for (int i = 0; i < 3; ++i) {
				current_mission_.sub_missions_ [i].text_ = "NO MISSION";	
				current_mission_.sub_missions_ [i].completed_ = false;	
			}
			Save();
			Disabled();
			
			PlayerPrefs.SetInt ("current_mission_id", mission_max_);
		}
		
		if (current_mission_id_ > 1)
			last_mission_ = LoadMissionFromXml (current_mission_id_-1);
		
		
		if(PlayerPrefs.GetInt("first_time") == 0)
		{
			SaveDictonary ("absolute_stats", absolute_stats_);
			SaveDictonary ("total_stats", total_stats);
			SaveDictonary ("best_stats", best_stats_);
			
			PlayerPrefs.SetInt("first_time", 1);
		}
		Load ();
		
		if(check_new_mission)
			CheckNewMission (send_analitycs);
	}
			
	public bool CheckNewMission (bool send_analitycs)
	{
		if (current_mission_.IsCompleted ()) {
			last_mission_ = current_mission_;
			current_mission_id_++;
			initial_mission_has_been_completed = true;
			
			if (current_mission_id_ < mission_max_) {
				current_mission_ = LoadMissionFromXml (current_mission_id_);
				for (int i = 0; i < (int)MissionType.mission_type_count; ++i) {
					total_stats_ [(MissionType)i] = 0;
				}
				
				for(int i = 0; i < current_mission_.sub_missions_.Length; i++)
				{
					SendAnalitycs(
						current_mission_.id_ * 1000 + i, 
						"Assign", 0, 
						current_mission_.sub_missions_[i].text_); 
				}
			} else {
				current_mission_ = new Mission ();
				current_mission_.id_ = mission_max_ + 1;
				current_mission_.sub_missions_ = new SubMission[3];
			
				for (int i = 0; i < 3; ++i) {
					current_mission_.sub_missions_ [i].text_ = "NO MISSION";	
					current_mission_.sub_missions_ [i].completed_ = false;	
				}
				Save();
			}
			PlayerPrefs.SetInt ("current_mission_id", current_mission_id_);
			//PlayerPrefs.SetInt ("multiplier", current_mission_id_);
			
			//reset session stats
			for (int i = 0; i < (int)MissionType.mission_type_count; ++i) {
				total_stats_ [(MissionType)i] = 0;
				session_stats_ [(MissionType)i] = 0;
				best_stats_ [(MissionType)i] = 0;
			}
			
			Save ();
			
			return true;
		}
		
		return false;
	}
	
	private void InitAbsoluteStats ()
	{
		absolute_stats_ = new Dictionary<MissionType, int> ();
		for (int i = 0; i < (int)MissionType.mission_type_count; ++i)
			absolute_stats_.Add ((MissionType)i, 0);
	}
	
	private void InitTotalStats ()
	{
		total_stats_ = new Dictionary<MissionType, int> ();
		for (int i = 0; i < (int)MissionType.mission_type_count; ++i)
			total_stats_.Add ((MissionType)i, 0);
	}
	
	private void InitSessionStats ()
	{
		session_stats_ = new Dictionary<MissionType, int> ();
		for (int i = 0; i < (int)MissionType.mission_type_count; ++i)
			session_stats_.Add ((MissionType)i, 0);
	}
	
	private void InitBestStats ()
	{
		best_stats_ = new Dictionary<MissionType, int> ();
		for (int i = 0; i < (int)MissionType.mission_type_count; ++i)
			best_stats_.Add ((MissionType)i, 0);
	}
	
	
	public int GetTotalStatsForType( MissionType type){
		return total_stats_[type];
	}
	
	public int GetBestStatsForType( MissionType type){
		return best_stats_[type];
	}
	
	private Mission LoadMissionFromXml (int missionIdToCheck)
	{		
		//Load from xml
		string xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/Missions");
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;
		XmlNodeList missions_nodes = xml_doc.DocumentElement.ChildNodes;
		mission_count_ = missions_nodes.Count;
		
		Mission mission_ = new Mission ();
		
		for (int j = 0; j < missions_nodes.Count; ++j) {	
			int mission_id = int.Parse (missions_nodes [j].Attributes ["id"].Value); 
			if (mission_id != missionIdToCheck)
				continue;
				
			XmlNodeList goals_missions = missions_nodes [j].ChildNodes;	
			mission_.id_ = mission_id;
			mission_.sub_missions_ = new SubMission[goals_missions.Count];
			
			int mission_number = 0;
			for (int i = 0; i < goals_missions.Count; ++i) {
				//check on #comment
				string child_name = goals_missions [i].LocalName;
				if (child_name != "#comment") {
					SubMission sub_mission = new SubMission ();
					sub_mission.type_ = (MissionType) System.Enum.Parse (typeof(MissionType), child_name, true);
					sub_mission.completed_ = false;
					switch (sub_mission.type_) {
					case MissionType.hit_boss:
					case MissionType.hit_boss_bouncing:
					case MissionType.hit_boss_pp1:
					case MissionType.hit_boss_pp2:
					case MissionType.hit_boss_pp3:
					case MissionType.hit_up_enemy:
					case MissionType.hit_tap_enemy:
					case MissionType.hit_down_enemy:
					case MissionType.hit_bouncing_enemy:
					case MissionType.hit_chasing_enemy:
					case MissionType.hit_fast_enemy:
					case MissionType.be_hit_by_avoid:
					case MissionType.be_hit_by_chainsaw:
					case MissionType.be_hit_by_fast_enemy:
					case MissionType.be_hit_by_fast_rockets:
					case MissionType.collect_coin:
					case MissionType.collect_heart:
					case MissionType.collect_multiplier:
					case MissionType.collect_invulnerability:
					case MissionType.collect_big_shoot:
					case MissionType.collect_restore:
					case MissionType.collect_cleaner_explosion:
					case MissionType.collect_magnetic:
					case MissionType.collect_freeze:	
					case MissionType.max_point:
					case MissionType.stun_boss:	
					case MissionType.combo_complete:
					case MissionType.level_perfect:
					case MissionType.mega_bomb_destroy:
					case MissionType.time_finished:
					case MissionType.no_hits_to_level:
					case MissionType.chainsaw_no_hit:
					case MissionType.no_power_punch_to_level:
						sub_mission.value_ = int.Parse (goals_missions [i].Attributes ["value"].Value);
						sub_mission.text_ = goals_missions [i].Attributes ["text"].Value;
						sub_mission.session_ = bool.Parse (goals_missions [i].Attributes ["session"].Value);
						break;
					default:
						break;
					}
					
					mission_.sub_missions_ [mission_number] = sub_mission;
					mission_number++;
				}
			}
		}
		
		if(mission_.sub_missions_ == null)
		{
			mission_ = new Mission ();
			mission_.id_ = mission_max_ + 1;
			mission_.sub_missions_ = new SubMission[3];
			
			for (int i = 0; i < 3; ++i) {
				mission_.sub_missions_ [i].text_ = "NO MISSION";	
				mission_.sub_missions_ [i].completed_ = false;	
			}
		}
		
		return mission_;
	}
	
	public void ResetAll ()
	{
		//Reset stats
		for (int i = 0; i < (int)MissionType.mission_type_count; ++i) {
			absolute_stats_ [(MissionType)i] = 0;
			total_stats_ [(MissionType)i] = 0;
			session_stats_ [(MissionType)i] = 0;
			best_stats_ [(MissionType)i] = 0;
		}
		
		//
		current_mission_id_ = 1;
		current_mission_ = LoadMissionFromXml (current_mission_id_);
		
		Save ();
	}
	
	public void Reset ()
	{	
		//Reset stats
		for (int i = 0; i < (int)MissionType.mission_type_count; ++i) {
			total_stats_ [(MissionType)i] = 0;
			session_stats_ [(MissionType)i] = 0;
			best_stats_ [(MissionType)i] = 0;
		}
		
		current_mission_id_ = 1;
		current_mission_ = LoadMissionFromXml (current_mission_id_);
		
		Save ();
		
		//PlayerPrefs.SetInt ("multiplier", 1);
		PlayerPrefs.SetInt ("current_mission_id", current_mission_id_);
		PlayerPrefs.SetInt ("best_score", 0);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
	
	//Behaviour for game
	public void UpdateMission (MissionType type, int v)
	{
		if(!enabled_)
			return;
		
		absolute_stats_ [type] += v;
		session_stats_ [type] += v;
		total_stats_ [type] += v;
		//Debug.Log (type+" "+session_stats_ [type]+" "+best_stats_[type]);
	
		if (session_stats_[type] > best_stats_[type])
			best_stats_[type] = session_stats_[type];
			
		CheckMission (type);
	}
	
	//Check Mission
	private void CheckMission (MissionType type)
	{
		for (int i = 0; i < current_mission_.sub_missions_.Length; ++i) {
			SubMission sub_mission = current_mission_.sub_missions_ [i];
			if(sub_mission.text_ == "NO MISSION")
				return;
			
			//Analitycs
			string progress_status = "Progress";
			
			//check id completed yet 
			if (sub_mission.completed_ == false) {
				//check to total and session
				if ((sub_mission.session_ == false && 
					 sub_mission.value_ <= total_stats_ [sub_mission.type_]) ||
					(sub_mission.session_ == true && 
					 sub_mission.value_ <= session_stats_ [sub_mission.type_])) {
					current_mission_.sub_missions_ [i].completed_ = true;
					
					//Spawn message
					GameObject popup = Instantiate (mission_pop_up_, mission_pop_up_.transform.position, Quaternion.identity) as GameObject;
					popup.GetComponentInChildren<tk2dTextMesh> ().text = current_mission_.sub_missions_ [i].text_;
					popup.GetComponentInChildren<tk2dTextMesh> ().Commit ();

					Save ();
					
					//Analitycs
					progress_status = "Complete";
				}
			}
			
			//Analitycs 
			if(sub_mission.type_ == type)
			{
				if(sub_mission.session_ == true)
				{
					SendAnalitycs(
						current_mission_.id_ * 1000 + i, 
						progress_status, 
						session_stats_ [sub_mission.type_],
						current_mission_.sub_missions_[i].text_);
				}
				else
				{
					SendAnalitycs(
						current_mission_.id_ * 1000 + i, 
						progress_status, 
						total_stats_ [sub_mission.type_],
						current_mission_.sub_missions_[i].text_);
				}
			}
		}
	}
	
	//Load
	public void Load ()
	{	
		//Load and Unsplit dictionaries
		LoadDictionary ("absolute_stats", absolute_stats_);		
		LoadDictionary ("total_stats", total_stats);
		LoadDictionary ("best_stats", best_stats_);
		
		current_mission_id_ = PlayerPrefs.GetInt ("current_mission_id");
		if (current_mission_id_ == 0)
			return;
		
		//Load current mission
		for (int i = 0; i < current_mission_.sub_missions_.Length; ++i) {
			string bool_txt = PlayerPrefs.GetString ("sub_mission_completed_" + i);
			if (bool_txt == "True")
				current_mission_.sub_missions_ [i].completed_ = true;
			else
				current_mission_.sub_missions_ [i].completed_ = false;
		}
	}
	
	//Load dictionary behavior
	private void LoadDictionary (string base_name, Dictionary<MissionType, int> dictionary)
	{
		List<MissionType> tmp_mission_list = new List<MissionType> ();
		List<int> tmp_value_list = new List<int> ();
		string tmp_text = ReadXml (base_name + "_keys.xml");
		if (tmp_text != "")
		{
#if UNITY_IPHONE || UNITY_ANDROID	
			tmp_mission_list = DeserializeMissionTypeList(tmp_text);
#else
			tmp_mission_list = (List<MissionType>)DeserializeObject (tmp_text, typeof(List<MissionType>));
#endif
		}
		tmp_text = ReadXml (base_name + "_value.xml");
		if (tmp_text != "")
		{
#if UNITY_IPHONE || UNITY_ANDROID	
			tmp_value_list = DeserializeIntList (tmp_text);
#else
			tmp_value_list = (List<int>)DeserializeObject (tmp_text, typeof(List<int>));
#endif
		}
		
		unsplitDictionary (dictionary, tmp_mission_list, tmp_value_list);
	}
	
	private void unsplitDictionary (Dictionary<MissionType, int> dictionary, 
								   List<MissionType> mission_list,
								   List<int> value_list)
	{
		for (int i = 0; i < mission_list.Count; ++i) {
			dictionary [mission_list [i]] = value_list [i];
		}
	}
	
	//Save                         
	public void Save ()
	{
		if(initialized_)
		{
			//Split and save dictionaries
			SaveDictonary ("absolute_stats", absolute_stats_);
			SaveDictonary ("total_stats", total_stats);
			SaveDictonary ("best_stats", best_stats_);
			//Save current mission
			for (int i = 0; i < current_mission_.sub_missions_.Length; ++i)
				PlayerPrefs.SetString ("sub_mission_completed_" + i, current_mission_.sub_missions_ [i].completed_.ToString ());
		}
	}
	
	//Save dictionary behavior
	private void SaveDictonary (string base_name, Dictionary<MissionType, int> dictionary)
	{
		List<MissionType> tmp_mission_list = new List<MissionType> ();
		List<int> tmp_value_list = new List<int> ();
		SplitDictionary (dictionary, tmp_mission_list, tmp_value_list);
		
#if UNITY_IPHONE || UNITY_ANDROID
		WriteXML (SerializeList<MissionType> (tmp_mission_list), base_name + "_keys.xml");
		WriteXML (SerializeList<int> (tmp_value_list), base_name + "_value.xml");
#else
		WriteXML (SerializeObject (tmp_mission_list, typeof(List<MissionType>)), base_name + "_keys.xml");
		WriteXML (SerializeObject (tmp_value_list, typeof(List<int>)), base_name + "_value.xml");
#endif
	}
	
	private void SplitDictionary (Dictionary<MissionType, int> dictionary, 
								 List<MissionType> mission_list,
								 List<int> value_list)
	{
		foreach (KeyValuePair<MissionType, int> pair in dictionary) {
			mission_list.Add (pair.Key);
			value_list.Add (pair.Value);
		}
	}

#if UNITY_IPHONE || UNITY_ANDROID	

	string SerializeList<T>(List<T> list)
	{
		string return_string = "";
		for(int i = 0; i < list.Count; i++)
		{
			return_string += list[i].ToString() + ';';
		}
		return_string = return_string.Remove(return_string.Length - 1);
		
		return return_string;
	}
	
	List<int> DeserializeIntList(string input_string)
	{
		List<int> list = new List<int>();
		string[] input_array = input_string.Split(';');
		for(int i = 0; i < input_array.Length; i++)
		{
			list.Add( int.Parse(input_array[i]) );
		}
		return list;
	}
	
	List<MissionType> DeserializeMissionTypeList(string input_string)
	{
		List<MissionType> list = new List<MissionType>();
		string[] input_array = input_string.Split(';');
		for(int i = 0; i < input_array.Length; i++)
		{
			list.Add( (MissionType)System.Enum.Parse (typeof(MissionType), input_array[i], true));
		}
		return list;
	}
#else
	
	string SerializeObject (object pObject, Type type_object)
	{ 
		string XmlizedString = null; 
		MemoryStream memoryStream = new MemoryStream (); 
		XmlSerializer xs = new XmlSerializer (type_object); 
		XmlTextWriter xmlTextWriter = new XmlTextWriter (memoryStream, Encoding.UTF8); 
		xs.Serialize (xmlTextWriter, pObject); 
		memoryStream = (MemoryStream)xmlTextWriter.BaseStream; 
		XmlizedString = UTF8ByteArrayToString (memoryStream.ToArray ()); 
		return XmlizedString; 
	}
	
	object DeserializeObject (string xml_string, Type type_object)
	{
		XmlDocument doc = new XmlDocument ();
		doc.LoadXml ( xml_string );
		XmlNodeReader reader = new XmlNodeReader (doc.DocumentElement);
		XmlSerializer ser = new XmlSerializer (type_object);
		return ser.Deserialize (reader);
	}
#endif
	
	string ReadXml (string filename)
	{
		//PlayerPrefs.DeleteKey(filename);
		string text = PlayerPrefs.GetString(filename);
		text = text.Replace("\ufeff", "");
		return text;
		/*
#if !UNITY_WEBPLAYER
		string text = "";
		StreamReader reader = File.OpenText (Application.persistentDataPath + "/" + filename ); 
		text = reader.ReadToEnd (); 
		reader.Close (); 
		
		return text; 
#else
		return "";
#endif*/
	}
	
	void WriteXML (string xml_text, string filename)
	{ 
        PlayerPrefs.SetString(filename, xml_text);
		/*
#if !UNITY_WEBPLAYER
		StreamWriter writer; 
		//TO-DO on device this may not work
		writer = File.CreateText( Application.persistentDataPath + "/" + filename );
		writer.Write (xml_text); 
		writer.Close (); 
#endif
	*/
	}
    
	string UTF8ByteArrayToString (byte[] characters)
	{      
		UTF8Encoding encoding = new UTF8Encoding (); 
		string constructedString = encoding.GetString (characters); 
		return (constructedString); 
	}
    
	byte[] StringToUTF8ByteArray (string pXmlString)
	{ 
		UTF8Encoding encoding = new UTF8Encoding (); 
		byte[] byteArray = encoding.GetBytes (pXmlString); 
		return byteArray; 
	}
	
	//getters
	public Mission current_mission ()
	{
		return current_mission_;
	}
	
	public Mission last_mission (){
		return last_mission_;//LoadMissionFromXml(current_mission_id_-1);
	}
	
	public int mission_count(){
		return mission_count_;
	}
	
	private string GetPath()
	{
		return Application.persistentDataPath+"/";
	}
	
	public void Disabled()
	{
		enabled_ = false;
	}
	
	void OnApplicationQuit(){
		Save();
	}
	
	//Analitycs
	private void SendAnalitycs(int quest_id, string progress_status, int quest_progress, string quest_name)
	{
		AnalitycsParams analitycs_param = new AnalitycsParams();
		analitycs_param.quest_id = quest_id;
		analitycs_param.progress_status = progress_status;
		analitycs_param.run_id = analitycs_.GetCurrentRunEvent().run_id;
		analitycs_param.quest_progress = quest_progress;
		analitycs_param.quest_name = quest_name;
		
		analitycs_params_.Add(analitycs_param);
	}
	
	public void NewRun()
	{
		analitycs_params_.Clear();
	}
	
	public void EndRun()
	{
		for(int i = 0; i < analitycs_params_.Count; i++)
		{
			if(analitycs_params_[i].progress_status == "Complete")
			{
				analitycs_.AddEvent(KeenIOEventType.quest_state);
			
				analitycs_.AddNewKey(KeenIOEventType.quest_state, "quest_id", analitycs_params_[i].quest_id);
				analitycs_.AddNewKey(KeenIOEventType.quest_state, "progress_status", analitycs_params_[i].progress_status);
				analitycs_.AddNewKey(KeenIOEventType.quest_state, "run_id", analitycs_params_[i].run_id);	
				analitycs_.AddNewKey(KeenIOEventType.quest_state, "quest_progress", analitycs_params_[i].quest_progress);	
				analitycs_.AddNewKey(KeenIOEventType.quest_state, "quest_name", analitycs_params_[i].quest_name); 	
										
				analitycs_.SendEvent(KeenIOEventType.quest_state);
			}
		}
		
		analitycs_params_.Clear();
	}
}
