using UnityEngine;
using System.Collections;

public class Unwalkable : MonoBehaviour {
	
	private GameObject player_;
	public GameObject up_collider_;
	public GameObject down_collider_;
	
	// Use this for initialization
	void Start () {
		player_ = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if(player_.transform.position.y > up_collider_.transform.position.y)
			up_collider_.SetActive(true);
		else
			up_collider_.SetActive(false);
		
		if(player_.transform.position.y < down_collider_.transform.position.y)
			down_collider_.SetActive(true);
		else
			down_collider_.SetActive(false);
	}
}
