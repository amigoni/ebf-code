using UnityEngine;
using System.Collections;

public class Puddle : MonoBehaviour {
	
	private PlayerMoveController player_move_controller_;
	private Animation animation_;
	
	void Awake (){
		player_move_controller_ =  GameObject.Find("Player").GetComponent<PlayerMoveController>();
		animation_ = GetComponent<Animation>();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerStay(Collider other){
		if(other.name == "HarvestCollider"){
			player_move_controller_.InPuddle();
			if(! animation_.isPlaying )
				animation_.Play();
		}	
	}
	
	void OnTriggerExit(Collider other){
		if(other.name == "HarvestCollider"){
			player_move_controller_.OutOfPuddle();
			if(animation_.isPlaying)
				animation_.Stop();
		}
	}
}
