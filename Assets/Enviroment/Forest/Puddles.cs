using UnityEngine;
using System.Collections;

public class Puddles : MonoBehaviour {
	
	public GameObject layout0_;
	public GameObject layout1_;
	public GameObject layout2_;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	public void Init(int configuration_number)
	{
		
		if (configuration_number == 0)
		{
			layout0_.SetActive(true);
			layout1_.SetActive(false);
			layout2_.SetActive(false);
		}
		else if (configuration_number == 1)
		{
			layout0_.SetActive(false);
			layout1_.SetActive(true);
			layout2_.SetActive(false);
		}
		else if (configuration_number == 2)
		{
			layout0_.SetActive(false);
			layout1_.SetActive(false);
			layout2_.SetActive(true);
		}
	}
	
}
