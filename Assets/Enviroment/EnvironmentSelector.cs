using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnvironmentSelector : MonoBehaviour {
	
	private PlayerData player_data_;
	private Environment current_environment_;
	public GameObject puddle_;
	public GameObject puddle_instance_;
	public GameObject player_;
	
	public class Environment
	{
		public string name;
		public bool is_unlocked;
	}
	
	List<Environment> environments_ = new List<Environment>();
	
	private GameObject white_transition_obj_;

	//Shuffle Bag 
	private ShuffleBag shuffle_bag_ = new ShuffleBag(2);
	
	public class ShuffleBag
	{
	    private System.Random random = new System.Random();
	    private List<Environment> data;
	 
	    private Environment currentItem;
		private Environment old_environment_;
	    private int currentPosition = -1;
	 
	    private int Capacity { get { return data.Capacity; } }
	    public int Size { get { return data.Count; } }
		
	    public ShuffleBag(int initCapacity)
	    {
	        data = new List<Environment>(initCapacity);
	    }
	
		
		public void Add(Environment item, int amount)
		{
		    for (int i = 0; i < amount; i++)
		        data.Add(item);
		 
		    currentPosition = Size - 1;
		}
		
		
		public Environment Next()
		{
		    if (currentPosition < 1)
		    {
				currentPosition = Size - 1;
		        currentItem = data[0];
		 
		        return currentItem;
		    }
		 
		    var pos = random.Next(currentPosition);
		 
		    currentItem = data[pos];
		    data[pos] = data[currentPosition];
		    data[currentPosition] = currentItem;
		    currentPosition--;
		 
		    return currentItem;
		}
	}	
	
	
	public void UpdateShuffleBag(int number_of_environments)
	{	
		if (number_of_environments == 1)
		{
			shuffle_bag_ = new ShuffleBag(2);
			foreach (Environment environment in environments_)
			{
				if (environment.is_unlocked == true)
					shuffle_bag_.Add(environment,2);
			}
		}
		else if (number_of_environments == 2)
		{
			shuffle_bag_ = new ShuffleBag(4);
			foreach (Environment environment in environments_)
			{
				if (environment.name == "desert")
					shuffle_bag_.Add(environment,3);
				else if (environment.name == "forest")
					shuffle_bag_.Add(environment,1);
			}
		}
		else if (number_of_environments == 3)
		{
			shuffle_bag_ = new ShuffleBag(8);
			foreach (Environment environment in environments_)
			{
				if (environment.name == "desert")
					shuffle_bag_.Add(environment,5);
				else if (environment.name == "forest")
					shuffle_bag_.Add(environment,2);
				else if (environment.name == "ice")
					shuffle_bag_.Add(environment,1);
			}
		}		
	}
	
	
	private void CheckShuffleBagSize()
	{
		int max_level = player_data_.GetInt("max_level_reached_single_player");
		
		if (max_level >= 6 && max_level< 12)
		{
			 if (environments_.Find(x => x.name == "forest").is_unlocked == false)
			{
				environments_.Find(x => x.name == "forest").is_unlocked = true;
				UpdateShuffleBag(2);
			}		
		}
		else if (max_level >= 12)
		{
			if(environments_.Find(x => x.name == "ice").is_unlocked == false)
			{
				environments_.Find(x => x.name == "ice").is_unlocked = true;
				UpdateShuffleBag(3);
			}
		} 
	}
	//end ShuffleBag
	
	
	void Awake()
	{
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		player_ = GameObject.Find("Player");
		
		environments_.Add(new Environment {name = "desert", is_unlocked = true});
		environments_.Add(new Environment {name = "ice", is_unlocked = false});
		environments_.Add(new Environment {name = "forest", is_unlocked = false});
		
		white_transition_obj_ = GameObject.Find("WhiteTransition");
		white_transition_obj_.SetActive(false);
		
		UpdateShuffleBag(1);
		CheckShuffleBagSize();
		
		current_environment_ = environments_[0];
	}
	
	
	public void ChangeEnvironment(bool with_animation)
	{		
		string old_enviroment_name = current_environment_ != null ? current_environment_.name : "";
		current_environment_ = shuffle_bag_.Next();
		ChangeEnvironment(current_environment_.name, old_enviroment_name, with_animation);
	}
	
	
	public void ChangeEnvironment(string enviroment_name, bool with_animation)
	{
		string old_enviroment_name = current_environment_ != null ? current_environment_.name : "";
		current_environment_ = environments_.Find(x => x.name == enviroment_name);
		ChangeEnvironment(enviroment_name, old_enviroment_name, with_animation);
	}
	
	
	private void ChangeEnvironment(string enviroment_name, string old_enviroment_name, bool with_animation)
	{
		if(enviroment_name != old_enviroment_name)
		{
			CheckShuffleBagSize();
			current_environment_ = environments_.Find(x => x.name == enviroment_name);
		
			if (with_animation == true && old_enviroment_name != enviroment_name)
			{
				//This triggers the SwipeEnvironment while animating
				white_transition_obj_.SetActive(true);
				white_transition_obj_.animation.Play();
			}
			else
				SwipeEnvironment();
		}
	}
	
	
	public void SwipeEnvironment()
	{
		GameObject backgroundContainer = GameObject.Find("BackgroundContainer");
		
		foreach (MeshRenderer background in backgroundContainer.GetComponentsInChildren<MeshRenderer>()){
			//Turn off all backgrounds but white transition
			if (background.name != "WhiteTransition")
				background.enabled = false;
			
			//Turn on the current background
			if (background.name == current_environment_.name)
			{
				background.enabled = true;
				
				if (background.name == "forest")
				{
					//Debug.Log("Spawning Puddles");
					puddle_instance_ = (GameObject) Instantiate(
						puddle_, 
						new Vector3(
							((Screen.width/Framework.Instance.graphics_manager().camera_scale())/2-240),
							GameObject.Find("Game").GetComponent<Game>().GetDeviceOffset().y,
							799),
						Quaternion.identity);
					
					//Assigning parent doesn't work on mobile or web build
					//puddle_instance_.transform.parent = GameObject.Find("BackgroundContainer/forest").transform;
					puddle_instance_.name = "puddle";
					int random = Random.Range(0,3);
					puddle_instance_.GetComponent<Puddles>().Init(random);
				}
				else 
				{
					if (puddle_instance_ != null)
						Destroy(puddle_instance_);
					player_.GetComponent<PlayerMoveController>().OutOfPuddle();
				}
			}
		}
		
		player_.GetComponent<PlayerMoveController>().UpdateEnvironment(current_environment_.name);
		
		Crate[] crates = GameObject.FindObjectsOfType(typeof(Crate)) as Crate[];
		
		foreach(Crate crate in crates)
		{
			crate.ChangeEnviroment();
		}
	}
	
	
	public string GetCurrentEnvironment()
	{
		return current_environment_.name;	
	}
}
