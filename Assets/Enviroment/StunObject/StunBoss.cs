using UnityEngine;
using System.Collections;

public class StunBoss : MonoBehaviour {
	
	private Boss boss_;
	private ZOrderObject z_boss_;
	private float hit_size_y_up = 22;
	private float hit_size_y_down = 22;
	
	private tk2dSprite sprite_;
	private tk2dAnimatedSprite exit_animated_sprite_;
	private tk2dAnimatedSprite fall_animated_sprite_;
	
	private bool on_ground_ = false;
	private int ground_y_ = 60;
	private float a_ = 100.8F;
	private float t_ = 0;
	// Use this for initialization
	void Start () {
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		z_boss_ = GameObject.Find("BossPrefab").GetComponent<ZOrderObject>();
		
		tk2dSprite[] sprites = GetComponentsInChildren<tk2dSprite>();
		for(int i = 0; i < sprites.Length; i++)
		{
			if(sprites[i].name == "Sprite")
				sprite_ = sprites[i];
			else if(sprites[i].name == "ExitAnimatedSprite")
				exit_animated_sprite_ = (tk2dAnimatedSprite)sprites[i];
			else if(sprites[i].name == "FallAnimatedSprite")
				fall_animated_sprite_ = (tk2dAnimatedSprite)sprites[i];
		}
		sprite_.renderer.enabled = false;	
		
		transform.position += new Vector3(GameObject.Find("Game").GetComponent<Game>().GetDeviceOffset().x, 60.0F * Random.value, 0.0F);
		
		ground_y_ = (int) (190 + 30.0F * Random.value);
		GetComponent<ZOrderObject> ().Init(ground_y_ - 70, true);
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!on_ground_)
		{
			t_ += Time.deltaTime;
			transform.Translate(0.0F, -0.5F * a_ * (t_ * t_), 0.0F);
			
			if(transform.position.y < ground_y_)
			{
				on_ground_ = true;
				
				fall_animated_sprite_.animationCompleteDelegate = AnimationCompleteDelegate ;
				fall_animated_sprite_.Play("StunAnimation");
			}
		}
	}
	
	void OnTriggerStay(Collider other) 
	{
		if(other.name == "DamageArea" && on_ground_)
		{
			if(CollideWithBoss ())
			{
				if(!boss_.stunned())
				{
					boss_.Play("ChainSawAnimationExitStun");
					boss_.PreStun();
					//Destroy after stun
					boss_.DestroyStumpAndCreate();
				}
			}
		}
	}
	
	private bool CollideWithBoss ()
	{
		float boss_baseline = z_boss_.GetBaseline ();
		float this_baseline = GetComponent<ZOrderObject> ().GetBaseline ();
		if ((this_baseline > boss_baseline - hit_size_y_down) && 
			(this_baseline < boss_baseline + hit_size_y_up) 	) 
		{
			return true;
		}
			
		return false;
	}
	
	public void PlayExit()
	{
		GetComponent<BoxCollider>().enabled = false;
		if(exit_animated_sprite_ != null)
		{
			if(!exit_animated_sprite_.Playing)
			{
				exit_animated_sprite_.Play();
				Invoke("DestroyAll", exit_animated_sprite_.ClipTimeSeconds);
			}
		}
	}
	
	public void DestroyAndRespawn()
	{
		Destroy(gameObject);
		boss_.InstantiateStump();
	}
	
	public void DestroyAll()
	{
		Destroy(gameObject);
	}
	
	public void AnimationCompleteDelegate(tk2dAnimatedSprite sprite, int clipId)
	{
		sprite_.renderer.enabled = true;				
		fall_animated_sprite_.renderer.enabled = false;
	}
}