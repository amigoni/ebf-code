using UnityEngine;
using System.Collections;

public class Clock : MonoBehaviour {
	
	private tk2dTextMesh text_mesh_;
	private float time_ = 30.0F;
	private Animation animation_;
	
	// Use this for initialization
	void Awake () {
		text_mesh_ = GameObject.Find("Time").GetComponent<tk2dTextMesh>();
		animation_ = GetComponent<Animation>();
	}
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void UpdateClock(float time)
	{
		if(time < 0.0F)
		{
			text_mesh_.text = "00:00";
			text_mesh_.Commit();
		}
		else
		{
			time_ = time;
			//calculare minutes
			int m = (int)(time_ / 60);
			int s = (int)(time_ - (m * 60));
			
			string time_str = m.ToString();
			if(m < 10)
				time_str = time_str.Insert(0, "0");
			time_str += ":";
			time_str += s.ToString();
			if(s < 10)
				time_str = time_str.Insert(3, "0");
			
			text_mesh_.text = time_str;
			text_mesh_.Commit();
		}
	}
	
	public void PlayIdle()
	{
		animation_.Play("ClockIdleAnimation");
	}
	
	public void PlayExit()
	{
		animation_.Play("ClockExitAnimation");
	}
	
	public void DestroyAll()
	{
		GameObject.Destroy(gameObject);
	}
}
