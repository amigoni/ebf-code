using UnityEngine;
using System;
using System.Collections;

// declares the enum
public enum CrateType
{
	coin1,
	coin2,
	heart,
	multiplier,
	invulnerability,
	cleaner_explosion,
	magnetics,
	freeze,
	rand,
	box, 
	white_coin,
	count
}

public class Crate : MonoBehaviour
{
	public CrateType type_;
	
	public GameObject animation_sprite_object_;
	public GameObject background_animated_sprite_object_;
	public GameObject shadow_sprite_object_;
	
	private tk2dAnimatedSprite animation_sprite_;
	private tk2dAnimatedSprite background_animated_sprite_;
	private tk2dSprite shadow_sprite_;
	private Animation animation_component_;
	
	private float start_time_;
	private int ground_y_;
	private bool is_on_ground_;
	
	private const int hit_size_y_ = 20;
	
	private Game game_;
	private Player player_;
	
	//Effects
	private const int multipler_value_ = 1;
	private const float freeze_duration_ = 4.0F;
	//End Effects
	private float life_timer_;
	
	public PopUp pop_up_;
	
	//audio
	private AudioManager audio_manager_;
	private AudioClip audio_clip_;
	
	//ground y
	public bool force_ground_y = false;
	public int ground_y;
	private const int length_baseline = 50;
	
	//Box
	private int box_count_;
	private Boss boss_;
	private BonusSetup bonus_setup_;
	
	//Mission manager
	private MissionsManager mission_manager_;
	
	private PlayerWalkableArea player_walkable_area_ = null;
	
	//taken
	private bool taken_ = false;
	
	// Use this for initialization
	void Awake()
	{
		animation_sprite_ = animation_sprite_object_.GetComponent<tk2dAnimatedSprite> ();
		background_animated_sprite_ = background_animated_sprite_object_.GetComponent<tk2dAnimatedSprite> ();
		shadow_sprite_ = shadow_sprite_object_.GetComponentInChildren<tk2dSprite> ();
		
		animation_component_ = animation_sprite_object_.GetComponent<Animation> ();		
		
		GetComponent<PoolObject>().Spawned = new EventHandler(SpawnFromPool);
		force_ground_y = false;
		
		mission_manager_ = GameObject.Find("Game").GetComponent<Game>().mission_manager();
		player_walkable_area_ = GameObject.Find("PlayerWalkableArea").GetComponent<PlayerWalkableArea>();
	}
	
	void Start ()
	{
		shadow_sprite_ = shadow_sprite_object_.GetComponentInChildren<tk2dSprite> ();
		
		//if(type_ != CrateType.coin1 || type_ != CrateType.coin2)
		//	GetComponent<BoxCollider>().size = new Vector3(0.6F, 0.6F, 1.0F);
		
		start_time_ = Time.time;
		//if(this.transform.position == new Vector3(0.0F, 0.0F, 0.0F))
		//	ground_y_ = 115 + (int)( Random.value * 100.0F );
		if(force_ground_y)
			ground_y_ = ground_y + 10;
		else
			ground_y_ = (int)player_walkable_area_.GetRandomPosition().y;
		
		is_on_ground_= false;
			
		//set ZOrder
		animation_sprite_object_.GetComponent<ZOrderObject> ().Init ((int)(ground_y_), true);
		
		game_ = GameObject.Find("Game").GetComponent<Game>();
		player_ = GameObject.Find("Player").GetComponent<Player>();
		
		shadow_sprite_.transform.position = new Vector3( (int)animation_sprite_.transform.position.x, 
														 (int)ground_y_,
														 (int)animation_sprite_.transform.position.z);
		background_animated_sprite_.transform.position = new Vector3( (int)animation_sprite_.transform.position.x, 
																	  (int)ground_y_,
														 			  (int)animation_sprite_.transform.position.z);
		
		background_animated_sprite_.renderer.enabled = false;
				
		if(type_ == CrateType.multiplier)
		{
			GetComponentInChildren<DigitsWidget>().Init();
			GetComponentInChildren<DigitsWidget>().UpdateValue( GameObject.Find("Game").GetComponent<Game>().multiplier() + 1);
		}
		
		//audio
		audio_manager_ = Framework.Instance.audio_manager();
		
		//Random position
		if(type_ != CrateType.rand)
		{
			if(this.transform.position == new Vector3(0.0F, 0.0F, 0.0F))
				this.transform.position = GetRandomPosition();
		}
		else
			GetComponent<RandCreate>().Init();
		
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		bonus_setup_ = boss_.bonus_setup();
		
		life_timer_ = 7.0F;
		box_count_ = 5;
		
		UpdateFallSprite();
	}
	
	void UpdateFallSprite()
	{
		//update fall sprite
		string sprite_name = "";
		switch(type_)
		{
		case CrateType.coin1:
			sprite_name = "Coin0101@1x";
			break;
		case CrateType.coin2:
			sprite_name = "Coin0201@1x";
			break;
		case CrateType.heart:
			sprite_name = "CrateLife01@1x";
			break;
		case CrateType.multiplier:
			sprite_name = "CrateMultiplier01@1x";
			break;
		case CrateType.invulnerability:
			sprite_name = "CrateInvulnerability01@1x";
			break;
		case CrateType.cleaner_explosion:
			sprite_name = "CrateMegaBomb01@1x";
			break;
		case CrateType.magnetics:
			sprite_name = "CrateMagnet01@1x";
			break;
		case CrateType.freeze:
			sprite_name = "CrateIce01@1x";
			break;
		case CrateType.box:
			sprite_name = "crate_9_fly";
			break;
		case CrateType.white_coin:
			sprite_name = "CoinWhite01@1x";
			break;
		}
		
		animation_sprite_.spriteId = animation_sprite_.GetSpriteIdByName( sprite_name );
	}
	
	Vector3 GetRandomPosition()
	{
		Vector3 rand_position = player_walkable_area_.GetRandomPosition();
		rand_position += new Vector3(0.0f, 400.0f, 0.0f);
		return rand_position;
	}
	
	public void ChangeSpawnPosition(Vector3 position, int y)
	{
		if(position.x > 300.0f)
			transform.position = new Vector3(300.0f, position.y, position.z);
		else
			transform.position = position;
		force_ground_y = true;
		ground_y = y;
		Start();
	}
	
	public void Init(Vector3 position)
	{
		this.transform.position = new Vector3(position.x, 600.0F, 0.0F);
		ground_y_ = (int)position.y;
	}
	
	// Update is called once per frame
	void Update ()
	{	
		//update life timer
		life_timer_ -= Time.deltaTime;
		if(life_timer_ < 0.0F && type_ != CrateType.box)
		{
			DestroyCrate();
			/*animation_component_.Play("DestroyCrateAnimation");
			Invoke("DestroyCrate", 1.0F);*/
		}
		
		GetComponent<BoxCollider>().center = new Vector3( 0.0F, 
													      animation_sprite_.transform.position.y - this.transform.position.y,
														  (int)animation_sprite_.transform.position.z);
		//update shadow
		shadow_sprite_.transform.position = new Vector3( (int)animation_sprite_.transform.position.x, 
														 (int)ground_y_ + 10,
														 (int)animation_sprite_.transform.position.z);
		background_animated_sprite_.transform.position = new Vector3( (int)animation_sprite_.transform.position.x, 
														 (int)ground_y_ + 15,
														 (int)animation_sprite_.transform.position.z - 0.5F);
		
		//update logic
		if (!is_on_ground_) {
			//Update position
			float t = Time.time - start_time_;
			
			if(player_.magnetic() && (type_ == CrateType.coin1 || type_ == CrateType.coin2))
			{
				Vector2 v_diff = player_.transform.position - animation_sprite_.transform.position;
				v_diff = new Vector2( Mathf.Lerp(0, v_diff.x, 5.0F * Time.deltaTime),
									  Mathf.Lerp(0, v_diff.y, 3.0F * Time.deltaTime));	
				//update ground
				float ground_y_diff = player_.GetComponent<ZOrderObject>().GetBaseline() - ground_y_;
				ground_y_ += (int)Mathf.Lerp(0, ground_y_diff, 100.0F * Time.deltaTime);
				
				//Z update
				animation_sprite_object_.GetComponent<ZOrderObject> ().UpdateBaseline ((int)(ground_y_));
				
				animation_sprite_.transform.Translate(v_diff.x, v_diff.y + (-3.0F * t * t), 0.0F);
			}
			else
				animation_sprite_.transform.Translate(0.0F, -9.8F * t * t, 0.0F);
			
			animation_component_.Stop();
				
			float h = 30;//animation_sprite_.collection.spriteDefinitions[ animation_sprite_.spriteId ].regionY;
			h *= 0.5F;
			if (animation_sprite_.transform.position.y < ground_y_ + h) {
				animation_sprite_.transform.position = new Vector3(
					animation_sprite_.transform.position.x,
					 ground_y_ + h);
				
				is_on_ground_ = true;
				
				shadow_sprite_.GetComponent<MeshRenderer>().enabled = false;
				
				PlayBackgroundLanding( 0 );
				
				//play landing animation
				switch (type_) {
				case CrateType.coin1:
					animation_sprite_.Play ("Money1Landing");
					break;
				case CrateType.coin2:
					animation_sprite_.Play ("Money2Landing");
					break;
				case CrateType.heart:
					animation_sprite_.Play ("LiveLanding");
					break;
				case CrateType.multiplier:
					animation_sprite_.Play ("MultiplierLanding");
					break;
				case CrateType.invulnerability:
					animation_sprite_.Play ("InvulnerabilityLanding");
					break;
				case CrateType.cleaner_explosion:
					animation_sprite_.Play ("ClearExplosionLanding");
					break;
				case CrateType.magnetics:
					animation_sprite_.Play ("MagneticsLanding");
					break;
				case CrateType.freeze:
					animation_sprite_.Play ("FreezeLanding");
					break;
				case CrateType.box:
					animation_sprite_.Play ("Money1Landing");
					break;
				case CrateType.white_coin:
					animation_sprite_.Play ("MilkCoinLanding");
					break;
				default:
				break;
					}
				}
			}	
	}
	
	void PlayBackgroundLanding(float clip_start_time)
	{
		background_animated_sprite_.renderer.enabled = true;
		string anim_string_ = "";
		string env_string = GameObject.Find("BackgroundContainer").GetComponent<EnvironmentSelector>().GetCurrentEnvironment();
		//FirstCharToUpper
		env_string = char.ToUpper(env_string[0]) + env_string.Substring(1);
		
		if(type_ == CrateType.coin1 || type_ == CrateType.coin2 || type_ == CrateType.white_coin)
		{
			anim_string_ = "CoinsBackground";
		}
		else if(type_ == CrateType.magnetics)
		{
			anim_string_ = "CrateBackgroundMagnet";
		}
		else if(type_ == CrateType.heart)
		{
			anim_string_ = "CrateBackgroundBottle";
		}
		else
			anim_string_ = "CrateBackground";
		
		background_animated_sprite_.Play(anim_string_ + env_string, clip_start_time);
		
		audio_manager_.Play("51_Coin Crate Lands", transform, 1.0F,false);
	}
	
	public void ChangeEnviroment()
	{
		if(background_animated_sprite_.renderer.isVisible)
			PlayBackgroundLanding( background_animated_sprite_.ClipTimeSeconds );
	}
	
	void OnTriggerStay(Collider other) 
	{
		if(taken_)
			return;
		
		if(other.name == "HarvestCollider" && !name.Contains("Box"))
		{
			if (CollideWithPlayer (other.gameObject) || player_.magnetic()) 
			{
				taken_ = true;
				
				MissionsManager.MissionType type_mission = MissionsManager.MissionType.mission_type_count;
				//Score update
				if((type_ == CrateType.coin1) || (type_ == CrateType.coin2))
				{
					game_.AddScore (Game.ScoreValue.money);
				}
				else
				{
					game_.AddScore (Game.ScoreValue.crates);
				}
				
				collider.enabled = false;
				
				switch (type_) {
				case CrateType.coin1:
				case CrateType.coin2:
					game_.AddCoin();
					type_mission = MissionsManager.MissionType.collect_coin;
					audio_manager_.PlayOnPlayer("17_Get Coin", 1.0F, 1.0F, false);
					break;
				case CrateType.heart:
					game_.AddHeart ();
					type_mission = MissionsManager.MissionType.collect_heart;
					Instantiate(pop_up_, animation_sprite_.transform.position, Quaternion.identity);
					audio_manager_.PlayOnPlayer("19_Get Life Up", 1.0F, 1.0F, false);
					break;
				case CrateType.multiplier:
					game_.AddMultiplier (multipler_value_);
					type_mission = MissionsManager.MissionType.collect_multiplier;
					audio_manager_.PlayOnPlayer("18_Get Crate", 1.0F, 1.0F, false);
					break;
				case CrateType.invulnerability:
					game_.SetGodModePower ();
					type_mission = MissionsManager.MissionType.collect_invulnerability;
					Instantiate(pop_up_, animation_sprite_.transform.position, Quaternion.identity);
					audio_manager_.PlayOnPlayer("18_Get Crate", 1.0F, 1.0F, false);
					break;
				case CrateType.cleaner_explosion:
					int destroyed = game_.DestroyAllEnemyInScene ();
					mission_manager_.UpdateMission (MissionsManager.MissionType.mega_bomb_destroy, destroyed);
					type_mission = MissionsManager.MissionType.collect_cleaner_explosion;
					Instantiate(pop_up_, animation_sprite_.transform.position, Quaternion.identity);
					audio_manager_.PlayOnPlayer("18_Get Crate", 1.0F, 1.0F, false);
					break;	
				case CrateType.magnetics:
					game_.ActivateMagnetics();
					type_mission = MissionsManager.MissionType.collect_magnetic;
					Instantiate(pop_up_, animation_sprite_.transform.position, Quaternion.identity);
					audio_manager_.PlayOnPlayer("18_Get Crate", 1.0F, 1.0F, false);
					break;
				case CrateType.freeze:
					game_.FreezeBoss(freeze_duration_);
					type_mission = MissionsManager.MissionType.collect_freeze;
					Instantiate(pop_up_, animation_sprite_.transform.position, Quaternion.identity);
					audio_manager_.PlayOnPlayer("18_Get Crate", 1.0F, 1.0F, false);
					break;	
				case CrateType.white_coin:
					game_.AddWhiteCoin();
					GameObject.Find("Game").GetComponent<BonusManager>().WhiteCoinsTaken();
					type_mission = MissionsManager.MissionType.collect_white_coin;
					audio_manager_.PlayOnPlayer("17_Get Coin", 1.0F, 1.0F, false);
					break;
				default:
					break;
				}
				
				mission_manager_.UpdateMission(type_mission, 1);
				
				DestroyCrate();
			}
		}
	}
	
	private bool CollideWithPlayer (GameObject game_object)
	{
		float player_baseline = GameObject.Find("Player").GetComponent<ZOrderObject> ().GetBaseline ();
		float this_baseline = animation_sprite_.GetComponent<ZOrderObject> ().GetBaseline ();
		if ((this_baseline > player_baseline - hit_size_y_ / 2) && 
			(this_baseline < player_baseline + hit_size_y_ / 2)) 
		{
			return true;
		}
			
		return false;
	}

	public void DestroyCrate()
	{	
		PoolManager.Despawn(gameObject);
	}
	
	public void DamageIt()
	{
		box_count_--;
		if(box_count_ == 0)
		{
			DestroyCrate();
			//spawn crates
			int crates_n = 5;
			RandBonus rnd = bonus_setup_.CurrentRandBonus();
			for(int i = 0; i < crates_n; i++)
			{
				GameObject.Find("BossPrefab").GetComponent<Boss>().bonus_manager().SpawnBonus(rnd.GetRandBonus());
			}
		}
	}
	
	private void SpawnFromPool(object sender, EventArgs e) 
    {	
		transform.position = GetRandomPosition();
		
		force_ground_y = false;
		animation_sprite_.transform.position = new Vector3(0.0F, 0.0F, 0.0F);
		animation_sprite_.transform.localPosition = new Vector3(0.0F, 0.0F, 0.0F);
		
		UpdateFallSprite();
		
		taken_ = false;
		collider.enabled = true;
		
		Start ();
	}
	
#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		if(force_ground_y)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(new Vector3(transform.position.x - length_baseline * 0.5F, ground_y , 100.0F),
							new Vector3(transform.position.x + length_baseline * 0.5F, ground_y , 100.0F));
			Gizmos.color = Color.white;
		}	
	}
#endif
}
