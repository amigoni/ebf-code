using UnityEngine;
using System.Collections;
using SimpleJSON;


public enum BonusType
{
	money,
	heart,
	multiplier,
	invulnerability,
	cleaner_explosion,
	magnetics,
	freeze,
	white_coin,
	count
};

public class BonusManager : MonoBehaviour {
	
	private Game game_;
	
	public GameObject money_1_;
	public GameObject money_2_;
	public GameObject heart_;
	public GameObject multiplier_;
	public GameObject invulnerability_;
	public GameObject cleaner_explosion_;
	public GameObject magnetics_;
	public GameObject freeze_;
	public GameObject white_coin_;
	
	public bool white_coins_taken_ = false;
	public bool white_coins_spawend_ = false;
	
	bool white_milk_coins_enabled_ = false;
	int initial_white_coins_today_ = 0;
	
	private ShuffleBagInt white_milk_coins_shuffle_bag_; 
	private RandBonus rand_bonus_;
	
	// Use this for initialization
	void Start () 
	{
		game_ = GameObject.Find("Game").GetComponent<Game>();
		initial_white_coins_today_ = Framework.Instance.InstanceComponent<PlayerData>().GetInt("free_white_coins_per_day");
		white_milk_coins_shuffle_bag_ = new ShuffleBagInt(3);
		CreateShuffleBag();	
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public Crate SpawnBonus(BonusType type)
	{
		Object crate = null;
		switch(type)
		{
		case BonusType.money:
			if(Random.value < 0.5F)
				crate = PoolManager.Spawn(money_1_);
			else
				crate = PoolManager.Spawn(money_2_);
			break;
		case BonusType.heart:
			crate = PoolManager.Spawn(heart_);
			break;
		case BonusType.multiplier:
			crate = PoolManager.Spawn(multiplier_);
			break;
		case BonusType.invulnerability:
			crate = PoolManager.Spawn(invulnerability_);
			break;
		case BonusType.cleaner_explosion:
			crate = PoolManager.Spawn(cleaner_explosion_);
			break;
		case BonusType.magnetics:
			crate = PoolManager.Spawn(magnetics_);
			break;
		case BonusType.freeze:
			crate = PoolManager.Spawn(freeze_);
			break;
		case BonusType.white_coin:
			crate = PoolManager.Spawn(white_coin_);
			break;
		case BonusType.count:
			break;	
		}
		
#if UNITY_EDITOR
		if(type == BonusType.count)
		{
			Debug.Log("Warging: There is a Bug!");
		}
#endif
		return ((GameObject)crate).GetComponent<Crate>();
	}
	

	static public CrateType ConvertToCrate(BonusType type)
	{
		switch(type)
		{
		case BonusType.money:
			if(Random.value < 0.5F)
				return CrateType.coin1;
			else
				return CrateType.coin2;
		case BonusType.heart:
			return CrateType.heart;
		case BonusType.multiplier:
			return CrateType.multiplier;
		case BonusType.invulnerability:
			return CrateType.invulnerability;
		case BonusType.cleaner_explosion:
			return CrateType.cleaner_explosion;
		case BonusType.magnetics:
			return CrateType.magnetics;
		case BonusType.freeze:
			return CrateType.freeze;
		case BonusType.white_coin:
			return CrateType.white_coin;
		}
		
		Debug.Log("Warging: There is a Bug!");
		return CrateType.count;
	}
	
	public void SpawnWhiteCoins(){
		if(!white_coins_taken_ && !white_coins_spawend_)
		{
			int free_white_coins_per_day = Framework.Instance.InstanceComponent<PlayerData>().GetInt("free_white_coins_per_day");
		
			if(free_white_coins_per_day > 0)
			{
				if(Random.value < 0.25f)
				{
					SpawnBonus(BonusType.white_coin);
					white_coins_spawend_ = true;
				}
			}
		}
	}
	
	
	public void SpawnWhiteCoinsNew()
	{
		//Spawns only once per round so it's safe otherwise you would need to check if htere is another one
		//if(white_coins_spawend_ == false)
		{
			int free_white_coins_per_day = Framework.Instance.InstanceComponent<PlayerData>().GetInt("free_white_coins_per_day");
			//Debug.Log(free_white_coins_per_day);
			
			if(free_white_coins_per_day > 0 && (initial_white_coins_today_ - free_white_coins_per_day <= 5 ))
			{
				int random = white_milk_coins_shuffle_bag_.Next();
				if(random == 2)
				{
					SpawnBonus(BonusType.white_coin);
					white_coins_spawend_ = true;
				}
			}
		}
	}
	
	public bool CanISpawnWhiteCoin()
	{
		//Debug.Log("Init coins: "+initial_white_coins_today_+"Current WMC: "+Framework.Instance.InstanceComponent<PlayerData>().GetInt("free_white_coins_per_day"));
		if (Framework.Instance.InstanceComponent<PlayerData>().GetInt("free_white_coins_per_day") >= initial_white_coins_today_ && white_coins_spawend_== false && white_coins_taken_ == false)
			return true;
		else		
			return false;
	}
	
	
	public void SpawnOnHitBoss()
	{
		if (Random.value < 0.75)
		{
			if(game_.game_mode() == Game.GameMode.singleplayer)
				SpawnRandomCrate();
			else // training or multiplayer
				SpawnBonus(BonusType.money);
		}
	}
	
	public void WhiteCoinsTaken()
	{
		white_coins_taken_ = true;
		int free_white_coins_per_day = Framework.Instance.InstanceComponent<PlayerData>().GetInt("free_white_coins_per_day");
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("free_white_coins_per_day", free_white_coins_per_day - 1);
	}
	
	public void SpawnTimeBonus(int time_left){
		int coins_count = (int) Mathf.Floor(time_left/20);
		
		//Debug.Log("Time bonus coins #: "+coins_count);
		
		for (int j = 0; j < coins_count; j++){
			SpawnBonus(BonusType.money);
		}	
	}
	
	
	private void CreateShuffleBag()
	{
		//money,heart,multiplier,invulnerable,megabomb,magnet,freeze
		rand_bonus_ = new RandBonus(200.0F,2.0F,2.0F,1.0F,3.0F,4.0F,0, RandType.shuffle_bag);
	}
	
	
	public void SpawnRandomCrate()
	{
		SpawnBonus(rand_bonus_.GetRandBonusShuffleBag());
	}
}

