using UnityEngine;
using System.Collections;

public class RandCreate : MonoBehaviour {
	
	private Crate crates_;
	
	private RandBonus rand_bonus_;
	public float r_money_ = 0.4F;
	public float r_heart_ = 0.1F;
	public float r_multiplier_ = 0.1F;
	public float r_invulnerability_ = 0.1F;
	public float r_cleaner_explosion_ = 0.1F;
	public float r_magnetics_ = 0.1F;
	public float r_freeze_ = 0.1F;	
	
	// Use this for initialization
	void Start () {
	}
	
	public void Init () {
		rand_bonus_ = new RandBonus(r_money_, r_heart_, r_multiplier_, r_invulnerability_, 
									r_cleaner_explosion_, r_magnetics_, r_freeze_, RandType.rnd);
		
		GetComponent<Crate>().type_ = BonusManager.ConvertToCrate( rand_bonus_.GetRandBonus() );
		GetComponent<Crate>().Init(this.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
