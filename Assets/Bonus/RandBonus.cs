using UnityEngine;
using System.Xml;
using System.Collections.Generic;

public enum RandType
{
	rnd,
	shuffle_bag,
	count
}

public class RandBonus
{
	// 0.0F < r_name < 1.0F
	// sum r_i = 1.0F
	private float r_money_ = 0.0F;
	private float r_heart_ = 0.0F;
	private float r_multiplier_ = 0.0F;
	private float r_invulnerability_ = 0.0F;
	private float r_cleaner_explosion_ = 0.0F;
	private float r_magnetics_ = 0.0F;
	private float r_freeze_ = 0.0F;	
	private float r_white_coin_ = 0.0F;	
	private RandType type_ = RandType.rnd;
	
	private ShuffleBag shuffle_bag_;
	
	//money,heart,multiplier,invulnerable,megabomb,magnet,freeze
	public RandBonus(float r_money, float r_heart, float r_multiplier, float r_invulnerability,
		    		 float r_cleaner_explosion, float r_magnetics, float r_freeze, RandType type)
	{
		type_ = type;
		
		r_money_ = r_money;
		r_heart_ = r_heart;
		r_multiplier_ = r_multiplier;
		r_invulnerability_ = r_invulnerability;
		r_cleaner_explosion_ = r_cleaner_explosion;
		r_magnetics_ = r_magnetics;
		r_freeze_ = r_freeze;
		
		if(type_ == RandType.rnd)
		{
			InitRandType();
		}
		else
		{
			InitShuffleBag((int)(r_money_+r_heart_+r_multiplier_+r_invulnerability_+r_cleaner_explosion_+r_magnetics_+r_freeze_));
		}
	}
	
	
		
	private void InitRandType()
	{
		//Recalculate white drop rate
		r_money_ -= r_white_coin_;
		
		r_heart_ += r_money_;
		r_multiplier_ += r_heart_;
		r_invulnerability_ += r_multiplier_;
		r_cleaner_explosion_ += r_invulnerability_;
		r_magnetics_ += r_cleaner_explosion_;
		r_freeze_ += r_magnetics_;		
		r_white_coin_ += r_freeze_;	
	}
	
	public BonusType GetRandBonus()
	{
		if(type_ == RandType.rnd)
		{
			return GetRandBonusRnd();
		}
		else
		{
			return GetRandBonusShuffleBag();
		}
		
		return BonusType.count;
	}
		
	private BonusType GetRandBonusRnd ()
	{
		float r = Random.value;
		
		if(r < r_money_)
		{
			return BonusType.money;
		} 
		else if(r < r_heart_)
		{
			return BonusType.heart;
		}
		else if(r < r_multiplier_)
		{
			return BonusType.multiplier;
		}
		else if(r < r_invulnerability_)
		{
			return BonusType.invulnerability;
		}
		else if(r < r_cleaner_explosion_)
		{
			return BonusType.cleaner_explosion;
		}
		else if(r < r_magnetics_)
		{
			return BonusType.magnetics;
		}
		else if(r < r_freeze_)
		{
			return BonusType.freeze;
		}
		else if(r <= r_white_coin_)
		{
			return BonusType.white_coin;
		}
		
		return BonusType.count;
	}
	
	
	
	//Shuffle Bag
	public void InitShuffleBag(int size)
	{
		shuffle_bag_ = new ShuffleBag(size);
		
		shuffle_bag_.Add(BonusType.money,r_money_);
		shuffle_bag_.Add(BonusType.heart, r_heart_);
		shuffle_bag_.Add(BonusType.multiplier, r_multiplier_);
		shuffle_bag_.Add(BonusType.invulnerability, r_invulnerability_);
		shuffle_bag_.Add(BonusType.cleaner_explosion, r_cleaner_explosion_);
		shuffle_bag_.Add(BonusType.magnetics, r_magnetics_);
		shuffle_bag_.Add(BonusType.freeze, r_freeze_);
		shuffle_bag_.Add(BonusType.white_coin, r_white_coin_);
	}
	
	public BonusType GetRandBonusShuffleBag()
	{
		return shuffle_bag_.Next();
	}
	
	
	
	private class ShuffleBag
	{
		private Random random = new Random();
	    private List<BonusType> data;
	 
	    private BonusType currentItem;
	    private int currentPosition = -1;
	 
	    private int Capacity { get { return data.Capacity; } }
	    public int Size { get { return data.Count; } }
	 
	    public ShuffleBag(int initCapacity)
	    {
	        data = new List<BonusType>(initCapacity);
	    }
	
		public void Add(BonusType item, float amount)
		{
		   amount = (int) amount;
			for (int i = 0; i < amount; i++)
		        data.Add(item);
		 
		    currentPosition = Size - 1;
		}
		
		public BonusType Next()
		{
		    if (currentPosition < 1)
		    {
		        currentPosition = Size - 1;
		        currentItem = data[0];
		 
		        return currentItem;
		    }
		 
		    var pos = Random.Range(0,currentPosition);
		 
		    currentItem = data[pos];
		    data[pos] = data[currentPosition];
		    data[currentPosition] = currentItem;
		    currentPosition--;
		 
		    return currentItem;
		}
	}	
	
	//end ShuffleBag
	
	//Getter
	public float r_money()
	{
		return r_money_;
	}
	
	public float r_heart()
	{
		return r_heart_;
	}
	
	public float r_multiplier()
	{
		return r_multiplier_;
	}
	
	public float r_invulnerability()
	{
		return r_invulnerability_;
	}
	
	public float r_cleaner_explosion()
	{
		return r_cleaner_explosion_;
	}
	
	public float r_magnetics()
	{
		return r_magnetics_;
	}
	
	public float r_freeze()
	{
		return r_freeze_;
	}
	
	public float r_white_coin()
	{
		return r_white_coin_;
	}
	
	//Load
	public static RandBonus LoadFromXml(XmlNode rand_node)
	{
		RandBonus enemy_params = new RandBonus(	
			float.Parse(rand_node.Attributes[0].Value),
			float.Parse(rand_node.Attributes[1].Value),
			float.Parse(rand_node.Attributes[2].Value),
			float.Parse(rand_node.Attributes[3].Value),
			float.Parse(rand_node.Attributes[4].Value),
			float.Parse(rand_node.Attributes[5].Value),
			float.Parse(rand_node.Attributes[6].Value),
			RandType.rnd
		);
		return enemy_params;
	}
};