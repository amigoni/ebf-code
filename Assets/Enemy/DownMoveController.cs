using UnityEngine;
using System.Collections;

public class DownMoveController : MonoBehaviour {
	
	private tk2dAnimatedSprite animations_;
	private tk2dAnimatedSprite shadow_sprite_;
	private tk2dAnimatedSprite glow_ = null;
	
	private Vector2 swipe_down_size_;
		
	private bool is_on_ground_;
	private Vector2 velocity_;
	private float ground_y_;
	
	private const float time_to_explosion_ = 3;
	private const float ray_damage_ = 50.0F;
	
	private float dt_;
	private float dx_;
	private float dy_;
	
	private int bouncing_count_;
	
	public GameObject explosion_;
	
	private bool pause_ = false;
	private bool stop_ = true;
	
	private PlayerMoveController player_move_controller_;

	// Use this for initialization
	void Start () {
	}
	
	public void Init()
	{
		tk2dAnimatedSprite[] animations = GetComponentsInChildren<tk2dAnimatedSprite>();
		
		foreach (tk2dAnimatedSprite animation in animations) {
            if(animation.name == "AnimatedSprite")
				animations_ = animation;
			else if(animation.name == "Shadow")
				shadow_sprite_ = animation;
			else if(animation.name == "Glow")
				glow_ = animation;			
        }
		
		is_on_ground_ = false;
		velocity_ = new Vector2(-(45.0F + Random.value * 75.0F), 70.0F);
		
		ground_y_ = GameObject.Find("BossPrefab").GetComponent<Boss>().GetBaseLine() + 30.0F;
		
		dt_ = time_to_explosion_;
		dx_ = 1.1F;
		dy_ = 1.1F;
		
		bouncing_count_ = 3;
		
		//get player
		player_move_controller_ = GameObject.Find("Player").GetComponent<PlayerMoveController>();
		
		//zOrder
		GetComponent<ZOrderObject> ().set_enabled(false);
		
		swipe_down_size_ = new Vector2(shadow_sprite_.scale.x, shadow_sprite_.scale.y);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(pause_ || stop_)
			return;
		
		if(is_on_ground_ == false)
		{
			float t = Time.deltaTime;
			velocity_.y += -1000.0F * t;

			transform.Translate(velocity_.x * t * dx_, velocity_.y * t * dy_, 0.0F);
			
			const int vel_y_limit = 100;		
			//On ground
			if( transform.position.y < ground_y_ )
     		{
				animations_.spriteId = animations_.GetSpriteIdByName ("round_bomb_fly_01");
				if(glow_ != null)
					glow_.spriteId = glow_.GetSpriteIdByName ("lower_bomb_fly_01_glow");
				
				transform.position = new Vector3(transform.position.x, ground_y_, transform.position.z);
				velocity_.y *= -0.8F;
				dy_ *= 1;
				bouncing_count_ --;
				
				if(bouncing_count_ == 0)
				{
					animations_.Play("DownOnGroundBlink");
					if(glow_ != null)
						glow_.Play("DownOnGroundGlo");
					
					is_on_ground_ = true;
					GetComponent<BoxCollider>().enabled = true;
					
					is_on_ground_ = true;
				}
			}
			else if(velocity_.y < -vel_y_limit || velocity_.y > vel_y_limit)
			{
				animations_.spriteId = animations_.GetSpriteIdByName ("round_bomb_fly_02");
				if(glow_ != null)
					glow_.spriteId = glow_.GetSpriteIdByName ("lower_bomb_fly_02_glow");
			}
			else if(velocity_.y > -vel_y_limit || velocity_.y < vel_y_limit)
			{
				animations_.spriteId = animations_.GetSpriteIdByName ("round_bomb_fly_03");
				if(glow_ != null)
					glow_.spriteId = glow_.GetSpriteIdByName ("lower_bomb_fly_03_glow");
			}
			//shadow update
			float baseline = this.GetComponent<ZOrderObject> ().GetBaseline();
			shadow_sprite_.transform.position = new Vector3(animations_.transform.position.x, 
															baseline + 15.0F,
															animations_.transform.position.z-1.0F);
		}
		else 
		{
			dt_ -= Time.deltaTime;
			if(dt_ < 0)
			{									
				//Calculate baseline hit point
				Vector2 v_dist = player_move_controller_.GetHitPosition() - 
								(new Vector2(animations_.transform.position.x, 
								   			 animations_.transform.position.y));
				if( v_dist.magnitude < ray_damage_)
				{
					GameObject.Find("Game").GetComponent<Game>().PlayerHitted( 
						GetComponent<EnemyInfo>().damage (),
						false,
						0);
				}
					
				Vector3 expl_pos = new Vector3(
					transform.position.x + 4.0F,
					transform.position.y + 22.0F,
					transform.position.z
				);
				
				GameObject obj = PoolManager.Spawn(explosion_);
				obj.transform.position = expl_pos;
				
				//combo
				Enemy enemy = GetComponent<Enemy> ();
				GameObject.Find("BossPrefab").GetComponent<Boss>().InvalidateCombo (enemy.group_id (), enemy.id());
				
				Destroy(this.gameObject);
			}
		}
	}
	
	public void Pause()
	{
		pause_ = true;
		
		animations_.Pause();
	}
	
	public void Resume()
	{
		pause_ = false;
		
		animations_.Resume();
	}	
	
	private void RemoveFromBoss()
	{
		transform.parent = null;
		this.GetComponent<ZOrderObject> ().Init ((int)(ground_y_ - 15), true);
		GetComponent<ZOrderObject> ().set_enabled(true);
	}
	
	public void Go()
	{
		Invoke("RemoveFromBoss", 0.2F);
		stop_ = false;
	}
}
