using UnityEngine;
using System;
using System.Collections;

public enum EnemyType
{
	tap_left,
	tap_right,
	tap_either,
	tap2_left,
	tap2_right,
	tap2_either,
	up,
	up2,
	down,
	laser,
	mole,
	fast_tap_left,
	fast_tap_right,
	fast_tap_either,
	chasing_tap_left,
	chasing_tap_right,
	chasing_tap_either,
	sin_tap_left,
	sin_tap_right,
	sin_tap_either,
	boucing_tap_left,
	boucing_tap_right,
	boucing_tap_either,
	count
}

public class Enemy : MonoBehaviour
{
	public int params_level_ = 0;
	public EnemyType enemy_type_;
	public float score_;
	public int group_id_;
	public bool on_boss_ = true;
	public Boss boss_ = null;
	
	private bool is_destructible_ = false;
	private int id_;
	
	public RandBonus rand_bonus_ = null;
	private BonusManager bonus_manager_;
	
	private tk2dSprite glow_sprite_ = null;
	private tk2dAnimatedSprite level_label_sprite_anim_ = null;
	private tk2dAnimatedSprite flame_sprite_anim_ = null;
	
	// Use this for initialization
	void Awake ()
	{
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		
		bonus_manager_ = GameObject.Find("Game").GetComponent<BonusManager>();
		//money,heart,multiplier,invulnerable,megabomb,magnet,freeze
		if(GameObject.Find("Game").GetComponent<Game>().game_mode() == Game.GameMode.singleplayer)
			rand_bonus_ = new RandBonus(0.95F, 0.01F, 0.01F, 0.005F, 0.01F, 0.15F, 0.0F, RandType.rnd);
		else
			rand_bonus_ = new RandBonus(1.00F, 0.00F, 0.00F, 0.00F, 0.00F, 0.00F, 0.00F, RandType.rnd);
		
		//Changed += new ChangedEventHandler(ListChanged);
		GetComponent<PoolObject>().Spawned = new EventHandler(SpawnFromPool);
		GetComponent<PoolObject>().Despawned = new EventHandler(DespawnFromPool);
	}
	
	private void SpawnFromPool(object sender, EventArgs e) 
    {	
		switch(enemy_type_)
		{
		case EnemyType.up:
		case EnemyType.up2:
			GetComponent<UpMoveController>().PreSpawn();
			break;
		case EnemyType.laser:
			GetComponent<LaserController>().PreSpawn();
			break;
		case EnemyType.mole:
			GetComponent<MoleController>().PreSpawn();
			break;
		default:
			break;
		}
		Init ();
    }
	
	private void DespawnFromPool(object sender, EventArgs e) 
    {		
		on_boss_ = true;
		is_destructible_ = false;
		
	  	GetComponent<ZOrderObject>().SpawnReset();
		
		switch(enemy_type_)
		{
		case EnemyType.up:
		case EnemyType.up2:			
			GetComponent<UpMoveController>().SpawnReset();
			break;
		case EnemyType.down:
			//GetComponent<DownMoveController>().Init();
			Debug.Log("DespawnFromPool: ERROR");
			break;
		case EnemyType.mole:
			GetComponent<MoleController>().SpawnReset();
			break;
		case EnemyType.laser:
			GetComponent<LaserController>().SpawnReset();
			break;
		default:
			GetComponent<TapMoveController>().SpawnReset();
			break;
		}
    }
	
	void Start()
	{

	}
	
	public void Init()
	{	
		if(enemy_type_ != EnemyType.mole && 
		   enemy_type_ != EnemyType.laser )
		{
			group_id_ = boss_.current_group_id();
			boss_.IncreaseSpawnedEnemy();
			id_ = boss_.spawned_enemy();
		}
		
		//TO-DO move to awake
		tk2dSprite[] sprites = this.GetComponentsInChildren<tk2dSprite>();
		foreach(tk2dSprite sprite in sprites)
		{
			if(sprite.name == "Glow")
			{
				glow_sprite_ = sprite;
			} else if(sprite.name == "LevelLabel")
			{
				level_label_sprite_anim_ = (tk2dAnimatedSprite)sprite;
			} else if(sprite.name == "RocketFlame")
			{
				flame_sprite_anim_ = (tk2dAnimatedSprite)sprite;
			}
		}
		
		if(glow_sprite_ != null)
		{
			if(boss_.GetGroupCount() != 1 && boss_.EnemyIsACombo(id_))
			{
				glow_sprite_.GetComponent<MeshRenderer>().enabled = true;
				glow_sprite_.GetComponent<Animation>().Play();
				boss_.DecreaseComboValue();
			}
			else 
			{
				glow_sprite_.GetComponent<MeshRenderer>().enabled = false;
				glow_sprite_.GetComponent<Animation>().Stop();
			}
		}
		
		switch(enemy_type_)
		{
		case EnemyType.up:
		case EnemyType.up2:			
			GetComponent<UpMoveController>().Init();
			break;
		case EnemyType.down:
			GetComponent<DownMoveController>().Init();
			break;
		case EnemyType.mole:
			GetComponent<MoleController>().Init();
			break;
		case EnemyType.laser:
			break;	
		default:
			GetComponent<TapMoveController>().Init();
			break;
		}
		
		//set enemy info
		GetComponent<EnemyInfo>().set_damage(boss_.GetCurrentParam(enemy_type_, params_level_, "damage"));
		GetComponent<EnemyInfo>().set_xp((int)boss_.GetCurrentParam(enemy_type_, params_level_, "xp"));
		
		if(level_label_sprite_anim_ != null)
			level_label_sprite_anim_.GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Camera.mainCamera.pixelRect.Contains(transform.position))
			is_destructible_ = true;
	}
	
	public void Pause ()
	{
		switch (enemy_type_) {
		case EnemyType.up:
		case EnemyType.up2:
			GetComponent<UpMoveController> ().Pause ();
			break;		
		case EnemyType.down:
			GetComponent<DownMoveController> ().Pause ();
			break;		
		case EnemyType.laser:
			GetComponent<LaserController> ().Pause ();
			break;	
		case EnemyType.mole:
			GetComponent<MoleController> ().Pause ();
			break;
		default: //Taps
			GetComponent<TapMoveController> ().Pause ();
			break;
		}
	}
	
	public void Resume ()
	{
		switch (enemy_type_) {
		case EnemyType.up:
		case EnemyType.up2:
			GetComponent<UpMoveController> ().Resume ();
			break;
		case EnemyType.down:
			GetComponent<DownMoveController> ().Resume ();
			break;	
		case EnemyType.laser:
			GetComponent<LaserController> ().Resume ();
			break;	
		case EnemyType.mole:
			GetComponent<MoleController> ().Resume ();
			break;	
		default: //Taps
			GetComponent<TapMoveController> ().Resume ();
			break;
		}
	}
	
	//
	public void Go()
	{
		on_boss_ = false;
		switch (enemy_type_) {
		case EnemyType.up:
		case EnemyType.up2:
			GetComponent<UpMoveController> ().Go ();
			break;
	    case EnemyType.down:
			GetComponent<DownMoveController> ().Go ();
			break;
		case EnemyType.mole:
			break;
		default: //Taps
			GetComponent<TapMoveController> ().Go ();
			break;
		}
	}
	
	public void SpawnBonus()
	{
		if(transform.parent == null)
		{
			Crate crate = bonus_manager_.SpawnBonus (rand_bonus_.GetRandBonus());
			Vector3 spawn_position = transform.position;
			spawn_position.x -= 15.0F;
			int ground_crate_ = (int)GetComponent<ZOrderObject>().GetBaseline();
			crate.ChangeSpawnPosition(spawn_position, ground_crate_ - 10);
		}
	}
	
	public void ChangeLevel(int params_level)
	{
		params_level_ = params_level;	
		string base_level = "";
		switch(params_level_)
		{
			case 0:
				base_level += "Normal";
				break;
			case 1:
				base_level += "Medium";
				break;
			case 2:
				base_level += "Heavy";
				break;
			default:
				base_level += "Error: no animation";
				break;
		}
			
		if(level_label_sprite_anim_ != null)
		{
			if(params_level_ > 0)
			{
				level_label_sprite_anim_.renderer.enabled = true;
				level_label_sprite_anim_.Play(base_level + "EnemyAnimation");
			}
			else
				level_label_sprite_anim_.renderer.enabled = false;
		}
		
		if(flame_sprite_anim_ != null)
		{
			flame_sprite_anim_.Stop();
			flame_sprite_anim_.Play( "Flame" + base_level);
		}
		
		//set enemy info
		//Debug.Log(boss_.GetCurrentParam(enemy_type_, params_level_, "damage").ToString());
		GetComponent<EnemyInfo>().set_damage(boss_.GetCurrentParam(enemy_type_, params_level_, "damage"));
		GetComponent<EnemyInfo>().set_xp((int)boss_.GetCurrentParam(enemy_type_, params_level_, "xp"));
		
		if(enemy_type_ == EnemyType.laser)
		{
			GetComponent<LaserController>().ChangeLevel(params_level_);
		}
	}
	
	//Getters
	public int group_id()
	{
		return group_id_;
	}
	
	public int id()
	{
		return id_;
	}
	
	public bool on_boss()
	{
		return on_boss_;
	}
	
	public bool no_parent()
	{
		return transform.parent == null;
	}
	
	public bool is_destructible()
	{
		return is_destructible_;
	}
	
	void OnDestroy()
	{
		if(!Application.isPlaying)
			Debug.LogError("Warning: Called OnDestroy on Enemy.cs! enemy_type =" + enemy_type_.ToString());
	}
}
