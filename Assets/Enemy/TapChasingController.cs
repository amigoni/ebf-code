using UnityEngine;
using System.Collections;

public class TapChasingController : TapMoveController {
	
	private GameObject player_;
	private float speed_y_ = 5;
	
	// Use this for initialization	
	void Start () {
		//Init();
	}
	
	public override void Init()
	{
		base.Init();
		Enemy enemy = GetComponent<Enemy>();
		speed_y_ = boss_.GetCurrentParam(enemy.enemy_type_, enemy.params_level_, "speed_y");
		player_ = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if(x_dist_ < x_active_z_order)
		{
			float update_x = speed_ * Time.deltaTime;
			x_dist_ += update_x;
			if(x_dist_ >= x_active_z_order)
			{
				this.transform.parent = null;
				GetComponent<ZOrderObject> ().Init((int)(- 35), false); 
				GetComponent<ZOrderObject> ().set_enabled(true);
			}
			transform.Translate(-update_x, 0.0F, 0.0F);
		}
		else
		{
			//track player
			float y_diff = transform.position.y - player_.transform.position.y;
			
			float current_y_update = 0;
			if(transform.position.y > player_.transform.position.y)
				current_y_update = -speed_y_ * Time.deltaTime;
			else if(transform.position.y < player_.transform.position.y)
				current_y_update = speed_y_ * Time.deltaTime;
	
			transform.Translate(-speed_ * Time.deltaTime, current_y_update, 0.0F);
			GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 35)); 
		}
	}
}
