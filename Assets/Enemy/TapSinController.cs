using UnityEngine;
using System.Collections;

public class TapSinController : TapMoveController {
	private float start_sin_time_ = 0;
	private float frequency_ = 0;
	private float amplitude_ = 0;
	private BoxCollider collider_;
	private Vector3 initial_position_;
	// Use this for initialization	
	void Start () {
		//Init();
		
		frequency_ = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "frequency");
		amplitude_ = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "amplitude");
		
		tk2dSprite[] animations = GetComponentsInChildren<tk2dSprite>();
		
		collider_ = GetComponent<BoxCollider>();
		
		initial_position_ = rocket_body_.gameObject.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if(x_dist_ < x_active_z_order)
		{
			float update_x = speed_ * Time.deltaTime;
			x_dist_ += update_x;
			if(x_dist_ >= x_active_z_order)
			{
				this.transform.parent = null;
				GetComponent<ZOrderObject> ().Init((int)(- 35), false); 
				GetComponent<ZOrderObject> ().set_enabled(true);
				
				start_sin_time_ = Time.time;
			}
			transform.Translate(-update_x, 0.0F, 0.0F);
		}
		else
		{
			float a = Time.time - start_sin_time_;
			transform.Translate(-speed_ * Time.deltaTime, 0.0F, 0.0F);
			float y_update = Mathf.Sin(a / 1 / frequency_) * amplitude_;
			rocket_body_.gameObject.transform.Translate(0.0F, y_update, 0.0F);
			collider_.center += new Vector3(0.0F, y_update, 0.0F);
			GetComponent<ZOrderObject> ().UpdateBaseline (- 35); 
		}
	}
	
	public override void SpawnReset()
	{
		base.SpawnReset();
		start_sin_time_ = 0;
		rocket_body_.gameObject.transform.localPosition = initial_position_;
		collider_.center = new Vector3(0.0F, 0.0F, 0.0F);
	}
}
