using UnityEngine;
using System.Collections;

public class MoleController : MonoBehaviour
{
	public float spawn_time_ = 2.0F;
	private float time_to_explosion_ = 0.4F;
	private float life_time_ = 5.0F;
	private const float ray_damage_ = 25.0F;
	private const float hit_size_y_ = 18.0F;
	private Boss boss_;
	
	public GameObject explosion_;
	private bool alive_ = false;
	
	private tk2dAnimatedSprite animated_sprite_;
	private tk2dAnimatedSprite animated_background_sprite_;
	
	private bool pause_ = false;
	private bool activated_ = false;
	
	void Awake ()
	{
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
	}
	
	public void Init()
	{
		time_to_explosion_ = boss_.GetCurrentParam(GetComponent<Enemy>().enemy_type_, GetComponent<Enemy>().params_level_, "time_to_explosion");
		life_time_ = boss_.GetCurrentParam(GetComponent<Enemy>().enemy_type_, GetComponent<Enemy>().params_level_, "life_time");
		
		tk2dSprite[] sprites = GetComponentsInChildren<tk2dSprite>();
		foreach (tk2dSprite sprite in sprites) {
            if(sprite.name == "AnimatedSprite")
				animated_sprite_ = (tk2dAnimatedSprite)sprite;
			else if(sprite.name == "AnimatedBackgroundSprite")
				animated_background_sprite_ = (tk2dAnimatedSprite)sprite;		
        }
		
		GetComponent<ZOrderObject> ().Init (-5.0f, false); 
		animated_sprite_.renderer.enabled = false;
		animated_background_sprite_.renderer.enabled = false;
		pause_ = false;
		
		Enemy enemy_ = GetComponent<Enemy>();
		enemy_.on_boss_ = false;	
		GetComponent<EnemyInfo>().set_damage(enemy_.boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "damage"));
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (pause_)
			return;
		
		if(!alive_)
		{
			spawn_time_ -= Time.deltaTime;
			if(spawn_time_ < 0.0F)
			{
				animated_sprite_.Play ("MoleEnemyEntering");
				animated_sprite_.renderer.enabled = true;
				
				string env_string = GameObject.Find("BackgroundContainer").GetComponent<EnvironmentSelector>().GetCurrentEnvironment(); 
				//FirstCharToUpper
				env_string = char.ToUpper(env_string[0]) + env_string.Substring(1);
				
				animated_background_sprite_.Play("MoleEnemy" + env_string + "Entering");
				animated_background_sprite_.renderer.enabled = true;
				
				alive_ = true;
			}	
		}
		else
		{
			if(activated_)
			{
				time_to_explosion_ -= Time.deltaTime;
			
				if (time_to_explosion_ < 0.0F) {
					Vector2 v_dist = GameObject.Find ("Player").GetComponent<PlayerMoveController> ().GetHitPosition ()
															- (new Vector2 (animated_sprite_.transform.position.x, 
																		animated_sprite_.transform.position.y));
					if (v_dist.magnitude < ray_damage_) {
						GameObject.Find ("Game").GetComponent<Game> ().PlayerHitted(
							GetComponent<EnemyInfo>().damage(),
							false,
							0.0F );
					}
						
					Vector3 expl_pos = new Vector3 (
							transform.position.x + 2.0F,
							transform.position.y + 32.0F,
							transform.position.z
						);
					
					GameObject obj = PoolManager.Spawn(explosion_);
					obj.transform.position = expl_pos;
					
					DestroyAll();
					PreSpawn();
				}
			}
			else
			{
				life_time_ -= Time.deltaTime;
				
				if(life_time_ < 0.0F)
				{
					animated_sprite_.Play ("MoleEnemyExit");
					
					string env_string = GameObject.Find("BackgroundContainer").GetComponent<EnvironmentSelector>().GetCurrentEnvironment(); 
					//FirstCharToUpper
					env_string = char.ToUpper(env_string[0]) + env_string.Substring(1);
					
					animated_background_sprite_.Play("MoleEnemy" + env_string + "Exit");
					
					life_time_ = 99.9F;
					
					animated_sprite_.animationCompleteDelegate = delegate(tk2dAnimatedSprite sprite, int clipId)
					{
						DestroyAll();
					};
				}
			}
		}
	}
	
	//pause
	public void Pause ()
	{
		pause_ = true;
		
		animated_sprite_.Pause ();
	}
	
	public void Resume ()
	{
		pause_ = false;
		
		animated_sprite_.Resume ();
	}	
	
	void OnTriggerStay (Collider other)
	{
		if(other.name == "HarvestCollider")
		{
			if(CollideWithPlayer())
			{
				activated_ = true;
				animated_sprite_.Play ("MoleEnemyIdle");
				
				string env_string = GameObject.Find("BackgroundContainer").GetComponent<EnvironmentSelector>().GetCurrentEnvironment(); 
				//FirstCharToUpper
				env_string = char.ToUpper(env_string[0]) + env_string.Substring(1);
				
				animated_background_sprite_.Play("MoleEnemy" + env_string + "Idle");
			}
		}
	}
	
	void DestroyAll()
	{
		GetComponentInChildren<MeshRenderer> ().enabled = false;
		PoolManager.Despawn(gameObject);
	}

	bool CollideWithPlayer ()
	{
		float player_baseline = GameObject.Find ("Player").GetComponent<ZOrderObject> ().GetBaseline ();
		float this_baseline = GetComponent<ZOrderObject> ().GetBaseline ();
		if ((this_baseline > player_baseline - hit_size_y_) && 
			(this_baseline < player_baseline + hit_size_y_) 	) 
		{
			return true;
		}
			
		return false;
	}
	
	public void SpawnReset()
	{
		animated_sprite_.animationCompleteDelegate = null;
		animated_sprite_.Stop();
		
		PreSpawn();
	}
	
	public void PreSpawn()
	{
		if(animated_sprite_ != null)
		{
			animated_sprite_.animationCompleteDelegate = null;
			animated_sprite_.Stop();
		}
		time_to_explosion_ = boss_.GetCurrentParam(GetComponent<Enemy>().enemy_type_, GetComponent<Enemy>().params_level_, "time_to_explosion");
		life_time_ = boss_.GetCurrentParam(GetComponent<Enemy>().enemy_type_, GetComponent<Enemy>().params_level_, "life_time");
			
		spawn_time_ = 2.0F;
		alive_ = false;
		activated_ = false;
	}
}
