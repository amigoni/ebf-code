using UnityEngine;
using System.Collections;

public class TapMoveController : MonoBehaviour {
	
	protected float speed_ = 0.0F;
	
	protected float resume_speed_;
	
	protected float x_dist_ = 0.0F;
	protected const float x_active_z_order = 40.0f;
	
	protected Enemy enemy_;
	protected Boss boss_;
	
	protected tk2dAnimatedSprite rocket_body_;
	protected tk2dSprite rocket_shadow_;
			
	// Use this for initialization	
	void Start () {
		/*if(name.Contains("TapEnemy"))
		{
			GetComponent<Enemy>().rand_bonus_ = new RandBonus(
				0.0F,
				0.0F,
				1.0F,
				0.0F,
				0.0F,
				0.0F,
				0.0F
				);
				
		}*/
			
		//Init();
	}
	
	// Update is called once per frame
	void Update () {
		if(x_dist_ < x_active_z_order)
		{
			float update_x = speed_ * Time.deltaTime;
			x_dist_ += update_x;
			if(x_dist_ >= x_active_z_order)
			{
				this.transform.parent = null;
				GetComponent<ZOrderObject> ().Init((int)(- 35), false); 
				GetComponent<ZOrderObject> ().set_enabled(true);
				rocket_shadow_.GetComponent<MeshRenderer>().enabled = true;
			}
			transform.Translate(-update_x, 0.0F, 0.0F);
		}
		else
		{
			transform.Translate(-speed_ * Time.deltaTime, 0.0F, 0.0F);
			GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 35)); 
		}
	}
	
	public virtual void Init()
	{
		tk2dSprite[] sprites = GetComponentsInChildren<tk2dSprite>();
		foreach (tk2dSprite sprite in sprites) {
            if(sprite.name == "RocketBody")
				rocket_body_ = (tk2dAnimatedSprite)sprite;
			else if(sprite.name == "RocketShadow")
				rocket_shadow_ = sprite;		
        }
		
		enemy_ = GetComponent<Enemy>();
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		
		GetComponent<ZOrderObject> ().set_enabled(false);
		if(boss_.IsRandomSetup())
			rocket_shadow_.GetComponent<MeshRenderer>().enabled = false;
	}
	
	public void Pause()
	{
		resume_speed_ = speed_;
		speed_ = 0.0F;
		
		//pause enemy
		tk2dAnimatedSprite[] animations = this.GetComponentsInChildren<tk2dAnimatedSprite> ();
		foreach(tk2dAnimatedSprite animation in animations)
		{
			animation.Pause();
		}
	}
	
	public void Resume()
	{
		speed_ = resume_speed_;
		
		//resume enemy
		tk2dAnimatedSprite[] animations = this.GetComponentsInChildren<tk2dAnimatedSprite> ();
		foreach(tk2dAnimatedSprite animation in animations)
		{
			animation.Resume();
		}
	}
	
	public void Go()
	{
		rocket_body_.Play();
		rocket_shadow_.renderer.enabled = true;
		speed_ = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "speed");
		GetComponent<ZOrderObject> ().Init();
	}
	
	public virtual void SpawnReset()
	{
		x_dist_ = 0;
		speed_ = 0;
		if(boss_.IsRandomSetup())
			rocket_shadow_.GetComponent<MeshRenderer>().enabled = false;
		GetComponent<ZOrderObject> ().set_enabled(false);
	}
}
