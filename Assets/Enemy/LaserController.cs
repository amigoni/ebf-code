using UnityEngine;
using System.Collections;

public class LaserController : MonoBehaviour {
	
	private AudioManager audio_manager_;
	
	private Animation laser_animation_;
	private BoxCollider collider_;
	
	private Boss boss_;
	
	private bool close_and_destroy_ = false;
	private bool pause_ = false;
	
	//Pool aux
	private GameObject sliced_sprite_obj_;
	private GameObject laser_ball_obj_;
	private GameObject laser_junction_obj_;
	
	private GameObject laser_shadow_obj_;
	private float init_time_ = 0.0F;
	
	// Use this for initialization
	void Awake()
	{
		audio_manager_ = Framework.Instance.audio_manager();
		
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		collider_ = GetComponent<BoxCollider>();
		
		laser_animation_ = GetComponent<Animation>();
		init_time_ = laser_animation_.animation["EnemyLaserAnimation"].time;
	}
	
	// Update is called once per frame
	void Update () {
		if(pause_)
			return;
		
		ZOrderUpdate();
	}
	
	private void ZOrderUpdate()
	{
		//z order update
		GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 35)); 
		
		Vector3 boss_position = GameObject.Find("LaserSpawn").transform.position;
		transform.position = new Vector3(
			boss_position.x,
			boss_position.y,
			transform.position.z
			);
	}
	
	//Closing callback
	public void Close()
	{
		laser_animation_.animation["EnemyLaserAnimation"].speed = -1;
		laser_animation_.animation["EnemyLaserAnimation"].time = laser_animation_.animation["EnemyLaserAnimation"].length;
		laser_animation_.Play("EnemyLaserAnimation");
		
		close_and_destroy_ =  true;
	}
	
	//Destroy CallBack
	public void DestroyAll()
	{
		if(close_and_destroy_)
		{
			PoolManager.Despawn(gameObject);
			boss_.CloseLaserCannon();
		}
	}
	
	//Activate collision
	public void ActiveCollider()
	{
		collider_.enabled = true;
	}
	
	//pause
	public void Pause()
	{
		pause_ = true;
	}
	
	public void Resume()
	{
		pause_ = false;
	}	
	
	public void SpawnReset()
	{
		close_and_destroy_ = false;
		sliced_sprite_obj_.GetComponent<MeshRenderer>().enabled = true;
		laser_ball_obj_.GetComponent<MeshRenderer>().enabled = true;
		laser_junction_obj_.GetComponent<MeshRenderer>().enabled = true;
		laser_shadow_obj_.GetComponent<MeshRenderer>().enabled = true;
	}
	
	public void PreSpawn()
	{
		GetComponent<Enemy>().on_boss_ = false;
		
		sliced_sprite_obj_ = GameObject.Find("LaserEnemy(Clone)0/Sliced Sprite");
		laser_ball_obj_ = GameObject.Find("LaserBall");
		laser_junction_obj_ = GameObject.Find("laserjunction");
		laser_shadow_obj_ = GameObject.Find("LaserShadow");;
		
		sliced_sprite_obj_.GetComponent<MeshRenderer>().enabled = true;
		laser_ball_obj_.GetComponent<MeshRenderer>().enabled = true;
		laser_junction_obj_.GetComponent<MeshRenderer>().enabled = true;
		laser_shadow_obj_.GetComponent<MeshRenderer>().enabled = true;
		
		laser_animation_.animation["EnemyLaserAnimation"].speed = 1;
		laser_animation_.animation["EnemyLaserAnimation"].time = init_time_;
		laser_animation_.Play("EnemyLaserAnimation");
		audio_manager_.Play("9_Boss Laser Souns", sliced_sprite_obj_.transform, 1.0F, true);
			
		collider_.enabled = false;
		GetComponent<ZOrderObject>().Reinit();
		
		//ChangeLevel(GetComponent<Enemy>().params_level_);
	}
	
	public void ChangeLevel(int params_level)
	{
		switch(params_level)
		{
			case 0:
				SetCurrentSprite("blue");
				break;
			case 1:
				SetCurrentSprite("green");
				break;
			case 2:
				SetCurrentSprite("red");
				break;
		}
	}
	
	private void SetCurrentSprite(string color_name)
	{
		sliced_sprite_obj_.GetComponent<tk2dSlicedSprite>().SetSprite("BossLaser00" + color_name);
		laser_ball_obj_.GetComponent<tk2dSprite>().SetSprite("BossLaser02" + color_name);
		laser_junction_obj_.GetComponent<tk2dSprite>().SetSprite("BossLaser07" + color_name);
	}
}
