using UnityEngine;
using System.Collections;

public class UpMoveController : MonoBehaviour {
	
	private tk2dAnimatedSprite animations_;
	private tk2dSprite glow_ = null;
	private tk2dAnimatedSprite parachute_animations_;
	private tk2dSprite x_sprite_;
	
	private Enemy enemy_;
	private Player player_;
	private Boss boss_;
	
	private float ground_y_;
	private bool on_ground_;
	
	private float speed_ = 0.0F;
	private float time_to_alarm_ = 1.0F;
	private float time_to_explosion_ = 2.0F;
	private bool stay_ = false;
	private float dt_;
	
	private const float ray_damage_ = 40.0F;
	
	public GameObject explosion_;
	
	private bool pause_ = false;
	private bool damaged_ = false;
	
	private AudioManager audio_manager_;
	
	public bool player_tracking = true;
	public float ground_y = 100.0F;
	public float delay = 2.0F;
	
	// Use this for initialization
	void Start () {
		GetComponent<ZOrderObject> ().set_enabled(false);
		//Init ();
	}
	
	public void Init()
	{
		tk2dSprite[] animations = GetComponentsInChildren<tk2dSprite>();
		
		foreach (tk2dSprite animation in animations) {
            if(animation.name == "BombBody")
				animations_ = (tk2dAnimatedSprite)animation;
			else if(animation.name == "Glow")
				glow_ = animation;	
			else if(animation.name == "ParachuteAnim")
				parachute_animations_ = (tk2dAnimatedSprite)animation;
			else if(animation.name == "XSprite")
				x_sprite_ = animation;		
        }
		
		enemy_ = GetComponent<Enemy>();
		player_ = GameObject.Find("Player").GetComponent<Player>();
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		
		if(player_tracking)
		{
			ground_y_ = player_.transform.position.y + 10;
			x_sprite_.renderer.enabled = false;
		}
		else
		{
			ground_y_ = ground_y;
			stay_ = false;
			x_sprite_.renderer.enabled = true;
		}
				
		if(gameObject.name.Contains("UpEnemy"))
		{
			time_to_explosion_ = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "time_to_explosion");
			parachute_animations_.renderer.enabled = false;
		}

		on_ground_ = false;
		
		//Audio
		audio_manager_ = Framework.Instance.audio_manager();
		
		if( !GameObject.Find("BossPrefab").GetComponent<Boss>().IsPatternSetup() )
		{
			delay = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "delay");
		}
		else
		{
			stay_ = true;
			
			if(gameObject.name.Contains("Up2Enemy"))
				audio_manager_.Play("36_Fast Rocket Dropping", this.transform, 0.5F, 1.0F, false);
		}
		
		animations_.Play();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(pause_)
			return;
		
		if(!on_ground_)
		{
			if(!stay_)
			{
				transform.Translate(0.0F, speed_ * Time.deltaTime, 0.0F);
				x_sprite_.transform.Translate(0.0F, -speed_ * Time.deltaTime, 0.0F);
			}
			else
			{
				delay -= Time.deltaTime;
				if(delay < 0.0F)
				{
					stay_ = false;
					if(GameObject.Find("BossPrefab").GetComponent<Boss>().IsPatternSetup())
					{
						if(player_tracking)
						{
							transform.position = new Vector2(player_.transform.position.x, ground_y_ + 300.0F);
							speed_ *= -1.0F;
							ground_y_ = player_.transform.position.y + 10;
							
							if(gameObject.name.Contains("Up2Enemy"))
								audio_manager_.Play("36_Fast Rocket Dropping", this.transform, 0.5F, 1.0F, false);
							
						}
						x_sprite_.renderer.enabled = true;
						x_sprite_.transform.localPosition = new Vector3(-4.0F, ground_y_ - 45 - transform.position.y, 200.0F);
					}
				}
			}
			
			if(!GameObject.Find("BossPrefab").GetComponent<Boss>().IsPatternSetup())
			{	
				if(transform.position.y > 550.0F && player_tracking)
				{
					speed_ *= -1.0F;
						
					if(gameObject.name.Contains("UpEnemy"))
					{
						stay_ = true;
						parachute_animations_.renderer.enabled = true;
					}
					else
					{
						animations_.Play("Up2Enemy");
					}
					
					if(player_tracking)
						transform.position = new Vector2(player_.transform.position.x, ground_y_ + 300.0F);
									
					x_sprite_.renderer.enabled = true;
					x_sprite_.transform.localPosition = new Vector3(-4.0F, ground_y_ - 45 - transform.position.y, 200.0F);
				}
			}
		
			//touch on ground
			if(transform.position.y < ground_y_ && speed_ < 0.0F)
			{
				on_ground_ = true;
				x_sprite_.renderer.enabled = false;
				
				transform.position = new Vector3(transform.position.x, ground_y_, transform.position.z);
				
				animations_.animationCompleteDelegate = this.OnAnimationComplete;
				animations_.Play("UpEnterOnGround");
				
				//if(glow_ != null)
				//	glow_.Play("UpEnterOnGroundGlow");
							
				dt_ = time_to_explosion_;
				
				//Audio
				//audio_manager_.Play("AirMissileAlarm", this.transform, 1.0F, false);
				
				if(gameObject.name.Contains("UpEnemy"))
				{
					parachute_animations_.GetComponent<Animation>().Play("ParachuteOut");
				}
				else if(gameObject.name.Contains("Up2Enemy"))
				{
					Destroy ();
				}
			}
		}
		else
		{
			dt_ -= Time.deltaTime;
			if(dt_ < 0)
			{									
				Vector2 v_dist = GameObject.Find ("Player").GetComponent<PlayerMoveController> ().GetHitPosition () - 
								(new Vector2(animations_.transform.position.x, 
											 this.GetComponent<ZOrderObject> ().GetBaseline()));
				if( v_dist.magnitude < ray_damage_)
				{
					GameObject.Find("Game").GetComponent<Game>().PlayerHitted( 
						GetComponent<EnemyInfo>().damage (), 
						false,
						0.0F);
				}
				
				//combo
				Enemy enemy = GetComponent<Enemy> ();
				GameObject.Find("BossPrefab").GetComponent<Boss>().InvalidateCombo (enemy.group_id (), enemy.id());

				Destroy();
			}
		}
	}
	
	private void ActivateAlarm()
	{
		if(damaged_)
			animations_.Play("UpGroundDamaged");
		else
			animations_.Play("UpGround");
	}
	
	void OnAnimationComplete(tk2dAnimatedSprite sprite, int clipId) 
	{
		animations_.animationCompleteDelegate = null;
		animations_.Play("UpGround");
		animations_.Stop();
		Invoke("ActivateAlarm", time_to_alarm_);
		dt_ = time_to_explosion_;
	}
				
	// Damage
	public void Damaged()
	{
		if(animations_.Playing)
			animations_.Play("UpGroundDamaged");
		else
		{
			animations_.Play("UpGroundDamaged");
			animations_.Stop();
		}
		
		damaged_ = true;
	}
	
	public bool IsDamaged()
	{
		return damaged_;
	}
	
	//Destroy
	private void Destroy()
	{	
		GameObject obj = PoolManager.Spawn(explosion_);
		obj.transform.position = transform.position;
		obj.transform.position += new Vector3(0.0f, -10.0f, 0.0f);
			
		CancelInvoke("ActivateAlarm");
		PoolManager.Despawn(this.gameObject);
	}
	
	//Pause
	public void Pause()
	{
		pause_ = true;
		
		animations_.Pause();
		if(parachute_animations_ != null)
			parachute_animations_.Pause();
	}
	
	public void Resume()
	{
		pause_ = false;
		
		animations_.Resume();
		if(parachute_animations_ != null)
			parachute_animations_.Resume();
	}
	
	private void RemoveFromBoss()
	{
		transform.parent = null;
		GetComponent<ZOrderObject> ().Init ((int)(ground_y_ - 50), true);
		GetComponent<ZOrderObject> ().set_enabled(true);
	}
	
	//Getter/Setter
	public bool IsOnGround()
	{
		if(animations_.CurrentClip.name == "UpGround")
			return true;
		return false;
	}
	
	public void Go()
	{
		Invoke("RemoveFromBoss", 0.2F);
		speed_ = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "speed");
		if(!player_tracking)
		{
			speed_ *= -1;
			x_sprite_.renderer.enabled = true;
			x_sprite_.transform.localPosition = new Vector3(-4.0F, ground_y_ - 45 - transform.position.y, 200.0F);
			
			if(gameObject.name.Contains("UpEnemy"))
			{
				parachute_animations_.renderer.enabled = true;
			}
			else
			{
				animations_.Play("Up2Enemy");
			}
		}
	}
	
	public void PreSpawn()
	{
		Init();
		if(GameObject.Find("BossPrefab").GetComponent<Boss>().IsRandomSetup())
		{
			GetComponent<ZOrderObject> ().set_enabled(false);
		}
		else
		{
			GetComponent<ZOrderObject> ().Init ((int)(ground_y_ - 50), true);
			GetComponent<ZOrderObject> ().set_enabled(true);
			if(GameObject.Find("BossPrefab").GetComponent<Boss>().IsRandomSetup())
				animations_.Play("Tap2MoveExit");
			else
				animations_.Play("Up2Enemy");
		}
	}
	
	public void SpawnReset()
	{
		speed_ = 0.0F;
		time_to_alarm_ = 1.0F;
		time_to_explosion_ = 2.0F;
		
		on_ground_ = false;
		x_sprite_.renderer.enabled = false;
		
		if(GetComponent<Enemy>().enemy_type_ == EnemyType.up)
		{
			parachute_animations_.renderer.enabled = true;
			animations_.Play("UpEnemyFly");
			delay = boss_.GetCurrentParam(enemy_.enemy_type_, enemy_.params_level_, "delay");
		}
		else
		{
			if(GameObject.Find("BossPrefab").GetComponent<Boss>().IsRandomSetup())
				animations_.Play("Tap2MoveExit");
			else
				animations_.Play("Up2Enemy");
		}
		
		damaged_ = false;
		stay_ = false;
		player_tracking = true;
		
		CancelInvoke("ActivateAlarm");
		CancelInvoke("RemoveFromBoss");
	}
	
#if UNITY_EDITOR
	private const int length_baseline_ = 50;
	void OnDrawGizmos ()
	{
		if(!player_tracking)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(new Vector3(transform.position.x - length_baseline_ * 0.5F, ground_y - 45, 100.0F),
							new Vector3(transform.position.x + length_baseline_ * 0.5F, ground_y - 45, 100.0F));
			Gizmos.color = Color.white;
		}	
	}
#endif
}
