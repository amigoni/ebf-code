using UnityEngine;
using System.Collections;

public class TapBouncingEnemy : TapMoveController {
	private AudioManager audio_manager_;
	
	private bool hitted_ = false;
	public GameObject popUp_;
	
	private float return_speed_ = 1.0F;
	private float boss_damage_ = 1.0F;
	
	void Start () {
		audio_manager_ = Framework.Instance.audio_manager();
	}
	
	public override void Init()
	{
		base.Init();
		
		Enemy enemy = GetComponent<Enemy>();
		GetComponent<EnemyInfo>().set_damage(boss_.GetCurrentParam(enemy.enemy_type_, enemy.params_level_, "damage"));
		boss_damage_ = boss_.GetCurrentParam(enemy.enemy_type_, enemy.params_level_, "boss_damage");
		return_speed_ = boss_.GetCurrentParam(enemy.enemy_type_, enemy.params_level_, "return_speed");
	}
	
	// Update is called once per frame
	void Update () {
		if(x_dist_ < x_active_z_order)
		{
			float update_x = speed_ * Time.deltaTime;
			x_dist_ += update_x;
			if(x_dist_ >= x_active_z_order)
			{
				this.transform.parent = null;
				GetComponent<ZOrderObject> ().Init((int)(- 35), false); 
				GetComponent<ZOrderObject> ().set_enabled(true);
			}
			transform.Translate(-update_x, 0.0F, 0.0F);
		}
		else
		{
			transform.Translate(-speed_ * Time.deltaTime, 0.0F, 0.0F);
			GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 35)); 
		}
	}
	
	public void InvertSpeed()
	{
		if(!hitted_)
		{
			speed_ = -return_speed_;
			hitted_ = true;
			this.tag = "BossDamage";
			
			GetComponent<EnemyInfo>().set_damage(boss_damage_);
			Instantiate(popUp_, transform.position, Quaternion.identity);
			
			GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.hit_bouncing_enemy, 1);
			
			audio_manager_.Play("35_Missile Bouncing Hit", this.transform, 1.0F, 1.0F, false);
			
			//Play animation
			rocket_body_.Play("BouncingPressAnimation");
			rocket_body_.animationCompleteDelegate = delegate {
				rocket_body_.Play("BouncingAnimation");
				rocket_body_.animationCompleteDelegate = null;
			};
		}
	}
	
	public bool hitted()
	{
		return hitted_;
	}
	
	public override void SpawnReset()
	{
		base.SpawnReset();
		this.tag = "Enemy";
		hitted_ = false;
		
		rocket_body_.Play("BouncingAnimation");
		rocket_body_.animationCompleteDelegate = null;
	}
}
