using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using System;

public class EnemyParams
{	
	public struct KeyParam
	{
		public EnemyType type;
		public int level;
		public string name;
	}
	
	public Dictionary<KeyParam, float> params_dict_ = new Dictionary<KeyParam, float>();
	
	public EnemyParams()
	{
		params_dict_ = new Dictionary<KeyParam, float> ();
	}
	
	//TO-DO like copy construnctor
	public EnemyParams(EnemyParams copy)
	{
		params_dict_ = new Dictionary<KeyParam, float> (copy.params_dict_);
	}
	
	public void SetParam(EnemyType enemy_type, int enemy_level, string param_name, float param_value)
	{
		KeyParam key = new KeyParam();
		key.type = enemy_type;
		key.level = enemy_level;
		key.name = param_name;
		
		params_dict_[key] = param_value;
	}
	
	public float GetParam(EnemyType enemy_type, int enemy_level, string param_name)
	{
		KeyParam key = new KeyParam();
		key.type = enemy_type;
		key.level = enemy_level;
		key.name = param_name;
		
		//try get 
		float v = 0.0F;
		if(params_dict_.TryGetValue( key, out v) )
		{
			return v;
		}
		
		key.level = 0;
		return params_dict_[key];
	}

	//Load
	public static EnemyParams LoadFromXml(XmlNodeList params_node)
	{
		EnemyParams enemy_params = new EnemyParams();
		return LoadFromXml(params_node, enemy_params);
	}
	
	public static EnemyParams LoadFromXml(XmlNodeList params_node, EnemyParams enemy_params)
	{
		EnemyParams local_enemy_params = new EnemyParams(enemy_params);
		EnemyType enemy_type;
		for(int i = 0; i < params_node.Count; i++)
		{
			string child_name = params_node[i].LocalName;
			enemy_type = (EnemyType)System.Enum.Parse (typeof(EnemyType), child_name, true);
			int count_attribute = params_node[i].Attributes.Count;
			float param_value;
			string param_name;
			for(int j = 0; j < count_attribute; j++)
			{
				param_value = float.Parse( params_node[i].Attributes[j].Value );
				param_name = params_node[i].Attributes[j].Name;
				//Add level
				local_enemy_params.SetParam(enemy_type, 0, param_name, param_value);
			}
			
			//load levels
			if(params_node[i].ChildNodes.Count != 0)
			{
				if(params_node[i].ChildNodes[0].ChildNodes.Count != 0)
				{	
					XmlNodeList levels_node = params_node[i].ChildNodes[0].ChildNodes;
					for(int i_level = 0; i_level < levels_node.Count; i_level++)
					{
						int level_number = int.Parse( levels_node[i_level].Attributes[0].Value );
						
						XmlNode over_param_node = levels_node[i_level].FirstChild;
						
						for(int j = 0; j < over_param_node.Attributes.Count ; j++)
						{
							param_value = float.Parse( over_param_node.Attributes[j].Value );
							param_name = over_param_node.Attributes[j].Name;
							//Add level
							local_enemy_params.SetParam(enemy_type, level_number, param_name, param_value);
						}
					}
				}
			}
		}	
		return local_enemy_params;
	}
}