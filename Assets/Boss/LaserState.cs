using UnityEngine;

public sealed class LaserState :  FSMState<Boss> {
	
	static readonly LaserState instance = new LaserState();
	public static LaserState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static LaserState() 
	{
		
	}
	
	private LaserState()
	{
	
	}

	private float dt_;
	
	public override void Enter(Boss b) 
	{
		dt_ = b.laser_duration();
	}
	
	public override void Execute(Boss b) 
	{
		if(b.on_pause())
			return;
		
		dt_ -= Time.deltaTime;
		
		if(dt_ < 0.0F)
		{
			b.EndLaser ();
			dt_ = 9999.9F;
		}
	}
	
	public override void Exit(Boss b) 
	{

	}
}