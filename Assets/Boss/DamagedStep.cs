using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DamagedStep
{
	public bool spawned_;
	public float value_;
	public List<string> sprites_list_;
	public float time_damage_explosion_;
	
	public DamagedStep (float v)
	{
		spawned_ = false;
		value_ = v;
		sprites_list_ = new List<string> ();
	}
		
	public bool Check (float normalized_life)
	{
		if (!spawned_) {
			if (value_ >= normalized_life) {
				spawned_ = true;
				return true;
			}
		}
			
		return false;
	}
		
	public void Reset ()
	{
		spawned_ = false;
	}
}