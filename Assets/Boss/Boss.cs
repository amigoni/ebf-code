using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MoveType
{
	move1 = 0,
	move2,
	move3,
	move4,
	move5,
	move6,
	stay,
	move,
	moveUp,
	moveDown,
	tracking,
	random,
	count
}
	
public class Boss : MonoBehaviour
{	
	private FiniteStateMachine<Boss> fsm_;
	private bool fsm_locked_ = false;
	private int current_group_id_ = 0;
	private Color current_color_group_;
	private int spawned_enemy_ = 0;
	private ComboManager combo_manager_;
	private PlayerMoveController player_;
	private GameObject main_boss_;
	private GameObject boss_;
	private Animation boss_animation_component_;
	private Animation animation_component_;
	private List<Enemy> enemy_to_spawn_ = new List<Enemy> ();
	private Transform enemy_to_spawn_in_animation_;
	private AudioManager audio_manager_;
	private GameObject gui_;
	
	//levels
	private LevelSetup levels_ = new LevelSetup ();
	private bool need_to_change_setup_ = false;
	public Transform tap_enemy;
	public Transform tap2_enemy;
	public Transform fast_tap_enemy;
	public Transform chasing_tap_enemy;
	public Transform sin_tap_enemy;
	public Transform bouncing_tap_enemy;
	public GameObject tap_left_transform_object;
	public GameObject tap_right_transform_object;
	public Transform up_enemy;
	public Transform up2_enemy;
	public GameObject up_transform_object;
	public Transform down_enemy;
	public GameObject down_transform_object;
	public Transform laser_enemy;
	public GameObject laser_transform_object;
	public Transform mole_enemy;
	private TrackingController tracking_controller_;
	private float offset_move_y_ = 25.0F;
	private BoxCollider walkable_area_;
	private float speed_before_pause_;
	private float laser_duration_ = 3.5F;
	private bool on_pause_ = false;
	const int first_level_change_ = 3;
	const int second_level_change_ = 5;
	private Color damage_color_ = new Color (1.0F, 0.5F, 0.5F);
	private const float damage_duration_ = 0.2F;
	private float init_x_;
	private float tap_animation_length_ = 0.4F;
	private int last_enemy_level_;
	
	//Shot fire
	private tk2dAnimatedSprite left_shot_fire_;
	private tk2dAnimatedSprite right_shot_fire_;
	
	
	//color
	private Color white_color_ = new Color (1.0F, 1.0F, 1.0F);
	private Color current_color_ = new Color (1.0F, 1.0F, 1.0F);
	
	//Freeze
	private bool freeze_ = false;
	private Color freeze_color_ = new Color (0.54F, 0.78F, 1.0F);
	private float freeze_prev_animation_normalized_time_;
	private float freeze_prev_animation_normalized_speed_;
	private string freeze_prev_animation_;
	private FSMState<Boss> freeze_prev_state_;
	
	//life
	private float initial_life_ = 100.0F;
	private float life_ = 100.0F;
	
	//stum /stun
	public GameObject stump_boss_prefab_;
	private GameObject stump_instance_;
	private float stun_prev_animation_normalized_time_;
	private float stun_prev_animation_normalized_speed_;
	private string stun_prev_animation_;
	private bool stunned_ = false;
	
	//ShotDamageArea
	private BoxCollider shot_damage_area_;
	
	//clock
	public GameObject clock_prefab_;
	private GameObject clock_instance_;
	
	//Damage Step
	private DamagedStep first_damaged_step_;
	private DamagedStep second_damaged_step_;
	private DamagedStep third_damaged_step_;
	private DamagedStep final_damaged_step_;
	
	//Damage Explosion
	public GameObject damage_explosion_;
	private const float damage_ray = 120;
	
	//Params
	private float attack_ = 1.0f;
	private float defense_ = 1.0f;
	private int time_bonus_ = 0;
	
	//Hit me
	private const float hit_me_time_ = 10.0f;
	private float dt_hit_me_time_ = 99.0f;
	private List<string> hit_me_message_ = new List<string>();
	private bool hit_message_enabled_ = false;
	
	// Use this for initialization
	void Start ()
	{		
		fsm_ = new FiniteStateMachine<Boss> ();
		fsm_.Configure (this, IdleState.Instance);
		
		gui_ = GameObject.Find("GUI");
		
		main_boss_ = GameObject.Find ("MainBoss");
		boss_ = GameObject.Find ("Boss");
		init_x_ = boss_.transform.position.x;
		
		//animation component
		animation_component_ = GetComponent<Animation> ();
		boss_animation_component_ = boss_.GetComponent<Animation> ();
		PlayIdle ();
		
		//get walkable area
		walkable_area_ = GameObject.Find ("BossWalkableArea").GetComponent<BoxCollider> ();
		
		//get Combo
		combo_manager_ = GetComponent<ComboManager> ();
		current_color_group_ = GetRandomColor ();
		
		//Init setup
		levels_.Init (
			GameObject.Find ("Game").GetComponent<Game> (), 
			this, 
			GameObject.Find ("BackgroundContainer").GetComponent<EnvironmentSelector> () 
		);	
		//check is multiplayer
		Game.GameMode game_mode = GameObject.Find("Game").GetComponent<Game>().game_mode();
		if(game_mode == Game.GameMode.multiplayer || game_mode == Game.GameMode.boss_training)
			GetComponent<LoadCustomBoss>().SetCustomBoss();
		
		//Intro
		levels_.FirstIntro();
		
		//life
		life_ = levels_.current_setup ().GetBossLife ();
		initial_life_ = life_;
		
		//tracking
		tracking_controller_ = GetComponent<TrackingController> ();
		tracking_controller_.enabled = false;
		
		//shot_damage_area_
		shot_damage_area_ = GameObject.Find ("ShotDamageArea").GetComponent<BoxCollider> ();
			
		//Player
		player_ = GameObject.Find ("Player").GetComponent<PlayerMoveController> ();
		
		//animation leg
		tap_animation_length_ = animation_component_.animation ["LeftTapAnim"].clip.length;
		
		//shot fire
		left_shot_fire_ = GameObject.Find ("LeftShotFire").GetComponent<tk2dAnimatedSprite> ();
		left_shot_fire_.animationCompleteDelegate = delegate(tk2dAnimatedSprite sprite, int clipId) {
			left_shot_fire_.renderer.enabled = false;
		};
		right_shot_fire_ = GameObject.Find ("RightShotFire").GetComponent<tk2dAnimatedSprite> ();
		right_shot_fire_.animationCompleteDelegate = delegate(tk2dAnimatedSprite sprite, int clipId) {
			right_shot_fire_.renderer.enabled = false;
		};
		
		//Audio Manager
		audio_manager_ = Framework.Instance.audio_manager();
		
		InitDamageStep ();
		
		//Hit Me
		dt_hit_me_time_ = hit_me_time_;
		hit_me_message_.Add("Come HIT ME!");
		hit_me_message_.Add("Scared? HIT ME!!!");
		hit_me_message_.Add("Time is ticking. HIT ME");
	}
	
	void InitDamageStep ()
	{
		first_damaged_step_ = new DamagedStep (0.75F);
		first_damaged_step_.sprites_list_.Add ("head_damage_07");
		first_damaged_step_.sprites_list_.Add ("left_arm_damage_08");
		first_damaged_step_.sprites_list_.Add ("left_shoulder_damage_03");
		//smoke
		first_damaged_step_.sprites_list_.Add("SmokeOneBodyAnimatedSprite");
		//explosion time
		first_damaged_step_.time_damage_explosion_ = 0.5F;
		
		second_damaged_step_ = new DamagedStep (0.5F);
		second_damaged_step_.sprites_list_.Add ("right_arm_damage_10");
		second_damaged_step_.sprites_list_.Add ("body_damage_06");
		second_damaged_step_.sprites_list_.Add ("left_arm_damage_05");
		//smoke
		second_damaged_step_.sprites_list_.Add("SmokeTwoTopCannonAnimatedSprite");
		//explosion time
		second_damaged_step_.time_damage_explosion_ = 0.4F;
		
		third_damaged_step_ = new DamagedStep (0.25F);
		third_damaged_step_.sprites_list_.Add ("left_shoulder_damage_01");
		third_damaged_step_.sprites_list_.Add ("left_arm_damage_04");
		//smoke
		third_damaged_step_.sprites_list_.Add("SmokeOneTopCannonAnimatedSprite");
		third_damaged_step_.sprites_list_.Add("SmokeOneLeftArmAnimatedSprite");
		//explosion time
		third_damaged_step_.time_damage_explosion_ = 0.3F;
		
		final_damaged_step_ = new DamagedStep (0.1F);
		final_damaged_step_.sprites_list_.Add ("left_shoulder_damage_02");
		final_damaged_step_.sprites_list_.Add ("head_damage_12");
		final_damaged_step_.sprites_list_.Add ("top_cannon_damage_09");
		final_damaged_step_.sprites_list_.Add ("top_cannon_damage_11");
		//smoke
		final_damaged_step_.sprites_list_.Add("SmokeTwoLeftArmAnimatedSprite");
		final_damaged_step_.sprites_list_.Add("SmokeTwoRightArmAnimatedSprite");
		//explosion time
		final_damaged_step_.time_damage_explosion_ = 0.1F;
	}
	
	// Update is called once per frame
	void Update ()
	{
		GetComponent<ZOrderObject> ().UpdateBaseline ((int)(80));
		if (on_pause_ || freeze_)
			return;
		
		fsm_.Update ();
		
		HitMessageUpdate();
		//Debug.Log(animation_component_.clip.name);
	}
	
	private void HitMessageUpdate()
	{
		if(hit_message_enabled_)
		{
			dt_hit_me_time_ -= Time.deltaTime;
			if(dt_hit_me_time_ < 0.0f)
			{
				dt_hit_me_time_ = hit_me_time_;
				GameObject.Find ("Game").GetComponent<Game>().BossBaloon(
					hit_me_message_[ Mathf.RoundToInt((hit_me_message_.Count - 1) * Random.value) ]);
			}
		}
	}
	
	public void StartAttack ()
	{
		ChangeState (MoveState.Instance);
	}
	
	//FSM
	public void ChangeState (FSMState<Boss> s)
	{
		if(!fsm_locked_)
		{
			fsm_.ChangeState (s);
		}
		else
		{
			Debug.Log("WARNING!");
		}
	}
	
	public FSMState<Boss> GetCurrentFSMState ()
	{
		return fsm_.CurrentState;
	}
	
	//*---------------------------------------- Spawn ----------------------------------------*//
	private Transform GetEnemyTransform (EnemyType type)
	{
		Transform transform_enemy = null;
		switch (type) {
		case EnemyType.tap_left:
		case EnemyType.tap_right:
		case EnemyType.tap_either:
			transform_enemy = tap_enemy;
			break;
		case EnemyType.tap2_left:
		case EnemyType.tap2_right:
		case EnemyType.tap2_either:
			transform_enemy = tap2_enemy;
			break;
		case EnemyType.fast_tap_left:
		case EnemyType.fast_tap_right:
		case EnemyType.fast_tap_either:
			transform_enemy = fast_tap_enemy;
			break;
		case EnemyType.up:
			transform_enemy = up_enemy;
			break;	
		case EnemyType.up2:
			transform_enemy = up2_enemy;
			break;		
		case EnemyType.down:
			transform_enemy = down_enemy;
			break;		
		case EnemyType.laser:
			transform_enemy = laser_enemy;
			break;
		case EnemyType.chasing_tap_left:
		case EnemyType.chasing_tap_right:
		case EnemyType.chasing_tap_either:
			transform_enemy = chasing_tap_enemy;
			break;
		case EnemyType.sin_tap_left:
		case EnemyType.sin_tap_right:
		case EnemyType.sin_tap_either:
			transform_enemy = sin_tap_enemy;
			break;
		case EnemyType.boucing_tap_left:
		case EnemyType.boucing_tap_right:
		case EnemyType.boucing_tap_either:
			transform_enemy = bouncing_tap_enemy;
			break;	
		}
		
		return transform_enemy;
	}
	
	public GameObject SpawnFromPool(string prefab_name)
	{		
		return PoolManager.Spawn(prefab_name);
	}
	
	public void AppendNextEnemy (EnemyType enemy_type, int enemy_level)
	{
		Transform enemy_transform = GetEnemyTransform (enemy_type);
		last_enemy_level_ = enemy_level;
		
		switch (enemy_type) {
		case EnemyType.tap2_left:
		case EnemyType.tap_left:
		case EnemyType.fast_tap_left:
		case EnemyType.chasing_tap_left:
		case EnemyType.sin_tap_left:	
		case EnemyType.boucing_tap_left:	
			CreateEnemy (
				enemy_transform, 
				enemy_level,
				tap_left_transform_object.GetComponent<Transform> ());
			break;
		case EnemyType.tap2_right:
		case EnemyType.tap_right:
		case EnemyType.fast_tap_right:
		case EnemyType.chasing_tap_right:	
		case EnemyType.sin_tap_right:
		case EnemyType.boucing_tap_right:
			CreateEnemy (
				enemy_transform, 
				enemy_level,
				tap_right_transform_object.GetComponent<Transform> ());
			break;
		case EnemyType.tap2_either:	
		case EnemyType.tap_either:
		case EnemyType.fast_tap_either:
		case EnemyType.chasing_tap_either:	
		case EnemyType.sin_tap_either:
		case EnemyType.boucing_tap_either:
			CreateEnemy (
				enemy_transform, 
				enemy_level,
				tap_left_transform_object.GetComponent<Transform> ());
			CreateEnemy (
				enemy_transform, 
				enemy_level,
				tap_right_transform_object.GetComponent<Transform> ());
			break;
		case EnemyType.up2:
		case EnemyType.up:
			break;	
		case EnemyType.down:
			CreateEnemy (
				enemy_transform, 
				enemy_level,
				down_transform_object.GetComponent<Transform> ());
			break;	
		case EnemyType.laser:
			break;
		default:
			Debug.Log ("Error in Boss.cs: SpawnEnemyAnimation, unknown enemy type");
			break;
		}
	}
	
	public void SpawnEnemyAnimation (EnemyType enemy_type, float time_to_shot)
	{
		switch (enemy_type) {
		case EnemyType.tap2_left:
		case EnemyType.tap_left:
		case EnemyType.fast_tap_left:
		case EnemyType.chasing_tap_left:
		case EnemyType.sin_tap_left:	
		case EnemyType.boucing_tap_left:	
			if (time_to_shot < tap_animation_length_)
				animation_component_.Play ("LeftTapFastAnim");
			else
				animation_component_.Play ("LeftTapAnim");
			break;
		case EnemyType.tap2_right:
		case EnemyType.tap_right:
		case EnemyType.fast_tap_right:
		case EnemyType.chasing_tap_right:	
		case EnemyType.sin_tap_right:
		case EnemyType.boucing_tap_right:
			if (time_to_shot < tap_animation_length_)
				animation_component_.Play ("RightTapFastAnim");
			else
				animation_component_.Play ("RightTapAnim");
			break;
		case EnemyType.tap2_either:	
		case EnemyType.tap_either:
		case EnemyType.fast_tap_either:
		case EnemyType.chasing_tap_either:	
		case EnemyType.sin_tap_either:
		case EnemyType.boucing_tap_either:
			if (time_to_shot < tap_animation_length_)
				animation_component_.Play ("EitherTapFastAnim");
			else
				animation_component_.Play ("EitherTapAnim");
			break;
		case EnemyType.up2:
		case EnemyType.up:
			enemy_to_spawn_in_animation_ = GetEnemyTransform (enemy_type);
			animation_component_.Play ("UpShotAnim");
			break;	
		case EnemyType.down:
			animation_component_.Play ("DownShotAnim");
			break;	
		case EnemyType.laser:
			audio_manager_.Play("5_Boss Opens", transform, 1.0f, 1.0f, false);
			animation_component_.Play ("LaserAnimationEntering");
			
			break;	
		default:
			Debug.Log ("Error in Boss.cs: SpawnEnemyAnimation, unknown enemy type");
			break;
		}	

		ChangeState (IdleState.Instance);
	}

	void DisableShotArea ()
	{
		shot_damage_area_.enabled = false;
	}
	
	//Create enemy Animation callbacks
	private void GoEnemy ()
	{
		for (int i = 0; i < enemy_to_spawn_.Count; i++)
			enemy_to_spawn_ [i].Go ();
		
		enemy_to_spawn_.Clear ();
	}
	
	public void DestroyUnspawnedEnemy ()
	{
		for (int i = 0; i < enemy_to_spawn_.Count; i++)
			PoolManager.Despawn(enemy_to_spawn_ [i].gameObject);
		
		enemy_to_spawn_.Clear ();
	}
	
	public void CreateTapLeftEnemy ()
	{
		GoEnemy ();
		left_shot_fire_.renderer.enabled = true;
		left_shot_fire_.Play ();
	}
	
	public void CreateTapRightEnemy ()
	{
		GoEnemy ();
		right_shot_fire_.renderer.enabled = true;
		right_shot_fire_.Play ();
	}
	
	public void CreateTapEitherEnemy ()
	{
		GoEnemy ();
		left_shot_fire_.renderer.enabled = true;
		right_shot_fire_.renderer.enabled = true;
		left_shot_fire_.Play ();
		right_shot_fire_.Play ();
	}
	
	public GameObject CreateEnemy (Transform enemy, int enemy_level, Transform trans)
	{
		GameObject obj = SpawnFromPool(enemy.name);
		obj.transform.position = trans.position;
		obj.transform.parent = trans.transform;
		obj.transform.position = new Vector3 (obj.transform.position.x, obj.transform.position.y, 0.0F);
		obj.transform.localPosition = new Vector3 (obj.transform.localPosition.x, obj.transform.localPosition.y, 0.0F);		
		obj.GetComponent<Enemy> ().ChangeLevel( enemy_level );
		if(obj.GetComponent<Enemy> ().enemy_type_ != EnemyType.up && 
		   obj.GetComponent<Enemy> ().enemy_type_ != EnemyType.up2)
			enemy_to_spawn_.Add (obj.GetComponent<Enemy> ());
		
		return obj;
	}
	
	public void CreateUpEnemy ()
	{
		GameObject obj = CreateEnemy(enemy_to_spawn_in_animation_, last_enemy_level_, up_transform_object.transform);
		obj.GetComponent<Enemy> ().Go ();
	}
	
	public void CreateDownEnemy ()
	{
		GoEnemy ();
	}
	
	public void CreateLaserEnemy ()
	{
		GameObject laser_obj = PoolManager.Spawn(laser_enemy.gameObject);
		laser_obj.transform.position = laser_transform_object.GetComponent<Transform> ().position;
		laser_obj.GetComponent<Enemy> ().ChangeLevel( last_enemy_level_ );
		
		ChangeState (LaserState.Instance);
		
		animation_component_.Play ("LaserAnimationIdle");
	}
	
	public void EndLaser ()
	{
		GameObject laser_object = GameObject.Find ("LaserEnemy(Clone)0");
		if(laser_object != null)
		{
			LaserController laser_enemy = laser_object.GetComponent<LaserController> ();
			laser_enemy.Close ();
		}
	}
	
	public void CloseLaserCannon ()
	{
		if( animation_component_.IsPlaying( "LaserAnimationIdle" ))
			animation_component_.Play ("LaserAnimationExit");
	}
	
	public void EndShot ()
	{
		levels_.current_setup ().EndShot ();
	}
	
	//move callback	
	public void RandomMove (float move_duration)
	{
		float y = main_boss_.transform.position.y;
		float y_offset = 25.0F + Random.value * offset_move_y_;
		int int_y_offset = Mathf.RoundToInt (y_offset);
		y += Random.value < 0.5f ? int_y_offset : -int_y_offset;
		
		if (y > walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F) {
			y = walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F;
			int_y_offset *= -1;
		}
		if (y < walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F) {
			y = walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F;
			int_y_offset *= -1;
		}
		Move(move_duration, int_y_offset, false);
	}
	
	public void Move(float move_duration, float move_y, bool absolute_position)
	{
		float y = move_y;
		if(!absolute_position)
			y += main_boss_.transform.position.y;
		
		//clamp moving value
		//y
		if (y > walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F) {
			y = walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F;
		}
		if (y < walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F) {
			y = walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F;
		}
		
		iTween.MoveTo (main_boss_, 
						iTween.Hash ("y", y, 
									 "time", move_duration,
									 "easetype", iTween.EaseType.linear));		
	}
	
	// Tracking
	public void EnableTracking (TrackingParam param)
	{
		tracking_controller_.Start (param);
	}
	
	public void DisableTracking ()
	{
		tracking_controller_.enabled = false;
	}
	
	public bool TrackingFinished ()
	{
		return tracking_controller_.TrackingFinished();
		//return tracking_controller_.enabled;
	}
	
	public void ForceTrackingDisabled()
	{
		tracking_controller_.enabled = false;
	}
	
	//playidle
	public void PlayIdle ()
	{	
		if(fsm_.CurrentState != StunState.Instance)
		{
			animation_component_.Play ("IdleTestSemiSmooth");
			foreach (AnimationState state in animation) {
				if (state.name == "IdleTestSemiSmooth")
					state.speed = 1.2F;
			}
		}
	}
	
	public void PlayPassiveIdle ()
	{		
		Play("IdlePassiveAnimation");
	}
	
	public void PlayAngryAnimation ()
	{	
		Play("AngryAnimation");
	}
	
	public void PlayAngryExitAnimation ()
	{	
		Play("AngryExitAnimation");
	}
	
	//utility
	Vector3 IntVector (Vector3 v)
	{
		return new Vector3 ((int)v.x, (int)v.y, (int)v.z);
	}
	
	//Combo Color Utility
	public void UpdateComboColor ()
	{
		current_color_group_ = GetRandomColor ();	
	}
	
	Color GetRandomColor ()
	{
		return new Color (0.28F, 0.37F, 0.98F);
	}
	
	//TO-DO add zorder to boss
	public float GetBaseLine ()
	{
		return transform.position.y + 50.0F;
	}
	
	//pause
	public void Pause ()
	{
		on_pause_ = true;
			
		foreach (AnimationState state in animation) {
			speed_before_pause_ = state.speed;
			state.speed = 0.0F;
		}
	}
	
	public void Resume ()
	{
		on_pause_ = false;
		
		foreach (AnimationState state in animation) {
			state.speed = speed_before_pause_;
		}
	}
	
	public bool on_pause ()
	{
		return on_pause_;
	}
	
	//combo
	public void InvalidateCombo (int id_group, int enemy_id)
	{
		combo_manager_.InvalidateCombo (id_group, enemy_id);
	}
	
	public void CheckComboValue (int id_group, int enemy_id)
	{
		if(IsRandomSetup())
			combo_manager_.CheckComboValue (id_group, enemy_id);
	}
	
	public void DecreaseComboValue ()
	{
		combo_manager_.DecreaseEnemySpawn ();
	}
	
	public bool EnemyIsACombo (int enemy_id)
	{
		return combo_manager_.EnemyIsACombo (enemy_id);
	}
	
	public void ActivateLaserCollider()
	{
		GameObject laser_obj = GameObject.Find("LaserEnemy(Clone)0");
		if(laser_obj != null)
		{
			laser_obj.GetComponent<LaserController>().ActiveCollider();
		}
	}
	
	public void EndState()
	{
		GameObject laser_obj = GameObject.Find("LaserEnemy(Clone)0");
		if(laser_obj != null)
		{
			Destroy(laser_obj);
			BlendAnimation("LaserAnimationExit");
		}
		
		combo_manager_.DestroyCallback ();
	}

	//Hit Boss
	public void PlayHitAnimation ()
	{
		SetSpritesColor (damage_color_);
		
		Invoke ("EndHitAnimation", damage_duration_);
	}
	
	public void EndHitAnimation ()
	{
		if (freeze_)
			SetSpritesColor (freeze_color_);
		else
			SetSpritesColor (new Color (1.0F, 1.0F, 1.0F));
	}
	
	//In Out callback
	public void InCallback ()
	{
		levels_.current_setup ().Start ();
	}
	
	public void OutCallback ()
	{
		levels_.prev_setup ().End ();
	}
	
	//Freeze	
	public void Freeze (float freeze_time)
	{
		//In these cases, don't activate
		if (freeze_ ||
			animation_component_.IsPlaying("BossEntering") || 
			animation_component_.IsPlaying("BossExit") )
			return;
		
		iTween.Pause();
		
		freeze_ = true;
		SetSpritesColor (freeze_color_);
		
		//save prev animation
		foreach (AnimationState state in animation) {
			if (state.enabled)
				freeze_prev_animation_ = state.clip.name;
		}
		freeze_prev_animation_normalized_time_ = animation [freeze_prev_animation_].normalizedTime;
		freeze_prev_animation_normalized_speed_ = animation [freeze_prev_animation_].normalizedSpeed;
				
		animation_component_.Play ("Freeze");
		Invoke ("UnFreeze", freeze_time);
		
		boss_animation_component_.Stop ();
		
		freeze_prev_state_ = fsm_.CurrentState;
		
		ChangeState (IdleState.Instance);
		
		InvokeRepeating ("PlayEndAnimation", freeze_time - 1.5F, 0.1F);
	}
	
	private void PlayEndAnimation ()
	{
		if (current_color_ == freeze_color_) {
			SetSpritesColor (white_color_);
		} else {
			SetSpritesColor (freeze_color_);
		}
	}
	
	private void UnFreeze ()
	{
		iTween.Resume();
		
		tk2dSprite[] sprites = GetComponentsInChildren<tk2dSprite> ();
		foreach (tk2dSprite sprite in sprites) {
			if (sprite.name == "Ding1" ||
				sprite.name == "Ding2" ||
				sprite.name == "Ding3" ||
				sprite.name == "Ding4" ||
				sprite.name == "ice_01" ||
				sprite.name == "ice2" ||
				sprite.name == "ice3" ||
				sprite.name == "ice4" ||
				sprite.name == "ice_5" ||
				sprite.name == "ice6" ||
				sprite.name == "ice_7" ||
				sprite.name == "ice8" ||
				sprite.name == "ice9")
				sprite.GetComponent<MeshRenderer> ().enabled = false;
		}
				
		freeze_ = false;
		
		foreach (AnimationState state in animation) {
			if(state.enabled)
			{
				state.normalizedTime = freeze_prev_animation_normalized_time_;
				state.normalizedSpeed = freeze_prev_animation_normalized_speed_;
			}
		}
		animation_component_.Play (freeze_prev_animation_);
		
		ResetSpritesColor ();
		
		levels_.current_setup().EndFreeze();
		
		ChangeState (freeze_prev_state_);
		CancelInvoke ("PlayEndAnimation");
	}
	
	//Flame
	public void Inflames ()
	{
		
	}
	
	//utility
	void ActiveAllChildren (string name_object)
	{
		GameObject game_obj = GameObject.Find (name_object);
		Renderer[] objs = game_obj.GetComponentsInChildren<Renderer> ();
		
		foreach (Renderer obj in objs) {
			obj.gameObject.transform.position -= new Vector3 (1000, 1000, 1000);
		}
	}
	
	void DisableAllChildren (string name_object)
	{
		GameObject game_obj = GameObject.Find (name_object);
		Renderer[] objs = game_obj.GetComponentsInChildren<Renderer> ();
		
		foreach (Renderer obj in objs) {
			obj.gameObject.transform.position += new Vector3 (1000, 1000, 1000);
		}
	}
	
	//color utility
	private void SetSpritesColor (Color color)
	{
		current_color_ = color;
		tk2dSprite[] sprites = GetComponentsInChildren<tk2dSprite> ();
		foreach (tk2dSprite sprite in sprites) {
			if (CheckSprite (sprite.name))
				sprite.color = color;
		}
	}

	private bool CheckSprite (string name)
	{
		if (name != "Glow" &&
			name != "RocketBody" &&
			name != "RocketFlame" &&
			name != "RocketShadow" &&
			name != "BombBody" &&
			name != "ParachuteAnim" &&
			name != "XSprite" &&
			name != "Ding1" &&
			name != "Ding2" &&
			name != "Ding3" &&
			name != "Ding4" &&
			name != "ice_01" &&
			name != "ice2" &&
			name != "ice3" &&
			name != "ice4" &&
			name != "ice_5" &&
			name != "ice6" &&
			name != "ice_7" &&
			name != "ice8" &&
			name != "ice9" &&
			name != "shadow" &&
			name != "LevelLabel")
			return true;
		
		return false;
	}
	
	private void ResetSpritesColor ()
	{
		SetSpritesColor (new Color (1.0F, 1.0F, 1.0F));
	}
	
	//hide / show sprite
	private void HideSprites (List<string> sprites_list)
	{
		foreach (string sprite_name in sprites_list) {
			GameObject.Find (sprite_name).GetComponent<MeshRenderer> ().enabled = false;
		}
	}
	
	private void ShowSprites (List<string> sprites_list)
	{
		foreach (string sprite_name in sprites_list) {
			GameObject.Find (sprite_name).GetComponent<MeshRenderer> ().enabled = true;
		}
	}

	//animation component
	public bool IsPlaying ()
	{
		return animation_component_.isPlaying;
	}
	
	public void BlendAnimation(string clip_name)
	{
		animation_component_.Blend(clip_name, 1.0f, 0.0f);
	}
	
	public void Play (string clip_name)
	{
		//Debug.Log(clip_name);
		if(fsm_.CurrentState != StunState.Instance)
			animation_component_.Play (clip_name);
	}
	
	public void Play (string clip_name, float speed)
	{	
		//Debug.Log(clip_name);
		animation_component_.Play (clip_name);
		foreach (AnimationState state in animation) {
			if (state.enabled)
				state.speed = speed;
		}
	}
	
	public void PlayFrom (string clip_name, float normalized_time)
	{		
		//Debug.Log(clip_name);
		animation_component_.Play (clip_name);
		foreach (AnimationState state in animation) {
			if (state.enabled)
				state.normalizedTime = normalized_time;
		}
	}
	
	public float GetAnimationNormalizedTime ()
	{
		float nt = 0.0F;
		foreach (AnimationState state in animation) {
			if (state.enabled)
				nt = state.normalizedTime;
		}
		
		return nt;
	}
	
	public void Rewind (string clip_name)
	{
		animation_component_.Rewind (clip_name);
	}
	
	public void Rewind (string clip_name, float normalized_time)
	{
		foreach (AnimationState state in animation) {
			if (state.enabled)
				state.normalizedTime = normalized_time;
		}
		animation_component_.Rewind (clip_name);
	}
	
	//Main Boss Animation Component
	public void StopOnBoss ()
	{
		boss_animation_component_.Stop ();
	}
	
	public void PlayOnBoss (string clip_name)
	{
		boss_animation_component_.Play (clip_name);
	}
	
	public void PlayOnBoss (string clip_name, float speed)
	{
		boss_animation_component_.Play (clip_name);
		foreach (AnimationState state in boss_animation_component_) {
			if (state.enabled)
				state.speed = speed;
		}
	}
	
	public void PlayOnBossFrom (string clip_name, float normalized_time)
	{
		foreach (AnimationState state in boss_animation_component_) {
			if (state.enabled)
				state.normalizedTime = normalized_time;
		}
		
		boss_animation_component_.Play (clip_name);
	}
	
	public bool IsMainAnimationComponentPlaying ()
	{
		return boss_animation_component_.isPlaying;
	}
	
	//getter
	public float GetCurrentParam (EnemyType type, int param_level, string param_name)
	{
		return levels_.current_setup ().GetCurrentParam (type, param_level, param_name);
	}
	
	public Color current_color_group ()
	{
		return current_color_group_;
	}

	
	public int current_group_id ()
	{
		return current_group_id_;
	}
	
	public void set_current_group_id (int current_group_id)
	{
		current_group_id_ = current_group_id;
	}
	
	public void IncreaseSpawnedEnemy()
	{
		spawned_enemy_++;
	}
	
	public int spawned_enemy ()
	{
		return spawned_enemy_;
	}
	
	public int GetGroupCount ()
	{
		return levels_.current_setup ().GetGroupCount ();
	}
	
	public float laser_duration ()
	{
		return laser_duration_;
	}
	
	public bool ThereIsOneCombo ()
	{
		return combo_manager_.ThereIsOneCombo ();
	}
	
	public bool CurrentGroupIsCombo ()
	{
		return levels_.current_setup ().CurrentGroupIsCombo ();
	}

	public bool need_to_change_setup ()
	{
		return need_to_change_setup_;
	}
	
	public Animation animation_component ()
	{
		return animation_component_;
	}
	
	public GameObject main_boss ()
	{
		return main_boss_;
	}
	
	public bool stunned ()
	{
		return stunned_;
	}
	
	public float attack()
	{
		return attack_;
	}
	
	public float defense()
	{
		return defense_;
	}
	
	//life
	public float life ()
	{
		return life_;
	}
	
	public void RestoreLife (float life)
	{
		life_ = life;
		initial_life_ = life_;
		gui_.GetComponent<GameUI>().SetBossLife(1);
	}
	
	public void RestoreDamageSprites ()
	{
		first_damaged_step_.Reset ();
		HideSprites (first_damaged_step_.sprites_list_);
		second_damaged_step_.Reset ();
		HideSprites (second_damaged_step_.sprites_list_);
		third_damaged_step_.Reset ();
		HideSprites (third_damaged_step_.sprites_list_);
		final_damaged_step_.Reset ();
		HideSprites (final_damaged_step_.sprites_list_);

		//remove damage explosion
		CancelInvoke("SpawnDamageExplosion");
	}
	
	private bool CanDamage()
	{
		return !IsPatternSetup();
	}
	
	public void AddDamage (float damage)
	{
		dt_hit_me_time_ = hit_me_time_;
		
		if (life_ > 0.0F && CanDamage()) {
			damage *= player_.GetComponent<Player>().AttackValueMult();

			life_ -= damage;
			
			if(life_ < 0.0F)
				life_ = 0.0F;
			
			gui_.GetComponent<GameUI>().SetBossLife(life_/initial_life_);
			
			if (life_ <= 0.0F) {	
				need_to_change_setup_ = true;
				gui_.GetComponent<GameUI>().StopTimeLeft();
			}
			
			//if in charge state
			if (levels_.IsPhysicAttackSetup ()) {
				levels_.physic_attack_setup ().StopAttack ();
			}
			
			//check damage sprite
			float normalized_life = life_ / initial_life_;
			if (first_damaged_step_.Check (normalized_life))
			{
				ShowSprites (first_damaged_step_.sprites_list_);
				UpdateDamageExplosion(first_damaged_step_.time_damage_explosion_);
			}
			if (second_damaged_step_.Check (normalized_life))
			{
				ShowSprites (second_damaged_step_.sprites_list_);
				UpdateDamageExplosion(second_damaged_step_.time_damage_explosion_);
			}
			if (third_damaged_step_.Check (normalized_life))
			{
				ShowSprites (third_damaged_step_.sprites_list_);
				UpdateDamageExplosion(third_damaged_step_.time_damage_explosion_);
			}
			if (final_damaged_step_.Check (normalized_life))
			{
				ShowSprites (final_damaged_step_.sprites_list_);
				UpdateDamageExplosion(final_damaged_step_.time_damage_explosion_);
			}
		}
	}
	
	public void UpdateDamageExplosion(float time)
	{
		CancelInvoke("SpawnDamageExplosion");
		InvokeRepeating("SpawnDamageExplosion", 0.0F, time);

	}
	
	public void SpawnDamageExplosion ()
	{
		//explosion
		Vector3	rand_position = new Vector3 (
				(transform.position.x - damage_ray * 0.5F) + damage_ray * Random.value + 250.0F,
				(transform.position.y - damage_ray * 0.5F) + damage_ray * Random.value + 150.0F,
				-80.0F);
		GameObject explosion = PoolManager.Spawn(damage_explosion_);
		explosion.transform.position = rand_position;
		explosion.transform.parent = main_boss_.transform;
	}
	
	
	//Combo
	public ComboManager combo_manager ()
	{
		return combo_manager_;
	}
	
	//Getter states
	public RandomSetup random_setup ()
	{
		return levels_.random_setup ();
	}
	
	public PhysicAttackSetup physic_attack_setup ()
	{
		return levels_.physic_attack_setup ();
	}
	
	public PatternSetup pattern_setup ()
	{
		return levels_.pattern_setup ();
	}	
	
	public BonusSetup bonus_setup ()
	{
		return levels_.bonus_setup ();
	}
	
	public bool IsRandomSetup()
	{
		return levels_.IsRandomSetup();
	}
	
	public bool IsPhysicAttackSetup()
	{
		return levels_.IsPhysicAttackSetup();
	}
	
	public bool IsPatternSetup()
	{
		return levels_.IsPatternSetup();
	}
	
	public bool CheckChangeSetup ()
	{
		if (need_to_change_setup_) {
			ChangeSetup ();
			
			need_to_change_setup_ = false;
			
			return true;
		}
		
		return false;
	}
	
	public void ForceChangeSetup ()
	{
		//Debug.Log("Changed");
		ChangeSetup ();
		CancelInvoke("PlayAngryAnimation");
		CancelInvoke("EndStunFromPowerPunch");
		CancelInvoke("ExitStunFromPowerPunch");
		CancelInvoke("EndStun");
	}
	
	void ChangeSetup ()
	{
		levels_.NextState ();
	}
	
	//Stun Boss
	public void StunFromPowerPunch(float stun_time)
	{
		if(levels_.current_setup().CanStun() &&
			stunned_ == false)
		{
			Invoke("EndStunFromPowerPunch", stun_time);
			Invoke("ExitStunFromPowerPunch", stun_time - 0.2f);
			
			Play ("IdleStun");
			ChangeState (StunState.Instance);
			levels_.current_setup().PrevPowerPunchStun();
			
			stunned_ = true;
		}
	}
	
	private void ExitStunFromPowerPunch()
	{
		GetComponent<BossSoundController>().BossIsStunnedExit();
		//Play ("GoBack");	
		//stunned_ = false;
	}
	
	private void EndStunFromPowerPunch()
	{
		//Hide stun sprite
		List<string> sprites_list = new List<string>();
		sprites_list.Add("head_container/bottom_head_sprite/tongue_sprite");
		sprites_list.Add("head_container/top_head_stun_sprite");
		sprites_list.Add("head_container/Star_1_sprite");
		sprites_list.Add("head_container/Star_2_sprite");
		sprites_list.Add("head_container/Star_3_sprite");
		sprites_list.Add("head_container/Star_4_sprite");
		HideSprites(sprites_list);
		
		stunned_ = false;
		levels_.current_setup().EndPowerPunchStun();
	}
	
	public void InstantiateStump ()
	{
		stump_instance_ = Instantiate (stump_boss_prefab_) as GameObject;
	}
	
	public void DestroyStump ()
	{
		if(stump_instance_ != null)
		{
			Destroy(stump_instance_);
			stump_instance_ = null;
		}
	}
	
	public void DestroyStumpAndCreate ()
	{
		if(stump_instance_ != null)
		{
			Destroy(stump_instance_);
			stump_instance_ = null;
		}
		((PhysicAttackSetup) levels_.current_setup()).set_need_stump(true);
	}
		
	public void RemoveInstantiateStump()
	{
		if(stump_instance_ != null)
			stump_instance_.GetComponent<StunBoss> ().PlayExit ();
	}
	
	public void SetNullStumpInstance()
	{
		stump_instance_ = null;
	}
	
	public void PreStun()
	{
		stunned_ = true;
		StopOnBoss();
		ForceTrackingDisabled();
	}
	
	public void Stun ()
	{
		stunned_ = true;
		boss_animation_component_.Stop ();
		
		Play ("IdleStun");
		ChangeState (IdleState.Instance);
		Invoke ("EndStun", levels_.physic_attack_setup ().GetStunTime ());
		ForceTrackingDisabled();
				
		GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.stun_boss, 1);
	}
	
	public void EndStun ()
	{
		Play ("GoBack");
		stunned_ = false;
		//return to init position
		iTween.MoveTo (boss_, 
						iTween.Hash ("x", GameObject.Find("BossAnchor").transform.position.x, 
									 "time", levels_.physic_attack_setup ().GetReturnTime (),
									 "easetype", iTween.EaseType.linear,
									 "onComplete", "EndStunReposition",
									 "onCompleteTarget", gameObject
									 ));
		
		GetComponent<BossSoundController>().BossIsStunnedExit();
	}
	
	private void EndStunReposition ()
	{
		EndShot ();
	}
	
	//Clock
	public Clock InstantiateClock ()
	{
		clock_instance_ = Instantiate (clock_prefab_) as GameObject;
		return clock_instance_.GetComponent<Clock> ();
	}
	
	public void DestroyClock ()
	{
		clock_instance_.GetComponent<Clock> ().PlayExit ();
	}
	
	public BonusManager bonus_manager()
	{
		return GetComponent<HitBoss>().bonus_manager();
	}
	
	//Invoke Utility
	public void BossInvoke (string method_name, float time)
	{
		if(fsm_.CurrentState != StunState.Instance)
			Invoke (method_name, time);
	}
	
	public void BossCancelInvoke (string method_name)
	{
		CancelInvoke (method_name);
	}
	
	public void EnableBossDamage()
	{
		GameObject.Find("BossPrefab").GetComponent<HitBoss>().set_damage_by_player( true );
	}
	
	public void DisableBossDamage()
	{
		GameObject.Find("BossPrefab").GetComponent<HitBoss>().set_damage_by_player( false );
	}
	
	public void DamagesPlayer(float damage_value)
	{
		//Damage Player
		Vector2 dist = player_.transform.position - GameObject.Find ("DamageArea").transform.position;
		if(dist.magnitude < 25 && 
		   GameObject.Find("Player").GetComponent<Player>().IsPlayerAlive())
		{
			GameObject.Find("Game").GetComponent<Game>().PlayerHitted(damage_value * attack(), true, -90.0F);
			GameObject.Find("Player").GetComponent<Player>().PushedByBoss();
			GameObject.Find("Game").GetComponent<Game>().DestroyAllEnemyOnBoss();
		}
	}
	
	public void RealEndShot()
	{
		if(!fsm_locked_)
			levels_.current_setup().RealEndShot();
	}
	
	public LevelSetup level_setup()
	{
		return levels_;
	}
	
	//multiplayer 
	public void UpdateParams( 
		float boss_att, 
		float boss_def,
		int time_bonus)
	{
		attack_ = boss_att;
		defense_ = boss_def;
		time_bonus_ = time_bonus;
	}
	
	public int time_bonus()
	{
		return time_bonus_;
	}
	
	public FiniteStateMachine<Boss> fsm()
	{
		return fsm_;
	}
	
	public void set_fsm_locked(bool fsm_locked)
	{
		fsm_locked_ = fsm_locked;
	}
	
	public bool fsm_locked()
	{
		return fsm_locked_;
	}		
	
	public void ClearEnemyToSpawn()
	{
		enemy_to_spawn_.Clear ();
	}
	
	public void DisableHitMessage()
	{
		hit_message_enabled_ = false;
		dt_hit_me_time_ = hit_me_time_;
	}
	
	public void EnabledHitMessage()
	{
		hit_message_enabled_ = true;
		dt_hit_me_time_ = hit_me_time_;
	}
}
