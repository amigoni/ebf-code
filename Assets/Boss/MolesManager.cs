using UnityEngine;
using System.Collections;
using System.Xml;

public struct MolesManagerParams{
	public float repeat_rate_;
}

public class MolesManager : MonoBehaviour {
	
	private MolesManagerParams moles_manager_params_;
	private float repeat_rate_dt_ = 0.0f;
	private PlayerWalkableArea player_walkable_area_;
	
	// Use this for initialization
	void Start () {
		player_walkable_area_ = GameObject.Find("PlayerWalkableArea").GetComponent<PlayerWalkableArea>();
		//StopSpawning();
	}
	
	// Update is called once per frame
	void Update () {
		repeat_rate_dt_ += Time.deltaTime;
		if(repeat_rate_dt_ > moles_manager_params_.repeat_rate_)
		{
			repeat_rate_dt_ -= moles_manager_params_.repeat_rate_;
			
			GameObject obj = PoolManager.Spawn("MoleEnemy");
			obj.transform.position = player_walkable_area_.GetRandomPosition();
		}
	}
	
	public void ChangeParams(MolesManagerParams moles_manager_params)
	{
		moles_manager_params_ = moles_manager_params;
		
		if(moles_manager_params_.repeat_rate_ <= 0.0f)
			enabled = false;
		else
			enabled = true;
	}
	
	public void StopSpawning()
	{
		enabled = false;
	}
	
	//Load xml
	static public MolesManagerParams LoadParamsFromXml(XmlNode params_node)
	{
		MolesManagerParams param = new MolesManagerParams();
		
		param.repeat_rate_ = float.Parse( params_node.Attributes["repeat_rate"].Value );
		
		return param;
	}
}
