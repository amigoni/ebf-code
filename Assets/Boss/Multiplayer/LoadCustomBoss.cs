using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class LoadCustomBoss: MonoBehaviour
{
	private EbFightAPI.Boss boss_config_;
	
	public void set_boss_config(EbFightAPI.Boss boss_config)
	{
		boss_config_ = boss_config;
	}
	
	public void SetCustomBoss()
	{
		Boss boss_;
		boss_ = GameObject.Find ("BossPrefab").GetComponent<Boss> ();
	
		Store store = Framework.Instance.InstanceComponent<Store> ();

		int boss_life = (int) store.GetUpgradablePropertyEffectByNameAsFloat ("boss_level_upgrade", (int) boss_config_.Level);
		
		//get enemy per level
		int boss_level_reload_speed_level = 0;
		int boss_level_shots_per_wave_level = 0;
		int boss_round_environment_level = 0;
		float time_to_next_group_ = 0;
		int enemy_group_length = 0;
		List<string> enviroment_list_ = new List<string> ();	
		for (int i = 0; i < boss_config_.Stages.Count; i++) {
			boss_level_reload_speed_level = (int) boss_config_.Stages[i].ReloadSpeed;
			boss_level_shots_per_wave_level = (int) boss_config_.Stages[i].ShotsPerWave;
			boss_round_environment_level = (int) boss_config_.Stages[i].Environment;

			time_to_next_group_ = store.GetUpgradablePropertyEffectByNameAsFloat ("boss_round_reload_speed", boss_level_reload_speed_level);
			enemy_group_length = (int)store.GetUpgradablePropertyEffectByNameAsFloat ("boss_round_shots_per_wave", boss_level_shots_per_wave_level);
			boss_.random_setup ().ChangeLevelParams (i, boss_life, time_to_next_group_);
			
			enviroment_list_.Add( store.GetUpgradablePropertyEffectByName ("boss_round_environment", boss_round_environment_level) );
			
			//Add shots
			List<Shot> shots = new List<Shot> ();
			Shot shot = new Shot();
			shot.level_ = i;
			shot.move_ = MoveType.random;
			
			List<EbFightAPI.BossWeaponInfo> weapons_list = boss_config_.Stages[i].Weapons;
			for (int j = 0; j < weapons_list.Count; j++) {
				string enemy_type = weapons_list[j].Kind;
				int enemy_level = (int)weapons_list[j].Level;
				shot.level_ = enemy_level;
				
				AddShots(enemy_type, shot, shots);
			}

			boss_.random_setup ().AddMultiplayerEnemyGroup (i, enemy_group_length, shots);
		}
			
		float boss_att = store.GetUpgradablePropertyEffectByNameAsFloat ("boss_att", (int)boss_config_.Attack);
		float boss_def = store.GetUpgradablePropertyEffectByNameAsFloat ("boss_def", (int)boss_config_.Defence);
		int time_bonus = 0;
		//Utilities
		for(int i = 0; i < boss_config_.Utilities.Count; i++)
		{
			if(boss_config_.Utilities[i].Kind == "boss_utilities_att")
			{
				boss_att *= 1.0F + store.GetCurrentPropertyEffectValueByName("boss_utilities_att"); 
			}
			else if(boss_config_.Utilities[i].Kind == "boss_utilities_def")
			{
				boss_def *= 1.0F + store.GetCurrentPropertyEffectValueByName("boss_utilities_def"); 
			}
			else if(boss_config_.Utilities[i].Kind == "boss_utilities_time")
			{
				time_bonus = (int)store.GetCurrentPropertyEffectValueByName("boss_utilities_time");
			}
		}
			
		boss_.UpdateParams (boss_att ,boss_def, time_bonus);
		
		
		boss_.level_setup().UpdateEnviroment(enviroment_list_);
	}
	
	private void AddShots (string enemy_type, Shot shot, List<Shot> shots)
	{
		string real_enemy_type = enemy_type;
		
		if(real_enemy_type == "laser")
		{
			shot.type_ = EnemyType.laser;
			shots.Add (shot);
			return;
		}
		else if(real_enemy_type == "rocket")
		{
			shot.type_ = EnemyType.up;
			shots.Add (shot);
			shot.type_ = EnemyType.up;
			shots.Add (shot);
			return;
		}
		else if(real_enemy_type == "fast_rocket")
		{
			shot.type_ = EnemyType.up2;
			shots.Add (shot);
			shot.type_ = EnemyType.up2;
			shots.Add (shot);
			return;
		}
		
		
		if(real_enemy_type == "bouncing")
		{
			real_enemy_type = "boucing_tap";
		}
		if(real_enemy_type == "avoid")
		{
			real_enemy_type = "tap2";
		}
		else if(real_enemy_type == "fast")
		{
			real_enemy_type = "fast_tap";
		}
		else if(real_enemy_type == "chasing")
		{
			real_enemy_type = "chasing_tap";
		}
	
		shot.type_ = (EnemyType)Enum.Parse(typeof(EnemyType), real_enemy_type + "_left"); 
		shots.Add (shot);
		shot.type_ = (EnemyType)Enum.Parse(typeof(EnemyType), real_enemy_type + "_right"); 
		shots.Add (shot);
		shot.type_ = (EnemyType)Enum.Parse(typeof(EnemyType), real_enemy_type + "_either"); 
		shots.Add (shot);
	}
}
