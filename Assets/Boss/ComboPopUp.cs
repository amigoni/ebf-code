using UnityEngine;
using System.Collections;

public class ComboPopUp : MonoBehaviour {
	
	enum ComboPopUpState
	{
		uncheck,
		valid,
		invalid
	}
		
	private tk2dSprite icon_down_sprite_;
	private tk2dSprite icon_tap_sprite_;
	private tk2dSprite icon_up_sprite_;
	private tk2dSprite icon_bouncing_sprite_;
	private tk2dSprite icon_fast_sprite_;
	private tk2dSprite icon_chasing_sprite_;
	private tk2dSprite icon_sin_tap_sprite_;
	
	private tk2dSprite icon_v_sprite_;
	private tk2dSprite icon_x_sprite_;
	
	private Animation animation_;
	 
	private ComboPopUpState popup_state_;
	private EnemyType enemy_type_;
	
	private int id_enemy_ = 0;
	
	// Use this for initialization
	void Start () {
		tk2dSprite[] sprites = GetComponentsInChildren<tk2dSprite>();
		
		foreach(tk2dSprite sprite in sprites)
		{
			if(sprite.name == "IconDownSprite")
			{
				icon_down_sprite_ = sprite;
			}
			else if(sprite.name == "IconTapSprite")
			{
				icon_tap_sprite_ = sprite;
			}
			else if(sprite.name == "IconUpSprite")
			{
				icon_up_sprite_ = sprite;
			}
			else if(sprite.name == "IconBouncingSprite")
			{
				icon_bouncing_sprite_ = sprite;
			}
			else if(sprite.name == "IconFastSprite")
			{
				icon_fast_sprite_ = sprite;
			}
			else if(sprite.name == "IconChasingSprite")
			{
				icon_chasing_sprite_ = sprite;
			}
			else if(sprite.name == "IconSinTapSprite")
			{
				icon_sin_tap_sprite_ = sprite;
			}
			else if(sprite.name == "IconVSprite")
			{
				icon_v_sprite_ = sprite;
			}
			else if(sprite.name == "IconXSprite")
			{
				icon_x_sprite_ = sprite;
			}
		}
		
		animation_ = GetComponent<Animation>();
		
		popup_state_ = ComboPopUpState.uncheck;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//behaviour
	public void Init(EnemyType enemy, int id_enemy)
	{
		enemy_type_ = enemy;
		id_enemy_ = id_enemy;
		
		Start ();
		
		icon_down_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_tap_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_up_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_bouncing_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_fast_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_chasing_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_sin_tap_sprite_.GetComponent<MeshRenderer>().enabled = false;
		
		switch(enemy)
		{
			case EnemyType.tap_left:
			case EnemyType.tap_either:
			case EnemyType.tap_right:
				icon_tap_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
			case EnemyType.up:
				icon_up_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
			case EnemyType.down:
				icon_down_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
			case EnemyType.boucing_tap_left:
			case EnemyType.boucing_tap_right:
			case EnemyType.boucing_tap_either:
				icon_bouncing_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
			case EnemyType.fast_tap_left:
			case EnemyType.fast_tap_right:
			case EnemyType.fast_tap_either:
				icon_fast_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
			case EnemyType.chasing_tap_left:
			case EnemyType.chasing_tap_right:
			case EnemyType.chasing_tap_either:
				icon_chasing_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
			case EnemyType.sin_tap_left:
			case EnemyType.sin_tap_right:
			case EnemyType.sin_tap_either:
				icon_sin_tap_sprite_.GetComponent<MeshRenderer>().enabled = true;
			break;
		}
		
		icon_v_sprite_.GetComponent<MeshRenderer>().enabled = false;
		icon_x_sprite_.GetComponent<MeshRenderer>().enabled = false;
	}
	
	public void Valid()
	{
		animation_.Play("ComboPopUpIconV");
		icon_v_sprite_.GetComponent<MeshRenderer>().enabled = true;
		
		popup_state_ = ComboPopUpState.valid;
	}
	
	public void Invalid()
	{
		animation_.Play("ComboPopUpIconX");
		icon_x_sprite_.GetComponent<MeshRenderer>().enabled = true;
		
		popup_state_ = ComboPopUpState.invalid;
	}
	
	//Getter
	public int id_enemy()
	{
		return id_enemy_;
	}
	/*
	public ComboPopUpType popup_state()
	{
		return popup_state_;
	}*/
}
