using UnityEngine;
using System.Collections;
using System.Xml;

public enum TrackingMoveType
{
	approach,
	linear,
	count
}

public enum TrackingType 
{
	none,
	time,
	match,
	count
}

public struct TrackingParam
{
	public TrackingType type_;
	public float time_;
	public TrackingMoveType move_type_;	
	//speed in normal, t in approach
	public float move_value_;
	public bool continuous_;
}

public class TrackingController : MonoBehaviour
{
	private TrackingParam param_;
	private float dt_ = 0.0F;
	
	private GameObject main_boss_;
	private BoxCollider walkable_area_;
	private GameObject player_;
	
	private float match_x_diff_ = 20.0F;
	private bool match_tracked_ = false;
	private float match_tracked_dt_ = 0.0F;
	
	private bool active_ = false;
	
	// Use this for initialization
	void Start ()
	{
		main_boss_ = GameObject.Find ("MainBoss");
		walkable_area_ = GameObject.Find ("BossWalkableArea").GetComponent<BoxCollider> ();
		player_ = GameObject.Find("Player");
	}
	
	public void Start(TrackingParam param)
	{
		param_ = param;
		dt_ = 0.0F;
		enabled = true;
		active_ = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch(param_.type_)
		{
		case TrackingType.time:
			UpdateTime();
			break;
		case TrackingType.match:
			UpdateMatch();
			break;
		}
		
		//move update
		switch(param_.move_type_)
		{
		case TrackingMoveType.approach:
			UpdateApproachTracking();
			break;
		case TrackingMoveType.linear:
			UpdateNormalTracking();
			break;
		}
		
		ClampMoving();
	}
	
	void UpdateTime()
	{
		dt_ += Time.deltaTime;
		if(dt_ > param_.time_)
		{
			active_ = false;
			if(!param_.continuous_)
				this.enabled = false;
		}
	}
	
	void UpdateMatch()
	{
		float diff = player_.transform.position.y - main_boss_.transform.position.y;
		if( match_x_diff_ > Mathf.Abs((int) diff) )
		{
			if(!match_tracked_)
			{
				match_tracked_dt_ = 0.0F;
			}
			match_tracked_ = true;
			//Debug.Log("tracking");
		}
		else
		{
			match_tracked_ = false;
			//Debug.Log("not tracking");
		}
		
		if(match_tracked_)
		{
			match_tracked_dt_ += Time.deltaTime;
			if(match_tracked_dt_ > param_.time_)
			{
				active_ = false;
				if(!param_.continuous_)
					this.enabled = false;
			}
		}
	}
	
	void UpdateNormalTracking()
	{
		//tracking
		float track_y = player_.transform.position.y - 110.0f;
		float diff_y = main_boss_.transform.position.y - track_y;
		float update_y = 0;
		if(diff_y > 3)
		{
			update_y = -(param_.move_value_ * Time.deltaTime);
		}
		else if(diff_y < -3)
		{
			update_y = (param_.move_value_ * Time.deltaTime);
		}
		
		
		//clamp
		if(update_y > Mathf.Abs(diff_y))
			update_y = diff_y;
		else if(update_y < -Mathf.Abs(diff_y))
			update_y = -diff_y;
		
		main_boss_.transform.position += new Vector3(0.0F, update_y, 0.0F);
	}
	
	void UpdateApproachTracking()
	{
		//tracking
		float track_y = player_.transform.position.y - 110.0f;
		float lerp_y = Mathf.Lerp (main_boss_.transform.position.y, track_y, param_.move_value_ * Time.deltaTime);
		main_boss_.transform.position = new Vector3 (main_boss_.transform.position.x, lerp_y, main_boss_.transform.position.z);
	}
	
	void ClampMoving()
	{
		//clamp moving
		if (main_boss_.transform.position.y > walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F) {
			main_boss_.transform.position = new Vector3 (main_boss_.transform.position.x, 
													   walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F,
													   main_boss_.transform.position.z);
		} else if (main_boss_.transform.position.y < walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F ) {
			main_boss_.transform.position = new Vector3 (main_boss_.transform.position.x, 
													   walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F,
													   main_boss_.transform.position.z);			
		}
	}
	
	//utility
	Vector3 IntVector (Vector3 v)
	{
		return new Vector3 ((int)v.x, (int)v.y, (int)v.z);
	}
	
	public bool TrackingFinished()
	{
		return active_;
	}
		
	//Load xml
	static public TrackingParam LoadParamsFromXml(XmlNode tracking_node)
	{
		TrackingParam param;
		
		string child_name = tracking_node.LocalName;
		param.type_ = (TrackingType)System.Enum.Parse (typeof(TrackingType), child_name, true);
		param.time_ = float.Parse (tracking_node.Attributes[0].Value );
		string move_type = tracking_node.Attributes[1].Value;
		param.move_type_ = (TrackingMoveType)System.Enum.Parse (typeof(TrackingMoveType), move_type, true);
		param.move_value_ = float.Parse (tracking_node.Attributes[2].Value );		
		param.continuous_ = bool.Parse (tracking_node.Attributes[3].Value );
		
		return param;
	}
}