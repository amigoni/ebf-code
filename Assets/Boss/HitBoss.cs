using UnityEngine;
using System.Collections;

public class HitBoss : MonoBehaviour {
	
	private const int hit_money_drop_ = 2;
	private int consecutive_hit_count_ = 0;
	private float hit_time_;
	
	private bool hit_by_aaatatata_ = false;
	
	public GameObject big_explosion_;
	public GameObject punch_explosion_;
	public Transform explosion_transform_;
	
	const int hit_size_y_up_ = 25;
	const int hit_size_y_down_ = 40;
	const int hit_size_y_up_big_ = 50;
	const int hit_size_y_down_big_ = 90;
	
	private Game game_;
	private Boss boss_;
	private AudioManager audio_manager_;
	private BonusManager bonus_manager_;
	private RandBonus rand_bonus_;
	private PowerPunchManager power_punch_manager_;
	private InGameTutorialManager in_game_tutorial_manager_;
	private ZOrderObject player_z_order_component_;
	private HitCount hit_count_;
	
	private PlayerData player_data_;
	private bool damage_by_player_ = true;
	
	private float defense_ = 1;
	
	// Use this for initialization
	void Start () {
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		audio_manager_= Framework.Instance.audio_manager();
		game_ = GameObject.Find("Game").GetComponent<Game>();
		bonus_manager_ = GameObject.Find("Game").GetComponent<BonusManager>();
		power_punch_manager_ = GameObject.Find("Game").GetComponent<PowerPunchManager>();
		in_game_tutorial_manager_ = GameObject.Find("Game").GetComponent<InGameTutorialManager>();
		player_z_order_component_ = GameObject.Find("Player").GetComponent<ZOrderObject> ();
		hit_count_ = GameObject.Find("HitCountContainer").GetComponent<HitCount>();
		hit_count_.gameObject.SetActive(false);
		
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
				
		defense_ = boss_.defense();
	}
	
	
	// Update is called once per frame
	void Update () {
		ClearHitCount();
	}
	
	void OnTriggerStay(Collider other) 
	{
		if(other.name.Contains("BouncingTapEnemy") ||
			other.name.Contains("Ball") )
			OnTriggerEnter(other);
	}
	
	void OnTriggerEnter(Collider other) 
	{
		//bonus_manager_.SpawnBonus(BonusType.cleaner_explosion);
		if(!damage_by_player_ && other.name == "AttackSprite" )
		{
			return;
		}
		if(other.tag == "BossDamage")
		{
			//bonus_manager_.SpawnBonus(BonusType.magnetics);
			float other_baseline;
			if(other.name.Contains("AttackSprite") || other.name.Contains("PowerPunchCollider"))
				other_baseline = player_z_order_component_.GetBaseline ();
			else	
				other_baseline = other.GetComponent<ZOrderObject> ().GetBaseline ();
			float boss_baseline = GetComponent<ZOrderObject> ().GetBaseline ();
			
			float hit_size_y_up = 0.0f;
			float hit_size_y_down = 0.0f;
			
			if(other.name.Contains("Ball"))
			{
				hit_size_y_up = hit_size_y_up_big_;
				hit_size_y_down = hit_size_y_down_big_;
			}
			else
			{
				hit_size_y_up = hit_size_y_up_;
				hit_size_y_down = hit_size_y_down_;
			}	
			
			if ((boss_baseline > other_baseline - hit_size_y_up) && 
				(boss_baseline < other_baseline + hit_size_y_down)) 
			{
				//Mission Manager
				MissionsManager.MissionType mission_type = MissionsManager.MissionType.hit_boss; 
				
				//Debug.Log(other.name);
				if (other.name.Contains("BouncingTapEnemy"))	
					mission_type = MissionsManager.MissionType.hit_boss_bouncing;
				else if (other.name.Contains("Ball"))
					mission_type = MissionsManager.MissionType.hit_boss_pp1;
				else if (other.name.Contains("PowerPunch"))	
				{
					if (power_punch_manager_.current_power_punch().type_ == PowerPunchType.a_ta_ta_ta && hit_by_aaatatata_ == false)
					{
						mission_type = MissionsManager.MissionType.hit_boss_pp2;
						hit_by_aaatatata_ = true;
					}		
					else if (power_punch_manager_.current_power_punch().type_ == PowerPunchType.huge_punch)
					{
						mission_type = MissionsManager.MissionType.hit_boss_pp3;
					}	
				}
				else //DD
				{
					Framework.Instance.InstanceComponent<Analitycs>().GetCurrentRound().boss_punches += 1;
					
					//Tutorial
					if (player_data_.GetBool("tutorial_in_game_done") == false)
					{
						in_game_tutorial_manager_.HitBoss();
					}		
				}
				
				//Debug.Log("Mission Type: "+mission_type);
				game_.mission_manager().UpdateMission(mission_type, 1);
				
				game_.AddScore(Game.ScoreValue.hit_boss);
				
				//crates
				if(consecutive_hit_count_ % hit_money_drop_ == 0 && other.name.Contains("BallOfFire") == false && other.name.Contains("BouncingTapEnemy") == false)
				{
					bonus_manager_.SpawnOnHitBoss();
				}
				
				if( other.name.Contains("BallOfFire") || other.name.Contains("BouncingTapEnemy"))
				{
					SpawnBigExplosion();
					
					if(other.name.Contains("BouncingTapEnemy"))
					{
						PoolManager.Despawn(other.gameObject);
					}
					else
						Destroy(other.gameObject);
				}
				else
				{
					//Consecutive Hit Count increase
					consecutive_hit_count_++;
				
					UpdateHitCountPopUp( consecutive_hit_count_ );
					hit_time_ = Time.time;
				}
				
				boss_.AddDamage(other.gameObject.GetComponent<EnemyInfo>().damage() / defense_);
				boss_.PlayHitAnimation();
				//Analitycs
				Framework.Instance.InstanceComponent<Analitycs>().GetCurrentRound().damage_dealt += (other.gameObject.GetComponent<EnemyInfo>().damage() / defense_);
				
				//PowerPunch
				if(other.name == "PowerPunchCollider" && 
				   power_punch_manager_.current_power_punch().type_ == PowerPunchType.huge_punch)
				{
					boss_.StunFromPowerPunch(
						((HugePunch) power_punch_manager_.current_power_punch()).stun_time()
					);
					
					//Tutorial
					if (player_data_.GetBool("tutorial_in_game_done") == false)
						in_game_tutorial_manager_.HitWithPowerPunch();
				}
				//Add xp
				game_.AddXp( other.GetComponent<EnemyInfo>().xp () );
				
				//Audio 
				string sound_name = "10_Boss is Hits By Hero Punch 1v";
				
				if (power_punch_manager_.IsHugePunchActive())
					sound_name = "28_Huge Punch Hits the Boss";
				else {
					float random = Random.Range(0,2);
				
					if (random == 0)
						sound_name = "10_Boss is Hits By Hero Punch 1v";
					else if (random == 1)
						sound_name = "10_Boss is Hits By Hero Punch 2v";
				}	
			
				audio_manager_.PlayOnPlayer(sound_name, 1.0F, 1.0F, false);
			}
		}
	}
	
	void OnTriggerExit(Collider other){
		if (other.name.Contains("PowerPunch"))		
			hit_by_aaatatata_ = false;
	}
	
	Vector3 GetRandomPosition()
	{
		return new Vector3((int)(20.0F + Random.value * 150.0F), 400.0F, 0.0F); 
	}
	
	void UpdateHitCountPopUp (int hitCount){
		if (hitCount == 1){
			hit_count_.Init();
		}
		
		hit_count_.UpdateCount(hitCount);
	}
	
	
	void ClearHitCount(){
		
		if(hit_count_.gameObject.activeInHierarchy)
		{
			//Clear Hit Count after 1.0 seconds of not hitting.
			if(Time.time - hit_time_ > 1.0F && consecutive_hit_count_ != 0){
				consecutive_hit_count_ = 0;
	        	
				hit_count_.PlayExit();
			}
		}
	}
	
	void SpawnBigExplosion()
	{
		//spawn explosion
		GameObject obj = PoolManager.Spawn(big_explosion_);
		obj.transform.position = new Vector3(explosion_transform_.position.x + Random.value * 50.0F,
											 explosion_transform_.position.y + Random.value * 50.0F,
											 this.transform.position.z - 150.0F); 
	}
	
	void SpawnPunchExplosion()
	{
		//spawn explosion
		GameObject obj = PoolManager.Spawn(punch_explosion_);
		obj.transform.position = new Vector3(explosion_transform_.position.x + Random.value * 50.0F,
											 explosion_transform_.position.y + Random.value * 50.0F,
											 this.transform.position.z - 150.0F);
	}
	//getter / setter
	public BonusManager bonus_manager()
	{
		return bonus_manager_;
	}
	
	public int consecutive_hit_count()
	{
		return consecutive_hit_count_;
	}
	
	public bool damage_by_player()
	{
		return damage_by_player_;
	}
	
	public void set_damage_by_player(bool b)
	{
		damage_by_player_ = b;
	}
	
	public void EndPowerPunch()
	{
		hit_by_aaatatata_ = false;
	}
}
