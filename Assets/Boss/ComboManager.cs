using UnityEngine;
using System.Collections;
using System.Collections.Generic;

struct Combo
{
	public int id_group_;
	public List<GameObject> icons_;
	public int count_;
}

public class ComboManager : MonoBehaviour {
	
	private AudioManager audio_manager_;
	
	public GameObject background_combo_prefab_;
	public GameObject icon_combo_prefab_;
	public Animation animation_;
	
	private GameObject background_;

	private Combo current_combo_;
	private bool active_ = false;
	private int spawned_enemy_ = 0;
	
	private const float x_spacing_ = 52.0F;
	
	private Game game_;
	
	private bool icon_destroyed_ = false;
	
	// Use this for initialization
	void Awake () {
		game_ = GameObject.Find("Game").GetComponent<Game>();
		animation_ = GetComponent<Animation>();
		
		audio_manager_= Framework.Instance.audio_manager();
	}
	
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void NewCombo(int id_group, List<Shot> shot_list, int star_enemy_id)
	{
		active_ = true;
		icon_destroyed_ = false;
		current_combo_.id_group_ = id_group;
		current_combo_.icons_ = new List<GameObject>();
		current_combo_.count_ = shot_list.Count;
		spawned_enemy_ = current_combo_.count_;
		
		//Spawn background
		background_ = (GameObject)Instantiate(background_combo_prefab_);
		
		//copy array
		for(int i = 0; i < shot_list.Count; i++)
		{
			current_combo_.icons_.Add( Instantiate(icon_combo_prefab_) as GameObject);
			current_combo_.icons_[i].GetComponent<ComboPopUp>().Init( shot_list[i].type_, star_enemy_id + i + 1 );
			current_combo_.icons_[i].transform.Translate(new Vector2(x_spacing_ * i, 0.0F));
			current_combo_.icons_[i].transform.parent = GameObject.Find("ComboPopUpBackgroundSprite").transform; 
		}
		
		game_.gui().PlayExitingScore();
	}
	
	public void CheckComboValue(int id_group, int enemy_id)
	{
		if(active_ && !icon_destroyed_)
		{
			if(current_combo_.id_group_ == id_group)
			{
				for(int i = 0; i < current_combo_.icons_.Count; i++)
				{
					if(current_combo_.icons_[i].GetComponent<ComboPopUp>().id_enemy() == enemy_id)
					{
						//Play audio6
						string n_string = (5-current_combo_.count_).ToString();
						if(n_string == "0")
							n_string = "1";
						audio_manager_.Play(
							"16_Combo Hit MIssile " + n_string + "v", 
							background_.transform, 
							1.0F,
							false);
						
						current_combo_.icons_[i].GetComponent<ComboPopUp>().Valid();
						current_combo_.count_--;
						break;
					}
				}
	
				if(current_combo_.count_ == 0)
				{
					//Tutorial
					if (Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done")== false)
						GameObject.Find("Game").GetComponent<InGameTutorialManager>().CompletedCombo();
					
					//Play win animation and destroy
					background_.GetComponent<Animation>().Play("ComboPopUpWin");
					audio_manager_.Play("13_Combo Success", background_.transform, 1.0F, false);
					Invoke("DestroyCallback", 1.0F);
					icon_destroyed_ = true;
					Invoke("DestroyIcons", 0.3F);
					game_.AddScore(Game.ScoreValue.combo);
					
					//GameObject.Find ("Game").GetComponent<BonusManager>().SpawnWhiteCoins();
					//reward
					//GameObject.Find ("Game").GetComponent<BonusManager>().SpawnRandomCrate();
					//for(int i = 0; i < current_combo_.icons_ .Count; i++)
					for(int i = 0; i < 15; i++)
						GameObject.Find ("Game").GetComponent<BonusManager>().SpawnBonus(BonusType.money);
					
					GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.combo_complete, 1);
				}
			}
		}
	}
	
	public void InvalidateCombo(int id_group, int enemy_id)
	{
		if(active_ && !icon_destroyed_)
		{
			if(current_combo_.id_group_ == id_group)
			{
				for(int i = 0; i < current_combo_.icons_.Count; i++)
				{
					if(current_combo_.icons_[i].GetComponent<ComboPopUp>().id_enemy() == enemy_id)
					{
						current_combo_.icons_[i].GetComponent<ComboPopUp>().Invalid();	
						break;
					}
				}
				background_.GetComponent<Animation>().Play("ComboPopUpLoose");
				audio_manager_.Play("12_Combo Fail", background_.transform, 1.0F, false);
				Invoke("DestroyCallback", 1.0F);
				icon_destroyed_ = true;
				Invoke("DestroyIcons", 0.3F);
			}
		}
	}
	
	//Destroycallback
	public void DestroyCallback()
	{
		if(active_)
		{
			Destroy(background_);
			active_ = false;
			game_.gui().PlayEnteringScore();
			
			if(!icon_destroyed_)
				DestroyIcons();
		}
	}
	
	public void DestroyIcons()
	{
		for(int i = 0; i < current_combo_.icons_.Count; i++)
		{
			Destroy(current_combo_.icons_[i].gameObject);
		}
	}
	
	//Getter
	public bool ThereIsOneCombo()
	{
		return active_;
	}
	
	public void DecreaseEnemySpawn()
	{
		spawned_enemy_ --;
	}
	
	public int spawned_enemy()
	{
		return spawned_enemy_;
	}
	
	public bool EnemyIsACombo(int enemy_id)
	{
		if(active_ && !icon_destroyed_)
		{
			for(int i = 0; i < current_combo_.icons_.Count; i++)
			{
				if(current_combo_.icons_[i].GetComponent<ComboPopUp>().id_enemy() == enemy_id)
				{
					return true;
				}
			}
		}
		return false;
	}
}
