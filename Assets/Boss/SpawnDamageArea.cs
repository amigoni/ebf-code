using UnityEngine;
using System.Collections;

public class SpawnDamageArea : MonoBehaviour {
	
	private Collider collider_;
	private const float duration_ = 0.5F;
	
	// Use this for initialization
	void Start () {
		collider_ = GetComponent<Collider>();
		
		GetComponent<ZOrderObject> ().Init((int)(- 35), false);
		GetComponent<EnemyInfo>().set_damage(3);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Spawn()
	{
		//collider_.enabled = true;
		
		Invoke("EndSpawn", duration_);
	}
	
	void EndSpawn()
	{
		collider_.enabled = false;
	}
}
