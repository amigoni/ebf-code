using UnityEngine;
using System.Collections;

public class BossSoundController : MonoBehaviour {
	
	private AudioManager audio_manager_;
	private AudioSource chain_saw_;
	
	private AudioSource laser_audio_source_ = null;
	private AudioSource	stunned_audio_source_ = null;
	
	// Use this for initialization
	void Start () {
		audio_manager_= Framework.Instance.audio_manager();
	}
	
	public void BossEnterSound(){
		audio_manager_.Play("2_Boss Entering", this.transform, 1.0F, 1.0F, false);
	}
	
	public void BossExitSound(){
		audio_manager_.Play("3_Boss Retriving", this.transform, 1.0F, 1.0F, false);
	}
	
	public void BossSnapSound(){
		audio_manager_.Play("4_Boss Recovers and Charges", this.transform, 1.0F, 1.0F, false);
	}
	
	public void BossShootsMissile(){
		audio_manager_.Play("33_Missile Launch", this.transform, 0.5F, 1.0F, false);
	}
	
	public void BossStun(){
		audio_manager_.Play("7_Boss Hits Stump", this.transform, 1.0F, 1.0F, false);
	}
	
	public void BossIsStunned(){
		if(stunned_audio_source_ == null)
			stunned_audio_source_ = audio_manager_.Play("8_Boss is stuned", this.transform, 1.0F, 1.0F, true);
	}
	
	public void BossIsStunnedExit(){
		if(stunned_audio_source_ != null)
			audio_manager_.Stop( stunned_audio_source_ );
		stunned_audio_source_ = null;
	}
	
	public void BossHasChainsaw(){
		chain_saw_ = audio_manager_.Play("6_Boss Has Chainsaw", this.transform, 1.0F, 1.0F, true);
	}
	
	public void BossHasChainsawExit(){
		audio_manager_.Stop(chain_saw_);
	}
	
	public void BossHasLaser()
	{
		laser_audio_source_ = audio_manager_.Play("9_Boss Laser Souns", this.transform, 1.0F, 1.0F, true);
	}
	
	public void BossHasLaserExit()
	{
		if(laser_audio_source_ != null)
			audio_manager_.Stop( laser_audio_source_ );
		laser_audio_source_ = null;
	}
}
