using UnityEngine;

public sealed class WaitingEndState :  FSMState<Boss> {
	
	static readonly WaitingEndState instance = new WaitingEndState();
	public static WaitingEndState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static WaitingEndState() 
	{
		
	}
	
	private WaitingEndState()
	{
	
	}
		
	//Variables
	private PatternSetup pattern_setup_;
	private float duration_ = 0.0F;
	
	public void Init()
	{
		pattern_setup_ = GameObject.Find("BossPrefab").GetComponent<Boss>().pattern_setup();
	}
	
	public void set_duration(float duration)
	{
		duration_ = duration;
	}
	
	
	public override void Enter(Boss b) 
	{
		
	}
	
	public override void Execute(Boss b) 
	{
		duration_ -= Time.deltaTime;
		pattern_setup_.UpdateClock( duration_ );
			
		if(!pattern_setup_.ThereAreEnemyInScene())
		{
			if(duration_ > 0)
			{
				//TO-DO remove a main pattern node
				pattern_setup_.SpawnPattern();
			}
			else
			{
				b.ForceChangeSetup();
			}
		}
	}
	
	public override void Exit(Boss b) 
	{
		
	}
}