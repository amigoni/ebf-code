using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public struct Shot
{
	public EnemyType type_;
	public int level_;
	public MoveType move_;
}

public struct EnemyGroupClass
{
	public int level_number_; //primary key
	public int id_;
	public List<Shot> enemy_list_;
	public bool is_combo_;
	public bool is_random_;
	public int length_;
}

public struct RandomLevelClass
{
	public int number_; //primary key
	public float time_to_shot_;
	public float time_to_next_group_;
	public int time_left_;
	public EnemyParams enemy_params_;
	public float boss_life_;	
	public List<TrackingParam> tracking_param_;
	public bool indestructible_during_group_;
	public bool push_player_during_shot_;
	public float push_damage_;
	public MolesManagerParams moles_manager_params_;
}

public class RandomSetup : BossSetup
{	
	private bool die_at_end_of_time_;
	private List<EnemyGroupClass> group_enemy_list_ = new List<EnemyGroupClass> ();
	private List<RandomLevelClass> level_list_ = new List<RandomLevelClass> ();
	private int current_enemy_group_ = 0;
	private int current_enemy_ = 0;
	
	private int current_group_id_;
	private float first_move_time_;
	private float second_move_time_;
	private float third_move_time_;
	
	private Shot last_shot_;

	//Random 
	private int prev_group_id_ = -1;
	
	private const float const_move_value = 20;
	
	private bool can_stun_ = false;
	
	//Init 
	public override void InitSetup ()
	{
		name_ = "RandomSetup";
		DeterminesGroupState.Instance.Init ();
		SpawnEnemyState.Instance.Init ();
	}
	
	public override void LoadFromXml ()
	{
		string xml_text = "";
		Game.GameMode game_mode = game_.game_mode();
		if(game_mode == Game.GameMode.multiplayer || game_mode == Game.GameMode.boss_training)
		{
			xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/BuildTheBossBase/RandomSetup");
		}
		else
		{
			if (Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done") == false)
				xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/Tutorial/RandomSetup");
			else
				xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/RandomSetup");
		}
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;

		XmlNodeList nodes = xml_doc.DocumentElement.ChildNodes;

		//read enemy default config
		XmlNodeList default_params_node = nodes [0].ChildNodes;
		EnemyParams default_enemy_params_ = EnemyParams.LoadFromXml(default_params_node);		
		
		//read levels
		die_at_end_of_time_ = bool.Parse (nodes [1].Attributes ["die_at_end_of_time"].Value);
		XmlNodeList speed_nodes = nodes [1].ChildNodes;
		for (int i = 0; i < speed_nodes.Count; ++i) {
			RandomLevelClass level;
			level.number_ = int.Parse (speed_nodes [i].Attributes ["number"].Value);
			level.time_to_shot_ = float.Parse (speed_nodes [i].Attributes ["time_to_shot"].Value);
			level.time_to_next_group_ = float.Parse (speed_nodes [i].Attributes ["time_to_next_group"].Value);
			level.boss_life_ = float.Parse (speed_nodes [i].Attributes ["boss_life"].Value);
			level.time_left_ = int.Parse (speed_nodes [i].Attributes ["time_left"].Value);
			level.indestructible_during_group_ = bool.Parse (speed_nodes [i].Attributes ["indestructible_during_group"].Value);
			level.push_player_during_shot_ = bool.Parse (speed_nodes [i].Attributes [6].Value);
			level.push_damage_ = float.Parse (speed_nodes [i].Attributes [7].Value);
			
			//Load tracking type
			XmlNodeList tracking_type_nodes = speed_nodes [i].ChildNodes[0].ChildNodes;
			level.tracking_param_ = new List<TrackingParam>();
			for (int j = 0; j < tracking_type_nodes.Count; ++j) 
			{
				level.tracking_param_.Add(TrackingController.LoadParamsFromXml( tracking_type_nodes[j]) );
			}
				
			//set params
			if(speed_nodes [i].ChildNodes.Count >= 2)
			{
				XmlNodeList params_node = speed_nodes [i].ChildNodes[1].ChildNodes;
				level.enemy_params_ = EnemyParams.LoadFromXml(params_node, default_enemy_params_);		
			}
			else
				level.enemy_params_ = default_enemy_params_;
			
			//Load moles manager
			level.moles_manager_params_ = MolesManager.LoadParamsFromXml(speed_nodes [i].ChildNodes[2]);
			
			level_list_.Add (level);
		}
		
		//load enemy
		XmlNodeList enemy_group_nodes = nodes [2].ChildNodes;
		for (int j = 0; j < enemy_group_nodes.Count; ++j) {
			EnemyGroupClass group_enemy;
			group_enemy.id_ = j;
			group_enemy.level_number_ = int.Parse (enemy_group_nodes [j].Attributes [0].Value);
			group_enemy.is_combo_ = bool.Parse(enemy_group_nodes [j].Attributes [1].Value.ToString());
			group_enemy.is_random_ = bool.Parse(enemy_group_nodes [j].Attributes [2].Value.ToString());
			if(group_enemy.is_random_ == true)
				group_enemy.length_ = int.Parse(enemy_group_nodes [j].Attributes [3].Value.ToString());
			else
				group_enemy.length_ = 0;
			
			XmlNodeList gesture_nodes = enemy_group_nodes [j].ChildNodes;
			group_enemy.enemy_list_ = new List<Shot> ();
			Shot shot;
			for (int i = 0; i < gesture_nodes.Count; ++i) {
				string child_name = gesture_nodes [i].LocalName;
				if (child_name != "#comment") {
					shot = new Shot();

					string attribute_name = gesture_nodes [i].Attributes [0].Value;
					shot.type_ = (EnemyType)System.Enum.Parse (typeof(EnemyType), attribute_name, true);
					shot.move_ = (MoveType)System.Enum.Parse (typeof(MoveType), gesture_nodes [i].Attributes [1].Value, true);
					
					if(gesture_nodes [i].Attributes.Count > 2)
						shot.level_ = int.Parse(gesture_nodes [i].Attributes [2].Value);
					else
						shot.level_ = 0;
					
					group_enemy.enemy_list_.Add( shot );
				}
			}
			
			group_enemy_list_.Add (group_enemy);
		}
	}
	
	public override void Intro ()
	{		
		boss_.EnabledHitMessage();
		boss_.DestroyUnspawnedEnemy();
		
		boss_.ChangeState (IdleState.Instance);
		boss_.Play ("BossEntering");
		
		game_.SetTimeLeft(level_list_ [game_.level ()].time_left_ );
		
		if ((Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done") == true) ||
			(GameObject.Find("Game").GetComponent<InGameTutorialManager>().tutorial_step() != 0))
		{
			//New enemy group
			AddNewEnemyGroup();
			Shot next_shot = GetNextShot();
			boss_.AppendNextEnemy(next_shot.type_, next_shot.level_);
		}
		
		//Moles Manager
		moles_manager_.ChangeParams( level_list_ [game_.level ()].moles_manager_params_ );
	}
	
	
	public override void Outro ()
	{			
		boss_.DisableHitMessage();
		boss_.Play ("BossExit");
		boss_.EndState();
			
		game_.StopTimeLeft();
		
		//Moles Manager
		moles_manager_.StopSpawning ();
		
		can_stun_ = false;
	}
	
	public override void Start ()
	{
		start_ = true;
		can_stun_ = true;
		
		if (Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done") == false)
		{
			int tutorial_step = GameObject.Find("Game").GetComponent<InGameTutorialManager>().tutorial_step();
			if (tutorial_step <= 2)
			{
				boss_.PlayPassiveIdle ();
				boss_.ChangeState (IdleState.Instance);	
			}	
			else
				boss_.ChangeState (IntroState.Instance);	
			
		}
		else
		{
			boss_.ChangeState (IntroState.Instance);	
			game_.GoTimeLeft();
		}
	}
	
	public override void End ()
	{
		start_ = false;
	}
	
	public override void EndShot ()
	{
		if(level_list_[game_.level()].push_player_during_shot_)
		{
			if(last_shot_.type_.ToString().Contains("tap"))
				boss_.DamagesPlayer(level_list_[game_.level()].push_damage_);
		}
		
		boss_.BossInvoke("RealEndShot", 0.3F);
	}
	
	public override void RealEndShot()
	{
		EnemyGroupClass en_class = group_enemy_list_ [current_enemy_group_];
		if ( (!en_class.is_random_ && (current_enemy_ < en_class.enemy_list_.Count - 1)) ||
		     (en_class.is_random_ && (current_enemy_ < en_class.length_ - 1) )				)
		{			
			current_enemy_++;
			
			MoveState.Instance.Init ((level_list_ [game_.level ()]).time_to_shot_);
			
			Shot shot = GetNextShot();
			boss_.AppendNextEnemy(shot.type_, shot.level_);
				
			//move
			MoveType move_type = shot.move_;
			float move_time = (level_list_ [game_.level ()]).time_to_shot_;
			NextMove(move_type, move_time);
			
			boss_.ChangeState (MoveState.Instance);
			
			boss_.PlayIdle ();
		} else {
			if(level_list_[game_.level()].indestructible_during_group_)
				boss_.EnableBossDamage();			
			AddNewEnemyGroup();	
			Shot shot = GetNextShot();
			boss_.AppendNextEnemy(shot.type_, shot.level_);
			
			DeterminesGroupState.Instance.set_time_to_next_group ((level_list_ [game_.level ()]).time_to_next_group_);
			boss_.ChangeState (DeterminesGroupState.Instance);
				
			boss_.PlayAngryExitAnimation ();	
			
			//move
			MoveType move_type = shot.move_;
			float move_time = (level_list_ [game_.level ()]).time_to_next_group_;
			NextMove(move_type, move_time);
		}
		
	}
	
	public void NextMove(MoveType type, float time)
	{
		//Decide next 
		MoveType move_type = type;
		
		//Randomize it
		
		if(type == MoveType.random)
		{
			move_type = (MoveType)Random.Range(0, (int)MoveType.count);
		}
		
		float offset = 0;
		
		if(Screen.width == 1024 || Screen.width == 2048)
		{
			offset = 30;
		}
		
		switch (move_type) {
		case MoveType.move1:
			boss_.Move (time, 30.0f + offset, true);
			break;
		case MoveType.move2:
			boss_.Move (time, 40.0F + offset, true);
			break;
		case MoveType.move3:
			boss_.Move (time, 50.0F + offset, true);
			break;
		case MoveType.move4:
			boss_.Move (time, 60.0F + offset, true);
			break;
		case MoveType.move5:
			boss_.Move (time, 70.0F + offset, true);
			break;
		case MoveType.move6:
			boss_.Move (time, 80.0F + offset, true);
			break;	
		case MoveType.stay:
			break;
		case MoveType.move:
			boss_.RandomMove (time);
			break;
		case MoveType.moveUp:
			boss_.Move (time, const_move_value, false);
			break;
		case MoveType.moveDown:
			boss_.Move (time, -const_move_value, false);
			break;
		case MoveType.tracking:
			int rnd_param = Random.Range(0, level_list_[game_.level()].tracking_param_.Count);
			TrackingParam tracking_param = level_list_[game_.level()].tracking_param_ [rnd_param];
			tracking_param.time_ = time;
			boss_.EnableTracking (tracking_param);
			break;
		}
		//End move
	}
	
	public override float GetCurrentParam(EnemyType type, int param_level,string param_name)
	{
		return (level_list_ [game_.level ()]).enemy_params_.GetParam(type, param_level, param_name);
	}
	
	public override int GetGroupCount()
	{
		return group_enemy_list_ [current_enemy_group_].enemy_list_.Count;
	}

	public override bool CurrentGroupIsCombo()
	{
		return group_enemy_list_ [current_enemy_group_].is_combo_ || boss_.ThereIsOneCombo();
	}
	
	public override bool IsFinished()
	{
		return boss_.life() <= 0.0F;
	}
	
	public override float GetBossLife()
	{
		return level_list_[game_.level()].boss_life_;
	}
	
	public override bool CanStun()
	{
		return can_stun_;
	}
	
	public override void PrevPowerPunchStun()
	{
		game_.DestroyAllEnemyOnBoss();
		boss_.BossCancelInvoke("RealEndShot");
		if(last_shot_.type_ == EnemyType.laser)
		{
			boss_.BlendAnimation("LaserAnimationExit");
			boss_.EndLaser ();
		}
	}
	
	public override void EndPowerPunchStun()
	{
		if ((Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done") == false) &&
		    (GameObject.Find("Game").GetComponent<InGameTutorialManager>().tutorial_step() == 3))
		{
			//New enemy group
			AddNewEnemyGroup();
			Shot next_shot = GetNextShot();
			boss_.AppendNextEnemy(next_shot.type_, next_shot.level_);
			
			StartAttack();
		}
		else
			RealEndShot();
	}
	
	public override void EndFreeze()
	{
		
	}
	
	//End 
	public void StartAttack()
	{
		MoveState.Instance.Init ((level_list_ [game_.level ()]).time_to_shot_);
		boss_.ChangeState (MoveState.Instance);
		EnemyGroupClass en_class = group_enemy_list_ [current_enemy_group_];
		MoveType move_type = en_class.enemy_list_ [current_enemy_].move_;
		float move_time = (level_list_ [game_.level ()]).time_to_shot_;
		NextMove(move_type, move_time);
		
		boss_.PlayIdle ();
	}
	
	Shot GetNextShot()
	{
		EnemyGroupClass enemy_class = group_enemy_list_ [current_enemy_group_];
		
		if(!enemy_class.is_random_)
		{
			last_shot_ = enemy_class.enemy_list_ [current_enemy_];
			return last_shot_;
		}
		
		int random_id = Random.Range(0, enemy_class.enemy_list_.Count);
		last_shot_ = enemy_class.enemy_list_ [random_id];
		return last_shot_;
	}
	
	public void SpawnEnemy ()
	{
		boss_.SpawnEnemyAnimation (last_shot_.type_, (level_list_ [game_.level ()]).time_to_shot_);	
		
		if(level_list_[game_.level()].indestructible_during_group_)
			boss_.DisableBossDamage();
	}
	
	public List<EnemyGroupClass> group_enemy_list ()
	{
		return group_enemy_list_;
	}
	
	public void AddNewEnemyGroup ()
	{		
		current_enemy_group_ = GetRandomLevelGroup ();
		
		current_enemy_ = 0;
		
		current_group_id_++;
		boss_.set_current_group_id(current_group_id_);
		boss_.UpdateComboColor ();
		
		if (group_enemy_list_ [current_enemy_group_].is_combo_)
			boss_.combo_manager ().NewCombo (
				current_group_id_, 
				group_enemy_list_ [current_enemy_group_].enemy_list_, 
				boss_.spawned_enemy ());
	}
	
	public int GetRandomLevelGroup ()
	{
		Game.GameMode game_mode = game_.game_mode();
		if(game_mode == Game.GameMode.multiplayer || game_mode == Game.GameMode.boss_training)
		{
			int level = game_.level ();
			for (int i = 0; i < group_enemy_list_.Count; i++)
			{
				if (group_enemy_list_ [i].level_number_ == level)
					return group_enemy_list_[i].id_;	
			}
		}
		else
		{
			int level = game_.level ();
			ArrayList enemy_level = new ArrayList ();			
			for (int i = 0; i < group_enemy_list_.Count; i++) {	
				if (boss_.ThereIsOneCombo ()) {
					if ((group_enemy_list_ [i].level_number_ == level ||  group_enemy_list_ [i].level_number_ == -1) && 
						group_enemy_list_ [i].is_combo_ == false && prev_group_id_ != group_enemy_list_ [i].id_)
						enemy_level.Add (group_enemy_list_ [i]);	
				} else if ((group_enemy_list_ [i].level_number_ == level ||  group_enemy_list_ [i].level_number_ == -1) &&
							prev_group_id_ != group_enemy_list_ [i].id_)
					enemy_level.Add (group_enemy_list_ [i]);
			}
			if(enemy_level.Count == 0)
				Debug.Log("There aren't group to spawn");
			int r = Mathf.RoundToInt (Random.value * (enemy_level.Count - 1));
			int rand_id_ = ( (EnemyGroupClass)(enemy_level[r] )).id_;
			for(int i = 0; i < group_enemy_list_.Count; i++)
			{
				if(rand_id_ == group_enemy_list_[i].id_)
				{
					prev_group_id_ = group_enemy_list_[i].id_;
					return i;
				}
			}
		}
		
		return -1;
	}

	//Multiplayer
	public void ChangeLevelParams(int id_level, float boss_life, float time_to_next_group_)
	{
		//perche faccio loadMultiplayer prima di tutto
		RandomLevelClass random_level_class = level_list_[id_level];
		random_level_class.boss_life_ = boss_life;
		random_level_class.time_to_next_group_ = time_to_next_group_;
		
		level_list_.Remove(level_list_[id_level]);
		level_list_.Insert(id_level, random_level_class);
	}
		
	
	public void AddMultiplayerEnemyGroup(int id_level, int length_, List<Shot> shots)
	{
		EnemyGroupClass group_enemy;
		
		group_enemy.level_number_ = id_level;
		group_enemy.length_ = length_;

		group_enemy.id_ = group_enemy_list_.Count;
		group_enemy.is_combo_ = false;
		group_enemy.is_random_ = true;
		
		group_enemy.enemy_list_ = shots;
			
		group_enemy_list_.Add (group_enemy);
	}
}
