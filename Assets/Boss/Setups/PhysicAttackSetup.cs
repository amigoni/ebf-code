using UnityEngine;
using System.Collections.Generic;
using System.Xml;

public enum PhysicAttackType
{
	front_attack,
	chainsaw,
	laser,
	none
};

public struct PhysicsAttackLevelClass
{
	public int level_number_; //primary key
	public List<TrackingParam> tracking_param_;
	public float attack_speed_;
	public float charge_speed_;
	public float stun_time_;
	public float boss_life_;
	public int time_left_;
	public float boss_attack_;
	public float return_time_;
	public MolesManagerParams moles_manager_params_;
};


public class PhysicAttackSetup: BossSetup
{
	private List<PhysicsAttackLevelClass> level_list_ = new List<PhysicsAttackLevelClass> ();
	private PhysicAttackType type_ = PhysicAttackType.none;
	private EnemyParams default_enemy_params_;
	private bool need_stump_ = true;
	
	//Init base class
	public override void InitSetup()
	{
		name_ = "PhysicAttackSetup";
		
		game_ = GameObject.Find("Game").GetComponent<Game>();
		
		ChargeState.Instance.Init();
		ChainsawState.Instance.Init();
		TrackingState.Instance.Init();
	}
	
	public override void LoadFromXml()
	{
		string xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/PhysicsAttackSetup");
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;
		
		XmlNodeList nodes = xml_doc.DocumentElement.ChildNodes;
		
		//read enemy default config
		XmlNodeList default_params_node = nodes [0].ChildNodes;
		default_enemy_params_ = EnemyParams.LoadFromXml(default_params_node);
		
		//load level
		XmlNodeList speed_nodes = nodes [1].ChildNodes;
		for (int i = 0; i < speed_nodes.Count; ++i) {
			PhysicsAttackLevelClass level;
			level.tracking_param_ = new List<TrackingParam>();
			level.level_number_ = int.Parse (speed_nodes [i].Attributes [0].Value);
			level.stun_time_ = float.Parse (speed_nodes [i].Attributes [1].Value);
			level.charge_speed_ = float.Parse (speed_nodes [i].Attributes [2].Value);
			level.attack_speed_ = float.Parse (speed_nodes [i].Attributes [3].Value);
			level.boss_life_ = float.Parse (speed_nodes [i].Attributes [4].Value);
			level.time_left_ = int.Parse (speed_nodes [i].Attributes [5].Value);
			level.boss_attack_ = float.Parse (speed_nodes [i].Attributes ["boss_attack"].Value);
			level.return_time_ = float.Parse (speed_nodes [i].Attributes ["return_time"].Value);
			
			//load tracking type
			XmlNodeList tracking_type_nodes = speed_nodes [i].ChildNodes;
			XmlNodeList tracking_nodes = tracking_type_nodes [0].ChildNodes;
			for (int j = 0; j < tracking_nodes.Count; ++j) 
			{
				level.tracking_param_.Add(TrackingController.LoadParamsFromXml( tracking_nodes[j]) );
			}
			
			//Load moles manager
			level.moles_manager_params_ = MolesManager.LoadParamsFromXml(speed_nodes [i].ChildNodes[1]);
			
			level_list_.Add (level);
		}
	}

	public override void Intro ()
	{
		boss_.EnabledHitMessage();
		boss_.ChangeState (IdleState.Instance);
		boss_.Play ("BossEntering");
		
		boss_.InstantiateStump();
		need_stump_ = false;
		
		GameObject.Find("DamageArea").GetComponent<EnemyInfo>().set_damage(level_list_ [game_.level ()].boss_attack_);
		
		game_.SetTimeLeft(level_list_ [game_.level ()].time_left_);
		
		//Moles Manager
		moles_manager_.ChangeParams( level_list_ [game_.level ()].moles_manager_params_ );
	}
	
	public override void Outro ()
	{
		boss_.DisableHitMessage();
		boss_.Play ("BossExit");
		boss_.DestroyStump();
		//boss_.RemoveInstantiateStump();
			
		game_.StopTimeLeft();
		
		//Moles Manager
		moles_manager_.StopSpawning();
	}
	
	public override void Start()
	{
		start_ = true;
		
		TrackingState.Instance.set_tracking_param(GetRandTrackingParam());
		boss_.ChangeState (TrackingState.Instance);
		//boss_.PlayIdle ();
		boss_.PlayPassiveIdle();
		
		game_.GoTimeLeft();
	}
	
	public override void EndShot()
	{
		//decide stump
		if(need_stump_)
		{
			if(Random.value < 0.5f)
			{
				boss_.InstantiateStump();
				need_stump_ = false;
			}
		}
		
		TrackingState.Instance.set_tracking_param(GetRandTrackingParam());
		boss_.ChangeState (TrackingState.Instance);
		boss_.PlayPassiveIdle();
	}
	
	public override void RealEndShot()
	{
		
	}
	
	public override void End ()
	{
		start_ = false;
	}
	
	public override float GetCurrentParam(EnemyType type, int param_level, string param_name)
	{
		return default_enemy_params_.GetParam(type, param_level, param_name);
	}
	
	public override int GetGroupCount()
	{
		return 0;
	}
	
	public override bool CurrentGroupIsCombo()
	{
		return false;
	}
	
	public override bool IsFinished()
	{
		return false;
	}
	
	public override float GetBossLife()
	{
		return level_list_[game_.level()].boss_life_;
	}

	public override bool CanStun()
	{
		return false;
	}
	
	public override void PrevPowerPunchStun()
	{
		//TO-DO add code
	}
	
	public override void EndPowerPunchStun()
	{
		//TO-DO add code
		EndShot();
	}
	
	public override void EndFreeze()
	{
		
	}
	
	//End base class
	public void StopAttack()
	{
		
	}
	
	public void DecideNextAttack()
	{
		type_ = PhysicAttackType.chainsaw;
		ChainsawState.Instance.set_attack_speed(level_list_[game_.level()].attack_speed_);
		ChainsawState.Instance.set_charge_speed(level_list_[game_.level()].charge_speed_);
		boss_.ChangeState(ChainsawState.Instance);
	}
		
	//Getter / setter
	public void set_type(PhysicAttackType type)
	{
		type_ = type;
	}
	
	public void set_need_stump(bool need_stump)
	{
		need_stump_ = need_stump;
	}
	
	public PhysicAttackType type()
	{
		return type_;
	}
	
	public TrackingParam GetRandTrackingParam()
	{
		int rand_type = (int)Mathf.Round(Random.value * (level_list_[game_.level()].tracking_param_.Count - 1));
		return level_list_[game_.level()].tracking_param_[rand_type];
	}
	
	public float GetStunTime()
	{
		return level_list_[game_.level()].stun_time_;
	}
	
	public float GetReturnTime()
	{
		return level_list_[game_.level()].return_time_;
	}
}