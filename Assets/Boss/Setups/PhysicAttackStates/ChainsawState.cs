using UnityEngine;

public sealed class ChainsawState :  FSMState<Boss> {
	
	static readonly ChainsawState instance = new ChainsawState();
	public static ChainsawState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static ChainsawState() 
	{
		
	}
	
	private ChainsawState()
	{
	
	}
		
	//Variables
	private PhysicAttackSetup physic_attack_setup_;
	private bool go_out_ = false;
	private float attack_speed_;
	private float charge_speed_;
	
	public void Init()
	{
		physic_attack_setup_ = GameObject.Find("BossPrefab").GetComponent<Boss>().physic_attack_setup();
	}
	
	public void set_attack_speed(float attack_speed)
	{
		attack_speed_ = attack_speed;
	}
	
	public void set_charge_speed(float charge_speed)
	{
		charge_speed_ = charge_speed;
	}
	
	public override void Enter(Boss b) 
	{
		physic_attack_setup_.set_type( PhysicAttackType.chainsaw );
		
		Framework.Instance.audio_manager().Play(
			"5_Boss Opens",
			GameObject.Find("BossPrefab").GetComponent<Boss>().transform,
			1.0f, 
			1.0f,
			false);
		b.Play("ChainSawAnimationEntering", charge_speed_);
		go_out_ = false;
	}
	
	public override void Execute(Boss b) 
	{
		if(!b.IsPlaying() && !b.IsMainAnimationComponentPlaying())
		{
			if(!go_out_)
			{
				if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)
					b.PlayOnBoss("ChargeForwardBackAnimation16_9", attack_speed_);
				else
					b.PlayOnBoss("ChargeForwardBackAnimation", attack_speed_);
				b.BlendAnimation("ChainSawChargeAnimation");
				go_out_ = true;
			}
			else
			{
				b.Play("ChainSawAnimationExit");
				b.ForceTrackingDisabled();
				go_out_ = false;
			}
		}
	}
	
	public override void Exit(Boss b) 
	{
		
	}
}