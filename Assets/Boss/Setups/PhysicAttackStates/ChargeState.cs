using UnityEngine;

public sealed class ChargeState :  FSMState<Boss> {
	
	static readonly ChargeState instance = new ChargeState();
	public static ChargeState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static ChargeState() 
	{
		
	}
	
	private ChargeState()
	{
	
	}
		
	//Variables
	private PhysicAttackSetup physic_attack_setup_;
	
	public void Init()
	{
		physic_attack_setup_ = GameObject.Find("BossPrefab").GetComponent<Boss>().physic_attack_setup();
	}
	
	public override void Enter(Boss b) 
	{
		physic_attack_setup_.set_type( PhysicAttackType.front_attack );
		
		b.PlayOnBoss("ChargeForwardBackAnimation");
	}
	
	public override void Execute(Boss b) 
	{
		if(!b.IsMainAnimationComponentPlaying())
		{
			b.EndShot();
		}
	}
	
	public override void Exit(Boss b) 
	{
		
	}
}