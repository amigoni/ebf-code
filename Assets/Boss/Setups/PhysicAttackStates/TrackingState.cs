using UnityEngine;

public sealed class TrackingState :  FSMState<Boss> {
	
	static readonly TrackingState instance = new TrackingState();
	public static TrackingState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static TrackingState() 
	{
		
	}
	
	private TrackingState()
	{
	
	}
	
	//Variables
	private PhysicAttackSetup physic_attack_setup_;
	private TrackingParam tracking_param_;
	private float tracking_time_ = 1;
	
	public void Init()
	{
		physic_attack_setup_ = GameObject.Find("BossPrefab").GetComponent<Boss>().physic_attack_setup();
	}
	
	public void set_tracking_param(TrackingParam tracking_param)
	{
		tracking_param_ = tracking_param;
	}
	
	public override void Enter(Boss b) 
	{
		b.PlayIdle();
		
		b.EnableTracking(tracking_param_);
	}
	
	public override void Execute(Boss b) 
	{
		if(!b.CheckChangeSetup ())
		{	
			if(!b.TrackingFinished())
			{
				physic_attack_setup_.DecideNextAttack();
			}
		}
		else
			b.DisableTracking();
	}
	
	public override void Exit(Boss b) 
	{

	}
}