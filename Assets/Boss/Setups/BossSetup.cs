using UnityEngine;

public abstract class BossSetup
{
	/*
	 * Init
	 * Intro
	 * Start
	 * Outro
	 */
	protected string name_ ="NONAME";
	
	protected Boss boss_;
	protected Game game_;
	protected MolesManager moles_manager_;
		
	protected bool start_ = false;
	
	//Getter
	public bool start()
	{
		return start_;
	}
	
	public string name()
	{
		return name_;
	}
	
    public void Init()
	{ 
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		game_ = GameObject.Find("Game").GetComponent<Game>();
		moles_manager_ = GameObject.Find("BossPrefab").GetComponent<MolesManager>();
		
		LoadFromXml();
		
		InitSetup();
	}
	abstract public void InitSetup();
	
	abstract public void LoadFromXml();
	
	abstract public void Intro();
	abstract public void Outro();
	
	abstract public void Start();
	abstract public void End();
	
	abstract public void EndShot();
	abstract public void RealEndShot();
	
	abstract public float GetCurrentParam(EnemyType type, int param_level, string param_name);
	
	abstract public int GetGroupCount();
	abstract public bool CurrentGroupIsCombo();
	
	abstract public bool IsFinished();
	
	abstract public float GetBossLife();
	
	abstract public bool CanStun();
	abstract public void PrevPowerPunchStun();
	abstract public void EndPowerPunchStun();
	
	abstract public void EndFreeze();
}