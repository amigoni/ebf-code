using UnityEngine;

public sealed class MoveState :  FSMState<Boss> {
	
	static readonly MoveState instance = new MoveState();
	public static MoveState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static MoveState() 
	{
		
	}
	
	private MoveState()
	{
	
	}
	
	private float time_to_next_shot_;
	
	public override void Enter(Boss b) 
	{

	}
	
	public override void Execute(Boss b) 
	{
		time_to_next_shot_ -= Time.deltaTime;
		if(time_to_next_shot_ < 0.0F)
			b.ChangeState(SpawnEnemyState.Instance);
	}
	
	public override void Exit(Boss b) 
	{

	}
	
	public void Init(float time_to_next_shot)
	{
		time_to_next_shot_ = time_to_next_shot;
	}	
}