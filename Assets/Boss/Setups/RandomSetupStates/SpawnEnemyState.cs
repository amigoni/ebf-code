using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class SpawnEnemyState :  FSMState<Boss>
{	
	static readonly SpawnEnemyState instance = new SpawnEnemyState ();

	public static SpawnEnemyState Instance {
		get {
			return instance;
		}
	}
	
	static SpawnEnemyState ()
	{
		
	}
	
	private SpawnEnemyState ()
	{
	
	}
	
	//Variables
	private RandomSetup random_setup_;
	private float time_to_next_enemy_ = 0;
	
	public void Init()
	{
		random_setup_ = GameObject.Find("BossPrefab").GetComponent<Boss>().random_setup();
	}
	
	public override void Enter (Boss b)
	{

	}
	
	public override void Execute (Boss b)
	{
		if(!b.CheckChangeSetup())
		{
			time_to_next_enemy_ -= Time.deltaTime;
			
			if(time_to_next_enemy_ < 0.0F)
			{
				random_setup_.SpawnEnemy();
			
			}
		}
	}
	
	public override void Exit (Boss b)
	{

	}
	
	//Getter / Setter
	public void set_time_to_next_enemy_(float time_to_next_enemy)
	{
		time_to_next_enemy_ = time_to_next_enemy;
	}
}