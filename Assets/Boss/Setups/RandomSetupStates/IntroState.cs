using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class IntroState :  FSMState<Boss>
{	
	static readonly IntroState instance = new IntroState ();

	public static IntroState Instance {
		get {
			return instance;
		}
	}
	
	static IntroState ()
	{
		
	}
	
	private IntroState ()
	{
	
	}
	
	public override void Enter (Boss b)
	{
		b.PlayAngryAnimation();
	}
	
	public override void Execute (Boss b)
	{
		if(!b.IsPlaying())
		{
			b.random_setup().StartAttack();
		}
	}
	
	public override void Exit (Boss b)
	{

	}
}