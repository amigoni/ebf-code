using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class DeterminesGroupState :  FSMState<Boss>
{	
	static readonly DeterminesGroupState instance = new DeterminesGroupState ();

	public static DeterminesGroupState Instance {
		get {
			return instance;
		}
	}
	
	static DeterminesGroupState ()
	{
		
	}
	
	private DeterminesGroupState ()
	{
	
	}
	
	//Variables
	private RandomSetup random_setup_;
	private float time_to_next_group_;
	
	public void Init()
	{
		random_setup_ = GameObject.Find("BossPrefab").GetComponent<Boss>().random_setup();
	}
	
	public override void Enter (Boss b)
	{
		float anim_time = b.animation_component()["AngryAnimation"].clip.length;
		b.Invoke("PlayAngryAnimation", time_to_next_group_ - anim_time);
	}
	
	public override void Execute (Boss b)
	{
		time_to_next_group_ -= Time.deltaTime;
		
		if(time_to_next_group_ < 0.0F)
		{
			//random_setup_.AddNewEnemyGroup();
		    b.ChangeState ( SpawnEnemyState.Instance );
		}
		
	
	}
	
	public override void Exit (Boss b)
	{

	}
	
	//Getter / Setter
	public void set_time_to_next_group (float time_to_next_group)
	{
		time_to_next_group_ = time_to_next_group;
	}
}