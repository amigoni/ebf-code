using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class LevelSetup
{
	public struct Level
	{
		public int number_;
		public List<string> setup_list_;
		public List<string> enviroment_list_;
	};
	
	//setups
	private List<Level> level_list_ = new List<Level> ();
	private int current_setup_list_ = 0;
	private int max_level_ = 0;
	private BossSetup current_setup_;
	private BossSetup prev_setup_;
	
	private RandomSetup random_setup_ = new RandomSetup ();
	private PhysicAttackSetup physic_attack_setup_ = new PhysicAttackSetup ();
	private PatternSetup pattern_setup_ = new PatternSetup ();
	private BonusSetup bonus_setup_ = new BonusSetup ();
	
	private Game game_;
	private Boss boss_;
	private EnvironmentSelector environment_selector_;
	//Getter / Setter
	public int max_level()
	{
		return max_level_;
	}
	
	public void Init(Game game, Boss boss, EnvironmentSelector environment_selector)
	{
		game_ = game;
		boss_ = boss;
		environment_selector_ = environment_selector;
			
		random_setup_.Init();
		physic_attack_setup_.Init();
		pattern_setup_.Init();
		bonus_setup_.Init();
		
		LoadSetup();
		
		current_setup_ = GetSetup(level_list_[game_.level()].setup_list_[current_setup_list_]);
		//current_setup_.Intro ();
	}
	
	void LoadSetup()	
	{
		string xml_text;
		Game.GameMode game_mode = game_.game_mode();
		if(game_mode == Game.GameMode.multiplayer || game_mode == Game.GameMode.boss_training)
		{
			xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/BuildTheBossBase/Levels");
		}
		else
		{
			xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/Levels");
		}
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;
		
		//Starting level
		if(PlayerPrefs.GetInt("Force Level") == 1)
		{
			game_.set_level( int.Parse( PlayerPrefs.GetString("Force Number Level") ));
		}
		else
		{
			game_.set_level( int.Parse(xml_doc.DocumentElement.Attributes[0].Value) );
		}
		
		XmlNodeList levels_nodes = xml_doc.DocumentElement.ChildNodes;
		Level level;
		for (int i = 0; i < levels_nodes.Count; ++i) {
			level = new Level();
			level.setup_list_ = new List<string>();
			level.enviroment_list_ = new List<string>();
			level.number_ = int.Parse(levels_nodes[i].Attributes[0].Value);
			
			XmlNodeList setup_nodes = levels_nodes[i].ChildNodes;
			foreach (XmlNode node in setup_nodes){
				level.setup_list_.Add( node.Attributes[0].Value );
				if(node.Attributes.Count > 1)
					level.enviroment_list_.Add( node.Attributes[1].Value );
				else 
					level.enviroment_list_.Add( "auto_environment" );
			}
			
			level_list_.Add(level);
			max_level_++;
		}
		
		game_.CreateResults();
	}
	
	public void UpdateEnviroment(List<string> enviroments)
	{
		for (int i = 0; i < enviroments.Count; ++i) 
		{
			level_list_[i].enviroment_list_[0] = enviroments[i];
		}
	}
	
	public void NextState()
	{
		game_.NextState();
		
		//next state, if level is finished update level
		int next_setup_number = current_setup_list_ + 1;
		BossSetup next_setup = null;
		if (next_setup_number == level_list_[game_.level()].setup_list_.Count)
		{
			current_setup_list_ = 0;
			bool next_level = game_.NextLevel();
			if(next_level == true)
			{
				next_setup = GetSetup(level_list_[game_.level()].setup_list_ [current_setup_list_]);
			}
		}
		else
		{
			current_setup_list_ = next_setup_number;
			next_setup = GetSetup(level_list_[game_.level()].setup_list_ [current_setup_list_]);
		}
		
		ChangeSetupState.Instance.Init (current_setup_, next_setup);
		boss_.ChangeState (ChangeSetupState.Instance);
		
		prev_setup_ = current_setup_;
		current_setup_ = next_setup;

		string enviroment_name = level_list_[game_.level()].enviroment_list_[current_setup_list_];
		if(enviroment_name != "auto_environment")
			environment_selector_.ChangeEnvironment(enviroment_name, true);
		else
			environment_selector_.ChangeEnvironment(true);		
	}
	
	public void FirstIntro()
	{
		current_setup_.Intro ();
		
		string enviroment_name = level_list_[game_.level()].enviroment_list_[current_setup_list_];
		if(enviroment_name != "auto_environment")
			environment_selector_.ChangeEnvironment(enviroment_name, false);
		else
			environment_selector_.ChangeEnvironment(false);
		
	}
	
	public BossSetup GetSetup(string setup_name)
	{
		if(random_setup_.name() == setup_name)
			return random_setup_;
		else if(physic_attack_setup_.name() == setup_name)
			return physic_attack_setup_;
		else if(pattern_setup_.name() == setup_name)
			return pattern_setup_;
		else if(bonus_setup_.name() == setup_name)
			return bonus_setup_;
		else
			return random_setup_;
			
		Debug.LogWarning("Error: setup not found");
		return null;
	}
	
	//Getter / Setter
	public BossSetup prev_setup()
	{
		return prev_setup_;
	}
	
	public BossSetup current_setup()
	{
		return current_setup_;
	}
	
	public RandomSetup random_setup()
	{
		return random_setup_;
	}
	
	public PhysicAttackSetup physic_attack_setup()
	{
		return physic_attack_setup_;
	}
	
	public PatternSetup pattern_setup()
	{
		return pattern_setup_;
	}
	
	public BonusSetup bonus_setup()
	{
		return bonus_setup_;
	}
		
	public bool IsRandomSetup()
	{
		return current_setup_ == random_setup_;
	}
	
	public bool IsPhysicAttackSetup()
	{
		return current_setup_ == physic_attack_setup_;
	}
	
	public bool IsPatternSetup()
	{
		return current_setup_ == pattern_setup_;
	}
	
	public List<Level> level_list()
	{
		return level_list_;
	}
}