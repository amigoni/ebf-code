using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public struct PatternClass
{
	public int id_;
	public GameObject game_object_prefab_;
	public int level_number_; //primary key
};


public struct PatternSetupLevelClass
{
	public int number_; //primary key
	public float duration_;
	public EnemyParams enemy_params_;
	public MolesManagerParams moles_manager_params_;
};

public class PatternSetup: BossSetup
{	
	private List<PatternSetupLevelClass> level_list_ = new List<PatternSetupLevelClass> ();
	private List<PatternClass> pattern_list_ = new List<PatternClass> ();
	
	public GameObject[] patterns_prefab_;
	
	private Clock current_clock_;
	
	public override void InitSetup()
	{
		name_ = "PatternSetup";
		
		//WaitingEndState.Instance.Init();
	}
	
	public override void LoadFromXml()
	{
		string xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/PatternSetup");
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;
		
		XmlNodeList nodes = xml_doc.DocumentElement.ChildNodes;
		//default params
		//read enemy default config
		XmlNodeList default_params_node = nodes [0].ChildNodes;
		EnemyParams default_enemy_params_ = EnemyParams.LoadFromXml(default_params_node);
		
		//load level
		XmlNodeList speed_nodes = nodes [1].ChildNodes;
		for (int i = 0; i < speed_nodes.Count; ++i) {
			PatternSetupLevelClass level;
			level.number_ = int.Parse (speed_nodes [i].Attributes [0].Value);
			level.duration_ = float.Parse (speed_nodes [i].Attributes [1].Value);
			XmlNodeList speed_node = speed_nodes [i].FirstChild.ChildNodes;
			//read params
			level.enemy_params_ = EnemyParams.LoadFromXml( speed_node, default_enemy_params_ );	
			//Load moles manager
			level.moles_manager_params_ = MolesManager.LoadParamsFromXml(speed_nodes [i].ChildNodes[1]);
			
			level_list_.Add (level);
		}
		
		//load pattern
		XmlNodeList patterns_nodes = nodes [2].ChildNodes;
		for (int i = 0; i < patterns_nodes.Count; ++i) {
			PatternClass patter_class;
			patter_class.id_ = i;
			patter_class.level_number_ = int.Parse (patterns_nodes [i].Attributes [0].Value);
			
			//load prefab
			string prefab_name = patterns_nodes [i].Attributes [1].Value;
			patter_class.game_object_prefab_ = Resources.Load("Patterns/"+prefab_name) as GameObject;
#if UNITY_EDITOR
			if(patter_class.game_object_prefab_ == null)
				patter_class.game_object_prefab_ = Resources.Load("Patterns/Full/"+prefab_name) as GameObject;
#endif
			
			pattern_list_.Add(patter_class);
		}
	}
	
	public override void Intro()
	{
		current_clock_ = boss_.InstantiateClock();
		boss_.ChangeState (WaitingEndState.Instance);
		
		WaitingEndState.Instance.set_duration( level_list_[game_.level()].duration_ );
		SpawnPattern();
		
		game_.SetMaxWalkableArea();
		
		//Disable
		GameObject.Find("DamageWallPrefabFront").GetComponent<BoxCollider>().enabled = false;
		
		//Moles Manager
		moles_manager_.ChangeParams( level_list_ [game_.level ()].moles_manager_params_ );
	}
	
	public override  void Outro()
	{
		boss_.DestroyClock();
		game_.SetMinWalkableArea();
		
		//Enable
		GameObject.Find("DamageWallPrefabFront").GetComponent<BoxCollider>().enabled = true;
		
		//Moles Manager
		moles_manager_.StopSpawning ();
	}
	
	public override  void Start()
	{
	
	}
	
	public override  void End()
	{
		
	}
	
	public override  void EndShot()
	{
		
	}
	
	public override void RealEndShot()
	{
		
	}
	
	public override bool CanStun()
	{
		return false;
	}
	
	public override void PrevPowerPunchStun()
	{
		
	}
	
	public override void EndPowerPunchStun()
	{
		
	}
	
	public override void EndFreeze()
	{
		
	}
	//End
	
	public override float GetCurrentParam(EnemyType type, int param_level, string param_name)
	{
		if(param_name == "speed")	
			return (level_list_ [game_.level ()]).enemy_params_.GetParam(type, param_level, param_name) + (Random.value * 10.0f);
		
		return (level_list_ [game_.level ()]).enemy_params_.GetParam(type, param_level, param_name);
	}
	
	public override  int GetGroupCount()
	{
		//TO-DO implement this
		return -1;
	}
	
	public override  bool CurrentGroupIsCombo()
	{
		return false;
	}
	
	public override  bool IsFinished()
	{
		return false;
	}
	
	public override float GetBossLife()
	{
		return 100.0F;
	}
	
	//End	
	public int GetRandomLevelGroup ()
	{
		int level = game_.level ();
		ArrayList pattern_level = new ArrayList ();			
		for (int i = 0; i < pattern_list_.Count; i++) {	
			if (pattern_list_ [i].level_number_ == level || pattern_list_ [i].level_number_ == -1 )
				pattern_level.Add (pattern_list_ [i]);
		}
		int r = Mathf.RoundToInt (Random.value * (pattern_level.Count - 1));
		int rand_id_ = ( (PatternClass)(pattern_level[r] )).id_;
		for(int i = 0; i < pattern_list_.Count; i++)
		{
			if(rand_id_ == pattern_list_[i].id_)
			{
				return i;
			}
		}
		
		return -1;
		
	}
	
	public void SpawnPattern()
	{
		GameObject g_obj = null;
		int random_level_group = GetRandomLevelGroup();
		foreach(Transform child in pattern_list_[random_level_group].game_object_prefab_.transform)
		{
			g_obj = boss_.SpawnFromPool(child.name);
			if(g_obj == null)
			{
				//Try get enemy from child
				//Enemy [] enemy = child.GetComponentsInChildren<Enemy>();
				foreach(Transform child_of_child in child.transform)
				{
					g_obj = boss_.SpawnFromPool(child_of_child.name);
					InitEnemy(g_obj, child_of_child.transform);
				}
			}
			else
				InitEnemy(g_obj, child);
		}
	}
	
	private void InitEnemy(GameObject g_obj, Transform child)
	{
		g_obj.transform.position = child.position;
		g_obj.transform.position += game_.GetDeviceOffset();
				
		//TO-DO add UpRocket copy
		UpMoveController up_controller = g_obj.GetComponent<UpMoveController>();
		if(up_controller != null)
		{
			up_controller.player_tracking = child.GetComponent<UpMoveController>().player_tracking;
			up_controller.ground_y = child.GetComponent<UpMoveController>().ground_y;
			if(Framework.Instance.device_type() == Framework.WMF_DeviceType.ipad)
				up_controller.ground_y += game_.GetDeviceOffset().y;
			if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)
				up_controller.transform.position += game_.GetDeviceOffset();
			up_controller.delay = child.GetComponent<UpMoveController>().delay;
		}
		//TO-DO add Mole copy
		MoleController mole_controller = g_obj.GetComponent<MoleController>();
		if(mole_controller != null)
		{
			mole_controller.spawn_time_ = child.GetComponent<MoleController>().spawn_time_;
			if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)
				mole_controller.transform.position += game_.GetDeviceOffset();
		}
		//Init Enemy
		g_obj.GetComponent<Enemy>().Init();
		g_obj.GetComponent<Enemy>().Go();
		g_obj.GetComponent<Enemy>().ChangeLevel(0);
	}
	
	public bool ThereAreEnemyInScene()
	{
		if(GameObject.FindWithTag("Enemy") != null)
			return true;
		
		return false;
	}
	
	public void UpdateClock(float duration )
	{
		current_clock_.UpdateClock( duration );
	}
}