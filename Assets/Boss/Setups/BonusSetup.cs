using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public struct BonusSetupLevelClass
{
	public int number_; //primary key
	public float duration_;
	public GameObject bonus_prefab_;
	public RandBonus rand_bonus_;
};

public class BonusSetup: BossSetup
{	
	private List<BonusSetupLevelClass> level_list_ = new List<BonusSetupLevelClass> ();
	
	public GameObject[] patterns_prefab_;
	
	private Clock current_clock_;
	
	public override void InitSetup()
	{
		name_ = "BonusSetup";
		
		WaitingEndState.Instance.Init();
	}
	
	public override void LoadFromXml()
	{
		string xml_text = Framework.Instance.resouces_manager().GetStringResource("Config/BonusSetup");
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;
		
		//load level
		XmlNodeList level_nodes = xml_doc.DocumentElement.ChildNodes;
		BonusSetupLevelClass level;
		for (int i = 0; i < level_nodes.Count; ++i) {
			level.number_ = int.Parse (level_nodes [i].Attributes [0].Value);
			level.duration_ = float.Parse (level_nodes [i].Attributes [1].Value);
			//load prefab
			string prefab_name = level_nodes [i].FirstChild.Attributes[0].Value;
			level.bonus_prefab_ = Resources.Load("Bonus/"+prefab_name) as GameObject;
			//load 
			level.rand_bonus_ = RandBonus.LoadFromXml(level_nodes [i].ChildNodes[1]);
			level_list_.Add (level);
		}
	}
	
	public override void Intro()
	{
		current_clock_ = boss_.InstantiateClock();
		boss_.ChangeState (WaitingBonusState.Instance);
		
		WaitingBonusState.Instance.set_duration( level_list_[game_.level()].duration_ );
		
		SpawnBonus();
	}
	
	public override  void Outro()
	{
		boss_.DestroyClock();
		
		//destroy box
		GameObject[] objs = GameObject.FindGameObjectsWithTag("Box");
		foreach(GameObject obj in objs)
			obj.GetComponent<Crate>().DestroyCrate();
	}
	
	public override  void Start()
	{
	
	}
	
	public override  void End()
	{
		
	}
	
	public override  void EndShot()
	{
		
	}
	
	public override void RealEndShot()
	{
		
	}
	
	public override float GetCurrentParam(EnemyType type, int param_level, string param_name)
	{
		return -1.0F;
	}
	
	public override  int GetGroupCount()
	{
		//TO-DO implement this
		return -1;
	}
	
	public override  bool CurrentGroupIsCombo()
	{
		return false;
	}
	
	public override  bool IsFinished()
	{
		return false;
	}
	
	public override float GetBossLife()
	{
		return 100.0F;
	}
	
	public override bool CanStun()
	{
		return false;
	}
	
	public override void PrevPowerPunchStun()
	{
		
	}
	
	public override void EndPowerPunchStun()
	{
		
	}
	
	public override void EndFreeze()
	{
		
	}
	//End	
	
	private void SpawnBonus()
	{
		GameObject.Instantiate(level_list_[game_.level()].bonus_prefab_);
	}
	
	public void UpdateClock(float duration )
	{
		current_clock_.UpdateClock( duration );
	}
	
		
	public bool ThereAreBoxInScene()
	{
		if(GameObject.FindWithTag("Box") != null)
			return true;
		
		return false;
	}
	
	//Getter / Setter
	public RandBonus CurrentRandBonus()
	{
		return level_list_[game_.level()].rand_bonus_;
	}
}