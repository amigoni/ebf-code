using UnityEngine;

public sealed class WaitingBonusState :  FSMState<Boss> {
	
	static readonly WaitingBonusState instance = new WaitingBonusState();
	public static WaitingBonusState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static WaitingBonusState() 
	{
		
	}
	
	private WaitingBonusState()
	{
	
	}
		
	//Variables
	private BonusSetup bonus_setup_;
	private float duration_ = 10.0F;
	
	public void set_duration(float duration)
	{
		duration_ = duration;
	}
	
	
	public override void Enter(Boss b) 
	{
		bonus_setup_ = b.bonus_setup();
	}
	
	public override void Execute(Boss b) 
	{
		duration_ -= Time.deltaTime;
		bonus_setup_.UpdateClock( duration_ );
			
		if(duration_ < 0.0F)
		{
			b.ForceChangeSetup();
		}
	}
	
	public override void Exit(Boss b) 
	{
		
	}
}