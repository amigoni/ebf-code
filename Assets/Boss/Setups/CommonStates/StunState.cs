using UnityEngine;

public sealed class StunState :  FSMState<Boss> {
	
	static readonly StunState instance = new StunState();
	public static StunState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static StunState() 
	{
		
	}
	
	private StunState()
	{
	
	}
	
	public override void Enter(Boss b) 
	{

	}
	
	public override void Execute(Boss b) 
	{

	}
	
	public override void Exit(Boss b) 
	{

	}
}