using UnityEngine;

public sealed class IdleState :  FSMState<Boss> {
	
	static readonly IdleState instance = new IdleState();
	public static IdleState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static IdleState() 
	{
		
	}
	
	private IdleState()
	{
	
	}
	
	public override void Enter(Boss b) 
	{

	}
	
	public override void Execute(Boss b) 
	{
		if( GameObject.Find("BossPrefab").GetComponent<Boss>().fsm().PreviousState == SpawnEnemyState.Instance)
			GameObject.Find("BossPrefab").GetComponent<Boss>().random_setup().SpawnEnemy();
	}
	
	public override void Exit(Boss b) 
	{
	
	}
}