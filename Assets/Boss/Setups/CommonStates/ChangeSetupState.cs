using UnityEngine;

public sealed class ChangeSetupState :  FSMState<Boss> {
	
	static readonly ChangeSetupState instance = new ChangeSetupState();
	public static ChangeSetupState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static ChangeSetupState() 
	{
		
	}
	
	private ChangeSetupState()
	{
	
	}
	
	private BossSetup outro_setup_;
	private BossSetup intro_setup_;
	private float life_;
	
	public void Init(BossSetup outro_setup, BossSetup intro_setup)
	{
		outro_setup_ = outro_setup;
		intro_setup_ = intro_setup;
		if(intro_setup_ != null)
		{
			life_ = intro_setup.GetBossLife();
		}
	}
	
	public override void Enter(Boss b) 
	{
		outro_setup_.Outro();
		b.set_fsm_locked(true);
	}
	
	public override void Execute(Boss b) 
	{
		if(!outro_setup_.start())
		{
			b.set_fsm_locked(false);
			b.RestoreLife(life_);
			b.RestoreDamageSprites();
			b.DestroyUnspawnedEnemy();
			if(intro_setup_ != null)
				intro_setup_.Intro();
		}
	}
	
	public override void Exit(Boss b) 
	{

	}
}