using UnityEngine;

public sealed class IdleAndResumeState :  FSMState<Boss> {
	
	static readonly IdleAndResumeState instance = new IdleAndResumeState();
	public static IdleAndResumeState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static IdleAndResumeState() 
	{
		
	}
	
	private IdleAndResumeState()
	{
	
	}
	
	private FSMState<Boss> resume_state_;
	private float time_;
	
	public void Init(FSMState<Boss> resume_state, float time)
	{
		resume_state_ = resume_state;
		time_ = time;
	}
	
	public override void Enter(Boss b) 
	{

	}
	
	public override void Execute(Boss b) 
	{
		time_ -= Time.deltaTime;
		if(time_ < 0.0F)
		{
			b.ChangeState(resume_state_);
		}
	}
	
	public override void Exit(Boss b) 
	{

	}
}