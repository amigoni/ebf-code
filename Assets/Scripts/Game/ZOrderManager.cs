using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZOrderManager : MonoBehaviour
{
	private List<ZOrderObject> z_object_list_ = new List<ZOrderObject> ();
	
	private ZOrderObject player_z_order_;
	private Boss boss_;
	
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateZOrder ();
	}
	
	//Add object pair
	public void AddZPair (ZOrderObject ot_obj)
	{
		z_object_list_.Add (ot_obj);
		
		UpdateZOrder ();
	}
	
	public void RemoveZPair (ZOrderObject ot_obj)
	{
		z_object_list_.Remove (ot_obj);
		
		UpdateZOrder ();
	}
		
	private void UpdateZOrder ()
	{
		z_object_list_.Sort (new TestSorter ());
		
		int start_z = 150;
		int the_real_i = start_z;
		bool next_boss = false;
		for (int i = z_object_list_.Count - 1; i >= 0; i--) 
		{
			if(next_boss)
			{
				next_boss = false;
				the_real_i -= 130;
			}
			if(z_object_list_[i].name == "BossPrefab")
			{
				the_real_i -= 130;
				next_boss = true;
			}
				
			if(z_object_list_[i].enabled())
			{
				z_object_list_ [i].transform.position = new Vector3(z_object_list_ [i].transform.position.x,
													 				z_object_list_ [i].transform.position.y, 
												    				the_real_i + i);
			}
		}
	}
	
	public void Reset ()
	{
		z_object_list_.Clear ();
	}
	
		
	public class TestSorter : IComparer<ZOrderObject>
	{

		public int Compare (ZOrderObject a, ZOrderObject b)
		{
			if( a != b && a.GetBaseline () == b.GetBaseline ())
			{
				//return a.start_time().CompareTo( b.start_time() );
				return a.transform.position.x.CompareTo( b.transform.position.x );
			}
			
			return a.GetBaseline ().CompareTo (b.GetBaseline ());
		}
	}
}
		

