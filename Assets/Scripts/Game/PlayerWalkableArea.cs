using UnityEngine;
using System.Collections;

public class PlayerWalkableArea : MonoBehaviour {
	
	private BoxCollider player_walkable_area_min_;
	private BoxCollider player_walkable_area_max_;
	private BoxCollider current_player_walkable_area_;
	
	private PlayerMoveController player_move_controller_;
	
	void Awake()
	{
#if UNITY_IPHONE
		//check iPhone 5
		if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)
		{
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ().size += new Vector3(20.0f, 0.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ().size += new Vector3(20.0f, 0.0f, 0.0f);
			
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<tk2dCameraAnchor>().offset += new Vector2(35.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<tk2dCameraAnchor>().offset += new Vector2(35.0f, 0.0f);			
		}
#elif UNITY_ANDROID
		GraphicsManager graphics_manager = Framework.Instance.graphics_manager();
		
		if(graphics_manager.ratio() == (1280.0F / 800.0F) || 
			graphics_manager.ratio() == (1280.0F / 752.0F) ||
			graphics_manager.ratio() == (1280.0F / 768.0F) ||
			graphics_manager.ratio() == (1280.0F / 720.0F) ||
			graphics_manager.ratio() == (1920.0F / (1200.0F - 50.0F)) ||
			graphics_manager.ratio() == (1920.0F / (1200.0F - 112.0F)) ||
			graphics_manager.ratio() == (1920.0F / 942.0F) ) 
		{
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ().size += new Vector3(70.0f, 0.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ().size += new Vector3(70.0f, 0.0f, 0.0f);
		}
		else if(graphics_manager.ratio() == (1024.0F / 600.0F) || 
				graphics_manager.ratio() == (1024.0F / 552.0F)) 
		{
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ().size += new Vector3(80.0f, 0.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ().size += new Vector3(80.0f, 0.0f, 0.0f);
		}
		else if(graphics_manager.ratio() == (800.0F / 480.0F)) 
		{
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ().size += new Vector3(55.0f, 0.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ().size += new Vector3(55.0f, 0.0f, 0.0f);
		}
		else if(graphics_manager.ratio() == (854.0F / 480.0F)) 
		{
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ().size += new Vector3(82.0f, 0.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ().size += new Vector3(82.0f, 0.0f, 0.0f);
		}	
		else if(graphics_manager.ratio() != (480.0F / 320.0F))
		{
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ().size += new Vector3(70.0f, 0.0f, 0.0f);
			GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ().size += new Vector3(70.0f, 0.0f, 0.0f);
		}
		
#endif
		
		player_walkable_area_min_ = GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMin").GetComponent<BoxCollider> ();
		player_walkable_area_max_ = GameObject.Find ("PlayerWalkableArea/PlayerWalkableAreaMax").GetComponent<BoxCollider> ();
		
		//Anchor point
		player_walkable_area_min_.GetComponent<tk2dCameraAnchor>().ForceUpdateTransform();
		player_walkable_area_max_.GetComponent<tk2dCameraAnchor>().ForceUpdateTransform();
		
		player_move_controller_ = GameObject.Find("Player").GetComponent<PlayerMoveController>();
		player_move_controller_.set_walkable_area(player_walkable_area_min_);
		
		current_player_walkable_area_ = player_walkable_area_min_;
		
	}
	
	void Start () {
		
	}
	
	void Update () {
	
	}
	
	public void SetMaxWalkableArea()
	{
		current_player_walkable_area_ = player_walkable_area_max_;
		player_move_controller_.set_walkable_area(current_player_walkable_area_);
	}
	
	public void SetMinWalkableArea()
	{
		current_player_walkable_area_ = player_walkable_area_min_;
		player_move_controller_.set_walkable_area (current_player_walkable_area_);
	}
	
	public Vector2 GetRandomPosition()
	{
		//TO-DO get the real inside walkable
		float width = 210.0f;
		if(current_player_walkable_area_ == player_walkable_area_max_)
			width = 330.0f;
		
		float x = 10.0f;
		if(Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)
			x += 55.0f;
		
		return new Vector2(x + Random.value * width, 
			current_player_walkable_area_.transform.position.y - current_player_walkable_area_.size.y * 0.5f - 30.0f +
			Random.value * 85.0f);
	}
	
	public BoxCollider current_player_walkable_area()
	{
		return current_player_walkable_area_;
	}
}
