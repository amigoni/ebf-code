using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
//You must include these namespaces
//to use BinaryFormatter
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using SimpleJSON;

public class PlayerData : MonoBehaviour {
	
	public JSONNode player_data_;
	private JSONNode dd_info_;
	
	//Used on Kongregate to save users on same pc
	private string user_id_ = "";
	
	// Use this for initialization
	void Awake () {
		//PlayerPrefs.DeleteAll();
		//Get the data
#if UNITY_WEBPLAYER
		//TO-DO enable for real web version
		user_id_ = Framework.Instance.InstanceComponent<WebKongregateAPI>().user_id();
		//TO-DO enabled for Kongregate+
		//user_id_ = "";
#elif UNITY_IPHONE || UNITY_ANDROID
		user_id_ = "Mobile";
#endif
        string data = Load();
		
        //If not blank then load it
        if(!string.IsNullOrEmpty(data))
        {
          	player_data_ = JSON.Parse(data);
			CheckVersion();
        }
		else {
			Init();
			Save ();
		}
		
		dd_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/DDInfo"));
	}
	
	private void Init(){
		player_data_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/PlayerInitData"));
	}
	
	private void CheckVersion()
	{
		//Get Current version
		JSONNode last_player_data = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/PlayerInitData"));
		if(GetFloat("version") != last_player_data["version"].AsFloat)
		{
			if(player_data_.Count < last_player_data.Count)
			{
				SetValue("version", last_player_data["version"].AsFloat);
				
				for(int i = 0; i < player_data_.Count; i++)
				{
					last_player_data[i] = player_data_[i];
				}
				
				player_data_ = last_player_data;
				Save();
			}
		}
	}
	
	public void Reset()
	{
		string user_name = GetString("user_name");
		int coins = GetInt("coins");
		int white_coins = GetInt("white_coins");
		
		player_data_ = null;
		Init();
		
		SetValue("user_name", user_name);
		SetValue("coins", coins);
		SetValue("white_coins", white_coins);
		
		Save ();
	}
	
	public string GetString( string name_){
		return player_data_[name_];
	}
	
	public float GetFloat( string name_){
		return player_data_[name_].AsFloat;
	}
	
	public double GetDouble( string name_){
		return player_data_[name_].AsDouble;
	}
	
	public int GetInt( string name_){
		return player_data_[name_].AsInt;
	}
	
	public bool GetBool( string name_){
		return player_data_[name_].AsBool;
	}
	
	public string SetValue(string name_, object value_){
		bool enabled = true;
		
		if (name_ == "dd_level" && (int.Parse(value_.ToString()) >= dd_info_.Count))
			enabled = false;
		
		if (enabled == true){
			player_data_[name_] = value_.ToString();
			Save ();
		}
		
		return player_data_[name_].ToString();
	}
	
	//Array
	public void SetArrayValue(string array_name, string name, object data){
		int id = GetIdFromName( name );
		player_data_[array_name][id] = data.ToString();
		Save ();
	}
	
	public bool GetBool(string array_name, string name){
		int id = GetIdFromName( name );
		return player_data_[array_name][id].AsBool;
	}
	
	int GetIdFromName(string name)
	{
		int id = -1;
		if(name == "double_bubble")
			id = 0;
		else if(name == "double_magnet")
			id = 1;
		else if(name == "life_boost")
			id = 2;
		else if(name == "att_boost")
			id = 3;
		else if(name == "ammo_boost")
			id = 4;
		
		return id;
	}
	
	void OnApplicationQuit() {
    	Save ();
    }
    
	private string Load()
	{
		return PlayerPrefs.GetString("PlayerData" + user_id_);
	}
	
	public void Save()
	{
		player_data_.SaveToPlayerPrefs("PlayerData" + user_id_);
	}
	
	public JSONNode GetData(){
		return player_data_;
	}
	
	public int GetCurrentHp()
	{
		return dd_info_[GetInt("dd_level")]["hp_max"].AsInt;
	}
	
#if UNITY_EDITOR
	//DEBUG
	public void PrintsAllFields()
	{
		string all_fields_str = "";
		for(int i = 0; i < player_data_.Count; i++)
		{
			all_fields_str += player_data_[i];
			all_fields_str += "   ";
		}
		
		Debug.Log(all_fields_str);
	}
	
#endif
}
