using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class BossPowerManager : MonoBehaviour {
	
	private JSONNode store_info_; //Static info for each item
	private EbFightAPI.Boss boss_;
	
	private float max_power_ = 120;
	private float available_power_ = 120;
	private int boss_round_ = 0;
	private string current_weapon_key_;
	private int current_weapon_difficulty_;
	private int current_weapon_round_;
	private int current_weapon_cost_;
	private int current_weapon_array_position_;

	public void SetCurrentWeapon (string key, int difficulty, int boss_round, int array_position)
	{
		current_weapon_key_ = key;
		current_weapon_difficulty_ = difficulty;
		current_weapon_round_ = boss_round;
		current_weapon_array_position_ = array_position;
		current_weapon_cost_ = store_info_["boss_round_weapons"]["weapons"][(int) boss_.Level][key]["difficulty_"+difficulty]["cost"].AsInt;
	
		//Debug.Log("Current Weapon "+key+" Difficulty "+difficulty+" Boss Round "+boss_round+" Cost "+current_weapon_cost_);
	}
	
	
	public void InitBoss (EbFightAPI.Boss boss)
	{
		boss_ = boss;
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		max_power_ = store_info_["boss_power_level"]["upgrades"][(int)boss.Power]["effect"].AsInt;
		available_power_ = max_power_;
		
		
		int round_counter = 0;
		foreach( EbFightAPI.StageInfo round in boss_.Stages)
		{
			BuyItem(round_counter, "boss_round_reload_speed", (int) round.ReloadSpeed, true);
			BuyItem(round_counter, "boss_round_shots_per_wave",(int) round.ShotsPerWave, true);
			BuyItem(round_counter, "boss_round_environment",(int) round.Environment, true);
		
			foreach(EbFightAPI.BossWeaponInfo weapon in round.Weapons)
			{
				int cost = store_info_["boss_round_weapons"]["weapons"][(int) boss_.Level][weapon.Kind]["difficulty_"+weapon.Level]["cost"].AsInt;
					
				available_power_ -= cost;
				if (available_power_ < 0)
					available_power_ = 0;
			}
			
			round_counter++;
		}
		
		Messenger.Broadcast("UpdatePowerIndicator", (available_power_.ToString()+"/"+max_power_.ToString()),available_power_/max_power_);
	}
	
	
	public void BuyWeapon(string key, int difficulty, int cost, int round)
	{
		//Debug.Log ("Buy Item "+key+" Difficulty "+difficulty+" Round "+round);
			
		//Sell old
		available_power_ += current_weapon_cost_;
	
		if (available_power_ > max_power_)
			available_power_ = max_power_;
		
			available_power_ -= cost;
		
		boss_.Stages[round].Weapons[current_weapon_array_position_].Kind = key;
		boss_.Stages[round].Weapons[current_weapon_array_position_].Level = difficulty;
		
		Messenger.Broadcast("UpdateBuildABossLevelRow", current_weapon_round_);
		Messenger.Broadcast("UpdatePowerIndicator", (available_power_.ToString()+"/"+max_power_.ToString()),available_power_/max_power_);
		
		/*
		//Analytics
		wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(key, 1);
		wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy Boss Weapon "+key.ToString(), "boss_power", cost, true,"Normal", reward_set.GetResourceSetId());
		reward_set.SendEvent();
		analytics_event.SendEvent();
		*/
	}
	
	
	public void ResetWeapon(int round, int array_position)
	{
		boss_.Stages[round].Weapons[array_position].Kind = "bouncing";
		boss_.Stages[round].Weapons[array_position].Level = 0;
	}
	
	
	
	private void BuyItem(int level, string item_name, int new_level, bool is_init)
	{
		JSONNode item = store_info_[item_name]["upgrades"][new_level];
		
		if (available_power_ >= item["cost"].AsInt)
			available_power_ -= item["cost"].AsInt;
		
		Messenger.Broadcast("UpdatePowerIndicator", (available_power_.ToString()+"/"+max_power_.ToString()),available_power_/max_power_);
		
		//Analytics
		/*
		if (is_init == false)
		{
			wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(item_name, 1);
			wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy Boss Round Item "+item_name.ToString(), "boss_power", item["cost"].AsInt, true,"Normal", reward_set.GetResourceSetId());
			reward_set.SendEvent();
			analytics_event.SendEvent();
		}	
		*/	
	}	
	
	
	private void SellItem(int level, string item_name)
	{
		JSONNode item = store_info_[item_name]["upgrades"][GetCurrentLevelForParameter(item_name)];
		
		available_power_ += item["cost"].AsInt;
		
		if (available_power_ > max_power_)
			available_power_ = max_power_;
	}
	
	
	
	public void ResetRound( int round_number)
	{
		ResetWeapon(round_number,0);
		ResetWeapon(round_number,1);
		ResetWeapon(round_number,2);
		ResetWeapon(round_number,3);
		
		//Do it Twice just in case you are at the highest level. 
		Downgrade(round_number, "boss_round_reload_speed");
		Downgrade(round_number, "boss_round_shots_per_wave");
		Downgrade(round_number, "boss_round_environment");
		
		Downgrade(round_number, "boss_round_reload_speed");
		Downgrade(round_number, "boss_round_shots_per_wave");
		Downgrade(round_number, "boss_round_environment");
		
		Messenger.Broadcast("UpdateBuildABossLevelRow", round_number);
	}
	
	
	public void ResetAllRounds()
	{
		ResetRound(0);
		ResetRound(1);
		ResetRound(2);
		
		available_power_ = max_power_;
		
		//Messenger.Broadcast("LevelParameterChanged");
		
		Messenger.Broadcast("UpdatePowerIndicator", (available_power_.ToString()+"/"+max_power_.ToString()), available_power_/max_power_);
	}
	
	
	private bool CheckIfYouCanAfford (int level, string item_name, int new_level)
	{
		bool can_buy = false;
		
		JSONNode item_sold = store_info_[item_name]["upgrades"][GetCurrentLevelForParameter(item_name)];
		JSONNode item_bought = store_info_[item_name]["upgrades"][new_level];
		
		if (available_power_ + item_sold["cost"].AsInt >= item_bought["cost"].AsInt) 
			can_buy = true;
		
		return can_buy;
	}
	
	
	public bool CheckIfYouCanAffordWeapon(int new_weapon_cost){
		bool can_buy = false;
		
		if (available_power_ + current_weapon_cost_ >= new_weapon_cost ) 
			can_buy = true;
		
		return can_buy;
	}
	
	
	private int CheckItemMax (string item_name)
	{
		return store_info_[item_name]["upgrades"].Count;
	}
	
	
	public void Upgrade (int boss_round, string item_name)
	{
		boss_round_ = boss_round;
		
		if (GetCurrentLevelForParameter(item_name) + 1 < CheckItemMax(item_name) && 
			CheckIfYouCanAfford(boss_round,item_name, GetCurrentLevelForParameter(item_name) +1))
		{
			SellItem(boss_round, item_name);
			BuyItem(boss_round, item_name, GetCurrentLevelForParameter(item_name) +1,false);
			
			if(item_name.ToString().Contains("boss_round_reload_speed"))
				boss_.Stages[boss_round].ReloadSpeed++;
			else if(item_name.ToString().Contains("boss_round_shots_per_wave"))
				boss_.Stages[boss_round].ShotsPerWave++;
			else if(item_name.ToString().Contains("boss_round_environment"))
				boss_.Stages[boss_round].Environment++;
			
			//Messenger.Broadcast("LevelParameterChanged");
			
			Debug.Log ("Available Power "+available_power_+"/"+max_power_);
		}
		else
			Debug.Log ("Maxed Out "+GetCurrentLevelForParameter(item_name));
		
	}
	
	
	public void Downgrade(int boss_level, string item_name)
	{
		boss_round_ = boss_level;
		
		if (GetCurrentLevelForParameter(item_name)  >= 1)
		{
			SellItem(boss_level, item_name);
			BuyItem(boss_level, item_name, GetCurrentLevelForParameter(item_name) -1, false);
			
			if(item_name.ToString().Contains("boss_round_reload_speed"))
				boss_.Stages[boss_round_].ReloadSpeed--;
			else if(item_name.ToString().Contains("boss_round_shots_per_wave"))
				boss_.Stages[boss_round_].ShotsPerWave--;
			else if(item_name.ToString().Contains("boss_round_environment"))
				boss_.Stages[boss_round_].Environment--;
				
			Debug.Log ("Available Power "+available_power_+"/"+max_power_);
			
			//Messenger.Broadcast("LevelParameterChanged");
		}
		else
			Debug.Log ("Bottomed Out "+GetCurrentLevelForParameter(item_name));
	}
	
	public void ResetItem(int boss_round, string item_name)
	{
		boss_round_ = boss_round;
		
		SellItem(boss_round, item_name);
		
		//Buy Item
		JSONNode item = store_info_[item_name]["upgrades"][0];
		
		if (available_power_ >= item["cost"].AsInt)
			available_power_ -= item["cost"].AsInt;
		
		
		if(item_name.ToString().Contains("boss_round_reload_speed"))
			boss_.Stages[boss_round_].ReloadSpeed = 0;
		else if(item_name.ToString().Contains("boss_round_shots_per_wave"))
			boss_.Stages[boss_round_].ShotsPerWave = 0;
		else if(item_name.ToString().Contains("boss_round_environment"))
			boss_.Stages[boss_round_].Environment = 0;
	
	}	
	
	
	void OnApplicationQuit() {
      //boss_temp_data_.SaveToPlayerPrefs("StoreData");
    }
	
	
	private int GetCurrentLevelForParameter (JSONNode item_name)
	{
		int current_value = 0;
		
		if(item_name.ToString().Contains("boss_round_reload_speed"))
			current_value = (int) boss_.Stages[boss_round_].ReloadSpeed;
		else if(item_name.ToString().Contains("boss_round_shots_per_wave"))
			current_value = (int) boss_.Stages[boss_round_].ShotsPerWave;
		else if (item_name.ToString().Contains("boss_round_environment"))
			current_value = (int) boss_.Stages[boss_round_].Environment;
		else if (item_name.ToString().Contains("attack"))
			current_value = (int) boss_.Attack;
		else if (item_name.ToString().Contains("defense"))
			current_value = (int) boss_.Defence;
		else if (item_name.ToString().Contains("level"))
			current_value = (int) boss_.Level;
		/*else if (item_name.ToString().Contains("hp"))
			current_value = (int) boss_.Hp;*/
		else if (item_name.ToString().Contains("power"))
			current_value = (int) boss_.Power;
		else if (item_name.ToString().Contains("rank"))
			current_value = (int) boss_.Rank;
					
	
		return current_value;
	}
	
	
	public JSONNode[] GetCurrentDataForParameter(string item_name)
	{
		int param_level = 0;
		if (item_name == "boss_round_reload_speed")
			param_level = (int) boss_.Stages[boss_round_].ReloadSpeed;
		else if (item_name == "boss_round_shots_per_wave")
			param_level = (int) boss_.Stages[boss_round_].ShotsPerWave;	
		else if (item_name == "boss_round_environment")
			param_level = (int) boss_.Stages[boss_round_].Environment;	
				
		
		JSONNode previous = store_info_[item_name]["upgrades"][param_level-1];
		JSONNode current = store_info_[item_name]["upgrades"][param_level];
		JSONNode next = store_info_[item_name]["upgrades"][param_level+1];
		
		return new JSONNode []{previous,current,next};
	}
	
	
	public void ChangeRound(int new_round_number){
		boss_round_ = new_round_number;
	}
	
	public int GetCurrentRound (){
		return current_weapon_round_;
	}
	
	public void RefreshPowerIndicatorValue(){
		Messenger.Broadcast("UpdatePowerIndicator", (available_power_.ToString()+"/"+max_power_.ToString()),available_power_/max_power_);
	}
	
	
	public float GetAvailablePower(){
		return available_power_;
	}
	
	
	public void UpdateMaxPower(){
		float old_max_power = max_power_;
		max_power_ = store_info_["boss_power_level"]["upgrades"][(int)boss_.Power]["effect"].AsInt;
		available_power_ += (max_power_-old_max_power);
		
		RefreshPowerIndicatorValue();
	}
	
	
	public bool IsCurrentWeapon(string key, int difficulty){
		bool return_value = false;
		
		if (key == current_weapon_key_ && difficulty == current_weapon_difficulty_)
			return_value = true;
			
		return return_value;	
	}
	
	
	public int GetCurrentWeaponCost()
	{
		return current_weapon_cost_;
	}
	
}