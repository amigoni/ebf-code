using UnityEngine;

public sealed class InitState :  FSMState<Game> {
	
	static readonly InitState instance = new InitState();
	public static InitState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static InitState() 
	{
		
	}
	
	private InitState()
	{
	
	}
	
	public override void Enter(Game g) 
	{
		
	}
	
	public override void Execute(Game g) 
	{
		g.ChangeState(GameState.Instance);
	}
	
	public override void Exit(Game g) 
	{

	}
}