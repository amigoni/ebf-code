using UnityEngine;
using System.Collections;

public class ZOrderObject : MonoBehaviour
{ 
	private float baseline_ = 0;
	private bool remove_on_destroy_ = true;
	private bool init_ = false;
	private bool abs_value_ = false;
	
	public const float length_baseline_ = 80.0F;
	
	private float start_time_;
	
	private bool enabled_ = true;
	
	// Use this for initialization
	void Start ()
	{
		Init ();
		start_time_ = Time.time;
	}
	
	public void Init()
	{
		start_time_ = Time.time;
		if(!init_)
		{
			init_ = true;
			
			baseline_ = transform.position.y;
		
			GameObject.Find("Game").GetComponent<Game> ().z_order_manager ().AddZPair (this);
		}
	}
	
	public void Reinit()
	{
		init_ = true;
		baseline_ = transform.position.y;
		GameObject.Find("Game").GetComponent<Game> ().z_order_manager ().AddZPair (this);
	}
	
	// Update is called once per frame
	void Update ()
	{

	}
	
	public void UpdateBaseline(int baseline)
	{
		baseline_ = baseline;
	}
	
	// Destroy!
	void OnDestroy ()
	{
		if( remove_on_destroy_ )
		{
			if(GameObject.Find("Game") != null)
				GameObject.Find("Game").GetComponent<Game> ().z_order_manager ().RemoveZPair (this);
		}
	}
	
	public void SpawnReset()
	{
		init_ = false;
		GameObject.Find("Game").GetComponent<Game> ().z_order_manager ().RemoveZPair (this);
	}
	
	void OnApplicationQuit()
	{
		remove_on_destroy_ = false;
	}
	
	//Gizmos
#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		if(!init_)
		{
			baseline_ = transform.position.y;
			
			Gizmos.DrawLine(new Vector3(transform.position.x - length_baseline_ * 0.5F, baseline_ , 100.0F),
							new Vector3(transform.position.x + length_baseline_ * 0.5F, baseline_ , 100.0F));
		}
		else
    		Gizmos.DrawLine(new Vector3(transform.position.x - length_baseline_ * 0.5F, GetBaseline() ,100.0F),
							new Vector3(transform.position.x + length_baseline_ * 0.5F, GetBaseline() ,100.0F));
			
	}
#endif
	 
	// Use this for initialization
	public void Init(float baseline, bool abs_value)
	{	
		GameObject.Find("Game").GetComponent<Game> ().z_order_manager ().RemoveZPair  (this);
	
		baseline_ = baseline;
		abs_value_ = abs_value;
		init_ = true;
		
		GameObject.Find("Game").GetComponent<Game> ().z_order_manager ().AddZPair (this);
	}
	
	//Getter
	public float GetBaseline()
	{
		if(abs_value_)
			return baseline_;
		
		return transform.position.y + baseline_;
	}
	
	public float start_time()
	{
		return start_time_;
	}
	
	public bool enabled()
	{
		return enabled_;
	}
	
	public void set_enabled(bool enabled)
	{
		enabled_ = enabled;
	}
}
