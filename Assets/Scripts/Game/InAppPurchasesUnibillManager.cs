using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using Unibill.Demo;
using EbFightAPI;
using System;
using System.Text;

public class InAppPurchasesUnibillManager : MonoBehaviour {
	
	JSONNode store_info_;
	private GameObject current_item_;
	private string current_item_key_;
	private string current_item_store_id_;
	private JSONNode unconfirmed_receipts_;
	
	private string last_localized_price_string_ = "";
		
	// Use this for initialization
	void Start () 
	{
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
		
		if (UnityEngine.Resources.Load ("unibillInventory") == null) {
			Debug.LogError("Unibill: You must define your purchasable inventory within the inventory editor!");
			this.gameObject.active = false;
			return;
		}

        // We must first hook up listeners to Unibill's events.
        Unibiller.onBillerReady += onBillerReady;
        Unibiller.onTransactionsRestored += onTransactionsRestored;
        Unibiller.onPurchaseCancelled += onCancelled;
		Unibiller.onPurchaseFailed += onFailed;
        Unibiller.onPurchaseComplete += onPurchased;

        // Now we're ready to initialise Unibill.
		Debug.Log("Unibill: Initialising");
        Unibiller.Initialise();
		
		UnibillError[] errors = Unibiller.Errors;
		for(int i = 0; i < errors.Length; i++)
			Debug.Log(errors[i].ToString());
	}
	
	
    /// <summary>
    /// This will be called when Unibill has finished initialising.
    /// </summary>
    private void onBillerReady(UnibillState state) 
	{
        Debug.Log("Unibill: onBillerReady:" + state);
		Framework.Instance.InstanceComponent<wmf.StaticParams>().ChangeValue("unibill_has_initialized", true);
    }
	
	
    /// <summary>
    /// This will be called after a call to Unibiller.restoreTransactions().
    /// </summary>
    private void onTransactionsRestored (bool success) 
	{
        Debug.Log("Unibill: Transactions restored.");
    }
	
	
    /// <summary>
    /// This will be called when a purchase completes.
    /// </summary>
    private void onPurchased(PurchasableItem item) 
	{
		//Debug.Log("item.localizedPriceString: " + item.localizedPriceString);
		//Debug.Log("item.localizedPrice: " + item.localizedPrice);
		//Debug.Log("Purchase OK: " + item.Id);
        //Debug.Log(string.Format ("{0} has now been purchased {1} times.",
        //                         item.name,
        //                         Unibiller.GetPurchaseCount(item)));
		last_localized_price_string_ = item.localizedPriceString;
		current_item_store_id_ = item.Id;
		current_item_key_ = GetCurrentItemKeyFromStoreID(item.Id);
		
		
		string [] purchases = Unibiller.GetAllPurchaseReceipts(item);
		
		if(purchases.Length > 0)
		{
			Debug.Log("Unibill: Last Receipt: "+ purchases[0]);
		
			byte[] bytesToEncode = Encoding.UTF8.GetBytes (purchases[0]);
			string encodedText = Convert.ToBase64String (bytesToEncode);
	
#if UNITY_IPHONE
			if (Framework.Instance.InstanceComponent<ServerAPI>().IsConnected() == true)
				Framework.Instance.InstanceComponent<ServerAPI>().FinalizePurchaseIOS(encodedText, OnConfirmedReceipts);
			else
				SaveReceiptToSendLater(item.Id, current_item_key_, encodedText);
#elif UNITY_ANDROID
			Framework.Instance.InstanceComponent<ServerAPI>().FinalizePurchaseAndroid(purchases[0], OnConfirmedReceipts);
#endif
			
		}
    }
	
	private string GetCurrentItemKeyFromStoreID(string store_id)
	{
		string item_key = "";
	
		for (int i = 0; i < store_info_.Count; i++)
		{
			if (store_info_[i]["category"].ToString() == "white_milk_coins")
			{
				if (store_info_[i]["apple_key"].ToString() == store_id)
					item_key = store_info_[i]["key"].ToString();
			}
		}
		
		return item_key;
	}
	
	
	private void SaveReceiptToSendLater(string store_id, string item_key, string receipt)
	{
		string json_string = "{\"id\":\""+store_id+"\",\"key\":\""+item_key+"\",\"receipt\":\""+receipt+"\"}";
		Debug.Log("Unibill: Saving Receipt JSON ");
		Debug.Log(json_string);
		
		//Fake name to fool people a bit
		PlayerPrefs.SetString("configurazione_dd_e_altro", json_string);
		PlayerPrefs.Save();
		Framework.Instance.InstanceComponent<ServerAPI>().CloseConnectingPopUp();
		Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().gameObject.SetActive(true);
		Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("oh oh Error","There was an error confirmiing your purchase.\nWe'll try again next time you connect");
	}
	
	
	public void RedeemUnconfirmedReceipt()
	{
		if (PlayerPrefs.HasKey("configurazione_dd_e_altro") == true)
		{
			Debug.Log("Unibill: There is an unconfirmed Receipt. Going to redeem it");
			//Fake name to fool people a bit
			string receipt_data = PlayerPrefs.GetString("configurazione_dd_e_altro");
			JSONNode data =  JSON.Parse(receipt_data);
			current_item_key_ = data["key"].ToString();
			current_item_store_id_ = data["id"].ToString();
			Debug.Log("Unibill: Redeemed Receipt id: "+current_item_key_);
			
			Framework.Instance.InstanceComponent<ServerAPI>().FinalizePurchaseIOS(data["receipt"].ToString(), OnConfirmedReceipts);
		}
		else
		{
			Debug.Log("Unibill: No receipts to redeem");
		}
	}
	
	
	private void OnConfirmedReceipts(ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success)
		{
			Debug.Log("Unibill: Success on Confirmed Receipt");
			//current_item_.GetComponent<CoinStoreItemPrefab>().OnFinalize(status, data);
			OnFinalize(status, data);
		}
		else
		{
			int verification_status = Int32.Parse(data.ErrorMessage);
			Debug.Log("Unibill: Error in confirming receipt: "+verification_status);
			
			if (verification_status == 21000 || verification_status == 21002 || verification_status == 21003 || verification_status == 21004)
			{
				//Delete the receipt, it's being hacked.
				Debug.Log("Unibill: Deleting Receipt. It's not a good receipt ");
				if (PlayerPrefs.HasKey("configurazione_dd_e_altro")) 
					PlayerPrefs.DeleteKey("configurazione_dd_e_altro");
			}
			
			Debug.Log("Unibill: Error on Confirm Receipt "+data.ErrorMessage);
			Framework.Instance.InstanceComponent<ServerAPI>().NotifyError();
		}	
		
		Framework.Instance.InstanceComponent<ServerAPI>().CloseConnectingPopUp();
	}
	

    /// <summary>
    /// This will be called if a user opts to cancel a purchase
    /// after going to the billing system's purchase menu.
    /// </summary>
    private void onCancelled(PurchasableItem item) 
	{
        Debug.Log("Unibill: Purchase cancelled: " + item.Id);
		Framework.Instance.InstanceComponent<ServerAPI>().CloseConnectingPopUp();
		//Framework.Instance.InstanceComponent<ServerAPI>().NotifyError("Can't Connect");
    }
    
	
    /// <summary>
    /// This will be called is an attempted purchase fails.
    /// </summary>
    private void onFailed(PurchasableItem item) 
	{
		Debug.Log("Unibill: Purchase failed: " + item.Id);
		Framework.Instance.InstanceComponent<ServerAPI>().CloseConnectingPopUp();
		Framework.Instance.InstanceComponent<ServerAPI>().NotifyError();
    }
	
	
	public void BuyItem ( string key, GameObject current_item)
	{
		RedeemUnconfirmedReceipt();
		current_item_ = current_item;
		Unibiller.initiatePurchase(key);
	}
	

	public string last_localized_price_string()
	{
		return last_localized_price_string_;
	}
	

	public void OnFinalize(ResponseStatus status, JSONResult data)
	{
		string key_ = current_item_key_;
		string localizedPriceString = Unibiller.GetPurchasableItemById(current_item_store_id_).localizedPriceString;
		string price_ = localizedPriceString.Substring(1);
		string currency_ = localizedPriceString.Remove(1);
		string id = Unibiller.GetPurchasableItemById(current_item_store_id_).Id;
		
		string description_ = store_info_[current_item_key_]["description"].ToString();	
		int tier_number = 0;
		string tier_str = store_info_[key_]["apple_key"].ToString().Substring( 39 );
		//Debug.Log("OnFinalize TIER STR: " + tier_str );
		if(!int.TryParse( tier_str, out tier_number))
			tier_number = 0;
		
		if (status == ResponseStatus.Success)
		{
			GameObject popup_window_ = (GameObject) Instantiate(Resources.Load("Framework/PopUpWindowPrefab"));
			popup_window_.transform.position = new Vector3(popup_window_.transform.position.x, popup_window_.transform.position.y, 1F);
			popup_window_.GetComponent<PopUpWindowPrefab>().Init("Success","Purchase Complete");
			
			EbFightAPI.Player old_data_player = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer();
			EbFightAPI.Player data_player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(data_player);
			
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", data_player.WhiteCoins.ToString());
				
			//##Analytics
			int wmc_bought = Convert.ToInt32(data_player.WhiteCoins - old_data_player.WhiteCoins);	
			PlayerData player_data = Framework.Instance.InstanceComponent<PlayerData>();
			int saved_wmc_bought = player_data.GetInt("hard_currency_bought");
			wmc_bought += saved_wmc_bought;
			player_data.SetValue("hard_currency_bought", wmc_bought);
			
			//int white_coins_balance = (int) data_player.WhiteCoins;
			//Framework.Instance.InstanceComponent<Analitycs>().SetCommonValue("hard_currency", white_coins_balance);	
			
#if UNITY_IPHONE 
			KongregateAPI.GetAPI().Mobile.TrackPurchase(store_info_[key_]["apple_key"].ToString(), 1);
#endif
			Messenger.Broadcast("RefreshCoins");
			
			// Fake name to hide it
			if (PlayerPrefs.HasKey("configurazione_dd_e_altro")) 
				PlayerPrefs.DeleteKey("configurazione_dd_e_altro");
			//Analitycs
			Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			analitycs_.AddEvent(KeenIOEventType.iap_currency_offer);
			analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "hard_currency_reward", wmc_bought);
			analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "type", key_);
			analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "currency_type", currency_);
			analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "currency_amount", price_);
			analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "context_of_offer", description_);
			analitycs_.AddNewKey(KeenIOEventType.iap_currency_offer, "itunes_packege_id", id);
			analitycs_.SendEvent(KeenIOEventType.iap_currency_offer);
			
		}
		else
		{
			Debug.Log("Unibill: Verification Error Message :"+data.ErrorMessage);
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().gameObject.SetActive(true);
			Framework.Instance.InstanceComponent<ServerAPI>().pop_up_window().Init("oh oh Error","There was an error processing your purchase.\nPlease try again.");
		}
	}			
	
	/*
	private void LoadUnconfirmedReceipts()
	{
		string data;
		
		data = JSON.Parse(PlayerPrefs.GetString("unconfirmed_receipts"));
		
		if(!string.IsNullOrEmpty(data))
        {
          	unconfirmed_receipts_ = JSON.Parse(data);
		}
		else
		{
			unconfirmed_receipts_ = new JSONNode();
		}
	}
	
	
	public void SaveUnconfirmedReceipts()
	{
		unconfirmed_receipts_.SaveToPlayerPrefs("unconfirmed_receipts");
	}
	*/
}
