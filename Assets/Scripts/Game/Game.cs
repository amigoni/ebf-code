using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
	public enum DDResultType
	{
		none,
		won,
		passed,
		died,
		perfect
	};
	
	public enum BossResultType
	{
		none,
		destroyed,
		survived,
		won,
		annihilated
	};
		
	public enum ScoreValue
	{
		money = 100,
		enemy = 200,
		crates = 150,
		hit_boss = 100,
		combo = 5000,
		time = 1,
		score_count	
	};
	
	public enum GameMode
	{
		singleplayer,
		multiplayer,
		boss_training
	}
	
	private static FiniteStateMachine<Game> fsm_;
	private AudioManager audio_manager_;
	private ZOrderManager z_order_manager_;
	private Boss boss_;
	private Player player_;
	private Store store_;
	private PlayerData player_data_;
	private GameUI gui_;
	private MissionsManager mission_manager_;
	private PowerPunchManager power_punch_manager_;
	private BossBaloonContainer boss_baloon_container_;
	private Options options_;
	public GameObject pause_scene_obj_;
	private GameObject pause_scene_;
	public GameObject options_scene_obj_;
	private GameObject options_scene_;
	public GameObject resurrection_scene_;
	public GameObject loading_scene_;
	private int level_ = 0;
	public int real_level_ = 0;
	private long score_ = 0;
	private int multiplier_ = 1;
	public GameObject cleaner_explosion_;
	private int number_of_resurrections_ = 0;
	private bool perfect_on_ = true;
	//Missions Switches
	private bool no_hit_till_level_enabled = true;
	private bool chainsaw_no_hit = true;
	private bool power_punch_has_been_used = false;
	private bool paused_ = false;
	//private float init_time_;
	private GainsSession gains_session_;
	
	//Common multiplayer & training
	private string session_id_;
	
	//multiplayer
	private List<DDResultType> dd_results_;
	private List<BossResultType> boss_results_;
	private int multiplayer_ammo_ = 1;
	private string contender_name_ = "";
	
	private PlayerWalkableArea player_walkable_area_ = null;
	
	//Analitycs
	private Analitycs analitycs_;
	private System.Diagnostics.Stopwatch length_ms_;
	
	//Game Mode
	private GameMode game_mode_ = GameMode.singleplayer;
	
	public void Awake ()
	{	
		fsm_ = new FiniteStateMachine<Game> ();
		fsm_.Configure (this, InitState.Instance);
		
		audio_manager_ = Framework.Instance.audio_manager ();
		audio_manager_.PlayBackgroundMusic ("Endless Boss Game");
		
		z_order_manager_ = GetComponent<ZOrderManager> ();
		boss_ = GameObject.Find ("BossPrefab").GetComponent<Boss> ();
		player_ = GameObject.Find ("Player").GetComponent<Player> ();
		gui_ = GameObject.Find ("GUI").GetComponent<GameUI> ();
		boss_baloon_container_ = GameObject.Find ("BossBaloonContainer").GetComponent<BossBaloonContainer> (); 
		boss_baloon_container_.gameObject.SetActive (false);
			
		mission_manager_ = this.GetComponent<MissionsManager> ();
		mission_manager_.Init (true, true);
		mission_manager ().NewRun ();
		
		//Get multipler
		multiplier_ = mission_manager_.current_mission_id_;// PlayerPrefs.GetInt ("multiplier");
		//first time save the multipler
		if (multiplier_ == 0) {
			multiplier_ = 1;
			//PlayerPrefs.SetInt ("multiplier", multiplier_);
		}
		
		store_ = Framework.Instance.InstanceComponent<Store> ();
		player_data_ = Framework.Instance.InstanceComponent<PlayerData> ();
		
		if (Framework.Instance.InstanceComponent<wmf.StaticParams> ().ContainsKey ("IsMultiplayer")) {
			if ((bool)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsMultiplayer")) {
				game_mode_ = GameMode.multiplayer;
				mission_manager_.Disabled ();
				
				EbFightAPI.MatchSession match_session = 
					(EbFightAPI.MatchSession)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("MatchSession");
				Framework.Instance.InstanceComponent<wmf.StaticParams> ().Remove ("MatchSession");			
				
				Random.seed = (int)match_session.Seed;
				boss_.GetComponent<LoadCustomBoss> ().set_boss_config (match_session.Contender.Boss);
				session_id_ = match_session.SessionId;
				contender_name_ = match_session.Contender.Username;
				gui_.SetContenderName (contender_name_);
			}
		}
		
		//Training
		if (Framework.Instance.InstanceComponent<wmf.StaticParams> ().ContainsKey ("IsTraining")) {
			if ((bool)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("IsTraining")) {
				EbFightAPI.MatchSession match_session = 
					(EbFightAPI.MatchSession)Framework.Instance.InstanceComponent<wmf.StaticParams> ().Get ("MatchSession");
				Framework.Instance.InstanceComponent<wmf.StaticParams> ().Remove ("MatchSession");
				session_id_ = match_session.SessionId;

				game_mode_ = GameMode.boss_training;
				mission_manager_.Disabled ();		
				EbFightAPI.Boss boss = Framework.Instance.InstanceComponent<ServerAPI> ().GetBoss ();
				boss_.GetComponent<LoadCustomBoss> ().set_boss_config (boss);
				
				gui_.SetContenderName ("Your Boss");
			}
		}
		
		if (game_mode_ == GameMode.singleplayer)
			gui_.SetContenderName ("");
		
		player_walkable_area_ = GameObject.Find ("PlayerWalkableArea").GetComponent<PlayerWalkableArea> ();
		
		//Analitycs
		analitycs_ = Framework.Instance.InstanceComponent<Analitycs> ();
#if UNITY_EDITOR
		//if(analitycs_.GetCurrentRunEvent() == null)
		//	analitycs_.NewRun(null);
#endif
		analitycs_.AddNewKey (KeenIOEventType.run_end, "pause_time_ms", 0.0f);
		analitycs_.AddNewKey (KeenIOEventType.run_end, "length_ms", 0.0f);
		analitycs_.AddNewKey (KeenIOEventType.run_end, "num_resurrections", 0);
	
		length_ms_ = new System.Diagnostics.Stopwatch ();
		length_ms_.Start ();
		
		
		//Tutorial
		if (player_data_.GetBool ("tutorial_in_game_done") == false) {
			mission_manager_.Disabled ();
#if UNITY_IPHONE && !UNITY_EDITOR
			//AdX.SendTutorialEnd();
#endif
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		//Load Options
		options_ = Framework.Instance.InstanceComponent<Options> ();

		player_.gameObject.GetComponent<PlayerMoveController> ().UpdateOptions (options_.options_params ());
		
		//TO-DO add system to persistant object
		PlayerDetails player_details = new PlayerDetails ();
		player_details.Init ();
		player_.set_player_details (player_details);
		gains_session_ = new GainsSession ();
		gains_session_.xp_ = player_details.current_xp ();
		
		gui_.UpdateMultiplier (multiplier_, false);
		gui_.SetLifes (player_.health ());
		
		GameObject fight_sprite = GameObject.Find ("GUI/GameSceneUI/Fight_sprite");
		fight_sprite.GetComponent<MeshRenderer> ().enabled = true;
		fight_sprite.animation.Play ();
		audio_manager_.PlayOnPlayer ("14_Voice Fight", 1.0F, 1.0F, false);
		
		power_punch_manager_ = GetComponent<PowerPunchManager> ();
		power_punch_manager_.Init ();

		analitycs_.NewRound ();

		analitycs_.AddEvent (KeenIOEventType.menu_opened);	
		analitycs_.AddNewKey (KeenIOEventType.menu_opened, "menu_name", "GameScene");
		analitycs_.SendEvent (KeenIOEventType.menu_opened);
		
		//GetComponent<BonusManager>().SpawnBonus(BonusType.invulnerability);
	}
	
	// Update is called once per frame
	private void Update ()
	{
		fsm_.Update ();
	}
	
	public void ChangeState (FSMState<Game> s)
	{
		fsm_.ChangeState (s);
	}
	
	//game Behaviour
	public void EnterInGame ()
	{
	
	}
	
	//GUI button behaviour
	public void StartGame ()
	{
		ChangeState (GameState.Instance);
	}
	
	public void PlayerHitted (float hit_value, bool pushed, float x_push)
	{
		hit_value *= player_.HitValueMult ();
		
		if (!player_.GodMode ()) {
			player_.Hitted (hit_value);
		
			DoCameraShake (3, 2);
			
			float player_health_ = player_.health ();
			gui_.SetLifes (player_health_);
			gui_.PlayHitAnimation ();
			
			if (player_health_ <= 0) {					
				if (game_mode_ == GameMode.singleplayer) {
					if (number_of_resurrections_ < 1 && (player_data_.GetInt ("white_coins") + GetWhiteMilkCoins ()) >= store_.GetResurrectCost ()) {
						if (!IsInvoking ("OpenResurrect"))
							Invoke ("OpenResurrect", 3.0F);	
					} else {
						if (!IsInvoking ("EndGame")) {
							Invoke ("EndGame", 3.0F);
							//Analitycs
							analitycs_.AddNewKey (KeenIOEventType.run_end, "end_type", "Died");
						}
					}	
				} else {
					//Collect results
					dd_results_ [real_level_] = DDResultType.died;
					MultiplayerEndGame (false);
					gui_.SetRoundResult (real_level_, boss_results_ [real_level_]);
				}
				//Analitycs
				analitycs_.GetCurrentRound ().is_win = false;
			} else if (pushed) {
				player_.Pushed (x_push);
			}
			
			CancelPerfect ();
			no_hit_till_level_enabled = false;
		}
	}
	
	public void TimeFinished ()
	{
		if (game_mode_ == GameMode.singleplayer) {
			EndGame ();			
		} else {
			dd_results_ [real_level_] = DDResultType.passed;
			CancelPerfect ();
			boss_.ForceChangeSetup ();
		} 
		
		gui_.TimeIsUp ();
		//Analitycs	
		analitycs_.AddNewKey (KeenIOEventType.run_end, "end_type", "Time Finished");
		analitycs_.GetCurrentRound ().is_win = false;
	
		mission_manager_.UpdateMission (MissionsManager.MissionType.time_finished, 1);
	}
	
	public void MultiplayerEndGame (bool win)
	{
		if (win)
			Invoke ("EndGame", 6.0f);
		else
			Invoke ("EndGame", 3.0f);
	}
	
	public void EndGame ()
	{		
		Framework.Instance.InstanceComponent<wmf.StaticParams> ().ChangeValue ("IsTraining", false);
		
		SpawnLoadingScene ();
			
		ResumeAll ();
		//Reset utilities Data
		player_data_.SetArrayValue ("utilities_equipped", "double_bubble", false);
		player_data_.SetArrayValue ("utilities_equipped", "double_magnet", false);
		player_data_.SetArrayValue ("utilities_equipped", "life_boost", false);
		player_data_.SetArrayValue ("utilities_equipped", "att_boost", false);
		player_data_.SetArrayValue ("utilities_equipped", "ammo_boost", false);
		
		iTween.Stop ();
		
		DontDestroyOnLoad (GameObject.Find ("StaticParams"));
		
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().score_ = score_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().xp_ = gains_session_.xp_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().coins_ = gains_session_.coins_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().white_coins_ = gains_session_.white_coins_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().prev_mission_ = mission_manager_.current_mission ();	
		//multiplayer
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().session_id_ = session_id_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().dd_results_ = dd_results_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().boss_results_ = boss_results_;
		GameObject.Find ("StaticParams").GetComponent<StaticParams> ().contender_name_ = contender_name_;
		
		mission_manager_.Save ();
		
		player_.player_details ().set_current_xp (gains_session_.xp_);
		player_.player_details ().EndGame (game_mode_);
		
		if (game_mode_ == GameMode.multiplayer) {
			//mission_manager_.EndRun();
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "EndGameMultiplayer");
			Application.LoadLevel ("GenericLoadingScene");
		} else if (game_mode_ == GameMode.boss_training) {
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "EndGameTraining");
			Application.LoadLevel ("GenericLoadingScene");
		} else if (game_mode_ == GameMode.singleplayer){	
			if (player_data_.GetBool ("tutorial_in_game_done") == false) {
				player_data_.SetValue ("tutorial_in_game_done", true);
				Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "MainMenu");
				Application.LoadLevel ("GenericLoadingScene");
			} else {
				if (player_.player_details ().starting_level () < player_.player_details ().current_level ()) {
					if (Framework.Instance.InstanceComponent<wmf.StaticParams> ().ContainsKey ("IsLevelUpDD") == false)
						Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("IsLevelUpDD", true);
					else
						Framework.Instance.InstanceComponent<wmf.StaticParams> ().ChangeValue ("IsLevelUpDD", true);
					
					mission_manager_.EndRun ();
					Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "LevelUp");
					Application.LoadLevel ("GenericLoadingScene");
				} else {
					mission_manager_.EndRun ();	
					Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "EndGame");
					Application.LoadLevel ("GenericLoadingScene");
				}
				
			}
		}
		
		analitycs_.SetValue (KeenIOEventType.run_end, "length_ms", GetLengthMs ());
	}
	
	public void OpenResurrect ()
	{
		PauseAll ();
		
		if (game_mode_ == GameMode.singleplayer)
		{
			if (player_data_.GetBool ("tutorial_in_game_done") == false)
				EndGame ();
			else	
				Instantiate (resurrection_scene_);
		}
		else
			EndGame ();
	}
	
	public void Resurrect ()
	{
		number_of_resurrections_ ++;
		
		player_.Resurrect ();
		gui_.SetLifes (player_.health ());
		gui_.AddTimeLeft (50);
		ResumeAll ();
	}
	
	
	//Camera shake
	public void DoCameraShake (int shake_value, float shake_time)
	{
		iTween.ShakePosition (Camera.main.gameObject, 
						iTween.Hash ("x", shake_value, 
									"y", shake_value, 
									"onComplete", "ResetCameraPosition",
									"onCompleteTarget", gameObject, 
									"time", shake_time));
	}
	
	void ResetCameraPosition ()
	{
		Camera.main.transform.position = new Vector3 (0.0F, 0.0F, Camera.main.transform.position.z);
	}
	
	//Pause behaviors
	public void PauseAll ()
	{
		iTween.Pause ();
		boss_.Pause ();
		Time.timeScale = 0;
		paused_ = true;
		player_.gameObject.GetComponent<PlayerMoveController> ().Pause ();
		
		GameObject[] game_objects = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject game_object in game_objects) {
			game_object.GetComponent<Enemy> ().Pause ();
		}
		
		//Disable Controls
		GameObject.Find ("GUI").GetComponent<DashController> ().SetControllerEnabled (false);
		GameObject.Find ("GUI").GetComponent<PowerPunchHoldButton> ().SetPowerPunchEnabled (false);
		GameObject.Find ("Player").GetComponent<PlayerAttackController> ().SetControllerEnabled (false);
		
#if UNITY_IPHONE || UNITY_ANDROID
		GameObject.Find("GUI/GestureController").GetComponent<GestureController>().SetControllerEnabled(false);
#endif
	}
	
	public void ResumeAll ()
	{
		Time.timeScale = 1;
		paused_ = false;
		iTween.Resume ();
		boss_.Resume ();
		player_.gameObject.GetComponent<PlayerMoveController> ().Resume ();
		
		GameObject[] game_objects = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject game_object in game_objects) {
			game_object.GetComponent<Enemy> ().Resume ();
		}
		
		//Enable Controls
		GameObject.Find ("GUI").GetComponent<DashController> ().SetControllerEnabled (true);
		GameObject.Find ("GUI").GetComponent<PowerPunchHoldButton> ().SetPowerPunchEnabled (true);
		GameObject.Find ("Player").GetComponent<PlayerAttackController> ().SetControllerEnabled (true);
#if UNITY_IPHONE || UNITY_ANDROID
		GameObject.Find("GUI/GestureController").GetComponent<GestureController>().SetControllerEnabled(true);
#endif
	}
	
	//Pause callback
	public void PauseGame ()
	{
		PauseAll ();
		
		//Instantiate and initializes prefab with script
		pause_scene_ = Instantiate (pause_scene_obj_) as GameObject;
		
		//set target object
/*		tk2dButton[] buttons = pause_scene_.GetComponentsInChildren<tk2dButton> ();
		foreach (tk2dButton button in buttons) {
			if (button.name == "PauseButton")
				button.targetObject = this.gameObject;
		}*/	
		pause_scene_.GetComponent<PauseScene> ().Init (score_, game_mode_);
		
		GameObject.Find ("PauseButton").GetComponent<BoxCollider>().enabled = false;
		
		Framework.Instance.audio_manager ().PlayUISound ("37_Menu Button Select", 1.0F, 1.0F, false);
	}
	
	public void ResumeGame ()
	{
		ResumeAll ();
		CloseSettingScene ();
		
		Destroy (pause_scene_);
		
		GameObject.Find ("PauseButton").GetComponent<BoxCollider>().enabled = true;
	}
	
	public void ExitGame ()
	{
		SpawnLoadingScene ();
		
		//Analitycs
		mission_manager_.EndRun ();
		
		analitycs_.GetCurrentRound ().is_win = false;
		
		analitycs_.AddNewKey (KeenIOEventType.run_end, "end_type", "Went to Menu");
		analitycs_.AddNewKey (KeenIOEventType.run_end, "avg_frame_rate", GameObject.Find ("Game").GetComponent<FPSCounter> ().GetAvgFPS ());
		analitycs_.AddNewKey (KeenIOEventType.run_end, "min_frame_rate", GameObject.Find ("Game").GetComponent<FPSCounter> ().GetMinFPS ());
		analitycs_.EndRun ();
	
		iTween.Stop (); 
		mission_manager_.Save ();
		
		Time.timeScale = 1;
		
		if (player_data_.GetBool ("tutorial_in_game_done") == false) {
			player_data_.SetValue ("tutorial_in_game_done", true);

		}
		
		Framework.Instance.InstanceComponent<wmf.StaticParams> ().ChangeValue ("IsTraining", false);
		
		//Application.LoadLevel ("MainMenu");
		if(game_mode_ == GameMode.boss_training)
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "BossDetail");
		else
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("next_scene", "MainMenu");
		Application.LoadLevel ("GenericLoadingScene");
	}
	
	public void OpenSettingScene ()
	{
		options_scene_ = Instantiate (options_scene_obj_) as GameObject;
		//set target object
		tk2dButton[] buttons = options_scene_.GetComponentsInChildren<tk2dButton> ();
		foreach (tk2dButton button in buttons)	
			button.targetObject = this.gameObject;
	}
	
	public void CloseSettingScene ()
	{
		Destroy (options_scene_);
		
		player_.gameObject.GetComponent<PlayerMoveController> ().UpdateOptions (options_.options_params ());
	}
	
	//End pause scene
	
	public void PowerPunchPause ()
	{
		iTween.Pause ();
		boss_.Pause ();
		
		player_.gameObject.GetComponent<PlayerMoveController> ().Pause ();
	}
	
	public void PowerPunchResume ()
	{
		iTween.Resume ();
		boss_.Resume ();
		
		player_.gameObject.GetComponent<PlayerMoveController> ().Resume ();
	}
	
	//Crates callbacks
	public void AddScore (ScoreValue score_type)
	{
		long old_score = score_;
		
		score_ += (long)score_type * multiplier_;
		
		gui_.UpdateScore (score_);
		
		//mission_manager_.UpdateMission(MissionsManager.MissionType.max_point, (int)score_type * multiplier_);
	}
	
	public void AddCoin ()
	{
		gains_session_.coins_ += 1;
		gui_.UpdateCoins (gains_session_.coins_);
	}
	
	public void AddWhiteCoin ()
	{
		gains_session_.white_coins_ += 1;
	}
	
	public void AddXp (int add_value)
	{
		gains_session_.xp_ += add_value;
	}
	
	public void AddHeart ()
	{
		player_.AddHeart ();
		
		float player_lifes_ = player_.health ();
		gui_.SetLifes (player_lifes_);
	}
	
	public void SetHealth (float player_health)
	{
		gui_.SetLifes (player_health);
	}
	
	public void AddMultiplier (int multiplier_value)
	{
		multiplier_ += multiplier_value;
		
		if (multiplier_ > 99)
			multiplier_ = 99;
		
		gui_.UpdateMultiplier (multiplier_, true);
	}
	
	public void ActivateMagnetics ()
	{
		player_.ActivateMagnetics ();
	}
	
	public void InflamesBoss ()
	{
		boss_.Inflames ();
	}
	
	public void FreezeBoss (float freeze_time)
	{
		boss_.Freeze (freeze_time);
	}
	
	public void SetGodModePower ()
	{
		player_.SetGodModePower ();
	}
	
	public void SetGodMode ()
	{
		player_.SetGodMode ();	
	}
	
	public void SetBigShoot ()
	{
		player_.SetBigShotNumber (5);
	}
	/*
	public void SetRestoring()
	{
		damage_wall_.RestoreWall();
	}
	*/
	public int DestroyAllEnemyInScene ()
	{
		int destroyed = 0;
		GameObject[] game_objects = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject game_object in game_objects) {
			if (!game_object.GetComponent<Enemy> ().on_boss ()) {
				GameObject obj = PoolManager.Spawn (cleaner_explosion_);
				obj.transform.position = game_object.transform.position;
					
				game_object.GetComponent<Enemy> ().SpawnBonus ();
				PoolManager.Despawn (game_object);
				
				Enemy enemy_script = game_object.GetComponent<Enemy> ();
				boss_.combo_manager ().InvalidateCombo (enemy_script.group_id (), enemy_script.id ());
			}
			
			destroyed++;
		}
		
		//moles
		game_objects = GameObject.FindGameObjectsWithTag ("Mole");
		foreach (GameObject game_object in game_objects) {
		
			GameObject obj = PoolManager.Spawn (cleaner_explosion_);
			obj.transform.position = game_object.transform.position;
					
			game_object.GetComponent<Enemy> ().SpawnBonus ();
			PoolManager.Despawn (game_object);
			
			destroyed ++;
		}
		
		return destroyed;
	}
	
	public int DestroyAllEnemyOnBoss ()
	{
		int destroyed = 0;
		GameObject[] game_objects = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject game_object in game_objects) {
			if (!game_object.GetComponent<Enemy> ().no_parent ()) {
				GameObject obj = PoolManager.Spawn (cleaner_explosion_);
				obj.transform.position = game_object.transform.position;
					
				game_object.GetComponent<Enemy> ().SpawnBonus ();
				PoolManager.Despawn (game_object);
				
				Enemy enemy_script = game_object.GetComponent<Enemy> ();
				boss_.combo_manager ().InvalidateCombo (enemy_script.group_id (), enemy_script.id ());
			}
			
			destroyed++;
		}
		
		boss_.ClearEnemyToSpawn ();
		
		return destroyed;
	}
	
	//getter / setter
	public GameUI gui ()
	{
		return gui_;
	}
	
	public int level ()
	{
		return level_;
	}
	
	public void set_level (int level)
	{
		level_ = level;
	}
	
	public long score ()
	{
		return score_;
	}
	
	public int GetCoins ()
	{
		return gains_session_.coins_;	
	}
	
	public int GetWhiteMilkCoins ()
	{
		return gains_session_.white_coins_;	
	}
	
	public int multiplier ()
	{
		return multiplier_;
	}
	
	public int number_of_resurrections ()
	{
		return number_of_resurrections_;
	}
	
	public ZOrderManager z_order_manager ()
	{
		return z_order_manager_;
	}
	
	public MissionsManager mission_manager ()
	{
		return mission_manager_;
	}
	
	public Options options ()
	{
		return options_;
	}
	
	public void set_options (Options options)
	{
		options_ = options;
		player_.gameObject.GetComponent<PlayerMoveController> ().UpdateOptions (options_.options_params ());
	}
		
	public bool NextLevel ()
	{	
		if (game_mode_ == GameMode.singleplayer) {
			if (level_ < boss_.level_setup ().max_level () - 1) {
				level_++;
			} else {
				//Loop the game
				level_ -= 4;
			}
		} else if(game_mode_ == GameMode.multiplayer || game_mode_ == GameMode.boss_training){
			if (level_ < 2)
				level_++;
			else
				return false;
		}
		
		return true;
	}
	
	public void NextState ()
	{
		bool perfect = CheckForPerfect ();
		
		if (boss_.IsPhysicAttackSetup () == true)
			CheckForChainSawNoHit ();
		
		CheckPowerPunchHasBeenUsed ();
		
		bool end = false;
		
		if (game_mode_ == GameMode.multiplayer || game_mode_ == GameMode.boss_training) {
			if (real_level_ < 2) {
				//Result Boss
				if (boss_.need_to_change_setup ())
					boss_results_ [real_level_] = BossResultType.destroyed;
				else
					boss_results_ [real_level_] = BossResultType.survived;
				
				//Result DD
				if (dd_results_ [real_level_] == DDResultType.none) {
					if (perfect) {
						dd_results_ [real_level_] = DDResultType.perfect;
						if (boss_results_ [real_level_] == BossResultType.destroyed)
							boss_results_ [real_level_] = BossResultType.annihilated;
					} else
						dd_results_ [real_level_] = DDResultType.won;
				}
					
				gains_session_.stage_results_ [real_level_] = true;			
			} else {
				//Result Boss
				if (boss_.need_to_change_setup ())
					boss_results_ [real_level_] = BossResultType.destroyed;
				else
					boss_results_ [real_level_] = BossResultType.survived;
					
				//Result DD
				if (dd_results_ [real_level_] == DDResultType.none) {
					if (perfect) {
						dd_results_ [real_level_] = DDResultType.perfect;
						if (boss_results_ [real_level_] == BossResultType.destroyed)
							boss_results_ [real_level_] = BossResultType.annihilated;
					} else
						dd_results_ [real_level_] = DDResultType.won;				
					//Analitycs
					analitycs_.GetCurrentRound ().is_win = true;
				}
				
				MultiplayerEndGame (true);
				real_level_--;
				gui_.ShowFightOver ();
				end = true;
			}							
		} else {
			GetComponent<BonusManager> ().SpawnWhiteCoinsNew ();
		}
		
		real_level_ ++;
		
		if (!end) {
			gui_.SetLevel (real_level_ + 1, perfect, (game_mode_ != GameMode.singleplayer));
		}
		if (game_mode_ == GameMode.multiplayer || game_mode_ == GameMode.boss_training)
			gui_.SetRoundResult (!end ? real_level_ - 1 : real_level_, boss_results_ [!end ? real_level_ - 1 : real_level_]);
		
		//Save max level unlocked
		if (real_level_ > player_data_.GetInt ("max_level_reached_single_player"))
			player_data_.SetValue ("max_level_reached_single_player", real_level_);
	
		//Analitycs
		analitycs_.NewRound ();
	}
	
	public int RealLevel ()
	{
		return real_level_;
	}
	
	public bool paused ()
	{
		return paused_;
	}
	
	public bool CheckForPerfect ()
	{
		if (perfect_on_ == true) {
			GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.level_perfect, 1);
			
			if (no_hit_till_level_enabled == true)
				GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.no_hits_to_level, 1);
			
			GameObject perfect_sprite = GameObject.Find ("GUI/GameSceneUI/Perfect_sprite");
			perfect_sprite.GetComponent<MeshRenderer> ().enabled = true;
			perfect_sprite.animation.Play ();
			audio_manager_.Play ("15_Voice Perfect", transform, 1.0F, 1.0F, false);
			
			if (game_mode_ == GameMode.singleplayer) {
				for (int i = 0; i < 12; i++)
					GetComponent<BonusManager> ().SpawnBonus (BonusType.money);
				
				GameObject.Find ("Game").GetComponent<BonusManager> ().SpawnRandomCrate ();
			}	
			return true;
		}
			
		perfect_on_ = true;
		return false;
	}
			
	public void CreateResults ()
	{
		int max_level = boss_.level_setup ().max_level ();
		dd_results_ = new List<DDResultType> (max_level);
		boss_results_ = new List<BossResultType> (max_level);
		
		for (int i = 0; i < max_level; i++)
			dd_results_.Add (DDResultType.none);
	
		for (int i = 0; i < max_level; i++)
			boss_results_.Add (BossResultType.none);
	}

	public void CancelPerfect ()
	{
		perfect_on_ = false;
		//Debug.Log("Perfect is OFF");
	}
	
	public void CheckForChainSawNoHit ()
	{
		if (chainsaw_no_hit == true) {
			GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.chainsaw_no_hit, 1);	
		}
		
		chainsaw_no_hit = true;
	}
	
	public void CancelChainSawNoHit ()
	{
		chainsaw_no_hit = false;
	}
	
	public void CheckPowerPunchHasBeenUsed ()
	{
		if (power_punch_has_been_used == false) {
			GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.no_power_punch_to_level, 1);	
		}
	}
	
	public void CancelPowerPunchHasBeenUsed ()
	{
		power_punch_has_been_used = true;
	}
	
	
	//TimeLeft
	public void SetTimeLeft (int time)
	{
		gui_.SetTimeLeft (time - boss_.time_bonus ());
	}
	
	public void GoTimeLeft ()
	{
		gui_.GoTimeLeft ();
	}
	
	public void StopTimeLeft ()
	{
		gui_.StopTimeLeft ();
	}
	
	public void TimeDown ()
	{
		int time_left = gui_.StartTimeDown ();
		GetComponent<BonusManager> ().SpawnTimeBonus (time_left);
	}
	
	//PowerPunch
	public bool SpawnPowerPunch (PowerPunchType type)
	{
		return power_punch_manager_.SpawnPowerPunch (type);
	}
	
	public PowerPunchManager power_punch_manager ()
	{
		return power_punch_manager_;
	}
	
	//Walkable Area
	public void SetMaxWalkableArea ()
	{
		player_walkable_area_.SetMaxWalkableArea ();
	}
	
	public void SetMinWalkableArea ()
	{
		player_walkable_area_.SetMinWalkableArea ();
	}
	
	public GameMode game_mode()
	{
		return game_mode_;
	}
/*	
	private float accum   = 0; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	private float timeleft; // Left time for current interval
	void OnGUI() 
	{	
		timeleft -= Time.deltaTime;
   		accum += Time.timeScale/Time.deltaTime;
    	++frames;
    
        // display two fractional digits (f2 format)
    	float fps = accum/frames;
			
        GUI.Label(new Rect(10, 10, 100, 30), fps.ToString());   
		GUI.Button(new Rect(170, 300, 170, 30), "Level DD: " + PlayerPrefs.GetInt("player_level").ToString() +
			" xp:" + (gains_session_.xp_ + PlayerPrefs.GetInt("player_xp")).ToString() +
			" Level: " + level_); 
		
    } 
	*/
	//Analitycs
	private float GetLengthMs ()
	{
		return (float)length_ms_.Elapsed.TotalMilliseconds;
	}
	
	public int GetMultiplayerAmmmo ()
	{
		int ammo = 0;
			
		if (player_data_.GetInt ("dd_ammo") >= multiplayer_ammo_)
			ammo = multiplayer_ammo_;
		else 
			ammo = player_data_.GetInt ("dd_ammo");
		
		return ammo;
	}
	
	public void SetMultiplayerAmmo (int new_value)
	{
		multiplayer_ammo_ = new_value;
	}
	
	private Vector3 iPad_offset = new Vector3 (0.0f, 32.0f, 0.0f);
	private Vector3 iPhone_offset = new Vector3 (55.0f, 0.0f, 0.0f);
	
	public Vector3 GetDeviceOffset ()
	{
		if (Framework.Instance.device_type () == Framework.WMF_DeviceType.ipad)
			return iPad_offset;
		else if (Framework.Instance.device_type () == Framework.WMF_DeviceType.iphone5)
			return iPhone_offset;
		
		return Vector3.zero;
	}
	
	private void SpawnLoadingScene ()
	{
		Instantiate (loading_scene_);
	}
	
	public void BossBaloon (string text)
	{
		boss_baloon_container_.PlayIntro (text);
	}
}
