using UnityEngine;
using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class PlayerDetails {
	
	private PlayerData player_data_;
	private JSONNode dd_info_;
	
	
	//persistant
	private int current_level_;
	private int starting_level_;
	private int starting_xp_;
	private int current_xp_;
	private int next_xp_;
	
	//stats
	private Dictionary<string, float> stats_;	
	
	public PlayerDetails()
	{
		
	}
	
	void Awake (){
		
		//Debug.Log(Framework.Instance.InstanceComponent<PlayerData>());
		
	}
	
	public void Init()
	{
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		dd_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/DDInfo"));
		
		current_level_ = player_data_.GetInt("dd_level"); 
		starting_level_ = current_level_;
		GameObject.Find("StaticParams").GetComponent<StaticParams>().starting_level_ = starting_level_;
		
		current_xp_ = player_data_.GetInt("dd_xp");
		starting_xp_ = current_xp_;
		GameObject.Find("StaticParams").GetComponent<StaticParams>().starting_xp_ = starting_xp_;
		
		GameObject.Find("StaticParams").GetComponent<StaticParams>().starting_coins_ = player_data_.GetInt("coins");
		
		next_xp_ = dd_info_[current_level_]["xp_to_next"].AsInt;
		stats_ = new Dictionary<string, float>();
		
		//stats
		AddStatFromStore("dd_hp_max");
		AddStatFromStore("dd_att_level");
		AddStatFromStore("dd_def_level");
	}
	
	/*
	private void LoadFromXml()
	{
		TextAsset xml_asset = Framework.Instance.resouces_manager().GetTextAssetResource("Config/dd_details");
		XMLParser parser = new XMLParser();
 		XMLNode node = parser.Parse(xml_asset.text);
		
		next_xp_ = int.Parse(node.GetValue("dd_details>0>dd_levels>0>level>" + current_level_ +">@xp_to_next"));
	
		//stats
		AddStatFromStore("health");
		AddStatFromStore("attack");
		AddStatFromStore("defense");
	}
	*/
	
	public void EndGame(Game.GameMode game_mode)
	{
		Store store = Framework.Instance.InstanceComponent<Store>();
		if(game_mode == Game.GameMode.singleplayer)
		{
			int old_current_level = current_level_;	
			
			next_xp_ = dd_info_[current_level_]["xp_to_next"].AsInt;
			
			while(current_xp_ >= next_xp_)
			{
				current_xp_ -= next_xp_;
				current_level_++;
				next_xp_ = dd_info_[current_level_]["xp_to_next"].AsInt;
			}
			
			player_data_.SetValue("dd_level", current_level_); 
			store.UpdateDDLevel(current_level_);
			player_data_.SetValue("dd_xp",current_xp_);
		}
		
		player_data_.SetValue("coins", (player_data_.GetInt("coins")+ GameObject.Find("StaticParams").GetComponent<StaticParams>().coins_));
		player_data_.SetValue("white_coins", (player_data_.GetInt("white_coins")+ GameObject.Find("StaticParams").GetComponent<StaticParams>().white_coins_));
		player_data_.Save();
/*		
		if(old_current_level == current_level_)
			return true;
		else 	
			return false;*/
	}
		
	void AddStatFromStore(string key)
	{
		Store store = Framework.Instance.InstanceComponent<Store>();
		stats_.Add(key, store.GetCurrentPropertyEffectValueByName(key));
		//Debug.Log(store.GetCurrentPropertyEffectValueByName(key));
	}
	
	public float GetStat(string key)
	{
		return stats_[key];
	}
	
	//Getter / Setter
	public int current_level()
	{
		return current_level_;
	}
	
	public int current_xp()
	{
		return current_xp_;
	}
	
	public int starting_level(){
		return starting_level_;
	}
	
	public int starting_xp()
	{
		return starting_xp_;
	}
	
	public float GetPercentageXP(){
		return current_xp_/next_xp_;
	}
	
	public void set_current_xp(int current_xp)
	{
		current_xp_ = current_xp;
	}

}
