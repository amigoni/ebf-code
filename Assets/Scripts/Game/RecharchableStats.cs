using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
//You must include these namespaces
//to use BinaryFormatter
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
 

public class RecharchableStats : MonoBehaviour {
	
	[Serializable()]
	public class Stat {
		public string name_;
		public System.DateTime start_time_;
		public float value_;
		public float recharge_rate_;
	}
	
	private Store store_ = null;
	public List<Stat> stats_list_ = null;
	public float recharge_rate_ = 0.1F; //Charge per second.
	
	public void SetValue( string name_, float value_){
		Stat stat = stats_list_.Find(x => x.name_ == name_);
		stat.start_time_ = System.DateTime.UtcNow; //- new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
		stat.value_ = value_;
		SaveData("recharchable_stats", stats_list_);
	}
	
	public float GetValue( string name_){
		Stat item = stats_list_.Find(x => x.name_ == name_);
		float max_value_ = store_.GetCurrentPropertyEffectValueByName(name_);
		
		System.TimeSpan time = System.DateTime.UtcNow - item.start_time_;
		//print (Mathf.Floor((float) time.TotalSeconds));
		
		item.value_ += (float)time.TotalSeconds*recharge_rate_;
		if (item.value_ > max_value_)
			item.value_ = max_value_;
		
		item.start_time_ = System.DateTime.UtcNow;
		
		return stats_list_.Find(x => x.name_ == name_).value_;
	}
	
	public Stat GetItem ( string name_){
		return stats_list_.Find(x => x.name_ == name_);
	}
	
	// Use this for initialization
	void Awake () {
		/*
		store_ = Framework.Instance.InstanceComponent<Store>();
		
		var data_ = PlayerPrefs.GetString("recharchable_stats");
	
		if(!string.IsNullOrEmpty(data_))
        {
            //Binary formatter for loading back
	        var b = new BinaryFormatter();
	        //Create a memory stream with the data
	        var m = new MemoryStream(Convert.FromBase64String(data_));
	        //Load back the scores
	        stats_list_ = (List<Stat>)b.Deserialize(m);
		}
		else{	
			stats_list_ = new List<Stat>();
			stats_list_.Add(new Stat{name_= "mana", start_time_ = System.DateTime.UtcNow, value_= store_.GetCurrentPropertyEffectValueByName("mana")-40, recharge_rate_ = 0.1F});
			//stats_list_.Add(new Stat{name_= "boss_energy", start_time_ = System.DateTime.UtcNow, value_= store.GetCurrentPropertyEffectValueByName("boss_energy")});
			
			SaveData("recharchable_stats", stats_list_);
		}
		*/
	}
	
	
	void Init(){
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void SaveData(string property, object objectName)
    {
		//Get a binary formatter
        var b = new BinaryFormatter();
        //Create an in memory stream
        var m = new MemoryStream();
        //Save the scores
        b.Serialize(m, objectName);
        //Add it to player prefs
        PlayerPrefs.SetString(property, 
            Convert.ToBase64String(
                m.GetBuffer()
            )
        );
    }
	
	void OnApplicationQuit() {
       SaveData("recharchable_stats", stats_list_);
    }
}
