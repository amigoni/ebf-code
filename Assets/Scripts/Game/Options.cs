using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public enum MoveOnShotType
{
	stop,
	move,
	slow
}

[Serializable()]
public class OptionsParams
{
	public InputControllerType input_controller_type;
	public MoveOnShotType move_on_shot;
	public bool floating;
	public bool music_on;
	public bool sfx_on;
}

public class Options : MonoBehaviour {
	OptionsParams options_params_ = null;
	
	// Use this for initialization
	void Awake () {
        bool first_time = !Load();
        if(first_time)
		{
			FirstInit();
		}

		UpdateParams();
	}
	
	void FirstInit()
	{
		if(options_params_ == null)
			options_params_ = new OptionsParams();
		
		options_params_.input_controller_type = InputControllerType.virtual_pad;
		options_params_.move_on_shot = MoveOnShotType.stop;
		options_params_.floating = true;
		options_params_.music_on = true;
		options_params_.sfx_on = true;
		
		Save ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//Behaviour
	public void Save()
	{
#if UNITY_IPHONE
		PlayerPrefs.SetString("options_params_.input_controller_type", options_params_.input_controller_type.ToString());
		PlayerPrefs.SetString("options_params_.move_on_shot", options_params_.move_on_shot.ToString());
		PlayerPrefs.SetString("options_params_.floating", options_params_.floating.ToString());
		PlayerPrefs.SetString("options_params_.music_on", options_params_.music_on.ToString());
		PlayerPrefs.SetString("options_params_.sfx_on", options_params_.sfx_on.ToString());
#else
        BinaryFormatter b = new BinaryFormatter();
        MemoryStream m = new MemoryStream();
        b.Serialize(m, options_params_);
        PlayerPrefs.SetString("OptionsParams", Convert.ToBase64String(m.GetBuffer()));
#endif
	}
	
	public bool Load()
	{
#if UNITY_IPHONE
		options_params_ = new OptionsParams();
		string str = PlayerPrefs.GetString("options_params_.input_controller_type", "NONE");
		if(string.IsNullOrEmpty(str) || str != "NONE")
			options_params_.input_controller_type = (InputControllerType)Enum.Parse(typeof(InputControllerType), str); 
		else 
			return false;
		
		str = PlayerPrefs.GetString("options_params_.move_on_shot", "NONE");
		if(string.IsNullOrEmpty(str) || str != "NONE")
			options_params_.move_on_shot = (MoveOnShotType)Enum.Parse(typeof(MoveOnShotType), str); 
		else 
			return false;
		
		str = PlayerPrefs.GetString("options_params_.floating", "NONE");
		if(string.IsNullOrEmpty(str) || str != "NONE")
			options_params_.floating = bool.Parse(str); 
		else 
			return false;
		
		str = PlayerPrefs.GetString("options_params_.music_on", "NONE");
		if(string.IsNullOrEmpty(str) || str != "NONE")
			options_params_.music_on = bool.Parse(str); 
		else 
			return false;
		
		str = PlayerPrefs.GetString("options_params_.sfx_on", "NONE");
		if(string.IsNullOrEmpty(str) || str != "NONE")
			options_params_.sfx_on = bool.Parse(str); 
		else 
			return false;
		
		return true;
#else
		string data_str = PlayerPrefs.GetString("OptionsParams");
		if(string.IsNullOrEmpty(data_str))
        {
			return false;	
		}
		BinaryFormatter b = new BinaryFormatter();
	    MemoryStream m = new MemoryStream(Convert.FromBase64String(data_str));
	    options_params_ = (OptionsParams)b.Deserialize(m);
		return true;
#endif
	}
	
	//Getter / Setter
	public OptionsParams options_params()
	{
		return options_params_;
	}
	
	public void ChangeOptionsParams(OptionsParams options_params)
	{
		options_params_ = options_params;
		Save();
			
		UpdateParams();
	}
	
	private void UpdateParams()
	{
		Framework.Instance.audio_manager().ChangeOptions( options_params_ );
	}
	
	public void Reset()
	{
		//for sure
		options_params_.move_on_shot = MoveOnShotType.stop;
		
		UpdateParams();
		Save();
	}
}
