using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StaticParams : MonoBehaviour {
	
	public long score_;
	public int xp_;
	public int coins_;
	public int white_coins_;
	public int starting_level_;
	public int starting_xp_;
	public int starting_coins_;
	public MissionsManager.Mission prev_mission_;
	//multiplayer
	public string session_id_;
	public List<Game.DDResultType> dd_results_;
	public List<Game.BossResultType> boss_results_;
	public string contender_name_;
}
