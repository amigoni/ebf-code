using UnityEngine;

public sealed class GameState :  FSMState<Game> {
	
	static readonly GameState instance = new GameState();
	public static GameState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	private float time_to_add_ = 0.3F;
	private float dt_ = 0.0F;
	
	static GameState() 
	{
		
	}
	
	private GameState()
	{
	
	}
	
	public override void Enter(Game g) 
	{
		g.EnterInGame();
		dt_ = time_to_add_;
	}
	
	public override void Execute(Game g) 
	{
		if(g.paused())
			return;
		
		dt_ -= Time.deltaTime;
		if(dt_ < 0.0F) 
		{
			dt_ += time_to_add_;
			g.AddScore( Game.ScoreValue.time );
		}
	}
	
	public override void Exit(Game g) 
	{

	}
}