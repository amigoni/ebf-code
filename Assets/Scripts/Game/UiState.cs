using UnityEngine;

public sealed class UiState :  FSMState<Game> {
	
	static readonly UiState instance = new UiState();
	public static UiState Instance 
	{
		get
		{
			return instance;
		}
	}
	
	static UiState() 
	{
		
	}
	
	private UiState()
	{
	
	}
	
	public override void Enter(Game g) 
	{
		
	}
	
	public override void Execute(Game g) 
	{

	}
	
	public override void Exit(Game g) 
	{

	}
}