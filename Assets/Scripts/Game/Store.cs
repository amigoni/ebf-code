using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
//You must include these namespaces
//to use BinaryFormatter
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using SimpleJSON;
using EbFightAPI;
 
public class Store : MonoBehaviour {
	
	private int coins_in_bank_ = 10000;
	public static string store_type_open_ = "upgrades"; //Keeps track of what type of store to open
	private XMLNode DDdetailsXMLNode;
	
    //StoreItems Table
	private JSONNode store_info_; //Static info for each item
	private PlayerData player_data_;
	private JSONNode dd_info_;
	
	
	private string current_item_key_;
	private int current_item_cost_;
	private string current_item_currency_;
	
	private int temp_white_coins_ = 0;
	
	Action buy_success_callback_;
	Action buy_failure_callback_;
	
	//private JSONNode ddStats_;
	
    void Awake()
    {
		//Just for testing
		//Reset(); 
		Init();
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		dd_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/DDInfo"));
    }	
	
	
	public void Init()
	{
		store_info_ = JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
	}
	
	
	
	public void BuyItemAsynch( string itemName, int quantity, Action success_callback, Action failure_callback)
	{
		buy_success_callback_ = success_callback;
		buy_failure_callback_ = failure_callback;
		
		BuyItem(itemName, quantity);
	}
	
	
	public bool BuyItem (string itemName, int quantity)
	{
		
		//Check for money
		float cost = 0F;
		bool enable_purchase = true;
		current_item_key_ = itemName;
		
		
		//Get Bank balance and currency
		int money_in_bank;
		string currency;
		if (store_info_[itemName]["currency"].ToString().Contains("white_coins"))
		{
			money_in_bank = player_data_.GetInt("white_coins");
			currency = "white_coins";
		}
		else
		{
			money_in_bank = player_data_.GetInt("coins");
			currency = "coins";
		}
		
		current_item_currency_ = currency;
		
		//Get Cost
		if(store_info_[itemName]["is_upgradable"].AsBool == true && itemName.Contains("boss") == false)
		{
			cost = store_info_[itemName]["upgrades"][player_data_.GetInt(itemName)]["next_cost"].AsInt;
		}
		else if(store_info_[itemName]["is_upgradable"].AsBool == true && itemName.Contains("boss") == true)
		{
			EbFightAPI.Boss boss_data =  GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData();
			int item_level = 0;
			if ( itemName == "boss_att")
				item_level = (int) boss_data.Attack;
			else if ( itemName == "boss_def")
				item_level = (int) boss_data.Defence;
			else if ( itemName == "boss_power_level")
				item_level = (int) boss_data.Power;
			else if ( itemName == "boss_level_upgrade")
				item_level = (int) boss_data.Level;
			
			cost = store_info_[itemName]["upgrades"][item_level]["next_cost"].AsInt;
		}
		else if (itemName.Contains("boss_utilities") == true)
		{
			cost = store_info_[itemName]["cost"].AsInt;
		}
		else if (itemName == "ammo")
		{
			if (store_info_[itemName]["key"].ToString() == "ammo" && 
				(player_data_.GetInt("dd_ammo")+1 <= store_info_["dd_ammo_level"]["upgrades"][player_data_.GetInt("dd_ammo_level")]["effect"].AsInt))
			{
				enable_purchase = true;
				cost = store_info_[itemName]["upgrades"][player_data_.GetInt("dd_ammo")]["cost"].AsInt;
			}
			else
			{
				enable_purchase = false;
				Debug.Log("No Ammo");
			}
		}
		else if (itemName == "resurrection")
		{
			int times_resurrected = GameObject.Find("Game").GetComponent<Game>().number_of_resurrections();
			cost = store_info_[itemName]["upgrades"][times_resurrected]["next_cost"].AsInt;
		}	
		else
			cost = store_info_[itemName]["cost"].AsInt;
		
		current_item_cost_ = (int) cost;
		
		//Perform Money Transaction
		if (currency == "coins")
		{
			if (cost <= money_in_bank)
			{
				player_data_.SetValue(currency, player_data_.GetInt(currency)-cost);
				ChangeDataAfterPurchase(itemName,cost,currency);
			}
			else
				enable_purchase = false;	
		}
		else if (currency == "white_coins")
		{
			if(itemName.ToString().Contains("boss") == true)
			{
				if (cost <= temp_white_coins_)
				{
					temp_white_coins_ -= (int)cost;
					ChangeDataAfterPurchase(itemName,cost,currency);
				}
				else
					enable_purchase = false;
			}	
			else
			{
				if ( itemName == "resurrection")
				{
					if (cost <= money_in_bank + GameObject.Find("Game").GetComponent<Game>().GetWhiteMilkCoins())
						player_data_.SetValue(currency, player_data_.GetInt(currency)-cost);
					else
						enable_purchase = false;
				}
				else
				{
					if (cost <= money_in_bank)
						Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins((int)(money_in_bank-cost), CheckIfYouCanPurchaseWithWhiteMilkCoins);
					else
						enable_purchase = false;
				}
				
			}
		}
		
		//##Analytics Transaction
		string analytics_currency_field_name = "";
		string analytics_currency_field_name2 = "";
		int analytics_value2 = 0;
		if (currency == "coins")
		{
			analytics_currency_field_name = "soft_currency_1_spent";
			analytics_currency_field_name2 = "soft_currency_1";
			analytics_value2 = player_data_.GetInt("coins");
			player_data_.SetValue("soft_currency_1_spent", player_data_.GetInt("soft_currency_1_spent")+cost );
		}
		else if (currency == "white_coins")
		{
			analytics_currency_field_name = "hard_currency_spent";	
			analytics_currency_field_name2 = "hard_currency";
			long temp_value = player_data_.GetInt("white_coins");
			analytics_value2 = (int) temp_value;
			player_data_.SetValue("hard_currency_spent", player_data_.GetInt("hard_currency_spent")+cost );
		}
		
		int int_cost = (int) cost;	
		
		Framework.Instance.InstanceComponent<Analitycs>().AddCommonValue(analytics_currency_field_name, int_cost);
		Framework.Instance.InstanceComponent<Analitycs>().SetCommonValue(analytics_currency_field_name2, analytics_value2);
		//##
		

		//Perform Transaction & Upgrades
		/*
		if (
			((cost <= money_in_bank && itemName.Contains("boss") == false) ||
			(cost <= temp_white_coins_ && itemName.Contains("boss") == false) ||	
			(cost <= temp_white_coins_ && itemName.Contains("boss") == true) ||
			(cost <= money_in_bank && store_info_[itemName]["currency"].ToString() == "coins" && itemName.Contains("boss") == true)
			)
			&& enable_purchase == true
			)
		
		
		{
			PerfomTransaction(itemName, cost, currency);
			
			// TODO finish Asynch Purchase if with WMC
			
			if (currency == "white_coins")
				Framework.Instance.InstanceComponent<ServerAPI>().SyncWhiteCoins(player_data_.GetInt("white_coins"), CheckIfYouCanPurchaseWithWhiteMilkCoins);
			else
				PerfomTransaction(itemName, cost, currency);	
				
		}
	
		else
		*/
		if (enable_purchase == false)
		{	
			Debug.Log ("You got no money man!! Go get some money!! Money: "+money_in_bank+" Cost: "+cost);
			
			if ((cost > money_in_bank && currency == "coins") ||
				(cost > temp_white_coins_ && currency == "white_coins"))
			{
				
				GameObject popup_window_ = (GameObject) Instantiate(Resources.Load("Framework/PopUpWindowPrefab"));
				popup_window_.GetComponent<PopUpWindowCoinsStore>().Init(store_info_[itemName]["currency"].ToString()); 
				/*
				if (GameObject.Find("MainCamera"))
					GameObject.Find("MainCamera").GetComponent<PopUpWindowCoinsStore>().Init(store_info_[itemName]["currency"].ToString());
				else
					Messenger.Broadcast("NotEnoughMoney");
				*/	
			} 
				
			
			//Analytics
			wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(itemName, 1);
			wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy "+itemName.ToString(), currency, cost, false,"Normal", reward_set.GetResourceSetId());
			reward_set.SendEvent();
			analytics_event.SendEvent();	
		}

		return enable_purchase;
	}
	
	
	void CheckIfYouCanPurchaseWithWhiteMilkCoins(ResponseStatus status, JSONResult data)
	{
		if (status == ResponseStatus.Success)
		{
			long sync_white_coins = data.As<long>();
			Debug.Log("Synched WMC: "+ sync_white_coins);
			player_data_.SetValue("white_coins", sync_white_coins);
			ChangeDataAfterPurchase(current_item_key_,current_item_cost_,current_item_currency_);
			buy_success_callback_();
			
#if UNITY_WEBPLAYER
			Framework.Instance.InstanceComponent<ServerAPI>().UpdatePlayerData(player_data_.player_data_.ToString(), OnUpdatePlayerData);
#endif
		}
		else
		{
			buy_failure_callback_();
			Debug.Log ("Error synching coins");
		}
	}
	
	public void OnUpdatePlayerData(EbFightAPI.ResponseStatus status, EbFightAPI.JSONResult data)
	{
		int i = 0;
		i++;
	}
	
	void ChangeDataAfterPurchase(string itemName, float cost, string currency)
	{
		//Player DD && Normal Coins Conversion
		if (itemName.Contains("boss") == false)
		{
			if (store_info_[itemName]["is_upgradable"].AsBool == true)
			{
				if(store_info_[itemName]["category"].ToString().Contains("power_punch"))
					player_data_.SetValue(itemName, player_data_.GetInt(itemName)+1);
				else
				{
					if (store_info_[itemName]["key"].ToString() != "ammo")
					{
						player_data_.SetValue(itemName, player_data_.GetInt(itemName)+1);
						if(store_info_[itemName]["key"].ToString() == "dd_ammo_level")
							Messenger.Broadcast("UpdateAmmoPrefab");
					}
					else
						Messenger.Broadcast("UpdateAmmoPrefab");
									
					//Analytics
					wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(itemName,1);	
					wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy "+itemName.ToString(), currency, cost, true,"Normal", reward_set.GetResourceSetId());
					reward_set.SendEvent();
					analytics_event.SendEvent();
				}	
			}
			
			if(store_info_[itemName]["category"].ToString().Contains("utility"))
			{
				player_data_.SetArrayValue("utilities_equipped", itemName, true);
				
				//Analytics
				wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(itemName, 1);	
				wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy "+itemName.ToString(), currency , cost, true,"Normal", reward_set.GetResourceSetId());
				reward_set.SendEvent();
				analytics_event.SendEvent();	
			}
			
			if (store_info_[itemName]["category"].ToString().Contains("normal_coins"))
			{
				player_data_.SetValue("coins", player_data_.GetInt("coins")+store_info_[itemName]["quantity"].AsInt);
				player_data_.SetValue("soft_currency_1_bought", player_data_.GetInt("soft_currency_1_bought")+store_info_[itemName]["quantity"].AsInt );
		
				//##Analytics
				Framework.Instance.InstanceComponent<Analitycs>().SetCommonValue("soft_currency_1_bought",  player_data_.GetInt("soft_currency_1_bought"));
				Framework.Instance.InstanceComponent<Analitycs>().SetCommonValue("soft_currency_1",player_data_.GetInt("coins"));
			}	
			
			if (store_info_[itemName]["key"].ToString() == "ammo" && 
				(player_data_.GetInt("dd_ammo")+1 <= store_info_["dd_ammo_level"]["upgrades"][player_data_.GetInt("dd_ammo_level")]["effect"].AsInt))
			{
				player_data_.SetValue("dd_ammo", player_data_.GetInt("dd_ammo")+1);
				
				//Analytics
				wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(itemName, 1);
				wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy "+itemName.ToString(), currency, cost, true,"Normal", reward_set.GetResourceSetId());
				reward_set.SendEvent();
				analytics_event.SendEvent();
			}
		}
		//Boss Upgrades
		else
		{
			if (itemName == "boss_att")
			{	
				GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Attack++;
			}	
			else if (itemName == "boss_def")
			{
				GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Defence++;
			}	
			else if (itemName == "boss_power_level")
			{
				GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Power++;
			}
			else if (itemName == "boss_level_upgrade")
			{
				GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Level++;
			}
			else if (itemName == "boss_utilities_att" || itemName == "boss_utilities_def" || itemName == "boss_utilities_time" )
			{
				SetBossUtility(itemName);
			}
			
			//Analytics
			wmf.KeenIOEvents.ResourceSet reward_set = new wmf.KeenIOEvents.ResourceSet(itemName, 1);
			wmf.KeenIOEvents.GameEconomyOffer analytics_event  = new wmf.KeenIOEvents.GameEconomyOffer("Buy "+itemName.ToString(), currency, cost, true,"Normal", reward_set.GetResourceSetId());
			reward_set.SendEvent();
			analytics_event.SendEvent();
		}
		
		Messenger.Broadcast("RefreshCoins");
	}
	
	
	void SetBossUtility(string key)
	{
		
		List<EbFightAPI.Utility> utilities = GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Utilities;
		EbFightAPI.Utility utility = new EbFightAPI.Utility();
		
		bool exists = false;
		for ( int i=0; i < utilities.Count; i++){
			if ( utilities[i].Kind == key){
				utilities[i].Expires = (long) GetTimeStampUnix() + 60*60*24;
				exists = true;
			}		
		}
		
		if (exists == false){
			utility.Kind = key;
			utility.Expires = (long) GetTimeStampUnix()+ 60*60*24;
		}
		
		utilities.Add(utility);
	}
	
	public int GetTempWhiteCoins()
	{
		return temp_white_coins_;
	}
	
	
	public void SetTempWhiteCoins()
	{
		temp_white_coins_ = player_data_.GetInt("white_coins");
		//Debug.Log("Temp white Coins "+temp_white_coins_);
	}
	
	
	public void ReconcileWhiteCoinsToTemp()
	{
		if (temp_white_coins_ < 0)
		{
			Debug.Log("Temp White Coins are negative? Setting to 0: "+temp_white_coins_);
			temp_white_coins_ = 0;
		}
			
		player_data_.SetValue("white_coins", temp_white_coins_);
		//Debug.Log("Reconciled white Coins "+player_data_.GetInt("white_coins"));
	}
	
	public int GetMaxUnlockedLevelForBossItem(string item_key, int boss_level)
	{
		int upgrade_level = 0;
		for (int i = 0; i < store_info_[item_key]["upgrades"].AsArray.Count; i++)
		{
			if (store_info_[item_key]["upgrades"][i]["unlocked_at_player_level"].AsInt == boss_level){
				if (store_info_[item_key]["upgrades"][i]["level"].AsInt >= upgrade_level)
					upgrade_level = store_info_[item_key]["upgrades"][i]["level"].AsInt;
			}
		}
		
		return upgrade_level;
	}
	
	
	public int GetMaxUnlockedLevelForItem(string item_key)
	{
		
		int	level = player_data_.GetInt("dd_level");	
		
		int upgrade_level = 0;
		for (int i = 0; i < store_info_[item_key]["upgrades"].AsArray.Count; i++)
		{
			if (store_info_[item_key]["upgrades"][i]["unlocked_at_player_level"].AsInt == level){
				if (store_info_[item_key]["upgrades"][i]["level"].AsInt >= upgrade_level)
					upgrade_level = store_info_[item_key]["upgrades"][i]["level"].AsInt;
			}
		}
		
		return upgrade_level;
	}
	
	
	public bool IsItemLocked(string key)
	{
		bool return_value = true;
		
		if(player_data_.GetInt("dd_level") >= store_info_[key]["unlocked_at_player_level"].AsInt)
			return_value = false;
		
		return return_value;
	}
	
	
	public void UpdateDDLevel (int level_number)
	{
		player_data_.SetValue("dd_level", level_number);
	}
	
	
	public List<JSONNode> GetStoreItemsInfoForCategory (string category)
	{
		List<JSONNode> return_list = new List<JSONNode>();
		
		for (int i = 0; i< store_info_.Count; i++){	
			if (store_info_[i]["category"].ToString() == category){
				return_list.Add(store_info_[i]);
			}	
		}
		
		return return_list;
	}
	
	
	public int GetCoins(){
		return coins_in_bank_;
	}
	
	
	public float GetCurrentPropertyEffectValueByName(string propertyName)
	{
		float return_value = 0.0F;
		if (propertyName == "dd_hp_max")
			return_value = dd_info_[player_data_.GetInt("dd_level")]["hp_max"].AsFloat;
		else if (propertyName == "dd_level")
			return_value = player_data_.GetInt("dd_level");
		else if (propertyName == "dd_att_level" ||
				propertyName == "dd_def_level" ||
				propertyName == "dd_ammo_level")
			return_value = store_info_[propertyName]["upgrades"][player_data_.GetInt(propertyName)]["effect"].AsFloat;
		else if (propertyName == "great_ball_of_fire" ||
				propertyName == "aaatatata" ||
				propertyName == "huge_punch" )
			return_value = store_info_[propertyName]["upgrades"][player_data_.GetInt(propertyName)]["effect"].AsFloat;
		else if (propertyName == "double_bubble" ||
				propertyName == "double_magnet" ||
				propertyName == "life_boost" ||
				propertyName == "att_boost" ||
				propertyName == "ammo_boost"||
				propertyName == "boss_utilities_att"||
				propertyName == "boss_utilities_def"||
				propertyName == "boss_utilities_time"
			)
			return_value = store_info_[propertyName]["effect"].AsFloat;
		
		return	return_value;
	}
	
	
	//In the future merge these function and GetCurrentPropertyEffectValueByName
	public float GetUpgradablePropertyEffectByNameAsFloat(string property_name, int level)
	{
		float return_value = 0.0F;
		return_value = store_info_[property_name]["upgrades"][level]["effect"].AsFloat;
		
		return return_value;
	}
	
	
	public string GetUpgradablePropertyEffectByName(string property_name, int level)
	{
		string return_value = "";
		return_value = store_info_[property_name]["upgrades"][level]["effect"];
		
		return return_value;
	}
	
	
	public object GetUpgradablePropertySettingByName(string property_name, int level, string setting)
	{
		object return_value = 0.0F;
		
		return_value = store_info_[property_name]["upgrades"][level][setting].AsFloat;
		
		return return_value;
	}	
	
	
	public bool AreThereUnpurchasedUpgrades(string who)
	{
		bool is_true= true;
		
		if(who == "dd"){
			if(IsUpgradedMaxedOut("dd_att_level", "dd") == true)
				is_true = false;
			if(IsUpgradedMaxedOut("dd_def_level", "dd") == true)
				is_true = false;
			if(IsUpgradedMaxedOut("dd_ammo_level", "dd") == true)
				is_true = false;
		}
		else
		{	
			if(IsUpgradedMaxedOut("boss_att", "boss") == true)
				is_true = false;
			if(IsUpgradedMaxedOut("boss_def", "boss") == true)
				is_true = false;
			if(IsUpgradedMaxedOut("boss_power_level", "boss") == true)
				is_true = false;
		}
			
		return is_true;
	}
	
	
	private bool IsUpgradedMaxedOut(string key, string who)
	{
		bool is_true = false;
		if (who == "dd")
		{
			if(Framework.Instance.InstanceComponent<PlayerData>().GetInt(key) >= GetMaxUnlockedLevelForItem(key))
				is_true = true;	
		}
		else
		{
			int bought_level_ = 0;
			if (key == "boss_att")
				bought_level_ = (int) GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Attack;
			else if (key == "boss_def")
				bought_level_ = (int) GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Defence;
			else if (key == "boss_power_level")
				bought_level_ = (int) GameObject.Find("MainCamera").GetComponent<BossDetail>().GetBossData().Power;
			
			if(bought_level_ >= GetMaxUnlockedLevelForItem(key))
				is_true = true;	
		}
	
		return is_true;
	}
	
	
	private double GetTimeStampUnix()
	{
		System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		var timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
		
		return timestamp;
	}
	
	
	public void Reset()
	{
		//PlayerPrefs.DeleteAll();
		Init();
	}
	
	
	public void DebugSetAttack(int level)
	{
		player_data_.SetValue("dd_att_level", level); 
	}
	
	
	public void DebugSetDefense(int level)
	{
		player_data_.SetValue("dd_def_level", level); 
	}
	
	
	public int GetResurrectCost()
	{
		return store_info_["resurrection"]["upgrades"][0]["next_cost"].AsInt;
	}
}