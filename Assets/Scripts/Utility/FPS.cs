using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {
	
	private float accum   = 0; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	private float timeleft; // Left time for current interval
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
  	void OnGUI() 
	{
		timeleft -= Time.deltaTime;
   		accum += Time.timeScale/Time.deltaTime;
    	++frames;
    

        // display two fractional digits (f2 format)
    	float fps = accum/frames;
			
        GUI.Button(new Rect(10, 10, 100, 30), fps.ToString());   
    }
}
