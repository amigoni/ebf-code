using UnityEngine;
using System.Collections;

public class EnemyInfo : MonoBehaviour{
	private float damage_ = 0.0F;
	private int xp_ = 0;
	
	//Getter / Setter
	public void set_damage(float damage)
	{
		damage_ = damage;
	}
	
	public float damage()
	{
		return damage_;
	}
	
	public void set_xp(int xp)
	{
		xp_ = xp;
	}
	
	public int xp()
	{
		return xp_;
	}
}
