using UnityEngine;
using System.Collections;

public class DisableAfterPlay : MonoBehaviour {
	
	private Animation animation_;
	
	// Use this for initialization
	void Start () {
		animation_ = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!animation_.isPlaying)
			enabled = false;
	}
}
