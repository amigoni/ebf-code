using UnityEngine;
using System.Collections;

public class RebuildSprite : MonoBehaviour {
	
	private tk2dBaseSprite sprite_;
	
	// Use this for initialization
	void Start () {
		sprite_ = GetComponent<tk2dBaseSprite>();
	}
	
	// Update is called once per frame
	void Update () {
		sprite_.Build();
	}
}
