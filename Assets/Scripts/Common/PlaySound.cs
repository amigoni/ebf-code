using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {
	
	public AudioClip audio_clip_ = null;
	public float volume_ = 1.0F;
	
	private AudioManager audio_manager_;
	
	public void Play () {
		if(audio_clip_ != null)
			Framework.Instance.audio_manager().Play(audio_clip_, this.transform, volume_, false);
	}
}
