using UnityEngine;
using System;
using System.Collections;

public class DestroyAfterPlayOnce : MonoBehaviour {
	
	private tk2dAnimatedSprite animation_tk2d_ = null;
	private Animation animation_unity_ = null;
	
	private PlaySound play_sound_ = null;
	// Use this for initialization
	void Start () {
		//Find in component
		animation_tk2d_ = GetComponent<tk2dAnimatedSprite>();
		animation_unity_ = GetComponent<Animation>();
		
		//Find in children
		animation_tk2d_ = GetComponentInChildren<tk2dAnimatedSprite>();
		animation_unity_ = GetComponentInChildren<Animation>();
		
		if(animation_tk2d_ != null)
		{
			GetComponent<PoolObject>().Spawned = new EventHandler(SpawnFromPool);
		}
		
		play_sound_ = GetComponent<PlaySound>();
	}
	
	void SpawnFromPool(object sender, EventArgs e) 
	{
		if(animation_tk2d_ != null)
			animation_tk2d_.Play();
		if(play_sound_ != null)
			play_sound_.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if(animation_tk2d_ != null)
		{
			UpdateTk2d();
		}
		else if(animation_unity_ != null)
		{
			UpdateUnity();
		}
	}
	
	void UpdateTk2d()
	{
		if(!animation_tk2d_.Playing)
		{
			//every explosion must be a pool object
			PoolManager.Despawn(this.gameObject);
		}
	}
	
	void UpdateUnity()
	{
		if(!animation_unity_.isPlaying)
		{
			Destroy(this.gameObject);
		}
	}
}
