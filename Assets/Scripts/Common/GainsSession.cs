using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
On the Boss Battle Log the round results could be: 
- none (round wasn't played, cause DD died before)
- won (Boss killed DD)
- survived (DD passed but didn't kill the boss)
- destroyed (DD kills the boss without being hit)
- annihilated (DD kills the boss with perfect) *This is icing on the cake. If you can put it would be great otherwise don't bother

ON the DD Battle the round results could be:
- none (DD didn't get to this round)
- won (DD killed boss)
- passed (DD passed level but didn't kill the boss)
- died (DD died)
- perfect (DD killed the bo
ss with Perfect) *If this is not a big issue we would like to have it.
*/
public struct StageResult
{
	public bool dd_perfect_;
}

public class GainsSession {
	public int coins_;
	public int white_coins_;
	public int xp_; 
	public List<bool> stage_results_;
	
	public GainsSession()
	{
		coins_ = 0;
		white_coins_= 0;
		xp_ = 0; 
		
		stage_results_ = new List<bool>();
		//Init
		stage_results_.Add(false);
		stage_results_.Add(false);
		stage_results_.Add(false);
	}
}
