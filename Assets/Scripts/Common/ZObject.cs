using UnityEngine;
using System.Collections;

public class ZObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		tk2dSprite sprite = GetComponent<tk2dSprite>();
		int baseline = (int)sprite.transform.position.y;
		//baseline -= (int)(sprite.collection.spriteDefinitions[sprite.spriteId].untrimmedBoundsData[1].y * 0.5F);
		
		this.GetComponent<ZOrderObject> ().Init( baseline, true ); 
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
