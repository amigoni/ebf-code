using UnityEngine;
using System.Collections;

public class RebuildTextMesh : MonoBehaviour {

	private tk2dTextMesh text_;
	
	// Use this for initialization
	void Start () {
		text_ = GetComponent<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		text_.Commit();
	}
}
