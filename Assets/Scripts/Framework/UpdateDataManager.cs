using UnityEngine;
using System.Collections;
using System.IO;
using EbFightAPI;

public class UpdateDataManager : MonoBehaviour {
	
	public delegate void CallbackAfterFinish();
	
#if UNITY_IPHONE	
	private const string BASE_URL = "http://whitemilkgames.com/milkythings/";
	private const string ITUNES_LINK = "http://itunes.apple.com/us/app/id677463016?mt=8";
	
	private bool update_enabled_ = true;
	
	private WWW myExtIPWWW;
	private ClientVersion last_client_version_;
		
	private bool can_check_version_ = true;
	
	private CallbackAfterFinish callback_ = null;
	
	void Start ()
	{
		//Create Data structure in any case
		System.IO.Directory.CreateDirectory(Application.persistentDataPath+"/Mobile/Config");
		//InvokeRepeating("CheckIfNeedToUpdate", 0.0f, 60.0f);
		InvokeRepeating("CheckIfNeedToUpdate", 0.0f, 3600.0f);
	}
	
	public void CheckIfNeedToUpdate()
	{
		if(update_enabled_)
		{
			if(can_check_version_)
			{
				can_check_version_ = false;
				Framework.Instance.InstanceComponent<ServerAPI>().GetVersion(OnGetVersion);
			}
		}
	}

	private void OnGetVersion(ResponseStatus status, JSONResult data)	
	{
		if (status == ResponseStatus.Success)
		{
			last_client_version_ = data.As<ClientVersion>();
			if ((last_client_version_.BuildVersion / 10.0f) > Framework.Instance.InstanceComponent<PlayerData>().GetFloat("client_build_version"))
			{
				Framework.Instance.scene_manager().LoadScene("UpdateNewVersionScene", TransitionType.add_scene);	
			}
			else if ((last_client_version_.DataVersion / 10.0f) > Framework.Instance.InstanceComponent<PlayerData>().GetFloat("client_data_version"))
			{
 				GetDataFile("StoreInfo.txt", "StoreInfo", true);
				Framework.Instance.scene_manager().LoadScene("UpdateDataScene", TransitionType.add_scene);
			}
		}
		can_check_version_ = true;
	}

	private void GetDataFile(string url_file_name, string file_name, bool remove_overprint)
	{
		myExtIPWWW = new WWW(BASE_URL + url_file_name);	
		
		//Debug.Log(BASE_URL + url_file_name + " myExtIPWWW");
		
		StartCoroutine(WaitForFile(file_name, remove_overprint));
	}
	
	IEnumerator WaitForFile(string file_name, bool remove_overprint)
	{
	    yield return myExtIPWWW; 
	   	if (myExtIPWWW.isDone)
		{
			if (myExtIPWWW.text != null && string.IsNullOrEmpty(myExtIPWWW.text) == false && myExtIPWWW.text != "")
			{
				string myExtIP = myExtIPWWW.text;
		
				System.IO.File.WriteAllText(Application.persistentDataPath+"/Mobile/Config/"+file_name+".txt", myExtIP);
				iPhone.SetNoBackupFlag(Application.persistentDataPath+"/Mobile/Config/"+file_name+".txt");
				
				//Debug.Log(Application.persistentDataPath+"/Mobile/Config/"+file_name+".txt"+" SAVED");
				//Debug.Log((last_client_version_.DataVersion/10F).ToString());
				
				//Update version number Locally
				Framework.Instance.InstanceComponent<PlayerData>().SetValue("client_data_version", (last_client_version_.DataVersion / 10F).ToString().Replace(',', '.'));
			}
			else
			{
				Debug.Log ("Update Data: Connected but error in getting "+file_name);
				GameObject pop_up_window_prefab_ = (GameObject)  Instantiate( Resources.Load("Framework/PopUpWindowPrefab"));
				pop_up_window_prefab_.GetComponent<PopUpWindowPrefab>().Init("Error Connecting", "Click to try again",OnOKCallback);
				pop_up_window_prefab_.GetComponent<PopUpWindowPrefab>().set_destroy_on_closing(true);
			}
		}
		
		ExitFromUpdateScene();
	}	
	
	void OnOKCallback ()
	{
		GetDataFile("StoreInfo.txt", "Mobile/Config/StoreInfo", false);
	}
	/*
	void CopyFileToPersistantPath (string file_name)
	{
		//Debug.Log("Copying :"+file_name);
		string file_text = Framework.Instance.resouces_manager().GetStringResource(file_name);
		//Debug.Log("Got Content");
		System.IO.File.WriteAllText(Application.persistentDataPath+"/"+file_name+".txt", file_text);
		iPhone.SetNoBackupFlag(Application.persistentDataPath+"/"+file_name+".txt");
		Debug.Log("Update Data :Copied "+file_name);
	}
	*/
	
	public void UpdateVersion()
	{
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("has_rated_game",true);
		Application.OpenURL(ITUNES_LINK);
	}
	
	public void ExitFromUpdateScene()
	{
		can_check_version_ = true;
		
		Framework.Instance.scene_manager().BackFromCurrentScene();
		if(callback_ != null)
		{			
			callback_();
			callback_ = null;
		}
	}
	
	public void SetEnabled()
	{
		bool old_update_enabled = update_enabled_;
		update_enabled_ = true;
		if(update_enabled_ != old_update_enabled)
			CheckIfNeedToUpdate();
	}
	
	public void SetDisabled()
	{
		update_enabled_ = false;
	}
	
	public bool AlreadySpawned(CallbackAfterFinish callback)
	{
		if(!can_check_version_)
		{
			callback_ = callback;
			return true;
		}
		return false;
	}
#else
	
	public void CheckIfNeedToUpdate()
	{
	}
	
	public void UpdateVersion()
	{
	}
	
	public void SetEnabled()
	{
	}
	
	public void SetDisabled()
	{
	}
	
	public bool AlreadySpawned(CallbackAfterFinish callback)
	{
		return false;
	}
	
#endif
	
}
