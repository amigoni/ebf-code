using UnityEngine;
using System.Collections;

public class ConnectingSavingPopUp : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		GameObject.Find("SavingSprite").GetComponent<MeshRenderer>().enabled = false;
		GameObject.Find("ConnectingSprite").GetComponent<MeshRenderer>().enabled = true;
	}
	
	void Start()
	{
		Init ();
	}
	
	void OnEnable()
	{
		Init ();
	}
	
	public void Init()
	{
		tk2dButton[] buttons = GetComponentsInChildren<tk2dButton>();
		Camera current_camera = Framework.Instance.scene_manager().GetCurrentCamera();
		foreach(tk2dButton button in buttons)
		{
			button.viewCamera = current_camera;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void SetSaving()
	{
		GameObject.Find("SavingSprite").GetComponent<MeshRenderer>().enabled = true;
		GameObject.Find("ConnectingSprite").GetComponent<MeshRenderer>().enabled = false;
	}
	
	public void SetConnecting()
	{
		gameObject.SetActive(true);
		GameObject.Find("SavingSprite").GetComponent<MeshRenderer>().enabled = false;
		GameObject.Find("ConnectingSprite").GetComponent<MeshRenderer>().enabled = true;
	}
}
