using System;
using System.Collections;
using System.Collections.Generic;
using EbFightAPI;
using UnityEngine;

public class WWWNetworkProvider : MonoBehaviour, INetworkProvider
{    
#if UNITY_EDITOR 
	public string Server = "http://whitemilktest.appspot.com/api";
   	//public string Server = "http://ebfight-unity.appspot.com/api";
	//public string Server = "http://localhost:8080/api";
#elif UNITY_WEBPLAYER
    // Kong App
	public string Server = "http://ebfight-kongregateplus.appspot.com/api";
	// Kong App Testing
	//public string Server = "http://ebfight-kongregate.appspot.com/api";
#elif UNITY_IPHONE
	// IOS release
	//public string Server = "http://ebfight-ios-version.appspot.com/api";
	// IOS testing
	public string Server = "http://ebfight-ios-testing.appspot.com/api";
#elif UNITY_ANDROID
	// ANDROID release
	public string Server = "http://ebfight-ios-version.appspot.com/api";
	// ANDROID testing
	//public string Server = "http://ebfight-ios-testing.appspot.com/api";
	//public string Server = "http://ebfight-ios-testing.appsp0t.com/api";
#endif

    public float Timeout = 10.0f;
	
	void Start()
	{
		
	}
	
	void Update()
	{
	}
	
    public void SendRequest(string url, Dictionary<string, string> data, Action<ResponseStatus, string> action)
    {
        StartCoroutine(SendRequestWithWWW(url, data, action));
    }

    private IEnumerator SendRequestWithWWW(string url, Dictionary<string, string> data, Action<ResponseStatus, string> action)
    {
        var form = new WWWForm();
        foreach (var item in data)
            form.AddField(item.Key, item.Value);
        
        var www = new WWW(Server + url, form);
        var elapsedTime = 0.0f;

        while(!www.isDone)
        {
            elapsedTime += Time.deltaTime;

            if (elapsedTime >= Timeout)
                break;

            yield return null;
        }
		
		Framework.Instance.InstanceComponent<ServerAPI>().CloseConnectingPopUp();
		
        if(!www.isDone || !string.IsNullOrEmpty(www.error))
        {  
			
			if(!string.IsNullOrEmpty(www.error) && 
				(url != "/authenticate-player" && 
				 url != "/register-player" &&
				 url != "/get-version" &&
				 url != "/get-battle-report"))
				Framework.Instance.InstanceComponent<ServerAPI>().NotifyError();
			
			action(ResponseStatus.Disconnected, MiniJSON.Json.Serialize(JSONResult.ToMiniJSON(
                new Dictionary<string, object>
                    {
                        {"error", true},
                        {"result", www.error}
                    })));
			
            yield break;
        }
		
        action(ResponseStatus.Success, www.text);
    }
}