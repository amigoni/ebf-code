using UnityEngine;
using System;
using System.Collections.Generic;
using EbFightAPI;
using System.Runtime.InteropServices;
using SimpleJSON;

public class ServerAPI : MonoBehaviour {
	
	//private string user_id_ = "1234";
	private string kong_token_ = "";
	
	private GameObject connecting_popup_;
	private PopUpWindowPrefab pop_up_window_;
	
	private Action<ResponseStatus, JSONResult> begin_match_action_;
	
	public void update_user_id(string user_id)
	{
		//user_id_ = user_id;
	}
	
	void Awake()
	{
		connecting_popup_ = (GameObject)  Instantiate( Resources.Load("Framework/ConnectingSavingPrefab"));
		connecting_popup_.SetActive(false);
		connecting_popup_.transform.parent = transform;
		
		EbFightAPI.Player player = new EbFightAPI.Player();
		player.Boss = new EbFightAPI.Boss();
		Framework.Instance.InstanceComponent<wmf.StaticParams>().Add("EbFightAPI.Player", player);
	}
	
	void Start () {
		gameObject.AddComponent<WWWNetworkProvider>();
		EbFight.Setup(GetComponent<WWWNetworkProvider>());
		
		GameObject pop_up_window_prefab_ = (GameObject)  Instantiate( Resources.Load("Framework/PopUpWindowPrefab"));
		pop_up_window_prefab_.SetActive(false);
		pop_up_window_prefab_.transform.parent = transform;
		pop_up_window_ = pop_up_window_prefab_.GetComponent<PopUpWindowPrefab>();
		pop_up_window_.set_destroy_on_closing(false); 
	}
	
	// Connections internal Callback
	public void CloseConnectingPopUp()
	{
		connecting_popup_.GetComponent<ConnectingSavingPopUp>().SetConnecting();
		connecting_popup_.SetActive(false);
	}
	
	public void NotifyError()
	{
		pop_up_window_.gameObject.SetActive(true);
		pop_up_window_.Init("We can't connect", "Please try again in a bit");		
	}
	
	public void NotifyError(string error_title,  string error_message)
	{
		pop_up_window_.gameObject.SetActive(true);
		pop_up_window_.Init(error_title, error_message);		
	}
	
	public PopUpWindowPrefab pop_up_window()
	{
		return pop_up_window_;
	}
	
	public void ClosePopUpWindow()
	{
		pop_up_window_.gameObject.SetActive(false);	
	}
	
	
	//EbFightAPI 
	public void AuthenticatePlayer(string user_id, string token, Action<ResponseStatus, JSONResult> action)
	{
		connecting_popup_.SetActive(true);
		EbFight.AuthenticatePlayer(user_id, token, action);
	}
	
	public void RegisterPlayer(string user_id, string user_name, string token, Action<ResponseStatus, JSONResult> action)
	{
		connecting_popup_.SetActive(true);
		EbFight.RegisterPlayer(user_id, "1234", user_name, GetCurrentPlatform(), token, action);
	}
	
	public void RegisterPlayer(string user_name, string token, Action<ResponseStatus, JSONResult> action)
	{
		connecting_popup_.SetActive(true);
		EbFight.RegisterPlayer("1234", user_name, GetCurrentPlatform(), token, action);
	}
	
	private string GetCurrentPlatform()
	{
#if UNITY_WEBPLAYER
		return "web";
#elif UNITY_IPHONE
		return "ios";
#elif UNITY_ANDROID
		return "android";
#endif
	}
	
	public void GetBossConfiguration(Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
	    	EbFight.GetBossConfiguration(action);
		}
    }
	
	public void UpdateBossConfiguration(EbFightAPI.Boss boss, Action<ResponseStatus, JSONResult> action)
	{
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			connecting_popup_.GetComponent<ConnectingSavingPopUp>().SetSaving();
			
			EbFight.UpdateBossConfiguration(boss, action);
		}
	}
	
	public void FreeWhiteCoinsLeftToday(int ddLevel, Action<ResponseStatus, JSONResult> action)
	{
		if(CheckPreCall())
		{
			EbFight.FreeWhiteCoinsLeftToday(ddLevel, action);
			connecting_popup_.SetActive(true);
		}
	}
	
	public void UpdatePlayerData(string dd_data, Action<ResponseStatus, JSONResult> action)
	{
		if(CheckPreCall())
		{
			EbFight.UpdatePlayerData(dd_data, action);
			//connecting_popup_.SetActive(true);
		}
	}
	
	public void GetPlayerBattleLog(Action<ResponseStatus, JSONResult> action)
	{
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.GetPlayerBattleLog(action);
		}
	}
	
	public void GetBossBattleLog(Action<ResponseStatus, JSONResult> action)
	{
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.GetBossBattleLog(action);
		}
	}
	
	public void GetBattleReport(Action<ResponseStatus, JSONResult> action)
	{
		if(CheckPreCall())
		{
			EbFight.GetBattleReport(action);
			connecting_popup_.SetActive(true);
		}
	}
	
	public void SyncWhiteCoins(int ownedWhiteCoins, Action<ResponseStatus, JSONResult> action)
    {	
		if(CheckPreCall())
		{
			EbFight.SyncWhiteCoins(ownedWhiteCoins, action);
			connecting_popup_.SetActive(true);
		}
	}
	
	public void FinalizePurchase(Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.FinalizePurchase(kong_token_, action);
		}
	}
	
	public void FinalizePurchaseIOS(string receipt, Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.FinalizePurchaseIOS(receipt, action);
		}
	}
	
	public void FinalizePurchaseAndroid(string receipt, Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.FinalizePurchaseAndroid(receipt, action);
		}
	}
	
	public void CollectWhiteCoins(Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.CollectWhiteCoins(action);
		}
	}
	
	//Match
	public void BeginMatch(Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			begin_match_action_ = action;
			if(!CheckUnfinishedMatch())
			{
				//EbFight.BeginMatch(OnBeginMatch);	
				EbFight.FindMatch(Framework.Instance.InstanceComponent<PlayerData>().GetInt("dd_att_level"), OnBeginMatch);	
			}
		}
	}
	
	public void BeginMatch(string contender_id, Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.BeginMatch(contender_id, action);
		}
	}
	
	public void Revenge(string unique_id, Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.Revenge(unique_id, action);
		}
	}
	
	public void EndMatch(string sessionId, string[] result, long score, Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			DeleteMatchInfo();
			EbFight.EndMatch(sessionId, result, (int)score, action);
		}
	}
	
	private void OnBeginMatch(ResponseStatus status, JSONResult data)
	{
		if(status == ResponseStatus.Success)
		{
			SaveLastMatchSession(data.Data);
			begin_match_action_(status, data);
		}
	}
	
	//Training
	public void BeginTraining(Action<ResponseStatus, JSONResult> action)
    {
		if(CheckPreCall())
		{
			connecting_popup_.SetActive(true);
			EbFight.BeginTraining(action);
		}
	}
	
	//Match info
	static string MATCH_KEY = "EbFightLastSession";
	public void SaveLastMatchSession(string session_text)
	{
		PlayerPrefs.SetString(MATCH_KEY, session_text);
	}
	
	public MatchSession GetLastMatchSession()
	{
		string session_text = PlayerPrefs.GetString(MATCH_KEY);
		if(session_text != "")
		{
			JSONResult result = JSONResult.FromData(session_text);
			return result.As<MatchSession>();
		}
		
		return null;
	}
	
	public void DeleteMatchInfo()
	{
		PlayerPrefs.DeleteKey(MATCH_KEY);
	}
	
	public bool CheckUnfinishedMatch()
	{
		MatchSession match_session = GetLastMatchSession();
		if(match_session != null)
		{
			EbFight.EndMatch(match_session.SessionId, new string[] { "died", "none", "none" }, 0, OnEndLastMatch);
			return true;
		}
		
		return false;
	}
			
	public void OnEndLastMatch(ResponseStatus status, JSONResult data)	
	{
		if(status == ResponseStatus.Success)
		{
			EbFightAPI.Player player = data.As<EbFightAPI.Player>();
			Framework.Instance.InstanceComponent<ServerAPI>().SavePlayer(player);
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("white_coins", player.WhiteCoins);
		}
		
		DeleteMatchInfo();
		connecting_popup_.SetActive(true);
		BeginMatch(begin_match_action_);
	}
	
	//utility
	public void SavePlayer(EbFightAPI.Player player)
	{
		wmf.StaticParams static_params = Framework.Instance.InstanceComponent<wmf.StaticParams>();
			
		if(static_params.ContainsKey("EbFightAPI.Player"))
			static_params.ChangeValue("EbFightAPI.Player", player);
		else
			static_params.Add("EbFightAPI.Player", player);
	}
	
	public EbFightAPI.Player GetPlayer()
	{
		return (EbFightAPI.Player) Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("EbFightAPI.Player");
	}
	
	public void SaveBoss(EbFightAPI.Boss boss)
	{
		EbFightAPI.Player player = GetPlayer();
		player.Boss = boss;
		
		SavePlayer(player);
	}
	
	public EbFightAPI.Boss GetBoss()
	{
		if(!Framework.Instance.InstanceComponent<wmf.StaticParams>().ContainsKey("EbFightAPI.Player"))
			return null;
		return ((EbFightAPI.Player) Framework.Instance.InstanceComponent<wmf.StaticParams>().Get("EbFightAPI.Player")).Boss;
	}
	
	public void GetVersion(Action<ResponseStatus, JSONResult> action)
	{
		EbFight.GetVersion(action);
	}
	
	//Platform depend
	public void PurchaseItem(string item_id, Action<ResponseStatus, JSONResult> on_purchase)
	{
#if UNITY_WEBPLAYER
		Framework.Instance.InstanceComponent<WebKongregateAPI>().PurchaseItem(item_id, on_purchase);
#endif
	}
	
#if UNITY_IPHONE || UNITY_ANDROID
	public void PurchaseItemMobile(string store_key,string item_name, GameObject current_item)
	{
		connecting_popup_.SetActive(true);
		Framework.Instance.InstanceComponent<InAppPurchasesUnibillManager>().BuyItem(store_key,current_item);		
	}
#endif
	
#if UNITY_IPHONE && !UNITY_EDITOR
	
	[DllImport ("__Internal")]
	private static extern bool _TestInternetConnect();
	
	public bool IsConnected()
	{
		return _TestInternetConnect();
	}
	
#else
	
	public bool IsConnected()
	{		
		if (Network.player.ipAddress.ToString() != "127.0.0.1")
	    {
	        return true;    
	    }
		
		 return false;
	}
#endif
	
	private bool CheckPreCall()
	{
		if(EbFight.UserId == null)
			return false;
				
		return true;
	}
	
	public BattleReport GetBattleReport(string json_data)
	{
		BattleReport battle_report = new BattleReport();
		
		JSONNode data_result = JSON.Parse(json_data);
		JSONNode data = data_result["result"];
		
		battle_report.MatchesWon = data["matches_won"].AsInt;
		battle_report.HasBossLeveledUp = data["has_boss_leveled_up"].AsBool;
		battle_report.WhiteCoinsEarned = data["white_coins_earned"].AsInt;
		battle_report.TotalGainedXp = data["total_gained_xp"].AsInt;
		battle_report.MatchesLost = data["matches_lost"].AsInt;
		battle_report.TotalMatches = data["total_matches"].AsInt;
		battle_report.WhiteCoinsChestCap = data["white_coins_chest_cap"].AsInt;
		battle_report.TotalPointsModifier = data["total_points_modifier"].AsInt;
		battle_report.NewLevel = data["new_level"].AsInt;
		battle_report.CoinsEarned = data["coins_earned"].AsInt;
		
		return battle_report;
	}
}
