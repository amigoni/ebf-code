using System;
using System.Collections.Generic;

namespace EbFightAPI
{
    public enum ResponseStatus
    {
        Error,
        Success,
        Disconnected
    }

    public interface INetworkProvider
    {
        void SendRequest(string url, Dictionary<String, String> data, Action<ResponseStatus, string> action);
    }
}
