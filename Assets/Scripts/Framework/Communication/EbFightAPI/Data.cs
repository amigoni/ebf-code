using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using MiniJSON;

namespace EbFightAPI
{
    public class ErrorData : IEquatable<ErrorData>
    {
        public List<string> Message { get; set; }

        public bool Equals(ErrorData other)
        {
            if (other == null)
                return false;

            return Message == other.Message;
        }
    }

    public class JSONResult
    {
        public static JSONResult FromData(string data)
        {
            return new JSONResult(data);
        }

        public static object ToMiniJSON(object data)
        {
            if (data == null)
                return null;

            var type = data.GetType();
            if(type.IsValueType)
                return data;

            if(type == typeof (string))
                return data;

            var listData = data as IList;
            if (listData != null)
                return listData.OfType<object>().Select<object, object>(ToMiniJSON).ToList();

            var dictData = data as IDictionary;
            if (dictData != null)
                return dictData;

            return type.GetProperties().ToDictionary(
                p => KeyNameFrom(p.Name),
                p =>
                    {
                        var value = p.GetValue(data, null);
                        return value != null ? ToMiniJSON(value) : null;
                    });
        }

        private JSONResult(string data)
        {
            Data = data;
            ParsedData = Json.Deserialize(data) as Dictionary<string, object>;
        }

        public string Data { get; private set; }

        public Dictionary<string, object> ParsedData { get; private set; }

        public bool IsError
        {
            get { return (bool)ParsedData["error"]; }
        }

        public string ErrorMessage
        {
            get { return !IsError ? null : As<ErrorData>().Message.Aggregate((buff, s) => buff + "\n" + s); }
        }

        public T As<T>()
        {
            return InternalAs<T>(ParsedData["result"]);
        }

        public List<T> AsListOf<T>()
        {
            var listData = ParsedData["result"] as List<object>;
            if(listData == null)
                throw new NotSupportedException("Unsupported type " + typeof(T).FullName);

            return InternalAsListOf<T>(listData);
        }

        private static List<T> InternalAsListOf<T>(IEnumerable<object> listData)
        {
            return listData.Select<object, T>(InternalAs<T>).ToList();
        }

        private static T InternalAs<T>(object data)
        {
            if(data is T)
                return (T)data;

            var type = typeof(T);
            var holder = (T)Activator.CreateInstance(type);
            var dataDict = data as Dictionary<string, object>;
            if(dataDict == null)
                throw new ArgumentException("Expected a JSON object, got " + data.GetType().FullName + " instead", "data");

            foreach(var propertyInfo in type.GetProperties())
            {
                var keyName = KeyNameFrom(propertyInfo.Name);
                var keyType = propertyInfo.PropertyType;
                var conversionMethod = ConversionMethodFor(keyType);
                var value = dataDict[keyName];
                var convertedData = value != null ? conversionMethod.Invoke(null, new[] {value}) : null;

                propertyInfo.SetValue(holder, convertedData, null);
            }

            return holder;
        }

        private static MethodInfo ConversionMethodFor(Type keyType)
        {
            var internalAsMethod = typeof(JSONResult).GetMethod("InternalAs", BindingFlags.NonPublic | BindingFlags.Static);
            var internalAsListOfMethod = typeof(JSONResult).GetMethod("InternalAsListOf", BindingFlags.NonPublic | BindingFlags.Static);

            return keyType.Name.Contains("List`1")
                ? internalAsListOfMethod.MakeGenericMethod(new[] { keyType.GetGenericArguments()[0] })
                : internalAsMethod.MakeGenericMethod(new[] { keyType });
        }

        private static string KeyNameFrom(string name)
        {
            var resultString = Regex.Replace(name, "([a-z])([A-Z])", "$1_$2");
            return resultString.ToLower();
        }
    }

    public class BossWeaponInfo : IEquatable<BossWeaponInfo>
    {
        public string Kind { get; set; }
        public long Level { get; set; }

        public bool Equals(BossWeaponInfo other)
        {
            if (other == null)
                return false;

            if (Kind != other.Kind)
                return false;

            return Level == other.Level;
        }
    }

    public class StageInfo : IEquatable<StageInfo>
    {
        public long Level { get; set; }
        public long ReloadSpeed { get; set; }
        public long ShotsPerWave { get; set; }
        public long Environment { get; set; }
        public List<BossWeaponInfo> Weapons { get; set; }

        public StageInfo()
        {
            Weapons = new List<BossWeaponInfo>();
        }

        public bool Equals(StageInfo other)
        {
            if (other == null)
                return false;

            if (Level != other.Level)
                return false;

            if (ReloadSpeed != other.ReloadSpeed)
                return false;

            if (ShotsPerWave != other.ShotsPerWave)
                return false;

            if (Environment != other.Environment)
                return false;

            if (Weapons.Count != other.Weapons.Count)
                return false;

            return !Weapons.Where((t, index) => !t.Equals(other.Weapons[index])).Any();
        }
    }

    public class Utility : IEquatable<Utility>
    {
        public string Kind { get; set; }
        public long Expires { get; set; }

        public bool Equals(Utility other)
        {
            if (other == null)
                return false;

            if (Kind != other.Kind)
                return false;

            //if (Expires != other.Expires)
            //    return false;

            return true;
        }
    }

    public class LeaderboardRecord : IEquatable<LeaderboardRecord>
    {
        public long Position { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public long Points { get; set; }

        public bool Equals(LeaderboardRecord other)
        {
            if (other == null)
                return false;

            if (Position != other.Position)
                return false;

            if (Username != other.Username)
                return false;

            if (UserId != other.UserId)
                return false;

            if (Points != other.Points)
                return false;

            return true;
        }
    }

    public class ClientVersion : IEquatable<ClientVersion>
    {
        public long BuildVersion { get; set; }
        public long DataVersion { get; set; }

        public bool Equals(ClientVersion other)
        {
            if (other == null)
                return false;

            if (BuildVersion != other.BuildVersion)
                return false;

            if (DataVersion != other.DataVersion)
                return false;

            return true;
        }
    }

    public class Boss : IEquatable<Boss>
    {
        public long Level { get; set; }
        public long Defence { get; set; }
        public long Attack { get; set; }
        public long Power { get; set; }
        public long Rank { get; set; }
        public long League { get; set; }
        public long Xp { get; set; }
        public long StagesCount { get; set; }
        public List<StageInfo> Stages { get; set; }
        public List<Utility> Utilities { get; set; } 

        public Boss()
        {
            Stages = new List<StageInfo>();
            Utilities = new List<Utility>();
        }

        public bool Equals(Boss other)
        {
            if (other == null)
                return false;

            if (Level != other.Level)
                return false;

            if (Defence != other.Defence)
                return false;

            if (Attack != other.Attack)
                return false;

            if (Power != other.Power)
                return false;

            if (Xp != other.Xp)
                return false;

            if (League != other.League)
                return false;

            if (Rank != other.Rank)
                return false;

            if (StagesCount != other.StagesCount)
                return false;

            if (Stages.Count != other.Stages.Count)
                return false;

            if (Stages.Where((t, index) => !t.Equals(other.Stages[index])).Any())
                return false;

            if (Utilities.Where((t, index) => !t.Equals(other.Utilities[index])).Any())
                return false;

            return true;
        }
    }

    public class MatchSession : IEquatable<MatchSession>
    {
        public long Seed { get; set; }
        public string SessionId { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public Player Contender { get; set; }

        public bool Equals(MatchSession other)
        {
            if (other == null)
                return false;

            if (Seed != other.Seed)
                return false;

            if (Username != other.Username)
                return false;

            if (UserId != other.UserId)
                return false;

            if (SessionId != other.SessionId)
                return false;

            return Contender.Equals(other.Contender);
        }
    }

    public class BattleReport : IEquatable<BattleReport>
    {
        public long TotalMatches { get; set; }
        public long CoinsEarned { get; set; }
        public long WhiteCoinsEarned { get; set; }
        public long MatchesWon { get; set; }
        public long MatchesLost { get; set; }
        public long TotalPointsModifier { get; set; }
        public long TotalGainedXp { get; set; }
        public bool HasBossLeveledUp { get; set; }
        public long NewLevel { get; set; }
        public long WhiteCoinsChestCap { get; set; }

        public bool Equals(BattleReport other)
        {
            if (other == null)
                return false;

            if (TotalMatches != other.TotalMatches)
                return false;

            if (CoinsEarned != other.CoinsEarned)
                return false;

            if (WhiteCoinsEarned != other.WhiteCoinsEarned)
                return false;

            if (MatchesWon != other.MatchesWon)
                return false;

            if (MatchesLost != other.MatchesLost)
                return false;

            if (TotalGainedXp != other.TotalGainedXp)
                return false;

            if (HasBossLeveledUp != other.HasBossLeveledUp)
                return false;

            if (NewLevel != other.NewLevel)
                return false;

            if (TotalPointsModifier != other.TotalPointsModifier)
                return false;

            if (WhiteCoinsChestCap != other.WhiteCoinsChestCap)
                return false;

            return true;
        }
    }

    public class BattleLogInfo : IEquatable<BattleLogInfo>
    {
        public List<BattleLog> Log { get; set; }
        public long Position { get; set; }
        public long League { get; set; }

        public bool Equals(BattleLogInfo other)
        {
            if (other == null)
                return false;

            if (Position != other.Position)
                return false;

            if (League != other.League)
                return false;

            if (Log.Where((t, index) => !t.Equals(other.Log[index])).Any())
                return false;

            return true;
        }
    }

    public class BattleLog : IEquatable<BattleLog>
    {
        public string OpponentId { get; set; }
        public string OpponentName { get; set; }
        public long OpponentRank { get; set; }
        public List<string> Results { get; set; }
        public long Created { get; set; }
        public long Modifier { get; set; }
        public long WhiteCoins { get; set; }
        public long GainedXp { get; set; }
        public string UniqueId { get; set; }
        public bool HadRevenge { get; set; }

        public bool Equals(BattleLog other)
        {
            if (other == null)
                return false;

            if (OpponentId != other.OpponentId)
                return false;

            if (OpponentName != other.OpponentName)
                return false;

            if (OpponentRank != other.OpponentRank)
                return false;

            if (Created != other.Created)
                return false;

            if (Modifier != other.Modifier)
                return false;

            if (WhiteCoins != other.WhiteCoins)
                return false;

            if (GainedXp != other.GainedXp)
                return false;

            if (UniqueId != other.UniqueId)
                return false;

            if (HadRevenge != other.HadRevenge)
                return false;

            if (Results.Where((t, index) => !t.Equals(other.Results[index])).Any())
                return false;

            return true;
        }
    }

    public class Player : IEquatable<Player>
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public long PlayerRank { get; set; }
        public long PlayerLeague { get; set; }
        public long WhiteCoins { get; set; }
        public Boss Boss { get; set; }
        public string Data { get; set; }

        public bool Equals(Player other)
        {
            if (other == null)
                return false;

            if (UserId != other.UserId) 
                return false;

            if (Username != other.Username)
                return false;

            if (WhiteCoins != other.WhiteCoins)
                return false;

            if (PlayerRank != other.PlayerRank) 
                return false;

            if (PlayerLeague != other.PlayerLeague)
                return false;

            if (Data != other.Data)
                return false;

            return Boss.Equals(other.Boss);
        }

    }
}