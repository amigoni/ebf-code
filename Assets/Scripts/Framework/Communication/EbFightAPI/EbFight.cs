﻿using System;
using System.Collections.Generic;
using MiniJSON;

namespace EbFightAPI
{
    public sealed class EbFight
    {
        public static string UserId { get; set; }
        public static INetworkProvider NetworkProvider { get; private set; }

        public static void Setup(INetworkProvider networkProvider)
        {
            if (NetworkProvider != null && networkProvider != null)
                throw new ArgumentException("NetworkProvider already set!");

            NetworkProvider = networkProvider;
			
			UserId = "";
        }

        public static void Echo(string message, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/echo",
                new Dictionary<string, string>
                    {
                        {"message", message}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void RegisterPlayer(string userId, string deviceId, string username, string platform, string token, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/register-player",
                new Dictionary<string, string>
                    {
                        {"user_id", userId},
                        {"device_id", deviceId},
                        {"username", username},
                        {"platform", platform},
                        {"token", token},
                    },
                (status, data) =>
                    {
                        var jsonResult = JSONResult.FromData(data);
                        if (!jsonResult.IsError)
                            UserId = jsonResult.As<Player>().UserId;
                        action(jsonResult.IsError ? ResponseStatus.Error : status, jsonResult);
                    });
        }

        public static void RegisterPlayer(string deviceId, string username, string platform, string token, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/register-player",
                new Dictionary<string, string>
                    {
                        {"user_id", ""},
                        {"device_id", deviceId},
                        {"username", username},
                        {"platform", platform},
                        {"token", token},
                    },
                (status, data) =>
                {
                    var jsonResult = JSONResult.FromData(data);
                    if (!jsonResult.IsError)
                        UserId = jsonResult.As<Player>().UserId;
                    action(jsonResult.IsError ? ResponseStatus.Error : status, jsonResult);
                });
        }

        public static void AuthenticatePlayer(string userId, string token, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/authenticate-player",
                new Dictionary<string, string>
                    {
                        {"user_id", userId},
                        {"token", token}
                    },
                (status, data) =>
                    {
                        var jsonResult = JSONResult.FromData(data);
                        if(!jsonResult.IsError)
                            UserId = jsonResult.As<Player>().UserId;
                        action(jsonResult.IsError ? ResponseStatus.Error : status, jsonResult);
                    });
        }

        public static void GetBossConfiguration(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/get-boss-configuration",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void UpdateBossConfiguration(Boss boss, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/update-boss-configuration",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"defence", boss.Defence.ToString()},
                        {"attack", boss.Attack.ToString()},
                        {"power", boss.Power.ToString()},
                        {"utilities", Json.Serialize(JSONResult.ToMiniJSON(boss.Utilities))},
                        {"stages", Json.Serialize(JSONResult.ToMiniJSON(boss.Stages))}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void BeginMatch(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/begin-match",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void BeginTraining(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/begin-training",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void BeginMatch(string contenderId, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/begin-match",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"contender_id", contenderId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void GetVersion(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/get-version",
                new Dictionary<string, string>()
				{
					{"user_id", UserId}
				},
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void FindMatch(int ddAttack, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/find-match",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"dd_attack", ddAttack.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void BeginMatchSpecific(string contenderUsername, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/begin-match-specific",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"contender_username", contenderUsername}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void EndMatch(string sessionId, string[] results, int score, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/end-match",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"session_id", sessionId},
                        {"results", Json.Serialize(JSONResult.ToMiniJSON(results))},
                        {"score", score.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void SyncWhiteCoins(int ownedWhiteCoins, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/sync-white-coins",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"owned_white_coins", ownedWhiteCoins.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void FreeWhiteCoinsLeftToday(int ddLevel, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/free-white-coins-left-today",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"dd_level", ddLevel.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void GetBattleReport(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/get-battle-report",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void GetPlayerBattleLog(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/get-player-battle-log",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void GetBossBattleLog(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/get-boss-battle-log",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void BuyCoinsPack(int pack, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/buy-coins-pack",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"pack", pack.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void GetPlayerLeaderboard(long league, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/get-player-leaderboard",
                new Dictionary<string, string>
                    {
                        {"league", league.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void GetBossLeaderboard(long league, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();

            NetworkProvider.SendRequest(
                "/get-boss-leaderboard",
                new Dictionary<string, string>
                    {
                        {"league", league.ToString()}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void FinalizePurchase(string token, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/finalize-purchase",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"token", token}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void FinalizePurchaseIOS(string receipt, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/finalize-purchase-ios",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"receipt", receipt}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }
		
		public static void FinalizePurchaseAndroid(string receipt, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/finalize-purchase-android",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"receipt", receipt}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void UpdatePlayerData(string data, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/update-player-data",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"data", data}
                    },
                (status, d) => ForwardAPIResultTo(status, d, action));
        }

        public static void CollectWhiteCoins(Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/collect-white-coins",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        public static void Revenge(string uniqueId, Action<ResponseStatus, JSONResult> action)
        {
            AssureValidNetworkProvider();
            AssureAuthenticated();

            NetworkProvider.SendRequest(
                "/revenge",
                new Dictionary<string, string>
                    {
                        {"user_id", UserId},
                        {"battle_log_id", uniqueId}
                    },
                (status, data) => ForwardAPIResultTo(status, data, action));
        }

        private static void ForwardAPIResultTo(ResponseStatus status, string data, Action<ResponseStatus, JSONResult> action)
        {
            var jsonResult = JSONResult.FromData(data);
            action(jsonResult.IsError ? ResponseStatus.Error : status, jsonResult);
        }

        private static void AssureValidNetworkProvider()
        {
            if(NetworkProvider == null)
                throw new ArgumentException("NetworkProvider has not been set yet!");
        }

        private static void AssureAuthenticated()
        {
            if (UserId == null)
                throw new ArgumentException("User has not been authenticated yet!");
        }

        public sealed class Debug
        {
            public static void ResetPlayerWhiteCoinsResetTime(string userId, Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/reset-player-white-coins-reset-time",
                    new Dictionary<string, string>
                    {
                        {"user_id", userId}
                    },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void ResetPlayerWhiteCoins(string userId, int amount, Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/reset-player-white-coins",
                    new Dictionary<string, string>
                    {
                        {"user_id", userId},
                        {"amount", amount.ToString()}
                    },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void DeletePlayer(string userId, Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/delete-player",
                    new Dictionary<string, string>
                    {
                        {"user_id", userId}
                    },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void SetPlayerRank(string userId, int rank, Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/set-player-rank",
                    new Dictionary<string, string>
                    {
                        {"user_id", userId},
                        {"rank", rank.ToString()}
                    },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void SetBossRank(string userId, int rank, Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/set-boss-rank",
                    new Dictionary<string, string>
                    {
                        {"user_id", userId},
                        {"rank", rank.ToString()}
                    },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void SetBossXp(string userId, int xp, Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/set-boss-xp",
                    new Dictionary<string, string>
                    {
                        {"user_id", userId},
                        {"xp", xp.ToString()}
                    },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void DeleteAllMatches(Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/delete-all-matches",
                    new Dictionary<string, string>(),
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void ClearDatastore(Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();

                NetworkProvider.SendRequest(
                    "/clear-datastore",
                    new Dictionary<string, string>(),
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }

            public static void SimulateNextDay(Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();
                AssureAuthenticated();

                NetworkProvider.SendRequest(
                    "/simulate-next-day",
                    new Dictionary<string, string>
                        {
                            {"user_id", UserId}
                        },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }
			
			public static void SimulateOneDayOfInactivityBoss(Action<ResponseStatus, JSONResult> action)
            {
                AssureValidNetworkProvider();
                AssureAuthenticated();

                NetworkProvider.SendRequest(
                    "/simulate-one-day-of-inactivity-boss",
                    new Dictionary<string, string>
                        {
                            {"user_id", UserId}
                        },
                    (status, data) => ForwardAPIResultTo(status, data, action));
            }
        }
    }
}
