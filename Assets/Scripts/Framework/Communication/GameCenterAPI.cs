using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public class GameCenterAPI : MonoBehaviour {

/*	
#region Singleton variables and functions
	private static GameCenterSingleton instance;
	
	public static GameCenterSingleton Instance
	{
		get
		{
			if(instance == null)
			{
				instance = new GameCenterSingleton();
				instance.Initialize();
			}
			return instance;
		}
	}
#endregion
*/
	
	private IAchievement[] achievements;
	
	public GameCenterAPI (){}
	
	public void Initialize()
	{
		GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
		if(!IsUserAuthenticated())
		{
			Social.localUser.Authenticate(ProcessAuthentication);
		}
	}	
	
	
	public bool IsUserAuthenticated()
	{
#if UNITY_IPHONE && !UNITY_EDITOR
		if(Social.localUser.authenticated)
		{
			return true;
		}
		else
		{
			Debug.Log("GameCenter: User not Authenticated");
			return false;
		}
#else
		return false;
#endif
	}	
	
	
	public string GetGameCenterID()
	{
		return Social.localUser.id;
	}
	
	
	public string GetGameCenterUserName()
	{
		return Social.localUser.userName;
	}
	
	
	public void ShowAchievementUI()
	{
		if(IsUserAuthenticated())
		{
			Social.ShowAchievementsUI();
		}
	}
	
	
	public void ShowLeaderboardUI()
	{
		if(IsUserAuthenticated())
		{
			Social.ShowLeaderboardUI();
		}
	}
	
	
	public void PostHighScore(long score)
	{
		//Social.CreateLeaderboard();
		//Social.CreateLeaderboard().id = "1";
		
		Social.ReportScore(score,"single_player_high_score", result => {
            if (result)
                Debug.Log ("Successfully posted");
            else
                Debug.Log ("Failed to post");
        });
	}		
	
	
	public bool AddAchievementProgress(string achievementID, float percentageToAdd)
	{
		IAchievement a = GetAchievement(achievementID);
		if(a != null)
		{
			return ReportAchievementProgress(achievementID, ((float)a.percentCompleted + percentageToAdd));
		}
		else
		{
			return ReportAchievementProgress(achievementID, percentageToAdd);
		}
	}	
	
	
	public bool ReportAchievementProgress(string achievementID, float progressCompleted)
	{
		if(Social.localUser.authenticated)
		{
			if(!IsAchievementComplete(achievementID))
			{
				bool success = false;
				Social.ReportProgress(achievementID, progressCompleted, result => 
				{
		    		if (result)
					{
						success = true;
						LoadAchievements();
		        		Debug.Log ("Successfully reported progress");
					}
		    		else
					{
						success = false;
		        		Debug.Log ("Failed to report progress");
					}
				});
				
				return success;
			}
			else
			{
				return true;	
			}
		}
		else
		{
			Debug.Log("ERROR: GameCenter user not authenticated");
			return false;
		}
	}
	
	
	public void ResetAchievements()
	{
		GameCenterPlatform.ResetAllAchievements(ResetAchievementsHandler);	
	}
	
	
	void LoadAchievements()
	{
		Social.LoadAchievements (ProcessLoadedAchievements);
	}
	
	
	void ProcessAuthentication(bool success)
	{
		if(success)
		{
			Debug.Log ("GameCenter: Authenticated, checking achievements");

            LoadAchievements();
			GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);	
		}
		else
		{
			Debug.Log ("GameCenter: Failed to authenticate");
		}
	}	
	
	
	void ProcessLoadedAchievements (IAchievement[] achievements) 
	{
		//Clear the list
		if(this.achievements != null)
		{
			this.achievements = null;	
		}
		
        if (achievements.Length == 0)
		{
            Debug.Log ("GameCenter Error: no achievements found");
		}
        else
		{
            Debug.Log ("GameCenter: Got " + achievements.Length + " achievements");
			this.achievements = achievements;
		}
	}
	
	
	bool IsAchievementComplete(string achievementID)
	{
		if(achievements != null)
		{
			foreach(IAchievement a in achievements)
			{
				if(a.id == achievementID && a.completed)
				{
					return true;	
				}
			}
		}
		
		return false;
	}
	
	
	IAchievement GetAchievement(string achievementID)
	{
		if(achievements != null)
		{
			foreach(IAchievement a in achievements)
			{
				if(a.id == achievementID)
				{
					return a;	
				}
			}
		}
		return null;
	}
	
	
	void ResetAchievementsHandler(bool status)
	{
		if(status)
		{
			//Clear the list
			if(this.achievements != null)
			{
				this.achievements = null;	
			}
			
			LoadAchievements();
			
			Debug.Log("GameCenter: Achievements successfully resetted!");
		}
		else
		{
			Debug.Log("GameCenter: Achievements reset failure!");
		}
	}
}

