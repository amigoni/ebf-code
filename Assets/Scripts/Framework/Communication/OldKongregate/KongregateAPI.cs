#if UNITY_WEBPLAYER
using UnityEngine;
using System.Collections;
using EbFightAPI;
using System;

public delegate void GetKongragateParams (string user_id,string user_name,string token);

public class WebKongregateAPI: MonoBehaviour
{
	private string user_id_ = "";
	private string token_ = "";
	private string user_name_ = "";
	
	private GetKongragateParams get_kongregate_callback_;
	private Action<ResponseStatus, JSONResult> on_finalize_callback_;
	
	bool is_guest_ = false;
	
	public void Init (GetKongragateParams callback)
	{
		get_kongregate_callback_ = callback;
		
		//Application.ExternalEval(@"console.log('++++++++++INIT: Init++++++++++');");
		Application.ExternalEval (
				  "if(typeof(kongregateUnitySupport) != 'undefined'){" +
				  " kongregateUnitySupport.initAPI('Framework/WebKongregateAPI', 'OnKongregateAPILoaded');" +
				  "};"
				);
		//Application.ExternalEval(@"console.log('++++++++++END: Init++++++++++');");
	}
	
	public void ShowRegistrationBox()
	{
		Application.ExternalEval(@"console.log('++++++++++ShowRegistrationBox: Init++++++++++');");
		Application.ExternalEval("kongregate.services.showRegistrationBox();");
		Application.ExternalEval(@"console.log('++++++++++ShowRegistrationBox: end++++++++++');");
	}
		
	public void SetGuest(string is_guest)
	{
		if(is_guest == "YES")
		{
			Application.ExternalEval(@"console.log('++++++++++SetGuest: yes++++++++++');");
			//ShowRegistrationBox();
			Framework.Instance.scene_manager().LoadScene("RegisterPage", TransitionType.change_scene);
			is_guest_ = true;
		}
		else
		{
			Application.ExternalEval(@"console.log('++++++++++SetGuest: no++++++++++');");
			is_guest_ = false;
		}
	}
	
	public void UpdateGuest()
	{
		Application.ExternalEval(@"
			if(kongregate.services.isGuest())
				kongregateUnitySupport.getUnityObject().SendMessage('Framework/WebKongregateAPI', 'SetGuest', 'YES');
			else
				kongregateUnitySupport.getUnityObject().SendMessage('Framework/WebKongregateAPI', 'SetGuest', 'NO');
		");
	}
	
	private void OnKongregateAPILoaded (string userInfoString)
	{
		//Application.ExternalEval(@"console.log('++++++++++INIT: OnKongregateAPILoaded++++++++++');");
		// Split the user info up into tokens
		string[] param = userInfoString.Split ("|" [0]);
		string user_id = param [0];
		string user_name = param [1];
		string token = param [2];
		
		user_id_ = user_id;
		user_name_ = user_name;
		token_ = token;
		
		get_kongregate_callback_ (user_id, user_name, token);
		get_kongregate_callback_ = null;
		
		Application.ExternalEval(@"
			if(kongregate.services.isGuest())
				kongregateUnitySupport.getUnityObject().SendMessage('Framework/WebKongregateAPI', 'SetGuest', 'YES');
			else
				kongregateUnitySupport.getUnityObject().SendMessage('Framework/WebKongregateAPI', 'SetGuest', 'NO');
		");
		
		SendInitialized();
		//Application.ExternalEval(@"console.log('++++++++++END: OnKongregateAPILoaded++++++++++');");
	}
	
	public void PurchaseItem(string item_id, Action<ResponseStatus, JSONResult> on_finalize)
	{
		on_finalize_callback_ = on_finalize;
		//Application.ExternalEval(@"console.log('++++++++++INIT: PurchaseItem++++++++++');");
		Application.ExternalEval(@"kongregate.mtx.purchaseItems(['"+item_id+@"'], function () { 
			kongregateUnitySupport.getUnityObject().SendMessage('Framework/WebKongregateAPI', 'OnPurchase', 'do');
			});");
		//Application.ExternalEval(@"console.log('++++++++++END: PurchaseItem++++++++++');");
	}
	
	public void OnPurchase(string nothing)
	{
		//TO-DO check if need delay to call this
		//Application.ExternalEval(@"console.log('++++++++++INIT: OnPurchase++++++++++');");

		EbFightAPI.EbFight.FinalizePurchase(token_, OnFinalize );
		
		//Application.ExternalEval(@"console.log('++++++++++END: OnPurchase++++++++++');");
	}
	
	private void OnFinalize(ResponseStatus status, JSONResult data)
	{
		on_finalize_callback_(status, data);
	}
	
	public void SendScore(long score)
	{
		Application.ExternalCall("kongregate.stats.submit", "Endless", score);
	}
	
	public void SendBadge(string name, long value_badge)
	{
		Application.ExternalCall("kongregate.stats.submit", name, value_badge);
	}
	
	private void SendInitialized()
	{
		Application.ExternalCall("kongregate.stats.submit", "initialized", 1);
	}
	
	//Getter / Setter
	public string user_id()
	{
		return user_id_;
	}
	
	public string token()
	{
		return token_;
	}
	
	public string user_name()
	{
		return user_name_;
	}
	
	public bool is_guest()
	{
		return is_guest_;
	}
}
#endif