using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EbFightAPI;
using MiniJSON;
using System;

namespace wmf
{
	namespace KeenIOEvents
	{
		public class GamePlayEvent
		{
			public float load_time_ms_ = 0.0f;
			public float session_length_seconds_ = 0.0f;
			
			public void Save()
			{
				PlayerPrefs.SetFloat("load_time_ms", load_time_ms_);
				PlayerPrefs.SetFloat("session_length_seconds", session_length_seconds_);
			}
			
			public void LoadAndSend()
			{
				load_time_ms_ = PlayerPrefs.GetFloat("load_time_ms");
				session_length_seconds_ = PlayerPrefs.GetFloat("session_length_seconds");
				
				if(session_length_seconds_ > 0)
				{
					Analitycs analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
					analitycs_.AddEvent(KeenIOEventType.game_plays);
					analitycs_.AddNewKey(KeenIOEventType.game_plays, "is_close", true);
					analitycs_.AddNewKey(KeenIOEventType.game_plays, "load_time_ms", load_time_ms_);
					analitycs_.AddNewKey(KeenIOEventType.game_plays, "session_length_seconds", session_length_seconds_);
					//TO-DO change IOS
					analitycs_.AddNewKey(KeenIOEventType.game_plays, "is_from_background", false);
					analitycs_.SendEvent(KeenIOEventType.game_plays);
				}
			}
		}
		
		public class RoundEvent
		{
			//Base params
			public string run_id = "0000";
			public int round_number = 0;
			
			public bool is_win = false;
			public float damage_dealt = 0.0f;
			public float damage_received_from_missiles = 0.0f;
			public float damage_received_from_attacks = 0.0f;
			public bool is_resurrect = false;
			public int missiles_punches = 0;
			public int rockets_punches = 0;
			public int missiles_dodged = 0;
			public int boss_punches = 0;
			public int missiles_hit = 0;
			public int rockets_hit = 0;
			
			public EbFightAPI.Boss contender_boss;
					
			public Analitycs analitycs_;
				
			public RoundEvent(string runid, int roundnumber)
			{
				run_id = runid;
				round_number = roundnumber;
				analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			}
			
			public void SendEndRun(EbFightAPI.Boss contender_boss)
			{
				analitycs_.AddEvent(KeenIOEventType.round_end);	
				
				analitycs_.AddNewKey(KeenIOEventType.round_end, "run_id", run_id);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "is_win", is_win);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "round_number", round_number);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "damage_dealt", damage_dealt);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "damage_received_from_missiles", damage_received_from_missiles);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "damage_received_from_attacks", damage_received_from_attacks);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "is_resurrect", is_resurrect);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "missiles_punches", missiles_punches);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "rockets_punches", rockets_punches);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "missiles_dodged", missiles_dodged);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "boss_punches", boss_punches);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "missiles_hit", missiles_hit);
				analitycs_.AddNewKey(KeenIOEventType.round_end, "rockets_hit", rockets_hit);
				
				analitycs_.SendEvent(KeenIOEventType.round_end);	
				
				if(contender_boss != null)
				{
					//Boss Configuration
					KeenIOEvents.BossConfiguration boss_configuration = new KeenIOEvents.BossConfiguration();
					
					analitycs_.AddEvent(KeenIOEventType.boss_configuration);
					analitycs_.AddNewKey(KeenIOEventType.boss_configuration, "run_id", analitycs_.GetCurrentRunEvent().run_id);
					analitycs_.AddNewKey(KeenIOEventType.boss_configuration, "round_number", null);
					analitycs_.AddNewKey(KeenIOEventType.boss_configuration, "resources", boss_configuration.SerializeBossConfiguration(contender_boss));
					analitycs_.AddNewKey(KeenIOEventType.boss_configuration, "change_id", boss_configuration.GetUniqueRunId());
					analitycs_.SendEvent(KeenIOEventType.boss_configuration);
				}
			}
		}
		
		public class RunEvent 
		{
			public int INT_NULL_VALUE = int.MinValue;
		
			//Base params
			public string run_id;
			public string mode;
			public string defender_id;
			public int defender_boss_level = -1;
			public int defender_battle_rating = -1;
			public bool is_revenge;
			public bool is_pvp;
			
			public EbFightAPI.Boss contender_boss = null;
			
			public List<RoundEvent> round_list_ = new List<RoundEvent>();
			public Analitycs analitycs_;
				
			public RunEvent()
			{
				analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
			}
			
			private string GetUniqueRunId(string contender_id)
			{
				string unique_id = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().UserId;
				unique_id += "-";
				unique_id += contender_id;
				unique_id += "-";
				unique_id += System.DateTime.UtcNow.Ticks;
				
				return unique_id;
			}	
			
			private void AddNullableInt(KeenIOEventType event_type, string filed_key, int field_value)
			{
				if(field_value == INT_NULL_VALUE)
					analitycs_.AddNewKey(event_type, filed_key, "null");
				else
					analitycs_.AddNewKey(event_type, filed_key, field_value);
			}
			
			private void AddEventFields(KeenIOEventType event_type)
			{				
				analitycs_.AddEvent(event_type);
				
				analitycs_.AddNewKey(event_type, "run_id", run_id);
				analitycs_.AddNewKey(event_type, "mode", mode);
				analitycs_.AddNewKey(event_type, "defender_id", defender_id);
				AddNullableInt(event_type, "defender_boss_level", defender_boss_level);
				AddNullableInt(event_type, "defender_battle_rating", defender_battle_rating);
				analitycs_.AddNewKey(event_type, "is_revenge", is_revenge);
				analitycs_.AddNewKey(event_type, "is_pvp", is_pvp);
			}
			
			private void InitRun()
			{
				AddEventFields(KeenIOEventType.run_start);
				analitycs_.SendEvent(KeenIOEventType.run_start);
			}
			
			public void SendMultiplayerRun(EbFightAPI.MatchSession match_session)
			{
				run_id = GetUniqueRunId(match_session.Contender.UserId);
				mode = "Multiplayer";
				defender_id = match_session.Contender.UserId;
				defender_boss_level = (int)match_session.Contender.Boss.Level;
				defender_battle_rating = (int)match_session.Contender.Boss.Rank;
				contender_boss = match_session.Contender.Boss;
				//TO-DO find solution to set revenge
				is_revenge = false;
				is_pvp = true;
					
				InitRun();
				InitEndRun();
			}
			
			public void SendSingleplayerRun()
			{
				run_id = GetUniqueRunId("endless");
				mode = "Endless";
				defender_id = "null";
				defender_boss_level = -1;
				defender_battle_rating = -1;
				is_revenge = false;
				is_pvp = false;
					
				InitRun();
				InitEndRun();
			}
			
			public void SendTrainingRun()
			{
				run_id = GetUniqueRunId("training");
				mode = "Training";
				EbFightAPI.Boss boss = Framework.Instance.InstanceComponent<ServerAPI>().GetBoss();
				defender_id = "null";
				defender_boss_level = Convert.ToInt32( boss.Level );
				defender_battle_rating = Convert.ToInt32( boss.Rank );
				is_revenge = false;
				is_pvp = false;
					
				InitRun();
				InitEndRun();
			}

			public void InitEndRun()
			{
				AddEventFields(KeenIOEventType.run_end);
			}
			
			public void SendEndRun()
			{
				for(int i = 0; i < round_list_.Count; i++)
					round_list_[i].SendEndRun(contender_boss);
				analitycs_.SendEvent(KeenIOEventType.run_end);	
			}
			
			public RoundEvent NewRound()
			{
				RoundEvent round_event = new RoundEvent(run_id, round_list_.Count);
				
				round_list_.Add(round_event);
				
				return round_event;
			}			
			
			public RoundEvent GetCurrentRound()
			{
				return round_list_[round_list_.Count - 1];
			}
		}
		
		
		public class BossConfiguration
		{
			List<Dictionary<string,object>> configuration_ = new List<Dictionary<string,object>>();
			SimpleJSON.JSONNode store_info_ = SimpleJSON.JSON.Parse(Framework.Instance.resouces_manager().GetStringResource("Config/StoreInfo"));
			
			public string GetUniqueRunId()
			{
				string unique_id = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().UserId;
				unique_id += "-";
				unique_id += System.DateTime.UtcNow.Ticks;
				
				return unique_id;
			}	
			
			public object SerializeBossConfiguration(EbFightAPI.Boss boss_data)
			{
				//string return_string = "";
				
				for (int i = 0; i < boss_data.StagesCount; i++)
				{
					EbFightAPI.StageInfo stage = boss_data.Stages[i];
					
					//Analytics
					SerializeItem("shots_per_wave", stage.ShotsPerWave.ToString(), i,store_info_["boss_round_shots_per_wave"]["upgrades"][(int) stage.ShotsPerWave]["cost"].AsInt);
					SerializeItem("environment", stage.Environment.ToString(), i,store_info_["boss_round_environment"]["upgrades"][(int) stage.Environment]["cost"].AsInt);
					SerializeItem("reload_speed", stage.ReloadSpeed.ToString(), i,store_info_["boss_round_reload_speed"]["upgrades"][(int) stage.ReloadSpeed]["cost"].AsInt);
					
					for(int j = 0; j < stage.Weapons.Count; j++)
					{	
						SerializeItem("Weapon", stage.Weapons[i].Kind+" "+stage.Weapons[i].Level.ToString(), i,  
							store_info_["boss_round_weapons"]["weapons"][(int) boss_data.Level][stage.Weapons[i].Kind]["difficulty_"+((int) stage.Weapons[i].Level)]["cost"].AsInt);	
					}
				}
				
				//return_string = Json.Serialize(configuration_);
				//return_string = MiniJSON.Json.Serialize(configuration_);
				//return_string = return_string.Replace("///","");
				//return_string = return_string.Replace(@"\","");
				
				//object return_json = MiniJSON.Json.Deserialize(return_string);	
				//Debug.Log(return_string);
				
				return configuration_;		
			}
			
			void SerializeItem(string resource_type, string resource_subtype, int round_number, int energy_cost)
			{
				Dictionary<string, object> dict = new Dictionary<string, object>();
		  		dict.Add("resource_type", resource_type );
				dict.Add("resource_subtype", resource_subtype);
				dict.Add("round_number", round_number);
				dict.Add("energy_cost", energy_cost);
				
				//string return_string = Json.Serialize(dict);
				//return_string.Replace("///","");
				//return_string.Replace(@"\","");
				
				configuration_.Add(dict);	
			}
		}
		
		
		public class ResourceSet
		{
			public Analitycs analitycs_;
			string unique_id;
			
			public ResourceSet(string item_name, int quantity)
			{
				unique_id = Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().UserId;
				unique_id += "-";
				unique_id += System.DateTime.UtcNow.Ticks;
				
				analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
				
				analitycs_.AddEvent(KeenIOEventType.resource_set);
				analitycs_.AddNewKey(KeenIOEventType.resource_set, "set_id", unique_id);
				analitycs_.AddNewKey(KeenIOEventType.resource_set, "resources", GenerateRewardString(item_name,quantity));
				
			}
			
			private object GenerateRewardString(string item_name, int quantity)
			{
				List<Dictionary<string,object>> rewards_ = new List<Dictionary<string,object>>();
				Dictionary<string, object> dict = new Dictionary<string, object>();
				dict.Add("resource_type", item_name );
				dict.Add("resource_subtype", null);
				dict.Add("resource_quantity", quantity);
				rewards_.Add(dict);
				//rewards_.Add(MiniJSON.Json.Serialize(dict));	
				
				//string return_string = MiniJSON.Json.Serialize(rewards_);
				
				//return_string = return_string.Replace("///","");
				//return_string = return_string.Replace(@"\","");
				
				//Debug.Log(return_string);
				
				//return return_string;
				return rewards_;
			}	
			
			public void SendEvent()
			{
				analitycs_.SendEvent(KeenIOEventType.resource_set);
			}
			
			public string GetResourceSetId()
			{
				return unique_id;
			}
		}
		
		
		public class GameEconomyOffer
		{
			private Analitycs analitycs_ ;
			
			public GameEconomyOffer(string type, string currency, float cost, bool is_purchased, string context_of_offer, string other_rewards_set_id)
			{
				
				analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
				
				
				analitycs_.AddEvent(KeenIOEventType.game_economy_offer);	
				analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "type", type);
				analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "other_rewards_set_id", other_rewards_set_id);
				
				if (currency == "white_coins")
				{
					analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "hard_currency_cost", (int)cost);
					//analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "hard_currency_reward", null);
				}
				
				else if (currency == "coins")
				{
					analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "soft_currency_1_cost", (int)cost);
					//analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "soft_currency_1_reward", null);
				}
				else
				{
					analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "other_cost_set_id", null);
					analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "other_reward_set_id", null);
				}
				
				
				analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "is_purchased", is_purchased);
				analitycs_.AddNewKey(KeenIOEventType.game_economy_offer, "context_of_offer", context_of_offer);
			}
			
			public void SendEvent()
			{
				analitycs_.SendEvent(KeenIOEventType.game_economy_offer);
			}
		}
		
	}
}
