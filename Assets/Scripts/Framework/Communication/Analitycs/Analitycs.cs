using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using MiniJSON;
using wmf.KeenIOEvents;
using EbFightAPI;

public enum KeenIOEventType
{
	common,
	run_start,
	run_end,
	round_end,
	menu_opened,
	game_economy_offer,
	quest_state,
	player_configuration,
	boss_configuration,
	player_level_up,
	iap_currency_offer,
	game_plays,
	resource_set,
	count
}

// TO-DO
// Add versioning support to common fields

public class Analitycs : MonoBehaviour {

	private const string NOT_ASSIGNED_STRING = null;
	private const string PLAYER_PREFS_COMMON_FIELD = "PLAYER_PREFS_COMMON_FIELD";
	
#if UNITY_WEBPLAYER || UNITY_EDITOR
	private KeenIO keen_io_ = null;
#endif
	private Dictionary<KeenIOEventType, Dictionary<string, object>> event_dict_ = new Dictionary<KeenIOEventType, Dictionary<string, object>>();
	private PlayerData player_data_;
	
	private float start_time_;
		
	private WWW myExtIPWWW;
	private WWW myExtIPWWW2;
	private string ip_address_ = NOT_ASSIGNED_STRING;
	private string browser_version_ = NOT_ASSIGNED_STRING;
	private string connection_type_ = NOT_ASSIGNED_STRING;
	private string country_code_ = NOT_ASSIGNED_STRING;
	
	// Use this for initialization
	void Awake () 
	{
#if UNITY_WEBPLAYER || UNITY_EDITOR
		keen_io_ = gameObject.AddComponent<KeenIO>();
#endif
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		
		//Load common fields
		LoadCommonFields();
#if UNITY_WEBPLAYER		
		GetBrowser();
#endif		

#if !UNITY_EDITOR
		if(Framework.Instance.InstanceComponent<ServerAPI>().IsConnected())
			GetIPAddress();
#endif
	
#if UNITY_IPHONE || UNITY_ANDROIND
		GetConnectionType();
#endif		
		//Testing();
		
		// TO-DO tmp
		if(player_data_.GetString("user_name") != "none")
			SendGamePlaysEvent();
	}

	public void SendGamePlaysEvent()
	{
		//before send last game_play
		GamePlayEvent game_play_event = new GamePlayEvent();
		game_play_event.LoadAndSend();
		
		AddEvent(KeenIOEventType.game_plays);
		AddNewKey(KeenIOEventType.game_plays, "is_close", false);
		start_time_ = System.DateTime.Now.Millisecond;
		AddNewKey(KeenIOEventType.game_plays, "load_time_ms", start_time_);
		//TO-DO change IOS
		AddNewKey(KeenIOEventType.game_plays, "is_from_background", false);
		SendEvent(KeenIOEventType.game_plays);
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	//Event
	public void AddEvent(KeenIOEventType event_type)
	{
		//for sure
		if(event_dict_.ContainsKey(event_type))
			DeleteEvent(event_type);
		
		event_dict_.Add(event_type, new Dictionary<string, object>());
	}
	
	public Dictionary<string, object> GetEvent(KeenIOEventType event_type)
	{
		return event_dict_[event_type];
	}
	
	private bool DeleteEvent(KeenIOEventType event_type)
	{
		return event_dict_.Remove(event_type);
	}
	
	//Event params
	public void AddNewKey(KeenIOEventType event_type, string field_key, object field_value)
	{
		if(event_dict_[event_type].ContainsKey(field_key))
		{
			Debug.LogWarning("Warning: " + event_type + " ContainsKey " + field_key);
			event_dict_[event_type].Remove( field_key );
			event_dict_[event_type].Add( field_key, field_value);
		}
		else
		{
			event_dict_[event_type].Add( field_key, field_value);
		}
	}
	
	private void AddNewCommonKey(string field_key, object field_value)
	{
		if(!event_dict_[KeenIOEventType.common].ContainsKey(field_key))
		{
			event_dict_[KeenIOEventType.common].Add( field_key, field_value);
		}
	}
	
	public void SetValue(KeenIOEventType event_type, string field_key, object field_value)
	{
		DoChecks(event_type, field_key);
		event_dict_[event_type][field_key] = field_value;
	}
	
	public void AddValue(KeenIOEventType event_type, string field_key, int field_value)
	{
		DoChecks(event_type, field_key);
		int current_field_value = ((int)event_dict_[event_type][field_key]);
		current_field_value += field_value;
		event_dict_[event_type][field_key] = current_field_value;
	}
	
	public void AddValue(KeenIOEventType event_type, string field_key, float field_value)
	{
		DoChecks(event_type, field_key);
		float current_field_value = ((float)event_dict_[event_type][field_key]);
		current_field_value += field_value;
		event_dict_[event_type][field_key] = current_field_value;
	}
	
	public void IncreaseValue(KeenIOEventType event_type, string field_key)
	{
		DoChecks(event_type, field_key);
		int field_value = System.Convert.ToInt32(event_dict_[event_type][field_key]);
		field_value++;
		event_dict_[event_type][field_key] = field_value;
	}
	
	//Common Event
	public void SetCommonValue(string field_key, object field_value)
	{
		DoChecks(KeenIOEventType.common, field_key);
		event_dict_[KeenIOEventType.common][field_key] = field_value;
	}
	
	public void AddCommonValue(string field_key, int field_value)
	{
		DoChecks(KeenIOEventType.common, field_key);
		if( event_dict_[KeenIOEventType.common][field_key].GetType() == typeof(string))
		{
			if( ((string)event_dict_[KeenIOEventType.common][field_key]) == NOT_ASSIGNED_STRING)
			{
				event_dict_[KeenIOEventType.common].Remove(field_key);
				event_dict_[KeenIOEventType.common].Add(field_key, field_value);
			}
		} 
		else 
		{
			int current_field_value = System.Convert.ToInt32(event_dict_[KeenIOEventType.common][field_key]);
			current_field_value += field_value;
			event_dict_[KeenIOEventType.common][field_key] = current_field_value;
		}
	}
	
	public void IncreaseCommonValue(string field_key)
	{
		DoChecks(KeenIOEventType.common, field_key);
		int field_value = System.Convert.ToInt32(event_dict_[KeenIOEventType.common][field_key]);
		field_value++;
		event_dict_[KeenIOEventType.common][field_key] = field_value;
	}
	
	public object GetCommonValue(string field_key)
	{
		return event_dict_[KeenIOEventType.common][field_key];
	}
	
	public int GetCommonIntValue(string field_key)
	{
		return System.Convert.ToInt32(event_dict_[KeenIOEventType.common][field_key]);
	}
	
	private bool DoChecks(KeenIOEventType event_type, string field_key)
	{
#if UNITY_EDITOR
		if(!event_dict_.ContainsKey(event_type))
		{
			Debug.LogError("Event Dictionary does not contain " + event_type + " Key.");
			return false;
		}
		else if(!event_dict_[event_type].ContainsKey(field_key))
		{
			Debug.LogWarning(event_type + " Event Dictionary does not contain " +  field_key + " Key.");
			return false;
		}
#endif
		return true;
	}
	
	public void SendEvent(KeenIOEventType event_type)
	{
		if( event_type == KeenIOEventType.menu_opened ||
			event_type == KeenIOEventType.round_end)
		return;
		
		SetCommonFields();
		SaveCommonFields();
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
		Dictionary<string, object> merged_collection = AddCommonFields(event_type);
		KongregateAPI.GetAPI().Analytics.AddEvent(event_type.ToString(), merged_collection);
#else		
		string json_collection = AddCommonFields(event_type);
		keen_io_.SendEvent( event_type.ToString(), json_collection );
#endif		
		DeleteEvent(event_type);
	}
	
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
	private Dictionary<string, object> AddCommonFields(KeenIOEventType event_type)
	{
		Dictionary<string, object> merged_dict = event_dict_[event_type];
		
	    foreach(var pair in event_dict_[KeenIOEventType.common])
			//Already in common Kongregate SDK
			if(pair.Key != "first_play_time")
	       		merged_dict.Add(pair.Key, pair.Value);
	    
		return merged_dict;
	}
#else
	private string AddCommonFields(KeenIOEventType event_type)
	{
		string merged_string = Json.Serialize( event_dict_[event_type] );
		merged_string = merged_string.Remove(merged_string.Length - 1);
		merged_string += ',';
		merged_string += Json.Serialize( event_dict_[KeenIOEventType.common] ).Substring(1);
		return merged_string;
	}
#endif
	
	private void Testing()
	{
		//Debug.Log(SystemInfo.deviceModel);
		//Debug.Log(SystemInfo.operatingSystem);
		//GetIPAddress();	
	}

	
	public void SetCommonFields()
	{
		EbFightAPI.Boss boss_data =  Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().Boss; 
		
#if UNITY_WEBPLAYER
		SetCommonValue("player_id", Framework.Instance.InstanceComponent<WebKongregateAPI>().user_id());
		SetCommonValue("kong_user_id", Framework.Instance.InstanceComponent<WebKongregateAPI>().user_id());
		SetCommonValue("kong_username", Framework.Instance.InstanceComponent<WebKongregateAPI>().user_name());
		SetCommonValue("game_username", Framework.Instance.InstanceComponent<WebKongregateAPI>().user_name());
#elif UNITY_IPHONE || UNITY_ANDROID
		SetCommonValue("player_id", player_data_.GetString("user_id"));
		SetCommonValue("game_username", player_data_.GetString("user_name"));
#endif
		//This value is platform dependent
		//SetCommonValue("player_id", Framework.Instance.InstanceComponent<WebKongregateAPI>().user_id());
		SetCommonValue("hard_currency", player_data_.GetInt("white_coins"));
		SetCommonValue("hard_currency_bought", player_data_.GetInt("hard_currency_bought"));
		SetCommonValue("hard_currency_spent", player_data_.GetInt("hard_currency_spent"));
		SetCommonValue("soft_currency_1", player_data_.GetInt("coins"));
		SetCommonValue("soft_currency_1_bought", player_data_.GetInt("soft_currency_1_bought"));
		SetCommonValue("soft_currency_1_spent", player_data_.GetInt("soft_currency_1_spent"));
		
		//These are reset every session in the init
		/*
		SetCommonValue("num_pve_played", 0);
		SetCommonValue("num_pvp_played", 0);
		SetCommonValue("num_pve_won", 0);
		SetCommonValue("num_pvp_won", 0);
		*/
		
		//SetCommonValue("num_boss_played", NOT_ASSIGNED_STRING);
		//SetCommonValue("num_boss_won", NOT_ASSIGNED_STRING);
		
		SetCommonValue("ab_test", NOT_ASSIGNED_STRING); //Later
		SetCommonValue("filter_type", NOT_ASSIGNED_STRING); //Later
		SetCommonValue("total_spent_in_usd", NOT_ASSIGNED_STRING); //Later
		SetCommonValue("first_play_time", ConvertUnitTimeToDateTime(player_data_.GetFloat("first_play_time"))); 
		SetCommonValue("device_type", SystemInfo.deviceModel); 
		SetCommonValue("client_os_type", GetOsVersion(SystemInfo.deviceModel));
		SetCommonValue("client_os_version", GetOsVersion(SystemInfo.deviceModel));
		SetCommonValue("player_level", player_data_.GetInt("dd_level"));
		SetCommonValue("boss_level", boss_data.Level);
		SetCommonValue("country_code", country_code_);
		SetCommonValue("data_connection_type", connection_type_);
		SetCommonValue("leaderboard_ranking", NOT_ASSIGNED_STRING);//?
		SetCommonValue("leaderboard_rating", NOT_ASSIGNED_STRING);//?
		SetCommonValue("dd_battle_rating", Framework.Instance.InstanceComponent<ServerAPI>().GetPlayer().PlayerRank);
		SetCommonValue("boss_battle_rating",  boss_data.Rank);
		SetCommonValue("ip_address", ip_address_);
		SetCommonValue("fb_user_id", NOT_ASSIGNED_STRING);//Later
		//Set Above
		//SetCommonValue("kong_user_id", Framework.Instance.InstanceComponent<WebKongregateAPI>().user_id());
		SetCommonValue("fb_username", NOT_ASSIGNED_STRING);//Later
		
		//Set Above as they depend on platform
		//SetCommonValue("kong_username", NOT_ASSIGNED_STRING);//Later
		//SetCommonValue("game_username", NOT_ASSIGNED_STRING);//Later
		
		SetCommonValue("browser", browser_version_);
		SetCommonValue("browser_version", browser_version_);
		SetCommonValue("tutorial_completed", player_data_.GetBool("tutorial_completed"));
		SetCommonValue("client_build_version", player_data_.GetString("client_build_version"));
		SetCommonValue("client_data_version", player_data_.GetString("client_data_version"));
		SetCommonValue("best_pve_score", player_data_.GetInt("single_player_high_score"));
		SetCommonValue("best_pvp_score", player_data_.GetInt("multiplayer_high_score"));
		SetCommonValue("dd_xp", player_data_.GetInt("dd_xp"));
		SetCommonValue("dd_hp", player_data_.GetCurrentHp());
		SetCommonValue("boss_xp", boss_data.Xp);
		SetCommonValue("boss_hp",  
			Framework.Instance.InstanceComponent<Store>().GetUpgradablePropertyEffectByNameAsFloat ("boss_level_upgrade", (int) boss_data.Level));
		SetCommonValue("dd_ammo_max", player_data_.GetInt("dd_ammo_level"));
		SetCommonValue("dd_attack", player_data_.GetInt("dd_att_level"));
		SetCommonValue("dd_defense", player_data_.GetInt("dd_def_level"));
		SetCommonValue("boss_attack", boss_data.Attack);
		SetCommonValue("boss_defense", boss_data.Defence);
		SetCommonValue("boss_power_max", boss_data.Power);
	}
	
	
	public void CreateCommonFields()
	{		
		AddEvent(KeenIOEventType.common);
		
		//// Unique identifier for each player. All unique ids should be discussed with Kongregate. Probably should be UUIDs.
		AddNewKey(KeenIOEventType.common, "player_id", NOT_ASSIGNED_STRING);
		
		// WHITE COINS
		//// Player's current hard currency balance.
		AddNewKey(KeenIOEventType.common, "hard_currency", NOT_ASSIGNED_STRING);
		
		//// Total amount of hard currency player has bought. 
		//// This field may be larger than hard_currency_bought because the player may have earned some hard currency for free.
		AddNewKey(KeenIOEventType.common, "hard_currency_bought", NOT_ASSIGNED_STRING);
		
		//// Total amount of hard currency player has spent.
		AddNewKey(KeenIOEventType.common, "hard_currency_spent", NOT_ASSIGNED_STRING);
		
		// COINS
		//// Player's current primary soft currency balance.
		AddNewKey(KeenIOEventType.common, "soft_currency_1", NOT_ASSIGNED_STRING);
		
		//// Total amount of primary soft currency player has bought. 
		//// This field may be larger than soft_currency_1_bought because the player may have earned some soft currency for free.
		AddNewKey(KeenIOEventType.common, "soft_currency_1_bought", NOT_ASSIGNED_STRING);
		
		//// Total amount of secondary soft currency player has spent.
		AddNewKey(KeenIOEventType.common, "soft_currency_1_spent", NOT_ASSIGNED_STRING);	
		
		//PvE PvP
		//// Number of pve games played.
		AddNewKey(KeenIOEventType.common, "num_pve_played", 0);
		
		//// Number of pvp games played.
		AddNewKey(KeenIOEventType.common, "num_pvp_played", 0);
		
		//// Number of DD pvp games won.
		AddNewKey(KeenIOEventType.common, "num_pve_won", 0);
		
		//// Number of DD pvp games won.
		AddNewKey(KeenIOEventType.common, "num_pvp_won", 0);
		
		//// Number of PvP games the player's boss played
		AddNewKey(KeenIOEventType.common, "num_boss_played", 0);
		
		//// Number of PvP games the player's boss won
		AddNewKey(KeenIOEventType.common, "num_boss_won", 0);
		
		//INFO
		//// A string indicating the currently active AB tests. To start we should always send null.
		AddNewKey(KeenIOEventType.common, "ab_test", "null");
		
		//// A string indicating a reason for filtering out this data from our analysis.
		//// To start we should always send null.
		AddNewKey(KeenIOEventType.common, "filter_type", "null");
		
		//// The number of (converted) dollars this player has spent on the game.
		//TO-DO server
		AddNewKey(KeenIOEventType.common, "total_spent_in_usd", NOT_ASSIGNED_STRING);
		
		//// UTC Timestamp of when the user was created.
		//TO-DO server
		AddNewKey(KeenIOEventType.common, "first_play_time", NOT_ASSIGNED_STRING);
		
		//// UTC Timestamp of when the user was created.
		AddNewKey(KeenIOEventType.common, "device_type", NOT_ASSIGNED_STRING);
		
		//// Type of machine the game is being played on.
		AddNewKey(KeenIOEventType.common, "client_os_type", NOT_ASSIGNED_STRING);
		
		//// Type of machine the game is being played on.
		AddNewKey(KeenIOEventType.common, "client_os_version", NOT_ASSIGNED_STRING);
		
		//// Player's current DD level.
		AddNewKey(KeenIOEventType.common, "player_level", NOT_ASSIGNED_STRING);
		
		//// Player's boss level.
		AddNewKey(KeenIOEventType.common, "boss_level", NOT_ASSIGNED_STRING);
		
		//// Player's country.
		AddNewKey(KeenIOEventType.common, "country_code", NOT_ASSIGNED_STRING);
		
		////Type of internet connection.
		AddNewKey(KeenIOEventType.common, "data_connection_type", NOT_ASSIGNED_STRING);
		
		////An ordinal value indicating player's current position in the primary leaderboard.
		////(Singleplayer).
		AddNewKey(KeenIOEventType.common, "leaderboard_ranking", NOT_ASSIGNED_STRING);
		
		////An interval value indicating player's current score in whatever metric determines their leaderboard position.
		////(Singleplayer).
		AddNewKey(KeenIOEventType.common, "leaderboard_rating", NOT_ASSIGNED_STRING);
		
		////Player's current battle rating for DD.
		AddNewKey(KeenIOEventType.common, "dd_battle_rating", NOT_ASSIGNED_STRING);
		
		////Player's current battle rating for Boss.
		AddNewKey(KeenIOEventType.common, "boss_battle_rating", NOT_ASSIGNED_STRING);
		
		////Player's ip address.
		AddNewKey(KeenIOEventType.common, "ip_address", NOT_ASSIGNED_STRING);
	
		////Player's unique identifier on fb.
		AddNewKey(KeenIOEventType.common, "fb_user_id", NOT_ASSIGNED_STRING);
		
		////Player's username on fb.
		AddNewKey(KeenIOEventType.common, "fb_username", NOT_ASSIGNED_STRING);
		
		////Player's unique identifier on kongregate.
		AddNewKey(KeenIOEventType.common, "kong_user_id", NOT_ASSIGNED_STRING);
		
		////Player's username on Kongregate.
		AddNewKey(KeenIOEventType.common, "kong_username", NOT_ASSIGNED_STRING);
		
		////Player's current username as chosen in this game.
		AddNewKey(KeenIOEventType.common, "game_username", NOT_ASSIGNED_STRING);
		
		////Type of browser in use, if player is on web.
		AddNewKey(KeenIOEventType.common, "browser", NOT_ASSIGNED_STRING);
		
		////Version number of browser in use, if player is on web.
		AddNewKey(KeenIOEventType.common, "browser_version", NOT_ASSIGNED_STRING);
		
		////True if this player has completed the tutorial, false otherwise.
		AddNewKey(KeenIOEventType.common, "tutorial_completed", NOT_ASSIGNED_STRING);
		
		////Version number of the game client.
		AddNewKey(KeenIOEventType.common, "client_build_version", NOT_ASSIGNED_STRING);
		
		////Version number of game logic. Any time any gameplay changes outside of a change to the client, this number should change.
		////For example if a CSV file, XML file, or CMS containing gameplay data is changed, this should increment.
		AddNewKey(KeenIOEventType.common, "client_data_version", NOT_ASSIGNED_STRING);
		
		////The highest score player has achieved in pve.
		AddNewKey(KeenIOEventType.common, "best_pve_score", NOT_ASSIGNED_STRING);
		
		////The highest score player has achieved in pve.
		AddNewKey(KeenIOEventType.common, "best_pvp_score", NOT_ASSIGNED_STRING);
		
		//Player
		////Player's current XP.
		AddNewKey(KeenIOEventType.common, "dd_xp", NOT_ASSIGNED_STRING);
		
		////Player's current HP.
		AddNewKey(KeenIOEventType.common, "dd_hp", NOT_ASSIGNED_STRING);
		
		////Player's current ammo max.
		AddNewKey(KeenIOEventType.common, "dd_ammo_max", NOT_ASSIGNED_STRING);
		
		////Player's current attack.
		AddNewKey(KeenIOEventType.common, "dd_attack", NOT_ASSIGNED_STRING);
		
		////Player's current defense.
		AddNewKey(KeenIOEventType.common, "dd_defense", NOT_ASSIGNED_STRING);
		
		//Boss
		////Boss's current XP.
		AddNewKey(KeenIOEventType.common, "boss_xp", NOT_ASSIGNED_STRING);
		
		////Boss's current HP.
		AddNewKey(KeenIOEventType.common, "boss_hp", NOT_ASSIGNED_STRING);
		
		////Boss's current power max.
		AddNewKey(KeenIOEventType.common, "boss_power_max", NOT_ASSIGNED_STRING);
		
		////Boss's current attack.
		AddNewKey(KeenIOEventType.common, "boss_attack", NOT_ASSIGNED_STRING);
		
		////Boss's current defense.
		AddNewKey(KeenIOEventType.common, "boss_defense", NOT_ASSIGNED_STRING);
		
	}
	
	private void InitCommonFields()
	{
		//PvE PvP
		SetCommonValue("num_pve_played", 0);
		SetCommonValue("num_pvp_played", 0);
		SetCommonValue("num_pvp_won", 0);
		SetCommonValue("num_boss_played", 0);
		SetCommonValue("num_boss_won", 0);
	}
	
	private void LoadCommonFields()
	{
#if UNITY_EDITOR
		PlayerPrefs.DeleteKey(PLAYER_PREFS_COMMON_FIELD);
#endif
		string common_fields = PlayerPrefs.GetString(PLAYER_PREFS_COMMON_FIELD);
		//Debug.Log(common_fields);
		if(string.IsNullOrEmpty( common_fields ) )
		{
			CreateCommonFields();
			//InitCommonFields();
			SaveCommonFields();
		}
		else
		{
			event_dict_.Add(KeenIOEventType.common, (Dictionary<string, object>)Json.Deserialize(common_fields));
			AddNewCommonKeys();
		}
	}
	
	private void AddNewCommonKeys()
	{
		//AddNewCommonKey(new_key, value);
	}
	
	private void SaveCommonFields()
	{
		PlayerPrefs.SetString(PLAYER_PREFS_COMMON_FIELD, Json.Serialize( event_dict_[KeenIOEventType.common] ));
		PlayerPrefs.Save();
	}
	
	
	private void OnApplicationQuit()
	{
		//Save GamePlayEvent data	
		GamePlayEvent game_play_event = new GamePlayEvent();
		game_play_event.load_time_ms_ = start_time_;
		game_play_event.session_length_seconds_ = Time.realtimeSinceStartup;
		game_play_event.Save();
		
		SaveCommonFields();
	}
	
	
	//Events
	private RunEvent run_event;
	
	public void NewRun(EbFightAPI.MatchSession match_session)
	{
		run_event = new RunEvent();
		if(match_session != null)
			run_event.SendMultiplayerRun(match_session);
		else
			run_event.SendSingleplayerRun();
	}
	
	public void NewTrainingRun()
	{
		run_event = new RunEvent();
		run_event.SendTrainingRun();
	}
	
	public void EndRun()
	{
		run_event.SendEndRun();
	}
	
	public RunEvent GetCurrentRunEvent()
	{
		return run_event;
	}
	
	public void NewRound()
	{
		run_event.NewRound();
	}
	
	public RoundEvent GetCurrentRound()
	{
		return run_event.GetCurrentRound();
	}
	
	//End Events
	
	private void GetIPAddress()
	{
		myExtIPWWW = new WWW("http://checkip.dyndns.org");	
		StartCoroutine(WaitForIP());
	}
 
	
	IEnumerator WaitForIP()
	{
	    yield return myExtIPWWW; 
	   	if (myExtIPWWW.isDone)
		{	
			if (myExtIPWWW.text != null && string.IsNullOrEmpty(myExtIPWWW.text) == false && myExtIPWWW.text != "")
			{
				string myExtIP = myExtIPWWW.text;
				myExtIP=myExtIP.Substring(myExtIP.IndexOf(":")+1);
				myExtIP=myExtIP.Substring(0,myExtIP.IndexOf("<"));
				ip_address_ = myExtIP;
				
				myExtIPWWW2 = new WWW("http://api.hostip.info/country.php?ip");	
				StartCoroutine(GetCountryCode());
			}
		}	
	}
	
	
	IEnumerator GetCountryCode()
	{
		yield return myExtIPWWW2; 
	   	if (myExtIPWWW2.isDone )
			country_code_ = myExtIPWWW2.text;
	}
	
	
	private void GetBrowser()
	{		
		Application.ExternalEval(
			"kongregateUnitySupport.getUnityObject().SendMessage('Framework/Analitycs', 'SetBrowserVersion', navigator.appVersion);" +
			"console.log(navigator.appVersion);" 
			);
	}
	
	
	private void SetBrowserVersion(string browser_version)
	{
		browser_version_ = browser_version;
	}
	
	
	private string GetBrowserVersion()
	{
		string browser_version = "";
		
		return browser_version;
	}
	
	
	private string OnBrowserName(string browser_name)
	{
		return browser_name;
	}
	
	
	private string OnBrowserVersion(string browser_version)
	{
		return browser_version;
	}
	

	public string GetOsVersion (string device_model)
	{
		string os_version = NOT_ASSIGNED_STRING;
#if UNITY_IPHONE
		os_version = iPhoneSettings.systemVersion;
#elif UNITY_WEBPLAYER
		os_version = SystemInfo.operatingSystem;
#endif	
		//CHECK ANDROID
			
		return os_version;	
	}
	
#if UNITY_IPHONE || UNITY_ANDROIND	
	private void GetConnectionType()
	{
		connection_type_ = Application.internetReachability.ToString();
	}
#endif
	
	System.DateTime ConvertUnitTimeToDateTime(double current_unix_time)
	{
		System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		dtDateTime = dtDateTime.AddSeconds(current_unix_time ).ToUniversalTime();
		
		return dtDateTime;
	}
}
