using UnityEngine;
using System.Collections;


public class KeenIO : MonoBehaviour {
	
#if UNITY_IPHONE && UNITY_EDITOR
	//test_endless_boss_fight_mobile
	private string project_id_ = "51f940a1897a2c3103000000";
	private string api_key_ = "658c7af025da90bafa8f1f886650dfa57ef47f91c000eb4085299012ffa829a248ff24483146e023baca2a37b13ccc3be63aa91f819435c8fd7de317c7e224b77d49bca16b6d7e4169967cf2a605421014152007b6b2bb091caf689b5979a83be316d842d18e4882a5a274a519296e5f";
#elif UNITY_IPHONE || UNITY_ANDROID
	//live_endless_boss_fight_mobile
	private string project_id_ = "51f9408e384331457c000006";
	private string api_key_ = "ffb4daaef04a278143c8af65c3cf78590cb75e69fd19aacec7019318e7d9249953bd477a81afb0e12f2792c5378a3389bb3b5c12ca6995ede9145f67fcb84089653721fa68c012a85ee304d6994b1e058426a3dfec1c1dbdc67ef539b910a3e3256fa9282ec790723c776dd19b9545d0";
#elif UNITY_WEBPLAYER && UNITY_EDITOR
	//test_endless_boss_fight_web
	private string project_id_ = "51f940bd897a2c2f01000000";
	private string api_key_ = "a3da3f8c6e4423b9b326556fb48ef2cbb6987230d21f08ef12c65cff1dfff2c172ecf8af1c8f69fb49d8558fa8e91b306683102bbafc7a46c1a8b2091ee200b64df6d4cab701557b5a91e52ca053430329b7f425379cceb1d766c727d113f94fa01d40a30ed83ad1af77226d48fd5e5f";
#elif UNITY_WEBPLAYER
	//contact@whitemilkgames.com
	//live_endless_boss_fight_web
	private string project_id_ = "51f940b1897a2c2fed000000";
	private string api_key_ = "f6e03cda4328ed4e1b3235e5eb6ef1b8ec3e1a2f40a140a47b1cef0039563bd22e2399e64520c5bc4f20891a06820ddd7ffd854f944b592b213087342a10c97159facd31376758022058fe6295aee77e7347a85b4f41529ceea4d24f2e2b7c1288e32ea4284a914fbf71cd73d96ab317";
#endif
	
	private float time_out_ = 10.0f;
	
	public void Init()
	{
		//Get common info
		
	}
	
	public void SendEvent(string collection, string JSONString)
    {
        StartCoroutine(SendEventWWW(collection, JSONString));
    }
	
	public IEnumerator SendEventWWW(string collection, string JSONString)
	{
		WWWForm form = new WWWForm();
        form.AddField("api_key", api_key_);
		
		System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
		System.Collections.Hashtable postHeader = form.headers;
		postHeader["Content-Type"] = "application/json";
		postHeader.Add("Content-Length", JSONString.Length);
		
		string url =
			"https://api.keen.io/3.0/projects/" +
			project_id_	+
			"/events/" +
			collection;
		WWW www = new WWW(
			url + "?api_key=" + api_key_,
			encoding.GetBytes(JSONString),
			postHeader
		);
		
        float elapsed_time = 0.0f;
		
		while(!www.isDone)
        {
            elapsed_time += Time.deltaTime;

            if (elapsed_time >= time_out_)
                break;

            yield return null;
        }
		
		yield return www;
		
		//Debug.Log(www.text);
	}
	
}
