
#if UNITY_IPHONE && !UNITY_EDITOR
/*
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class AdX{
	
	private const string SAVE_NAME = "AdXCache";
	private static List<string> list_caching_ = new List<string>();
	
	[DllImport ("__Internal")]
	private static extern bool _SendEventNative(string event_str);
	[DllImport ("__Internal")]
	private static extern bool _SendEventNativeWithData(string event_str, string data_str);
	
	private static void SendEvent(string event_str)
	{
		if(_SendEventNative(event_str))
		{
			for(int i = 0; i < list_caching_.Count; i++)
			{
				_SendEventNative(list_caching_[i]);
			}
			list_caching_.Clear();
			PlayerPrefs.SetString(SAVE_NAME, "");
		}
		else
		{
			//caching
			list_caching_.Add(event_str);
			//Save cache
			string caching_string = "";
			for(int i = 0; i < list_caching_.Count; i++)
			{
				caching_string += list_caching_[i] + ";";
			}
			PlayerPrefs.SetString(SAVE_NAME, caching_string);
		}
	}
	
	public static void Init()
	{
		//Realod cache
		string caching_string = PlayerPrefs.GetString(SAVE_NAME);
		string[] event_array = caching_string.Split(';');
		for(int i = 0; i < event_array.Length; i++)
		{
			list_caching_.Add(event_array[i]);
		}
	}
	
	public static void SendLaunch()
	{
		SendEvent("launch");
	}

	public static void SendSession(int id)
	{
		if(id == 2 || id == 10)
		{
			string event_string = "session";
			event_string += id.ToString();
			SendEvent(event_string);
		}
	}
	
	public static void SendDay(int id)
	{
		string event_string = "day";
		event_string += id.ToString();
		SendEvent(event_string);
	}
	
	public static void SendSale()
	{
		SendEvent("sale");
	}
	
	public static void SendInAppEvent(int tier_number)
	{
		if(tier_number == 1)
			tier_number = 5;
		else if(tier_number == 2)
			tier_number = 10;
		else if(tier_number == 3)
			tier_number = 20;
		else if(tier_number == 4)
			tier_number = 40;
		
		string event_string = "t";
		event_string += tier_number < 10 ? ("0" + tier_number.ToString()) : tier_number.ToString();
		event_string += "_premium_currency";
		SendEvent(event_string);
		
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("first_sale"))
		{
			SendFirstTimeSale();
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("first_sale", false);
		}
			
	}

	public static void SendFirstTimeSale()
	{
		SendEvent("first_time_sale");
	}
	
	public static void SendSaleEvent(string localized_price_string)
	{
		_SendEventNativeWithData("sale", localized_price_string);
	}
}
*/
#endif