using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TransitionType
{
	add_scene,
	change_scene,
	back
}

public class SceneManager : MonoBehaviour{
	
	private GameObject scenes_node_;
	
	//private string last_scene_name_;
	private TransitionType last_transition_type_;
	private Stack<string> scene_stack_ = new Stack<string>();
	
	//TO-DO add a auto z order
	private float z_order_ = 0.0f;
	
	//Camera
	/*
	private tk2dCamera camera_ = null;
	
	public void OnLevelWasLoaded (int level) 
	{
		tk2dCamera[] cameras = GameObject.FindObjectsOfType(typeof(tk2dCamera)) as tk2dCamera[];
		for (int i = 0; i < cameras.Length; i++) 
		{
			if(cameras[i] != camera_)
			{
				cameras[i].enabled = false;
				if(cameras[i].GetComponent<AudioListener>() != null)
					cameras[i].GetComponent<AudioListener>().enabled = false;
			}
		}
		
	}
	*/
	
	public void Init()
	{
		scenes_node_ = new GameObject("Scenes");
		scenes_node_.transform.parent = Framework.Instance.framework_object().transform;
		/*
		camera_ = GameObject.Find("Main Camera").GetComponent<tk2dCamera>();
		DontDestroyOnLoad( camera_ );
		*/
	}
	
	public void BackFromCurrentScene()
	{
		LoadScene("Framework/Scenes/" + scene_stack_.Pop(), TransitionType.back);
	}
	
	public void LoadScene(string scene_name, TransitionType transition_type)
	{
		LoadScene(scene_name, transition_type, 0.0f);
	}
		
	public void LoadScene(string scene_name, TransitionType transition_type, float z_order)
	{
		last_transition_type_ = transition_type;
		z_order_ = z_order;
		
		if(transition_type == TransitionType.change_scene)
		{
			Application.LoadLevel(scene_name);
			
			scene_stack_.Clear();
			scene_stack_.Push(scene_name);
		}
		else if(transition_type == TransitionType.add_scene)
		{
			//TO-DO add collider
			//ToggleButtons(false);
			
			Application.LoadLevelAdditive(scene_name);
			
			scene_stack_.Push(scene_name);
		}
		else if(transition_type == TransitionType.back)
		{
			//TO-DO add collider
			//ToggleButtons(true);
			
			Destroy( GameObject.Find( scene_name ) );
			
		}
		
	}
	
	public void FirstFrame()
	{
		GameObject support_camera = GameObject.Find("WMG.SupportCamera");
		if(support_camera != null)
			Destroy( support_camera );
		
		string last_scene = scene_stack_.Pop();
		scene_stack_.Push(last_scene);
		GameObject scene_node_ = new GameObject(last_scene);
		BoxCollider box_collider = scene_node_.AddComponent<BoxCollider>();
		//TO-DO this is a temporary Hack
		box_collider.center = new Vector3(0.0f, 0.0f, 10.0f);
		box_collider.size = new Vector3(9999.9f, 9999.9f, 0.01f); 
			
		//Reset Camera button
		Camera right_camera = GetCurrentCamera();
		tk2dButton[] buttons = GameObject.Find("/Root").GetComponentsInChildren<tk2dButton>();
		for(int i = 0; i < buttons.Length; i++)
		{
			buttons[i].viewCamera = right_camera;
		}
		//Camera Anchor
		tk2dCameraAnchor[] anchor = GameObject.Find("/Root").GetComponentsInChildren<tk2dCameraAnchor>();
		for(int i = 0; i < anchor.Length; i++)
		{
			anchor[i].tk2dCamera = right_camera.GetComponent<tk2dCamera>();
		}
		
		//
		tk2dUIManager.Instance.UICamera = right_camera;
		
		//TO-DO Change this name
		GameObject.Find("/Root").transform.parent = scene_node_.transform;
		
		//Force z order
		scene_node_.transform.parent = scenes_node_.transform;
		scene_node_.transform.position = new Vector3(
			scene_node_.transform.position.x,
			scene_node_.transform.position.y,
			z_order_);
		
		//NotifyManager
		Framework.Instance.InstanceComponent<NotifyManager>().NewSceneLoaded();
	}
	 
	public GameObject GetRootNode(string scene_name)
	{
		return GameObject.Find("Framework/Scenes/" + scene_name + "/Root");
	}	
	
	public Camera GetCurrentCamera()
	{
		if(Camera.mainCamera != null)
			return Camera.mainCamera;
		if(Camera.allCameras[0] != null)
			return Camera.allCameras[0];
		if(Camera.current != null)
			return Camera.current;
		if(Camera.main != null)
			return Camera.main;
		
		return GameObject.FindObjectOfType(typeof(Camera)) as Camera;
	}
	
	public void AssignsCameraToAnchors(string scene_name)
	{
		Camera right_camera = GetCurrentCamera();
		tk2dCameraAnchor[] anchor = GameObject.Find("Framework/Scenes/" + scene_name + "/Root").GetComponentsInChildren<tk2dCameraAnchor>();
		for(int i = 0; i < anchor.Length; i++)
		{
			anchor[i].tk2dCamera = right_camera.GetComponent<tk2dCamera>();
		}
	}
	
	public void AssignsCameraToButtons(string scene_name)
	{
		Camera right_camera = GetCurrentCamera();
		tk2dButton[] buttons = GameObject.Find("Framework/Scenes/" + scene_name + "/Root").GetComponentsInChildren<tk2dButton>();
		for(int i = 0; i < buttons.Length; i++)
		{
			buttons[i].viewCamera = right_camera.GetComponent<tk2dCamera>().camera;
		}
	}
}