using UnityEngine;
using System.Collections;

public class GraphicsManager
{
	private float camera_scale_ = 1.0F;
	private Vector2 native_resolution_ = new Vector2(480.0F, 320.0F);
	private float pixel_point_ratio_ = 1.0F;
	private float ratio_ = 1.0F;
	
#if UNITY_IPHONE
	public void Init()
	{
		//Be aware that 60fps games eat batteries for breakfast :)
		Application.targetFrameRate = 60;
		
		if(Screen.width <= 480 && Screen.height <= 320) 
		{
			//IPhone3GS
			Application.targetFrameRate = 30;
			tk2dSystem.CurrentPlatform = "1x";
			camera_scale_ = 1.0F;
			Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 960 && Screen.height <= 640) 
		{
			if(iPhoneSettings.generation == iPhoneGeneration.iPodTouch4Gen )
			{
				//On Ipod4Gen we use no retina assets
				tk2dSystem.CurrentPlatform = "1x";
				Application.targetFrameRate = 30;
				Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
				pixel_point_ratio_ = 2.0F;
			}
			else
			{
				//IPhone4 retina
				tk2dSystem.CurrentPlatform = "2x";
				Application.targetFrameRate = 30;
				Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone4);
				pixel_point_ratio_ = 1.0F;
			}
			camera_scale_ = 2.0F;
		}
		else if(Screen.width <= 1024 && Screen.height <= 768) 
		{	
			//IPad 1, IPad 2
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = 2.0F;
			Application.targetFrameRate = 60;
			Framework.Instance.set_device_type(Framework.WMF_DeviceType.ipad);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width == 1136 && Screen.height == 640) 
		{	
			//IPhone5
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = 2.0F;
			Application.targetFrameRate = 60;
			Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone5);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 2048 && Screen.height <= 1536) 
		{	
			//IPad retina
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = 4.0F;
			Application.targetFrameRate = 60;
			Framework.Instance.set_device_type(Framework.WMF_DeviceType.ipad);
			pixel_point_ratio_ = 2.0F;
		}
	}
	
#elif UNITY_WEBPLAYER
	
	public void Init()
	{
		//Kong version
		tk2dSystem.CurrentPlatform = "2x";
		camera_scale_ = 2.0F;
		Application.targetFrameRate = 60;
	}

#elif UNITY_ANDROID
	
	public void Init()
	{
		if(Screen.width > Screen.height)
			ratio_ = ((float)Screen.width / (float)Screen.height);
		else
			ratio_ = ((float)Screen.height / (float)Screen.width);
		
		Debug.Log("GRAPHICS MANAGER");
		Debug.Log(Screen.width);
		Debug.Log(Screen.height);
		Debug.Log(ratio_);
		if(Screen.width <= 480 && Screen.height <= 320) 
		{
			//HVGA Landscape
			Application.targetFrameRate = 30;
			tk2dSystem.CurrentPlatform = "1x";
			camera_scale_ = 1.0F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 800 && Screen.height <= 480) 
		{
			//WVGA Landscape
			Application.targetFrameRate = 30;
			tk2dSystem.CurrentPlatform = "1x";
			camera_scale_ = 1.5F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 854 && Screen.height <= 480) 
		{
			//FWVGA Landscape
			Application.targetFrameRate = 30;
			tk2dSystem.CurrentPlatform = "1x";
			camera_scale_ = 1.5F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if((Screen.width <= 1024 && Screen.height <= 600) ||
				(Screen.width <= 600 && Screen.height <= 1024)) 
		{
			//WVGA Landscape
			Application.targetFrameRate = 30;
			tk2dSystem.CurrentPlatform = "1x";
			camera_scale_ = 1.8F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if((Screen.width == 1280 && Screen.height == 720) || (Screen.width == 720 && Screen.height == 1280))
		{
			//WVGA Landscape
			Application.targetFrameRate = 60;
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = 2.25F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 1280 && Screen.height <= 800) 
		{
			//WVGA Landscape
			Application.targetFrameRate = 60;
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = 2.35F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 1920 && Screen.height <= 1200) 
		{
			//WVGA Landscape
			Application.targetFrameRate = 60;
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = 3.5F;
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else if(Screen.width <= 2560 && Screen.height <= 1600) 
		{
			//WVGA Landscape
			Application.targetFrameRate = 60;
			tk2dSystem.CurrentPlatform = "2x";
			camera_scale_ = Mathf.Min(Screen.width / 480.0F, Screen.height / 320.0F);
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}
		else
		{
			//WVGA Landscape
			Application.targetFrameRate = 30;
			tk2dSystem.CurrentPlatform = "1x";
			camera_scale_ = Mathf.Min(Screen.width / 480.0F, Screen.height / 320.0F);
			//Framework.Instance.set_device_type(Framework.WMF_DeviceType.iphone);
			pixel_point_ratio_ = 1.0F;
		}

	}
	
#endif
	
	//called every time the scene was loaded
	public void InitCamera()
	{
		tk2dCamera.inst.resolutionOverride = new tk2dCameraResolutionOverride[1];
		tk2dCamera.inst.resolutionOverride[0] = new tk2dCameraResolutionOverride();
		tk2dCamera.inst.resolutionOverride[0].width = Screen.width;
		tk2dCamera.inst.resolutionOverride[0].height = Screen.height;
		tk2dCamera.inst.resolutionOverride[0].scale = camera_scale_;
		tk2dCamera.inst.forceResolutionInEditor = true;
		tk2dCamera.inst.forceResolution = new Vector2(Screen.width, Screen.height);
	}
	
	public Vector2 ScreenToInput(Vector2 position)
	{
		Vector2 return_position = new Vector2( 
			position.x / camera_scale_, 
			position.y / camera_scale_);
			
		return return_position;
	}
	
	//Debug
	public void ResizeGUI()
	{
		
	}
	
	//Getter / Setter
	public float camera_scale()
	{
		return camera_scale_;
	}
	
	public Vector2 native_resolution()
	{
		return native_resolution_;
	}
	
	
	public float pixel_to_point_ratio()
	{
		return pixel_point_ratio_;
	}
	
	public float ratio()
	{
		return ratio_;
	}
}
