using UnityEngine;
using System.Collections;

public class ResourcesManager
{

	private string prefix_ = "";
	
	//Getter / Setter
	public string prefix ()
	{
		return prefix_;
	}
	
	public void set_prefix (string prefix)
	{
		prefix_ = prefix;
	}
	
	//
	public Object GetResource (string path)
	{
		//That's not the real absolute path :)
		string absolute_path_ = "";
		if (prefix_ != "") {
			absolute_path_ = prefix_;
			absolute_path_ += "/";
		}
		absolute_path_ += path;
		
		Object resource = Resources.Load (absolute_path_);
		if(resource == null)
		{
			resource = Resources.Load (path);
		}
		
		return resource;
	}
	
	public string GetStringResource (string path)
	{
#if !UNITY_WEBPLAYER
		//Debug.Log(Application.persistentDataPath + "/Mobile/" + path + ".txt");
		if(System.IO.File.Exists(Application.persistentDataPath + "/Mobile/" + path + ".txt"))
		{
			string resource = System.IO.File.ReadAllText(Application.persistentDataPath + "/Mobile/" + path + ".txt");
			if(!string.IsNullOrEmpty( resource ) && resource != "")
				return resource;
		}
#endif
		return (GetResource (path) as TextAsset).text;
	}
}
