using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DigitsWidget : MonoBehaviour {
	
	private List<tk2dAnimatedSprite> digits_list_ = new List<tk2dAnimatedSprite>();
		
	void Awake() {
		if(digits_list_ .Count != 0)
			return;
		
		tk2dAnimatedSprite[] digits_ = this.GetComponentsInChildren<tk2dAnimatedSprite>();
		//copy in list
		for(int i = 0; i < digits_.Length; i++)
		{
			digits_list_.Add( digits_[i] );
		}
		//sort
		digits_list_.Sort(new OrderByName());
	}
	
	public void Init()
	{
		Awake();
	}
	
	public void UpdateValue(long init_value)
	{
		long rest_value = init_value;	
		long digit = 0;
		for(int i = 0; i < digits_list_.Count; i++)
		{
			digit = rest_value % 10;
			rest_value /= 10;
			digits_list_[i].SetFrame((int)digit);
		}
	}
		
	public class OrderByName : IComparer<tk2dAnimatedSprite>
	{

		public int Compare (tk2dAnimatedSprite a, tk2dAnimatedSprite b)
		{			
			return a.name.CompareTo( b.name );
		}
	}
}
