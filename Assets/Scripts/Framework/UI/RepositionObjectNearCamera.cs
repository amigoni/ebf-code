using UnityEngine;
using System.Collections;

namespace wmf
{
	namespace ui
	{
		public class RepositionObjectNearCamera : MonoBehaviour
		{
			void Start ()
			{
				Init ();
			}
	
			void Update()
			{
				
			}
			
			public void Init ()
			{
				Camera camera = Framework.Instance.scene_manager().GetCurrentCamera();
				if(camera != null)
				{
					transform.position = new Vector3 (
						transform.position.x,
						transform.position.y,
						camera.transform.position.z - camera.depth * 0.5F);
				}
			}
		}
	}
}