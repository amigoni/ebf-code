using UnityEngine;
using System.Collections;

public class RateMePopUp : MonoBehaviour {
	
	GameObject pop_up_window_prefab_;
	PopUpWindowPrefab pop_up_window_;
	// Use this for initialization
	
	Analitycs analitycs_;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init()
	{
		analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
		//int matches_played_ = analitycs_.GetCommonIntValue("num_pve_played") + analitycs_.GetCommonIntValue("num_pvp_played");	
		//Debug.Log("Matches Played :"+matches_played_);
		//if ( matches_played_ % 10 == 0 &&  matches_played_ <= 40 && matches_played_ != 0 && Framework.Instance.InstanceComponent<PlayerData>().GetBool("has_rated_game") == false) 
		{
			pop_up_window_prefab_ = (GameObject)  Instantiate( Resources.Load("Framework/PopUpWindowPrefab"));
			pop_up_window_prefab_.SetActive(false);
			pop_up_window_prefab_.transform.parent = transform;
			pop_up_window_ = pop_up_window_prefab_.GetComponent<PopUpWindowPrefab>();
			pop_up_window_.set_destroy_on_closing(true);
			pop_up_window_prefab_.SetActive(true);
			pop_up_window_.Init("Rate EBF", "If you like EBF rate us!!!\nWe'll get you 1000 coins :)", OnYesPressed, OnNoPressed);
		}	
	}
	
	void OnYesPressed ()
	{
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("has_rated_game",true);
		Store store = Framework.Instance.InstanceComponent<Store>();
		store.BuyItem("normal_coins_rate_me",1);
		Framework.Instance.InstanceComponent<PlayerData>().SetValue("should_show_rate_pop_up", false);
		
		//string URL = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=677463016";
		string URL2 = "https://userpub.itunes.apple.com/WebObjects/MZUserPublishing.woa/wa/addUserReview?id=677463016&type=Purple+Software";
		Application.OpenURL(URL2);
	}
	
	void OnNoPressed ()
	{
		
	}
	
}
