namespace wmf
{
	namespace ui
	{
	    public sealed class Utility
	    {
			public static string FormattingWithZeros(string txt, int digits)
			{		
				int zeros = digits - txt.Length;
				for (int i = 0; i < zeros; i++)
					txt = txt.Insert(0, "0");
				
				return txt;
			}
			
			public static string FormattingWithZeros(int number, int digits)
			{		
				return FormattingWithZeros(number.ToString(), digits);
			}
		}
	}
}