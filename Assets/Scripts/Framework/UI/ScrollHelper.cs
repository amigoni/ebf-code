using UnityEngine;
using System.Collections;

public class ScrollHelper : MonoBehaviour {
	
	public GameObject content_container_;
	
	bool scroll_area_is_scrolling_ = false;
	bool scroll_area_is_clicked_ = false;
	bool has_pressed_and_moved_ = false;
	bool touch_enabled_ = true;
	Vector3 prev_scroll_position_ = new Vector3 (0,0,0);
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log("X :"+transform.position.x+" Y:"+transform.position.y);
		
		scroll_area_is_scrolling_ = false;
		
		if ((content_container_.transform.position - prev_scroll_position_).magnitude > 0.5F)
			scroll_area_is_scrolling_ = true;
		
		prev_scroll_position_ = content_container_.transform.position;
		
		if (scroll_area_is_scrolling_ == true && scroll_area_is_clicked_ == true)
			has_pressed_and_moved_ = true;
		else if (scroll_area_is_scrolling_ == false && scroll_area_is_clicked_ == false)
			has_pressed_and_moved_ = false;
		else if (scroll_area_is_scrolling_ == false && scroll_area_is_clicked_ == true)
			has_pressed_and_moved_ = false;
		else if (scroll_area_is_scrolling_ == true && scroll_area_is_clicked_ == false)
			has_pressed_and_moved_ = true;
		//Debug.Log(has_pressed_and_moved_);
		
		if (has_pressed_and_moved_ == false)
			touch_enabled_ = true;
		else
			touch_enabled_ = false;
	}
	
	
	private void OnReleaseTouchScrollArea()
	{
		Debug.Log("Scroll Released");
		Invoke("TriggerRelease", 0.5F);
	}
	
	private void TriggerRelease()
	{
		scroll_area_is_clicked_ = false;
	}
	
	
	public void OnPressedTouchScrollArea()
	{
		Debug.Log("Scroll Pressed x: "+transform.position.x+" Released: "+transform.position.y);
		scroll_area_is_clicked_ = true;
	}
	
	
	public bool GetTouchEnabled ()
	{
		return touch_enabled_;
	}
}
