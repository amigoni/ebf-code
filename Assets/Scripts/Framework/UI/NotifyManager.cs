using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Notification
{
	public List<string> buttons_list_;
	public bool completed_;
	
	public Notification()
	{
		buttons_list_ = new List<string>();
		completed_ = false;
	}
}

public class NotifyManager : MonoBehaviour {
	
	private const string SPRITE_NAME = "NotifySprite";
	
	Notification boss_training_;
	
	// Use this for initialization
	void Start () {
		boss_training_ = new Notification();
		boss_training_.buttons_list_.Add("/MainCamera/ButtonMyBossContainer/ButtonMyBoss");
		boss_training_.buttons_list_.Add("/MainCamera/BossTrainingButtonContainer/BossTrainingTabButtonContainer");
		
		Load();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnLevelWasLoaded(int level)
	{
		NewSceneLoaded();
	}
	
	public void NewSceneLoaded()
	{
		HideAllNotifications();
	}
	
	void HideAllNotifications()
	{
		GameObject[] objs = GameObject.FindGameObjectsWithTag("Notification");
		for(int i = 0; i < objs.Length; i ++)
		{
			string path_name = GetGameObjectPathWithoutName(objs[i]);
				
			if( (boss_training_.completed_ == false ) && !boss_training_.buttons_list_.Contains(path_name) )
			{
				objs[i].GetComponent<MeshRenderer>().enabled = false;
			}
			else if( boss_training_.completed_ )
			{
				objs[i].GetComponent<MeshRenderer>().enabled = false;
			}
		}
	}
	
	public void NotifyButton(GameObject button_obj)
	{
		string path_name = GetGameObjectPath(button_obj);
		if(boss_training_.buttons_list_[boss_training_.buttons_list_.Count -1] == path_name)
		{
			boss_training_.completed_ = true;
			button_obj.transform.FindChild(SPRITE_NAME).GetComponent<MeshRenderer>().enabled = false;
			Save();
		}
	}
	
	public static string GetGameObjectPath(GameObject obj)
	{
	    string path = "/" + obj.name;;
	    while (obj.transform.parent != null)
	    {
	        obj = obj.transform.parent.gameObject;
	        path = "/" + obj.name + path;
	    }
	    return path;
	}
	
	public static string GetGameObjectPathWithoutName(GameObject obj)
	{
	    string path = "";
	    while (obj.transform.parent != null)
	    {
	        obj = obj.transform.parent.gameObject;
	        path = "/" + obj.name + path;
	    }
	    return path;
	}
	
	private void Save()
	{
		PlayerPrefs.SetString("boss_training_.completed_", boss_training_.completed_.ToString());
	}
	
	private void Load()
	{
		boss_training_.completed_ = bool.Parse( PlayerPrefs.GetString("boss_training_.completed_", "false") );
	}
}
