using UnityEngine;
using UnityEditor;
 
public class SetGameSizeWindow: EditorWindow {
 
    private Vector2 size_ = new Vector2(800, 600);
 
    [MenuItem ("WhiteMilkGames/Set Game Size...")]
    static void Init () {
       SetGameSizeWindow window = (SetGameSizeWindow)(EditorWindow.GetWindow(typeof(SetGameSizeWindow)));
    }
 
    public static EditorWindow GetMainGameView() {
       System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
       System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
       System.Object Res = GetMainGameView.Invoke(null,null);
       return (EditorWindow)Res;
    }
 
    void OnGUI () {
       size_.x = EditorGUILayout.IntField("X", (int)size_.x);
       size_.y = EditorGUILayout.IntField("Y", (int)size_.y);
       if (GUILayout.Button("Set")) {
         EditorWindow gameView = GetMainGameView();
         Rect pos = gameView.position;
         pos.y = pos.y - 0;
         pos.width = size_.x;
         pos.height = size_.y + 17;
         gameView.position = pos;
       }
    } 
} 