using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class GetSpriteInfo : EditorWindow {
	
	private const string key_no_collection_ = "WHITE MILK GAME: NO COLLECTION KEY";
	private static Dictionary<string, int> collection_list_ = new Dictionary<string, int>();
	private static List<KeyValuePair<string, string>> base_sprite_list_ = new List<KeyValuePair<string, string>>();
	
	private bool show_collections_ = false;
	private bool calculate_every_frame_ = false;
	private Vector2 scroll_position_ = new Vector2();
	private static Dictionary<string, bool> show_collection_list_ = new Dictionary<string, bool>();
	
	[MenuItem ("WhiteMilkGames/Get Sprite Info...")]
	static void Init () {
		GetSpriteInfo window = (GetSpriteInfo)(EditorWindow.GetWindow(typeof(GetSpriteInfo)));
		window.maxSize = new Vector2(500.0f, 500.0f);
		window.minSize = window.maxSize;
		window.Show();
		
		GetInfo();
	}
	
	void OnGUI()
	{
		EditorGUILayout.BeginVertical();
		
		scroll_position_ = EditorGUILayout.BeginScrollView(
			scroll_position_, 
			GUILayout.Width (500.0f), 
			GUILayout.Height (500.0f)
		);
        
		calculate_every_frame_ = EditorGUILayout.Toggle ("calculate_every_frame_", calculate_every_frame_);
		
		//Show main info	
		int collection_used = collection_list_.Count;
		if(collection_list_.ContainsKey(key_no_collection_))
			collection_used--;
		
		EditorGUILayout.LabelField("Collection used: " + collection_used);
		
		EditorGUILayout.LabelField("Textures name: " + collection_used);
		
		show_collections_ = EditorGUILayout.Toggle ("Show Collections Info", show_collections_);
    	if(show_collections_)
		{
			foreach(KeyValuePair<string, int> collection_pair in collection_list_)
			{
				show_collection_list_[collection_pair.Key] = 
					EditorGUILayout.Toggle ("", show_collection_list_[collection_pair.Key] );
				
				EditorGUILayout.LabelField("Name collection: " + collection_pair.Key + " n: " + collection_pair.Value);
				
				if(show_collection_list_[collection_pair.Key] )
				{
					foreach(KeyValuePair<string, string> base_sprite_pair in base_sprite_list_)
					{
						if( collection_pair.Key == base_sprite_pair.Key)
						{
							EditorGUILayout.LabelField(" - " + base_sprite_pair.Value);
						}
					}
				}
			}
		}
		
		EditorGUILayout.EndScrollView();
		EditorGUILayout.EndVertical();
	}
	
	void Update () {
		if(calculate_every_frame_)
		{
			GetInfo();
		}
	}
	
	static void GetInfo()	
	{
		collection_list_.Clear();
		base_sprite_list_.Clear();
		
		tk2dBaseSprite[] all_base_sprite = (tk2dBaseSprite[])GameObject.FindObjectsOfType(typeof(tk2dBaseSprite));
		foreach(tk2dBaseSprite base_sprite in all_base_sprite)
		{
			IncreaseCollectionValue(
				base_sprite.Collection != null ? base_sprite.Collection.name : key_no_collection_,
				base_sprite.name
			);
		}	
		
	}
	
	static void IncreaseCollectionValue(string collection_name, string base_sprite_name)
	{
		if( collection_list_.ContainsKey( collection_name ) )
		{
			collection_list_[collection_name]++;
		}
		else
		{
			collection_list_.Add( collection_name, 1);
			if(!show_collection_list_.ContainsKey(collection_name))
				show_collection_list_.Add( collection_name, false);
		}
		
		base_sprite_list_.Add( 
			new KeyValuePair<string, string>(collection_name, base_sprite_name)
		);
	}
}
