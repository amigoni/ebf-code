using UnityEngine;
using System.Collections;

public class Framework {
	public enum WMF_DeviceType
	{
		web,
		iphone,
		iphone4,
		iphone5,
		ipad
	}
	private WMF_DeviceType device_type_;
	public WMF_DeviceType device_type()
	{
		return device_type_;
	}
	
	public void set_device_type(WMF_DeviceType device_type)
	{
		device_type_ = device_type;
	}
	
	//Init Singleton
	private static Framework instance_;
	
	private Framework () 
	{
		if (instance_ != null)
		{
			Debug.LogError ("Cannot have two instances of singleton.");
			return;
		}
		//Init ();
	}
 
	public static Framework Instance
	{
		get
		{
			if (instance_ == null)
			{
				instance_ = new Framework ();
#if UNITY_EDITOR
				//Called in InitScene
				instance_.Init();
#endif
			}
 
			return instance_;
		}
	}
	//End Singleton
	
	private GameObject framework_object_ = null;
	private ResourcesManager resources_manager_ = null;
	private GraphicsManager graphics_manager_ = null;
	private AudioManager audio_manager_ = null;
	private SceneManager scene_manager_ = null;
	
	public void Init()
	{	
		if(framework_object_ == null)
		{
			framework_object_ = new GameObject("Framework");
			framework_object_.AddComponent<FrameworkComponent>();
			//framework_object_.AddComponent<tk2dUIManager>();
			GameObject.DontDestroyOnLoad(framework_object_);
			
			graphics_manager_ = new GraphicsManager();
			graphics_manager_.Init();
			resources_manager_ = new ResourcesManager();
#if UNITY_WEBPLAYER
			resources_manager_.set_prefix("Kongregrate");
#elif UNITY_IPHONE || UNITY_ANDROID
			resources_manager_.set_prefix("Mobile");
			//resources_manager_.set_prefix("State2Tuning");
#endif
			audio_manager_ = framework_object_.AddComponent<AudioManager>();
			audio_manager_.Init();
			scene_manager_ = framework_object_.AddComponent<SceneManager>();
			scene_manager_.Init();
			
			InstanceComponent<Options>();
			InstanceComponent<NotifyManager>();
			
			//Kongregate
			GameObject kong_object = new GameObject("Kongregate");
			kong_object.AddComponent<KongregateGameObject>();
#if UNITY_IPHONE || UNITY_ANDROID
			KongregateAPI.GetAPI().Mobile.ButtonHide();
#endif
			GameObject.DontDestroyOnLoad(kong_object);
		}
	}
	
	//Getter
	public GameObject framework_object()
	{
		return framework_object_;
	}
	public ResourcesManager resouces_manager()
	{
		return resources_manager_;
	}
	
	public GraphicsManager graphics_manager()
	{
		return graphics_manager_;
	}
	
	public AudioManager audio_manager()
	{
		return audio_manager_;
	}
	
	public SceneManager scene_manager()
	{
		return scene_manager_;
	}
	
	public T InstanceComponent<T>() where T : Component 
	{
  		T component;
  		GameObject game_object = GameObject.Find("Framework/" + typeof(T).ToString());
  		if(game_object == null) 
		{
  			game_object = new GameObject(typeof(T).ToString());
			game_object.transform.parent = framework_object_.transform;
   			component = game_object.AddComponent<T>();
   			GameObject.DontDestroyOnLoad(game_object);
 		}
		else 
		{
   			component = game_object.GetComponent<T>();
  		}
		
#if UNITY_EDITOR
		if (typeof(T).ToString() == "ResourcesManager" ||
			typeof(T).ToString() == "GraphicsManager" ||
			typeof(T).ToString() == "AudioManager" ||
			typeof(T).ToString() == "SceneManager" )
		{
			Debug.LogError("WMF: for " + typeof(T).ToString() + " use direct Getter!");
		}
#endif
  		return component;
 	}
}
