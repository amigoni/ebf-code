using UnityEngine;
using System.Collections;

public class LocalNotificationsManager : MonoBehaviour {

	void Start () 
	{
#if UNITY_IOS
		if (NotificationServices.scheduledLocalNotifications.Length > 0) 
		{
			NotificationServices.CancelAllLocalNotifications();
			
			LocalNotification setCountNotif = new LocalNotification();
			setCountNotif.fireDate = System.DateTime.Now;
			setCountNotif.applicationIconBadgeNumber = -1;
			setCountNotif.hasAction = false;
			NotificationServices.ScheduleLocalNotification(setCountNotif);
			
			Debug.Log("Local Notifications: Cleared all Notifications");
		}
		
		int random_message = Random.Range(0,3);
		string message = "Something has happend!!!";
		
		if (random_message == 0)
			message = "Something has happend!!!";
		else if (random_message == 1)
			message = "Your boss is beating lots of people!!!";	
		else if (random_message == 2)		
			message = "Your boss has been fought!!!";	
		else if (random_message == 3)
			message = "What's going on in here?";
		
		int random_hour = Random.Range(16,21);
		int random_minute = Random.Range(0,59);
		
		System.DateTime now = System.DateTime.Now;
		System.DateTime new_time = new System.DateTime(now.Year, now.Month, now.Day, random_hour,11,random_minute);
		
		var notif = new LocalNotification();
		notif.fireDate = new_time.AddDays(2);
		notif.alertBody = message;
		notif.applicationIconBadgeNumber = 1;
		NotificationServices.ScheduleLocalNotification(notif);
		
		Debug.Log("Local Notifications: Schedule a Notification for:"+notif.fireDate+" Message: "+notif.alertBody); 
		
		Debug.Log("Local Notifications: Number of Active Notifications: "+NotificationServices.scheduledLocalNotifications.Length);
#endif
	}
}
