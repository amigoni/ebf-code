using UnityEngine;
using System.Collections;

public class FrameworkComponent : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnLevelWasLoaded(int level)
	{

	}
	
	void OnApplicationPause(bool pause)
	{
#if UNITY_IPHONE && !UNITY_EDITOR
		if(pause == false)
		{
			Framework.Instance.InstanceComponent<UpdateDataManager>().CheckIfNeedToUpdate();
			
			/*
			//Session event
			int session = Framework.Instance.InstanceComponent<PlayerData>().GetInt("session");
			session++;
			Framework.Instance.InstanceComponent<PlayerData>().SetValue("session", session);
			AdX.SendSession(session);
			*/
			if(GameObject.Find("Game") != null)
				if(GameObject.Find("Game").GetComponent<Game>() != null)
					if(!GameObject.Find("Game").GetComponent<Game>().paused ())
						GameObject.Find("Game").GetComponent<Game>().PauseGame();
		}
#endif
	}
}
