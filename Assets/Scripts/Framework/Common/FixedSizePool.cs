public class FixedSizePool<T> where T : new() {
    private T[] pool_;
    private int next_item_ = -1;
	private int available_items_ = -1;
	private int capacity_;
	
	//Getter / Setter
	public int available_items()
	{
		return available_items_;
	}
	
	public int capacity()
	{
		return capacity_;
	}
	
    public FixedSizePool(int capacity) {
		capacity_ = capacity;
		
        pool_ = new T[capacity_];
        for (int i = 0; i < capacity_; ++i) {
            pool_[i] = new T();
        }
    }

    public T Fetch() {
        return pool_[++next_item_];
    }

    public void Release(T instance) {
        pool_[next_item_--] = instance;
    }

}