using UnityEngine;
using System.Collections;

public class AssignsCameraAnchor : MonoBehaviour {
	
	
	
	// Use this for initialization
	void Awake () {
		SetCamera();
	}
	
	void Start () {
		SetCamera();
	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<tk2dCameraAnchor>().tk2dCamera == null)
		{
			SetCamera();
		}
	}
	
	
	void OnEnable()
	{
		if(Framework.Instance.scene_manager().GetCurrentCamera() != null)
		{
			SetCamera();
			
			// Assign Camera to Buttons
			tk2dButton[] buttons = GetComponentsInChildren<tk2dButton>(true);
			
			for (int i =0; i < buttons.Length; i++)
			{
				buttons[i].viewCamera =  Framework.Instance.scene_manager().GetCurrentCamera();
			}	
		}
	}
	
	void SetCamera()
	{
		if(Framework.Instance.scene_manager().GetCurrentCamera() != null)
			GetComponent<tk2dCameraAnchor>().tk2dCamera = Framework.Instance.scene_manager().GetCurrentCamera().GetComponent<tk2dCamera>();
	}

}
