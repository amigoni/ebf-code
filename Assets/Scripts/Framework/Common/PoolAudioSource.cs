using UnityEngine;
using System;
using System.Collections;

public class PoolAudioSource : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init(float clip_length) 
	{
		Invoke("Despawn", clip_length);
	}
	
	private void Despawn()
	{
		PoolManager.Despawn(gameObject);
	}
}
