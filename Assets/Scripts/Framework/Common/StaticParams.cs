using UnityEngine;
using System.Collections.Generic;

namespace wmf
{
	public class StaticParams : MonoBehaviour
	{
	
		private Dictionary<string, object> dict_ = new Dictionary<string, object> ();
	
		// Use this for initialization
		void Awake ()
		{
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("IsMultiplayer", false);
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("IsTraining", false);
			Framework.Instance.InstanceComponent<wmf.StaticParams> ().Add ("IsRevenge", false);
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
	
		public void Add (string key, object obj)
		{
			dict_.Add (key, obj);	
		}
	
		public bool Remove (string key)
		{
			return dict_.Remove (key);	
		}
		
		public object Get (string key)
		{
#if UNITY_EDITOR
		if(!ContainsKey(key))
			Debug.LogError("Static Params: " + key + " not found!");
#endif
			return dict_ [key];
		}
	
		public bool ContainsKey (string key)
		{
			return dict_.ContainsKey (key);	
		}
	
		public void ChangeValue (string key, object obj)
		{
			dict_ [key] = obj;
		}
	}
}
