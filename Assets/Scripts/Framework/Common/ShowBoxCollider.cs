using UnityEngine;
using System.Collections;

public class ShowBoxCollider : MonoBehaviour {	
	private GameObject obj_instance_;
	private GameObject obj_with_box_collider_;
	
	// Use this for initialization
	void Start () {
		GameObject obj = (GameObject) Resources.Load("Framework/Common/SpriteBoxCollider");
		obj_instance_ = (GameObject) Instantiate(obj);
		obj_instance_.name = this.name + "_sprite_box_collider";
		obj_instance_.GetComponent<tk2dSprite>().scale = new Vector3(2.0F, 2.0F, 1.0F);
		
		obj_with_box_collider_ = gameObject;
		obj_instance_.transform.parent = Framework.Instance.framework_object().transform;
	}
	
	// Update is called once per frame
	void Update () {
		obj_instance_.transform.position = new Vector3(
			obj_with_box_collider_.transform.position.x + obj_with_box_collider_.GetComponent<BoxCollider>().center.x,
			obj_with_box_collider_.transform.position.y + obj_with_box_collider_.GetComponent<BoxCollider>().center.y,
			-600);
		obj_instance_.GetComponent<tk2dSprite>().scale = obj_with_box_collider_.GetComponent<BoxCollider>().size / 2.0F;
	}
	
	public void ChangeColor()
	{
		obj_instance_.GetComponent<tk2dSprite>().color = new Color(0.0F, 0.0F, 0.0F);
	}
}
