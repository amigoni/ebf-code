using UnityEngine;
using System.Collections;

public class HideOnEndAnimation : MonoBehaviour {
	
	private Animation animation_;
	
	// Use this for initialization
	void Start () {
		animation_ = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		//TO-DO speed up
		if(!animation_.isPlaying)
			GetComponent<MeshRenderer>().enabled = false;
	}
	
	public void Play()
	{
		GetComponent<MeshRenderer>().enabled = true;
		animation_.Play();
	}
}
