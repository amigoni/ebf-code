using UnityEngine;
using System.Collections.Generic;

public class FPSCounter : MonoBehaviour {
	private float update_interval = 0.5f;
	private float accum = 0.0f;			 	// FPS accumulated over the interval
	private float frames = 0; 			 	// Frames drawn over the interval
	private float timeleft = 0.0f;		 	// Left time for current interval
	
	private List<float> fps_list_ = new List<float>();
	
	// Use this for initialization
	void Start () {
	
	}
	
	void Update()
	{
		timeleft -= Time.deltaTime;
	    accum += Time.timeScale/Time.deltaTime;
	    ++frames;
	 
	    // Interval ended - update GUI text and start new interval
	    if( timeleft <= 0.0f )
	    {
			fps_list_.Add( accum/frames );
	        timeleft = update_interval;
	        accum = 0.0f;
	        frames = 0;
	    }
	}
	
	public float GetAvgFPS()
	{
		float sum = 0.0f;
		for(int i = 0; i < fps_list_.Count; i++)
		{
			sum += fps_list_[i];
		}
		return sum / fps_list_.Count;
	}
	
	public float GetMinFPS()
	{
		float min = 99999.0f;
		for(int i = 0; i < fps_list_.Count; i++)
		{
			if(fps_list_[i] < min && fps_list_[i] != 0.0f)
				min = fps_list_[i];
		}
		return min;
	}
}
