using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// Ottimizzare il giro dell'audio 
public class AudioManager : MonoBehaviour
{
	private bool music_actived_ = true;
	private bool sfx_actived_ = true;
	
	private GameObject background_music_object_ = null;
	private GameObject framework_object_ = null;
	
	private List<AudioClip> audio_clips_ = new List<AudioClip>();
	
	public void Init()
	{
		framework_object_ = Framework.Instance.framework_object();
		
		//All sound preloaded
		Object[] all_audio_clip = Resources.LoadAll("Audio");
		for (int i = 0; i < all_audio_clip.Length; i++) 
		{
			if(all_audio_clip[i].name != "Endless Boss Game" && 
			   all_audio_clip[i].name != "Endless Boss Menu"  )	
				AddToPoolAudioClips(all_audio_clip[i].name);
		}
		
	}
	
	public void ChangeOptions(OptionsParams options_params)
	{
		bool old_music_actived_ = music_actived_;
		music_actived_ = options_params.music_on;
		if(old_music_actived_ && !music_actived_)
		{
			StopBackgroundMusic();
		}
		else if(!old_music_actived_ && music_actived_)
		{
			ResumeBackgroundMusic();
		}
			
		sfx_actived_ = options_params.sfx_on;
	}
	
	// Very usefull
	public AudioSource PlayOnPlayer(string clip_path, float volume, float pitch, bool loop)
	{
		AudioClip audio_clip = LoadAudioClipResource(clip_path);
		return Play(audio_clip, GameObject.Find("Player").transform, volume, 1.0F, loop);
	}
	
	public AudioSource Play(string clip_path, Transform emitter, float volume, float pitch, bool loop)
	{
		AudioClip audio_clip = LoadAudioClipResource(clip_path);
		return Play(audio_clip, emitter, volume, pitch, loop);
	}
	
	public AudioSource Play(string clip_path, Transform emitter, float volume, bool loop)
	{
		AudioClip audio_clip = LoadAudioClipResource(clip_path);
		return Play(audio_clip, emitter, volume, 1.0F, false);
	}
	
	public AudioSource Play(AudioClip clip, Transform emitter, float volume, bool loop)
    {
		return Play(clip, emitter, volume, 1.0F, loop);
	}
	
    public AudioSource Play(AudioClip clip, Transform emitter, float volume, float pitch, bool loop)
    {
		if(!sfx_actived_)
			volume = 0.0f;
		
        //Create an empty game object
		GameObject go;
		if(PoolManager.PoolExists("AudioObject") && Application.loadedLevelName == "GameScene")
		{
			go = PoolManager.Spawn("AudioObject");
		}
		else
		{
        	go = new GameObject ("Audio: " +  clip.name);
			go.transform.parent = emitter;
		}
		
        go.transform.position = emitter.position;
 
        //Create the source
        AudioSource source = go.GetComponent<AudioSource>();
		if(source == null)
			source = go.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
		source.loop = loop;
        source.Play ();
		if(!loop)
		{
			if(PoolManager.PoolExists("AudioObject") && Application.loadedLevelName == "GameScene")
				go.GetComponent<PoolAudioSource>().Init(clip.length);
			else
        		Destroy (go, clip.length);
			
		}
        return source;
    }
	
	
	public AudioSource PlayUISound(string clip_path, float volume, float pitch, bool loop)
    {
		AudioClip audio_clip = LoadAudioClipResource(clip_path);
		return Play(audio_clip, transform, volume, pitch, loop);	
    }
	
	public void Stop(AudioSource clip)
	{
		Destroy (clip.gameObject);
	}
	
	public void PlayBackgroundMusic(string clip_path)
	{
		if(background_music_object_ != null)
		{
			if(clip_path == background_music_object_.GetComponent<AudioSource>().clip.name &&
			   background_music_object_.GetComponent<AudioSource>().isPlaying)
				return;
			
			Destroy(background_music_object_);
			background_music_object_ = null;
		}
		
		AudioClip audio_clip = Resources.Load("Audio/Music/" + clip_path) as AudioClip;
		
	    background_music_object_ = new GameObject ("BackgroundMusic: " +  audio_clip.name);
	    background_music_object_.transform.position = framework_object_.transform.position;
	    background_music_object_.transform.parent = framework_object_.transform;
	
	    //Create the source
	    AudioSource source = background_music_object_.AddComponent<AudioSource>();
	    source.clip = audio_clip;
	    source.volume = 1.0f;
	    source.pitch = 1.0f;
		source.loop = true;
	        	
		if(music_actived_)
			source.Play ();
	}
	
	public void StopBackgroundMusic()
	{
		if(background_music_object_ != null)
			background_music_object_.GetComponent<AudioSource>().Stop();
	}
	
	public void ResumeBackgroundMusic()
	{
		if(background_music_object_ != null)
			background_music_object_.GetComponent<AudioSource>().Play();
	}
	
	//Pooling
	public AudioClip LoadAudioClipResource(string clip_path)
	{
		//Contains
		for(int i = 0; i < audio_clips_.Count; i++)
		{
			if(audio_clips_[i].name == clip_path)
				return audio_clips_[i];
		}
		
		Debug.LogWarning("Audio: " + clip_path + " no preloaded!");
		
		AddToPoolAudioClips(clip_path);
		for(int i = 0; i < audio_clips_.Count; i++)
		{
			if(audio_clips_[i].name == clip_path)
				return audio_clips_[i];
		}
		
		return new AudioClip();
	}
	
	public void AddToPoolAudioClips(string clip_path)
	{
		//Contains
		for(int i = 0; i < audio_clips_.Count; i++)
		{
			if(audio_clips_[i].name == clip_path)
				return;
		}
		
		audio_clips_.Add(Resources.Load("Audio/" + clip_path) as AudioClip);
	}
}
