using UnityEngine;
using System.Collections;

public class PopUpWindowCoinsStore : MonoBehaviour {
	
	
	GameObject popup_window_;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init(string currency){
		popup_window_ = gameObject;//(GameObject) Instantiate(Resources.Load("Framework/PopUpWindowPrefab"));
		
		string title_text = "Not enough White Milk Coins?";
		string description_text = "Do you want to buy some?";
		
		if(currency == "coins")
		{
			title_text = "Not enough Coins";
		}
			
		popup_window_.GetComponent<PopUpWindowPrefab>().Init(title_text,description_text, YesCallback, NoCallback);
	}
	
	public void SetZPosition(float z_position)
	{
		popup_window_.transform.position = new Vector3(popup_window_.transform.position.x, popup_window_.transform.position.y, z_position);
	}
	
	void YesCallback(){
		
		float z_position = 0;
		if (transform.position.z < 0)
			z_position = transform.position.z-60;
		
		if(GameObject.Find("Framework/Scenes/CoinsStoreScene/Root") == null)
			Framework.Instance.scene_manager().LoadScene("CoinsStoreScene", TransitionType.add_scene, z_position);	
		else
			GameObject.Find("Framework/Scenes/CoinsStoreScene/Root").GetComponent<CoinsStoreScene>().BackPressed();
	}
				
	void NoCallback(){
			
	}	
}
