using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {
	
	public float full_width_;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SetProgress ( float progressValue){
		GetComponent<tk2dSlicedSprite>().dimensions =  new Vector2(progressValue*full_width_,GetComponent<tk2dSlicedSprite>().dimensions.y);
	}
}
