using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KongregateGameObject : MonoBehaviour {

  string mAuthToken;
  long mUserId;
  bool mGuest;
  bool mRecentPurchase;
  bool mPurchaseReady;
  bool mHasGun = false;
  bool mInventory = false;
  string mCustomStatId = "Score";
  string mCustomStatValue = "10";

  void OnEnable () {

    KongregateAPI.Settings.Debug = true;
    KongregateAPI.Settings.ButtonCalcScreenCoordinates = true; // if the SDK should handle recalculating the button coordinates for different screen sizes (based on 1024/768)
    KongregateAPI.Settings.ApiDomain = GetAPIDomain(); //this should default to m.kongregate.com, you can remove this line all together
    KongregateAPI.Settings.KeenProjectId = GetKeenProjectId();;
    KongregateAPI.Settings.KeenWriteKey = GetKeenWriteKey();
		
    // the following can be removed if not using the AdX Wrapper
    KongregateAPI.Settings.AppleId = GetAppleId();
    KongregateAPI.Settings.AdxId = GetAdxId();

    // KongregateAPI.Settings.DisableAutoAnalytics = true; // Uncomment to disable Automatic Analytics
    // KongregateAPI.Settings.WindowPausesSound = false;
    // KongregateAPI.Settings.WindowPausesUnity = true;

    // by default, the API will be enabled on platforms that it supports (currently: iOS)
    // when using the API on platforms that it doesn't support, it will simply ignore calls to it to fail silently
    // therefore you can keep calls throughout your code in place such as 'kongregate.Stats.Submit("Wins", 1);'
    //
    // if for some reason you do not want Kongregate API to run on a platform that it does support, you can set
    // the following flag to false to have it behave as if it did not support it:
    //
    // KongregateAPI.Enabled = false;

    KongregateAPI kongregate = KongregateAPI.Initialize(GetGameId(), GetAPIKey());
    kongregate.SetEventListener("Kongregate", "HandleKongregateEvent"); //GameObjectName, methodName

    kongregate.Analytics.SetCommonPropsCallback(() => {
      Dictionary<string,object> commonProps = new Dictionary<string, object>();
      commonProps.Add("common prop", "common prop val");
      return commonProps;
    });

    //configure up our button
    kongregate.Mobile.ButtonSetNativeRendering(false); // true by default, false means render in Unity rather than native
    kongregate.Mobile.ButtonSetX(-100);
    kongregate.Mobile.ButtonSetY(-100);
    kongregate.Mobile.ButtonSetSize(48);
    //kongregate.Mobile.ButtonShow();
	kongregate.Mobile.ButtonHide();

    //SetupPurchases();
  }

/*
  void OnGUI () {
    KongregateAPI kongregate = KongregateAPI.GetAPI();

    // isReady() is true if we are on iOS platform and the API is initalized properly
    if (kongregate.IsReady()) {

      // add a simple button to demonstrate hidding and showing the K button
      if(GUI.Button (new Rect (Screen.width/2 - 80 - 5,10,80,40), "Toggle K")) {
        if (kongregate.Mobile.ButtonIsHidden()) {
          kongregate.Mobile.ButtonShow();
        } else {
          kongregate.Mobile.ButtonHide();
        }
      }

      // add a simple button to demonstrate submitting stats
      if(GUI.Button (new Rect (Screen.width/2 + 5,10,80,40), "Submit Win")) {
        kongregate.Stats.Submit("Wins", 1);
      }

      if ( mPurchaseReady ) {
        if(GUI.Button (new Rect (Screen.width/2 - 80 -5,60,80,40), "Purchase")) {
#if UNITY_IPHONE
          StoreKitBinding.purchaseProduct( "consumable_test_unity", 1 );
#endif
#if UNITY_ANDROID
          Debug.Log ("let's buy this thing");
          GoogleIAB.purchaseProduct( "consumable_test_unity" );
#endif
        }
#if UNITY_IPHONE
        if(GUI.Button (new Rect (Screen.width/2 + 5,60,80,40), "Restore")) {
          StoreKitBinding.restoreCompletedTransactions();
        }
#endif
        if (mRecentPurchase) {
          GUI.Box (new Rect (Screen.width/2 - 70,Screen.height/2 - 30,140,60), "Purchase Successful!");
          if (GUI.Button (new Rect (Screen.width/2 - 20,Screen.height/2,40,20), "OK")) {
            mRecentPurchase = false;
          }
        }
      }

      // add text fields to demonstrate submitting generic stat types
      GUI.Label(new Rect(Screen.width/2 + 95, 10, 50, 20), "Stat:");
      mCustomStatId = GUI.TextField(new Rect(Screen.width/2 + 140, 10, 80, 20), mCustomStatId);
      GUI.Label(new Rect(Screen.width/2 + 95, 35, 50, 20), "Value:");
      mCustomStatValue = GUI.TextField(new Rect(Screen.width/2 + 140, 35, 80, 20), mCustomStatValue);
      if(GUI.Button (new Rect(Screen.width/2 + 230, 10, 80, 40), "Submit")) {
        long value = 0;
        if (long.TryParse (mCustomStatValue, out value)) {
              kongregate.Stats.Submit(mCustomStatId, long.Parse(mCustomStatValue));
        }
      }

      if(GUI.Button (new Rect(Screen.width/2 + 430, 10, 100, 40), "Inventory")) {
        mInventory = false;
        kongregate.Mtx.RequestUserItemList();
      }

      if(mInventory) {
        GUI.Label(new Rect(10, 120, 200, 20), "Has AWESOME GUN: " + mHasGun);
      } else {
        GUI.Label(new Rect(10, 120, 200, 20), "Requesting inventory...");
      }

      // add a simple button to demonstrate submitting analytics
      if(GUI.Button (new Rect(Screen.width/2 + 330, 10, 80, 40), "Game Play")) {
        Dictionary<string,object> evt = new Dictionary<string,object>();
        string[] items = new string[]  { "Demon Card","Goblin Card","Angel Card"};
        evt["items"] = items;
        kongregate.Analytics.AddEvent("angrybots_game_play", evt);
      }

      // if our button is not hidden, we need to draw it
      if (!kongregate.Mobile.ButtonIsHidden()) {
        if (kongregate.Mobile.ButtonIsNativeRendering()) {
          //nothing to do, rendering is handled in native SDK
        } else {
          // draw our button
          if (GUI.Button (kongregate.Mobile.ButtonGetRect(), kongregate.Mobile.ButtonGetTexture(), "label")) {
            Debug.Log ("You clicked the Kong button!");
            kongregate.Mobile.OpenKongregateWindow();
          }
        }

      } //IsHidden
    }// IsReady
  }// OnGUI

  void SetupPurchases() {
#if UNITY_IPHONE
    StoreKitManager.productListReceivedEvent += allProducts => {
      Debug.Log( "received total products: " + allProducts.Count );
      mPurchaseReady = true;
    };
    StoreKitManager.purchaseSuccessfulEvent += transaction => {
      Debug.Log( "purchased product: " + transaction );
      KongregateAPI.GetAPI().Mobile.TrackPurchase(transaction.productIdentifier, transaction.quantity);
    };
    StoreKitBinding.requestProductData( new string[] { "consumable_test_unity" } );
#endif
#if UNITY_ANDROID
    GoogleIABManager.billingSupportedEvent += () => {
      Debug.Log ("billing is supported");
      mPurchaseReady = true;
      var skus = new string[] { "consumable_test_unity" };
      GoogleIAB.queryInventory( skus );
    };
    GoogleIABManager.queryInventorySucceededEvent += (purchases, skus) => {
      Debug.Log ("query inventory succeeded");
      GooglePurchase owned = purchases.Find (delegate(GooglePurchase purchase) { return purchase.productId == "consumable_test_unity"; });
      if (owned != null) {
        Debug.Log("we own: " + owned.productId + "  - consuming");
        GoogleIAB.consumeProduct("consumable_test_unity");
      }
    };
    GoogleIABManager.purchaseSucceededEvent += purchase => {
      Debug.Log( "purchased product: " + purchase);
      KongregateAPI.GetAPI().Mobile.TrackPurchase(purchase.productId, 1);
      if (purchase.productId == "consumable_test_unity") {
        GoogleIAB.consumeProduct("consumable_test_unity");
      }
    };
    GoogleIAB.init( GetGooglePublicKey() );
#endif
  }

  void OnApplicationFocus (bool focus) {
	  KongregateAPI kongregate = KongregateAPI.GetAPI();
    if (focus) {
      kongregate.OnResume();
    } else {
      kongregate.OnPause();
    }
  }

	 */
  void HandleKongregateEvent(string eventName) {
    KongregateAPI kongregate = KongregateAPI.GetAPI();
    switch(eventName) {
      case KongregateAPI.KONGREGATE_EVENT_READY:
        // Kongregate API is ready to go
        // You may now access the current Kongregate User, submit stats, etc.
        // You may also check to see if the game auth token is available. If this is the first time
        // the user lauched your game, they may not have an AuthToken yet. A
        // KONGEGATE_EVENT_GAME_AUTH_CHANGED will follow (if they have an internet connection). You only
        // need the Game Auth Token, if you plan to perform server-side verification.
        Debug.Log("Kongregate API is ready");
        kongregate.Mtx.RequestUserItemList();
        break;
      case KongregateAPI.KONGREGATE_EVENT_USER_CHANGED:
        // this could be one of:
        // 1) user was a guest and then logged in/registered
        // 2) user was logged in and then logged out and is now guest
        // 3) user was logged in and then logged out and is now a different user
        // Note that the user id will come back for Guests as well. To check for a
        // guest, use the IsGuest() method.
        // if you are also concerned about the Auth Token, you should instead listen
        // for KONGREGATE_EVENT_USER_AUTH_CHANGED as it is a superset of this event
        mUserId = kongregate.Services.GetUserId();
        mGuest = kongregate.Services.IsGuest();
        Debug.Log("user "+ mUserId+" "+(mGuest ? "is" : "is not")+" a guest");
        kongregate.Mtx.RequestUserItemList();
        break;
      case KongregateAPI.KONGREGATE_EVENT_GAME_AUTH_CHANGED:
        // User's game auth token has changed, either by way of login, logout, or if their password changes.
        // This event will always follow KONGREGATE_EVENT_USER_CHANGED (if they have intenet access).
        // Note that Guests will also have an Auth Token and will cause this to event to fire as well.
        // Guest token is the same for all Guests. you can check if the user is a Guest using the
        // isGuest() method. Their is no need to perform server-side verification for the Guests token.
        // If you want to listen for a single event that covers all the scenarios of a user change or
        // an auth token change, this is the event to listen for
        mUserId = kongregate.Services.GetUserId();
        mAuthToken = kongregate.Services.GetGameAuthToken();
        mGuest = kongregate.Services.IsGuest();
        Debug.Log("user "+ mUserId+" "+(mGuest ? "is" : "is not")+" a guest");
        Debug.Log("game auth has changed to:"+mAuthToken);
        break;
      case KongregateAPI.KONGREGATE_EVENT_OPENING:
        // Kongregate Window is opening
        Debug.Log("Kongregate Window is opening");
        break;
      case KongregateAPI.KONGREGATE_EVENT_CLOSED:
        // Kongregate Window is clsoed
        Debug.Log("Kongregate Window is closed");
        break;
      case KongregateAPI.KONGREGATE_EVENT_USER_INVENTORY:
        // User inventory is now available, we can call hasItem to check for promotional items
        mInventory = true;
        mHasGun = kongregate.Mtx.HasItem("promo-gun");
        Debug.Log("Kongregate inventory received, has gun: " + mHasGun);
        break;
      case KongregateAPI.KONGREGATE_EVENT_SERVICE_UNAVAILABLE:
        // A request to Kongregate failed. This will typically occur when the user does not
        // have a network connection or Kongregate is experiencing a scheduled downtime. Requests
        // will be stored locally until the service becomes available again. Game clients
        // should only need to handle this event if they require server side auth,
        // do not have a game auth token yet, and wish to message to the user that they are unable
        // to validate them.
        Debug.Log("Kongregate Service Unavailable");
        break;
    }
    // the following should be called to let the API know processing is complete
    // this is particularly important for when pausing the game via iOS (default)
    kongregate.MessageReceived(eventName);
  }

  // The following uses reflection to find the Game Id from a Constants file,
  // Replace all of this with your GameID
  long GetGameId() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if ( type == null ) {
      return 0;
    }
    return (long)type.GetField("KONGREGATE_GAME_ID").GetValue(null);
  }

  // The following uses reflection to find the API Key from a Constants file,
  // Replace all of this with your API Key
  string GetAPIKey() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if ( type == null ) {
      return "";
    }
    return (string)type.GetField("KONGREGATE_API_KEY").GetValue(null);
  }

  // The following uses reflection to find the API Domain from a Constants file,
  // Replace all of this with your GameID
  string GetAPIDomain() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if ( type == null ) {
      return "m.kongregate.com";
    }
    return (string)type.GetField("KONGREGATE_API_DOMAIN").GetValue(null);
  }

  // The following returns public key for google services. You may use your own
  // for testing. Kongregate will need to provide you with the public key that
  // will be used once we upload it to the Google play store.
  string GetGooglePublicKey() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if ( type == null ) {
      return "your-public-key";
    }
    return (string)type.GetField("KONGREGATE_GOOGLE_PUBLIC_KEY").GetValue(null);
  }

  // use reflection to find the Keen Product ID from a constant file.
  // Replace with the Keen Product ID for your game.
  string GetKeenProjectId() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if (type == null) {
      return "";
    }
    return (string)type.GetField("KONGREGATE_KEEN_PROJECT_ID").GetValue (null);
  }

  // use reflection to find the Keen Write Key from a constant file.
  // Replace with the Keen Write Key for your game.
  string GetKeenWriteKey() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if (type == null) {
      return "";
    }
    return (string)type.GetField("KONGREGATE_KEEN_WRITE_KEY").GetValue (null);
  }

  // use reflection to find the Apple Id from a constant file.
  // Replace with the Apple Id for your game.
  string GetAppleId() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if (type == null) {
      return "";
    }
    return (string)type.GetField("KONGREGATE_APPLE_ID").GetValue (null);
  }

  // use reflection to find the Adx URL Scheme from a constant file.
  // Replace with the Adx URL Scheme for your game.
  string GetAdxId() {
    System.Type type = System.Type.GetType("KongregateConstants");
    if (type == null) {
      return "";
    }
    return (string)type.GetField("KONGREGATE_ADX_ID").GetValue (null);
  }
	
	public void OpenKongregateWindow()
	{
		KongregateAPI kongregate = KongregateAPI.GetAPI(); 
		if (kongregate.IsReady())
			kongregate.Mobile.OpenKongregateWindow();
	}	
}
