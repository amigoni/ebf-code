using UnityEngine;

public class HugePunch : PowerPunch 
{	
	private AudioManager audio_manager_;
	
	private Vector3 collider_position_ = new Vector3(0.0F, 25.0F, 0.0F);
	private Vector2 collider_size_ = new Vector2(150.0F, 100.0F);
	
	private float stun_time_ = 1.0F;
	
	public override void Init()
	{
		audio_manager_ = Framework.Instance.audio_manager();
		
		base.Init ();
		type_ = PowerPunchType.huge_punch;
		end_type_ = PowerPunchEndType.time_one_shot;
		
		charge_anim_ = "HugePunchCharge";
		charge_idle_anim_ = "HugePunchChargeIdle";
		player_shot_anim_ = "HugePunchShot";
		
		need_invoke_ = true;
		time_to_invoke_ = 0.3F;
		
		stun_time_ = Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("huge_punch");
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "ammo_boost"))
			stun_time_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("ammo_boost");
		
		damage_ *= player_.GetStat("dd_att_level");
	}
	
	public override void Reset()
	{
		remaining_ = 10.0F;
		base.Reset();
	}
	
	public override void Do()
	{
		SetCollider(collider_position_, collider_size_);
			
		base.Do(); 
	}
	
	public override void InvokeMethod()
	{
		DisableCollider();
		base.SetEnd();
	}
	
	public float stun_time()
	{
		return stun_time_;
	}
}
