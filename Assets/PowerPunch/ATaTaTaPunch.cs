using UnityEngine;

public class ATaTaTaPunch : PowerPunch 
{
	private Vector3 collider_position_ = new Vector3(0.0F, 25.0F, 0.0F);
	private Vector2 collider_size_ = new Vector2(60.0F, 120.0F);
	
	public override void Init()
	{
		base.Init ();

		type_ = PowerPunchType.a_ta_ta_ta;
		end_type_ = PowerPunchEndType.time;
		
		charge_anim_ = "ATaTaTaCharge";
		charge_idle_anim_ = "ATaTaTaChargeIdle";
		player_shot_anim_ = "ATaTaTaShot";
		
		need_invoke_repeating_ = true;
		time_to_invoke_ = 0.03f;
		
		damage_ = 1.0f;
		damage_ *= player_.GetStat("dd_att_level");
		
		collider_size_ *= Framework.Instance.graphics_manager().camera_scale();
	}
	
	public override void Reset()
	{
		remaining_ = Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("aaatatata");
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "ammo_boost"))
			remaining_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("ammo_boost");
			
		base.Reset();
	}
	
	public override void Do()
	{
		base.Do(); 
		
		if(audio_source_ == null)
		{
			audio_source_ = audio_manager_.PlayOnPlayer("27_Atatata Fire", 1.0f, 1.0f, true);
		}			

		SetCollider(collider_position_, collider_size_);
		player_.SetHiddenGodMode();
	}
	
	public override void TouchEnded()
	{
		base.TouchEnded();
		
		power_punch_shot_support_.Stop();
		power_punch_shot_support_.renderer.enabled = false;
		player_.ShowAnimations();
		active_ = false;
		
		player_.animations().Stop();
		DisableCollider();
		power_punch_manager_.CancelInvoke();
		
		if(audio_source_ != null)
		{
			audio_manager_.Stop(audio_source_);
			audio_source_ = null;
		}
		
		player_.EndHiddenGodMode();
	}
	
	public override void InvokeMethod()
	{
		if(GetEnableCollider())
		{
			DisableCollider();
		}
		else
		{
			EnableCollider();
		}
	}
}
