using UnityEngine;
using System.Collections;

public class GreatBallOfFire : PowerPunch {	
	public GameObject ball_prefab_;
	
	private Vector3 ball_offset_position_ = new Vector3(65.0F, -5.0F, 0.0F);
		
	private Vector3 collider_position_ = new Vector3(20.0F, 0.0F, 0.0F);
	private Vector2 collider_size_ = new Vector2(25.0F, 25.0F);
	
	public override void Init()
	{
		audio_manager_ = Framework.Instance.audio_manager();
		type_ = PowerPunchType.great_ball_of_fire;
		end_type_ = PowerPunchEndType.consumable;
		//remaining_ = Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("great_ball_of_fire");
		
		charge_anim_ = "GreatBallOfFireCharge";
		charge_idle_anim_ = "GreatBallOfFireChargeIdle";
		player_shot_anim_ = "GreatBallOfFireShot";
		
		ball_prefab_ = LoadPrefab("BallOfFire");
		
		need_invoke_ = true;
		time_to_invoke_ = 0.2F;
		
		base.Init ();
	}
	
	public override void Reset()
	{
		remaining_ = Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("great_ball_of_fire");
		
		base.Reset();
	}
	
	public override void Do()
	{
		base.Do();
		
		SetCollider(collider_position_, collider_size_);
		audio_manager_.PlayOnPlayer("26_Great Ball of Fire Shot", 1.0F, 1.0F, false);
	}
	
	public override void InvokeMethod()
	{
		DisableCollider();
		GameObject ball = power_punch_manager_.InstantiatePrefab(ball_prefab_);		
		ball.transform.Translate(player_.transform.position + ball_offset_position_);
	}
}
