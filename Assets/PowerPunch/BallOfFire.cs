using UnityEngine;
using System.Collections;

public class BallOfFire : MonoBehaviour {
	
	public GameObject explosion_;
	
	private tk2dAnimatedSprite animation_;
	private float speed_;
	
	private float hit_size_y = 40.0f;
	private float boss_damage_ = 15.0F;
	
	private Boss boss_;
	
	// Use this for initialization
	void Start () {
		animation_ = GetComponent<tk2dAnimatedSprite>();
		speed_ = 300.0F;
		
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "ammo_boost"))
			boss_damage_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("ammo_boost");
		
		GetComponent<EnemyInfo>().set_damage(boss_damage_ * GameObject.Find("Player").GetComponent<Player>().GetStat("dd_att_level"));
		
		boss_ = GameObject.Find ("BossPrefab").GetComponent<Boss> ();
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 35)); 
		
		transform.Translate(speed_ * Time.deltaTime, 0.0F, 0.0F);
		
		if(transform.position.x > 550.0F)
			GameObject.Destroy(this.gameObject);
	}
	
	void OnTriggerStay(Collider other){
		OnTriggerEnter(other);
	}
	
	void OnTriggerEnter(Collider other) 
	{
		if(other.tag == "Enemy")
		{	
			float this_baseline = this.GetComponent<ZOrderObject> ().GetBaseline ();
			float other_baseline = other.GetComponent<ZOrderObject> ().GetBaseline ();
			if ((other_baseline > this_baseline - hit_size_y / 2) && 
				(other_baseline < this_baseline + hit_size_y / 2)) 
			{
				if(!other.GetComponent<Enemy>().on_boss_)
					DestroyObject(other.gameObject);
			}
		}
	}
	
	void DestroyObject(GameObject enemy)
	{
		PoolManager.Despawn(enemy);
		
		GameObject obj = PoolManager.Spawn(explosion_);
		obj.transform.position = transform.position;
		
		//Combo
		if(!enemy.name.Contains("MoleEnemy"))
		{
			Enemy enemy_script = enemy.GetComponent<Enemy> ();
			boss_.CheckComboValue (enemy_script.group_id (), enemy_script.id ());
		}
	}
}
