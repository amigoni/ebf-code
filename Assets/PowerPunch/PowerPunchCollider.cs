using UnityEngine;
using System.Collections;

public class PowerPunchCollider : MonoBehaviour {
	
	public GameObject explosion_;
	
	private const float hit_size_y = 40.0f;
	private Boss boss_;
	private PlayerAttackController player_attack_controller_;
	private ZOrderObject z_order_player_;
	
	// Use this for initialization
	void Start () {
		boss_ = GameObject.Find ("BossPrefab").GetComponent<Boss> ();
		player_attack_controller_ = GameObject.Find ("Player").GetComponent<PlayerAttackController>();
		
		z_order_player_ = GameObject.Find("Player").GetComponent<ZOrderObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerStay(Collider other){
		OnTriggerEnter(other);
	}
	
	void OnTriggerEnter(Collider other) 
	{
		if(other.tag == "Enemy")
		{	
			float this_baseline = z_order_player_.GetBaseline ();
			float other_baseline = other.GetComponent<ZOrderObject> ().GetBaseline ();
			if ((other_baseline > this_baseline - hit_size_y / 2) && 
				(other_baseline < this_baseline + hit_size_y / 2)) 
			{
				if(!other.GetComponent<Enemy>().on_boss_)
					DestroyObject(other.gameObject);
			}
		}
	}
	
	void DestroyObject(GameObject enemy)
	{
		PoolManager.Despawn(enemy);
		
		GameObject obj = PoolManager.Spawn(explosion_);
		obj.transform.position = player_attack_controller_.GetCurrentColliderPosition() + new Vector3(62.0f, 0.0f, 0.0f);
		
		//Combo
		if(!enemy.name.Contains("MoleEnemy"))
		{
			Enemy enemy_script = enemy.GetComponent<Enemy> ();
			boss_.CheckComboValue (enemy_script.group_id (), enemy_script.id ());
		}
	}
}
