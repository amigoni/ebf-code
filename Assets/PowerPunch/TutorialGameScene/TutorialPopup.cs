using UnityEngine;
using System.Collections;


public enum TutorialPopUpType
{
	dd_stats_,
	dd_power_punches_,
	boss_stats_,
	boss_build_,
	boss_utilities_,
	boss_weapons_,
	multiplayer_,
	boss_battle_log_,
	dd_battle_log_,
	ebf_kong_plus_,
	feedback_mobile_,
	boss_training_,
	count
}


public class TutorialPopup : MonoBehaviour {
	
	public GameObject dd_stats_;
	public GameObject dd_power_punches_;
	public GameObject boss_stats_;
	public GameObject boss_build_;
	public GameObject boss_utilities_;
	public GameObject boss_weapons_;
	public GameObject multiplayer_;
	public GameObject boss_battle_log_;
	public GameObject dd_battle_log_;
	public GameObject ebf_kong_plus_;
	public GameObject feedback_mobile_;
	public GameObject boss_training_;
	
	public void Init(TutorialPopUpType type, float z_position)
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, z_position);
		
		if ( type == TutorialPopUpType.dd_stats_)
			dd_stats_.SetActive(true);
		else if (type == TutorialPopUpType.dd_power_punches_)
			dd_power_punches_.SetActive(true);
		else if (type == TutorialPopUpType.boss_stats_)
			boss_stats_.SetActive(true);
		else if (type == TutorialPopUpType.boss_build_)
			boss_build_.SetActive(true);
		else if (type == TutorialPopUpType.boss_utilities_)
			boss_utilities_.SetActive(true);
		else if (type == TutorialPopUpType.boss_weapons_)
			boss_weapons_.SetActive(true);
		else if (type == TutorialPopUpType.multiplayer_)
			multiplayer_.SetActive(true);
		else if (type == TutorialPopUpType.boss_battle_log_)
			boss_battle_log_.SetActive(true);
		else if (type == TutorialPopUpType.dd_battle_log_)
			dd_battle_log_.SetActive(true);
		else if (type == TutorialPopUpType.ebf_kong_plus_)
			ebf_kong_plus_.SetActive(true);
		else if (type == TutorialPopUpType.feedback_mobile_)
			feedback_mobile_.SetActive(true);
		else if (type == TutorialPopUpType.boss_training_)
			boss_training_.SetActive(true);
	}
	
	
	// Use this for initialization
	void Start () {
	
	}
	
#if UNITY_ANDROID
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			ClosePressed();
	}
#endif
	
	
	void Pause()
	{
		foreach (AnimationState state in animation)
		{
			state.speed = 0;
		}
	}
	
	void ClosePressed ()
	{
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
		foreach (AnimationState state in animation)
		{
			state.speed = 1;
		}
		Invoke("DestroyMe", 0.5F);
	}
	
	
	void FeedbackPressed()
	{
		Framework.Instance.audio_manager().PlayUISound("37_Menu Button Select", 1.0F, 1.0F, false);
#if UNITY_IPHONE || UNITY_ANDROID
		Debug.Log("Sending Email");
		Application.OpenURL("mailto:support@whitemilkgames.com?subject=Feedback%20EBF%20" + 
			Framework.Instance.InstanceComponent<PlayerData>().GetString("user_name") );
		//Application.OpenURL("http://whitemilkgames.com/support/");
#else
		Application.ExternalEval("window.open('http://whitemilkgames.com/support/','_blank')");
#endif		
	}
	
	
	void DestroyMe()
	{
		Destroy(gameObject);
	}
}
