using UnityEngine;
using System.Collections;

public enum PowerPunchEndType
{
	time,
	time_one_shot,
	consumable,
	count
}

public abstract class PowerPunch
{	
	public PowerPunchType type_;
	
	protected Boss boss_;
	protected Game game_;
	
	protected PowerPunchManager power_punch_manager_;
	protected Player player_;
	
	protected tk2dAnimatedSprite power_punch_support_;
	protected tk2dAnimatedSprite power_punch_shot_support_;
	protected BoxCollider power_punch_collider_;
	
	public GameObject game_object_prefab_;
	public GameObject game_object_instance_;
	
	public float duration_ = 3.0F;
	
	protected bool is_finished_ = false;
	
	private int ammo_consumption_ = 1;
		
	//Charge power punch
	protected string charge_anim_ = "";
	//protected Vector3 charge_anim_offset_ = new Vector3();
	protected string charge_idle_anim_ = "";
	//protected Vector3 charge_idle_anim_offset_ = new Vector3();
	protected string player_shot_anim_ = "";
	//protected Vector3 player_shot_anim_offset_ = new Vector3();
	
	protected PowerPunchEndType end_type_;
	protected float damage_;
	//used to timer or used to count
	protected float remaining_ = 0.0F;
	
	protected bool active_ = false;

	//fake invoke
	protected bool need_invoke_ = false;
	protected bool need_invoke_repeating_ = false;
	protected bool remove_invoke_on_touch_end_ = false;
	
	protected float time_to_invoke_ = 0.0F;
	
	protected AudioManager audio_manager_ = null;
	protected AudioSource audio_source_ = null;
	
	private bool invalidate_next_do_ = false;
	
	public virtual void Init ()
	{
		power_punch_manager_ = GameObject.Find ("Game").GetComponent<PowerPunchManager> ();
		boss_ = GameObject.Find ("BossPrefab").GetComponent<Boss> ();
		game_ = GameObject.Find ("Game").GetComponent<Game> ();
		player_ = GameObject.Find ("Player").GetComponent<Player> ();
		
		power_punch_support_ = GameObject.Find("PowerPunchSupport").GetComponent<tk2dAnimatedSprite>();
		power_punch_shot_support_ = GameObject.Find("PowerPunchShotSupport").GetComponent<tk2dAnimatedSprite>();
		power_punch_collider_ = GameObject.Find("PowerPunchCollider").GetComponent<BoxCollider>();
		audio_manager_ = Framework.Instance.audio_manager();
	}
	
	public virtual void Reset()
	{
		is_finished_ = false;
	}
	
	public virtual void Do()
	{
		active_ = true;
		
		power_punch_shot_support_.renderer.enabled = true;
		power_punch_shot_support_.Play(player_shot_anim_);
		power_punch_shot_support_.animationCompleteDelegate = EndShotAnimation;
		
		player_.HideAnimations();
		
		if(end_type_ == PowerPunchEndType.time_one_shot)
		{
			//SetEnd();
		}
	}
	
	private void EndShotAnimation(tk2dAnimatedSprite anim, int clip_id)
	{
		power_punch_shot_support_.renderer.enabled = false;
		player_.ShowAnimations();
		active_ = false;
		
		if(end_type_ == PowerPunchEndType.consumable)
		{
			remaining_ --;
			player_.UpdatePowerPunchIndicator(remaining_);
		}
		else if(end_type_ == PowerPunchEndType.time_one_shot)
		{
			SetEnd();
		}
	}
	
	public virtual void TouchEnded()
	{
		GameObject.Find("BossPrefab").GetComponent<HitBoss>().EndPowerPunch();
		
		if(power_punch_support_.CurrentClip.name == charge_anim_)
		{
			invalidate_next_do_ = true;
		}
	}
	
	public virtual void InvokeMethod()
	{
	}
	
	public void Intro ()
	{
		PlayChargeAnimation();
		player_.ShowPowerPunchIndicator(remaining_);
		player_.SetHiddenGodMode();
		//set damage
		power_punch_collider_.GetComponent<EnemyInfo>().set_damage(damage_);
	}
	
	public void Outro ()
	{
		TouchEnded();
		PlayExitAnimation();
		player_.HidePowerPunchIndicator();
	}
	
	protected void PlayChargeAnimation()
	{
		power_punch_support_.renderer.enabled = true;
		power_punch_support_.Play(charge_anim_);
		power_punch_support_.animationCompleteDelegate = EndChargeAnimation;
		
		player_.HideAnimations();
	}
	
	private void EndChargeAnimation(tk2dAnimatedSprite anim, int clip_id)
	{		
		power_punch_support_.Play(charge_idle_anim_);
		power_punch_support_.animationCompleteDelegate = null;
		
		player_.EndHiddenGodMode();
		
		player_.ShowAnimations();
		
		audio_manager_.PlayOnPlayer("25_Hero Power Punch Activated", 1.0F, 1.0F,false);
		

		power_punch_manager_.DoPowerPunch();
	}
	
	private void PlayExitAnimation()
	{
		power_punch_support_.renderer.enabled = false;
    	power_punch_manager_.RemovePowerPunch();
	}
	
	//Getter / Setter
	public void set_is_finished(bool is_finished)
	{
		is_finished_ = is_finished;
	}
	
	public bool CheckEnd ()
	{
		if(end_type_ == PowerPunchEndType.time)
		{
			remaining_ -= Time.deltaTime;
			player_.UpdatePowerPunchIndicator(remaining_);
		}
		
		if(remaining_ <= 0.0F)
		{
			SetEnd();
		}
		
		return is_finished_;
	}
		
	protected void SetEnd()
	{
		is_finished_ = true;
		power_punch_support_.Stop();
		DisableCollider();
	}
	
	protected GameObject LoadPrefab(string name)
	{
		return Resources.Load("PowerPunch/"+name) as GameObject;
	}
	
	protected void SetCollider(Vector3 position, Vector2 size)
	{
		power_punch_collider_.transform.position = position;
		power_punch_collider_.transform.position += player_.transform.position;
		power_punch_collider_.size = size;
		power_punch_collider_.size += new Vector3(0.0F, 0.0F, 500.0F);
		power_punch_collider_.enabled = true;
	}
	
	protected void EnableCollider()
	{
		power_punch_collider_.enabled = true;
	}
	
	protected void DisableCollider()
	{
		power_punch_collider_.enabled = false;
	}
	
	public bool GetEnableCollider()
	{
		return power_punch_collider_.enabled;
	}
	
	public virtual int GetAmmoConsumption()
	{
		return ammo_consumption_;
	}
	
	//Getter / Setter
	public bool need_invoke()
	{
		return need_invoke_;
	}
	
	public bool need_invoke_repeating()
	{
		return need_invoke_repeating_;
	}
	
	public float time_to_invoke()
	{
		return time_to_invoke_;
	}
	
	public bool active()
	{
		return active_;
	}
	
	public bool CanDo()
	{
		bool can_do = !active_ && power_punch_support_.CurrentClip.name != charge_anim_ && !invalidate_next_do_;
		invalidate_next_do_ = false;
		return can_do;
	}
}
