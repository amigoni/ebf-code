using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PowerPunchType{
	great_ball_of_fire = 0,
	a_ta_ta_ta = 1,
	huge_punch = 2,
	count = 3
}

public class PowerPunchManager : MonoBehaviour {
	
	private AudioManager audio_manager_;
	
	private Game game_;
	private List<PowerPunch> power_punch_list_ = new List<PowerPunch>();
	private int[] power_punch_value_list_;
	
	private PowerPunch current_power_punch_ = null;
	
	private bool outro_after_invoke_ = false;
	private bool is_usable_ = false;
	
	private PlayerData player_data_;
	
	// Use this for initialization
	void Start () {
		game_ = GameObject.Find("Game").GetComponent<Game>();
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		
		audio_manager_ = Framework.Instance.audio_manager();
	}
	// Update is called once per frame
	void Update () {
		if(current_power_punch_ != null)
		{
			if(current_power_punch_.CheckEnd())
			{
				current_power_punch_.Outro();
				current_power_punch_ = null;
			}
		}
	}
	
	public void Init () {
		//Add from Store
		power_punch_list_.Add(new GreatBallOfFire());
		power_punch_list_.Add(new ATaTaTaPunch());
		power_punch_list_.Add(new HugePunch());
		
		for(int i = 0; i< power_punch_list_.Count; ++i)
		{
			power_punch_list_[i].Init();
		}
	}
	
	public bool SpawnPowerPunch(PowerPunchType type)
	{
		if(type != PowerPunchType.count && current_power_punch_ == null)
		{
			current_power_punch_ = power_punch_list_.Find(x => x.type_ == type);
			
			int current_ammo = 1;
			if (game_.game_mode() == Game.GameMode.singleplayer)
				current_ammo = player_data_.GetInt("dd_ammo");
			else
				current_ammo = game_.GetMultiplayerAmmmo();
			
			if (player_data_.GetBool("tutorial_in_game_done") == false)
				current_ammo = 10;
			
			int ammo_consumption = current_power_punch_.GetAmmoConsumption();			
			if(ammo_consumption <= current_ammo)
			{
				outro_after_invoke_ = false;
				
				if (player_data_.GetBool("tutorial_in_game_done") == true){
					if (game_.game_mode() == Game.GameMode.singleplayer)
						player_data_.SetValue("dd_ammo", current_ammo - ammo_consumption);
					else
					{
						game_.SetMultiplayerAmmo(current_ammo - ammo_consumption);
						player_data_.SetValue("dd_ammo", player_data_.GetInt("dd_ammo") - ammo_consumption);	
					}
				}	
					
				current_power_punch_.Reset();
				current_power_punch_.Intro();	
				
				return true;
			}
			else{
				audio_manager_.PlayOnPlayer("24_Hero Power Punch not enough ammo", 1.0F, 1.0F, false);
				current_power_punch_ = null;
			}
		}
		
		return false;
	}
	
	private void EndPowerPunch()
	{
		if(!IsInvoking("PowerPunchCallback"))
			OutroPowerPunch();
		else
			outro_after_invoke_ = true;
	}
	
	private void OutroPowerPunch()
	{
		current_power_punch_.Outro();
	}
	
	public void RemovePowerPunch()
	{
		current_power_punch_ = null;
	}
	
	public bool DoPowerPunch()
	{
		if(current_power_punch_ != null)
		{
			if(current_power_punch_.CanDo())
			{
				current_power_punch_.Do();
				RegisterCallBack();
				return true;
			}
			
			return true;
		}
		return false;
	}
	
	public void RegisterCallBack()
	{
		if(current_power_punch_.need_invoke_repeating())
		{
			InvokeRepeating("PowerPunchCallback", 0.0F, current_power_punch_.time_to_invoke());
		}
		if(current_power_punch_.need_invoke() && !IsInvoking("PowerPunchCallback"))
		{
			Invoke("PowerPunchCallback", current_power_punch_.time_to_invoke());
		}
	}
	
	public void TouchEnded()
	{
		if(current_power_punch_ != null)
		{
			current_power_punch_.TouchEnded();
		}
	}
	
	public void CancelInvoke()
	{
		CancelInvoke("PowerPunchCallback");
	}
	
	public GameObject InstantiatePrefab(GameObject obj)
	{
		return Instantiate(obj) as GameObject;
	}
				
	private void PowerPunchCallback()
	{
		if(current_power_punch_ != null)
		{
			current_power_punch_.InvokeMethod();
			if(outro_after_invoke_)
				OutroPowerPunch();
		}
	}
	
	public bool IsActive()
	{
		if(current_power_punch_ != null)
			return current_power_punch_.active();
		return false;
	}
	
	public bool IsHugePunchActive()
	{
		if(current_power_punch_ != null && player_data_.GetString("power_punch_equipped") != "huge_punch")
			return current_power_punch_.active();
		return false;
	}
	
	public PowerPunch current_power_punch()
	{
		return current_power_punch_;
	}
}
