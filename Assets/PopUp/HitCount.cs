using UnityEngine;
using System.Collections;

public class HitCount : MonoBehaviour {
	
	private Animation animation_;
	private DigitsWidget digits_widget_;
	private int timer_ = 0;
	private GameObject gui_;
	private int current_hit_counter_ = 0;
	
	void Awake()
	{
		animation_ = GetComponent<Animation>();
		
		digits_widget_ = GameObject.Find("HitDigitsWidget").GetComponent<DigitsWidget>();
		
		gui_ = GameObject.Find("GUI");
		transform.position = new Vector3(
			transform.position.x,
			transform.position.y,
			gui_.transform.position.z
		);
	}
	
	public void Init()
	{
		gameObject.SetActive(true);
		animation_.Play("HitCountEnterAnimation");
	}
	
	public void UpdateCount(int count)
	{
		digits_widget_.UpdateValue(count);
		if(IsInvoking("Hide"))
			CancelInvoke("Hide");
	}
	
	public void PlayExit()
	{
		animation_.Play("HitCountExitAnimation");
		Invoke("Hide",1.0F);
	}
	
	public void Hide(){
		gameObject.SetActive(false);
	}
}
