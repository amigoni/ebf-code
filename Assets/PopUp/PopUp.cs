using UnityEngine;
using System.Collections;

public class PopUp : MonoBehaviour {
	
	private tk2dSprite sprite_;
	private Animation animation_;
	
	// Use this for initialization
	void Start () {
		sprite_ = GetComponentInChildren<tk2dSprite>();
		animation_ = GetComponentInChildren<Animation>();
		
	}
	
	// Update is called once per frame
	void Update () {
		sprite_.Build();
		
		if(!animation_.isPlaying)
		{
			Destroy(this.gameObject);
		}
	}
}
