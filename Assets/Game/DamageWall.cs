using UnityEngine;
using System.Collections;

public class DamageWall : MonoBehaviour {
	
	private Animation animation_;
	private tk2dTextMesh text_mesh_;
	
	public GameObject alert_object_;
	
	private int current_value_ = 20;
	private const int decrease_value_ = -1;
	private const int restore_value_ = 4;
	
	private Boss boss_;
	
	// Use this for initialization
	void Start () {
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		
		/*
		animation_ = GameObject.Find("RocketSignContainer").GetComponent<Animation>();
		text_mesh_ = GameObject.Find("RocketSpriteNumberTextMesh").GetComponent<tk2dTextMesh>();
		
		text_mesh_.text = current_value_.ToString();
		text_mesh_.Commit();
		*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other) 
	{
		//Ignores laser
		Enemy enemy_component = other.GetComponent<Enemy>();
		if(enemy_component != null)
		{
			if(enemy_component.is_destructible())
			{
				//combo
				if(other.name.Contains("Bouncing"))
				{
					if(!other.GetComponent<TapBouncingEnemy>().hitted())
						boss_.InvalidateCombo (enemy_component.group_id (), enemy_component.id());
				}
				else
					boss_.InvalidateCombo (enemy_component.group_id (), enemy_component.id());
				
				//destroy
				PoolManager.Despawn(other.gameObject);
				
				//Analitycs
				if(gameObject.name == "DamageWallPrefabBack")
					Framework.Instance.InstanceComponent<Analitycs>().GetCurrentRound().missiles_dodged += 1;
			}
		}
	}
	
	private void UpdateSignValue(int l_value)
	{
		animation_.Play("RocketSignAnimation");
		
		//Update text
		current_value_ += l_value;
		text_mesh_.text = current_value_.ToString();
		text_mesh_.Commit();
	}
	
	//behaviour
	public void RestoreWall()
	{
		//restore 20%
		UpdateSignValue( restore_value_ ); 
	}
	
}