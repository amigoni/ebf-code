using UnityEngine;
using System.Collections;

enum PlayerState
{
	normal,
	god_mode
};

public class Player : MonoBehaviour {
	
	private AudioManager audio_manager_;
	
	private float initial_health_ = 1.0F;
	private float health_;
	private float defense_ = 1;
	private PlayerState current_state_;
	private bool punch_enabled_ = true;
	private tk2dAnimatedSprite animations_;
	
	public GameObject magnetic_object_;
	private GameObject magnetic_instance_;
	private tk2dAnimatedSprite magnetic_animation_;
	
	private Animation god_animation_;
	private float god_mode_hit_ = 2.0F;
	private float god_mode_hit_counter_ = 0.0F;
	private bool god_mode_power_ = false;
		
	private const float hit_god_duration_ = 1.0F;
	private const float resurrect_start_duration_ = 3.0F;
	private bool god_mode_ = false;
	
	private float magnetism_duration_ = 10.0F;
		
	private int big_shot_number_ = 0;
	
	private Animation end_invulnerability_;
	//power up
	private MeshRenderer magnetics_back_sprite_;
	private MeshRenderer magnetics_sprite_;
	private bool magnetism_ = false;
	
	private MeshRenderer invulnerability_back_sprite_;
	private MeshRenderer invulnerability_sprite_;
	
	//Power Punch Bar
	private tk2dSlicedSprite power_punch_indicator_;
	private GameObject power_punch_indicator_bg_;
	private float power_up_initial_value_ = 0.0F;
	
	//Player details
	private PlayerDetails player_details_;
	
	private DashController dash_controller_;
	
	//TO-DO add to xml
	private const float boss_damage_ = 1.0F;
	private const int boss_xp_ = 1;
	
	private const float x_push_ = -90.0F;
	
	public GameObject pushed_boss_explosion_ = null;
	
	// Use this for initialization
	void Start () {
		audio_manager_ = Framework.Instance.audio_manager();
		dash_controller_ = GameObject.Find("GUI").GetComponent<DashController>();
		
		god_animation_ = GetComponent<Animation>();
		animations_ = GameObject.Find("AnimationSupport").GetComponent<tk2dAnimatedSprite> (); 
			
		magnetics_back_sprite_ = GameObject.Find("MagneticsBackSprite").GetComponent<MeshRenderer>();
		magnetics_sprite_= GameObject.Find("MagneticsSprite").GetComponent<MeshRenderer>();
		
		invulnerability_back_sprite_ = GameObject.Find("InvulnerabilityBackSprite").GetComponent<MeshRenderer>();
		invulnerability_sprite_ = GameObject.Find("InvulnerabilitySprite").GetComponent<MeshRenderer>();
		end_invulnerability_ = GameObject.Find("Invulnerability").GetComponent<Animation>();
		
		power_punch_indicator_ = GameObject.Find("Player/PowerPunchContainer/PowerPunchIndicator").GetComponent<tk2dSlicedSprite>();
		power_punch_indicator_bg_ = GameObject.Find("Player/PowerPunchContainer/PowerPunchIndicatorBg");
		
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "double_bubble"))
			god_mode_hit_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("double_bubble");
			
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "double_magnet"))
			magnetism_duration_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("double_magnet");
	}
	
	// Update is called once per frame
	void Update () {
		//TO-DO this is acrocchio...
		//GetComponent<Rigidbody>().velocity = new Vector3();
	}
	
	//
	public void Hitted(float hit_value)
	{
		//TO-DO new
		if(god_mode_power_)
		{
			god_mode_hit_counter_--;
			Color alpha_color = new Color(1.0F, 1.0F, 1.0F, 1.0F * (god_mode_hit_counter_ / god_mode_hit_ ));
			invulnerability_back_sprite_.GetComponent<tk2dSprite>().color = alpha_color; 
			invulnerability_sprite_.GetComponent<tk2dSprite>().color = alpha_color;
			if(god_mode_hit_counter_ <= 0.0F)
			{
				EndGodModePower();
			}
			SetHiddenGodMode();
			Invoke("EndHiddenGodMode", 1.0f);
		}
		else
		{
			health_ -= (hit_value / defense_);
			
			GetComponent<PlayerAttackController>().EndAttack();
			if( health_ > 0)
			{
				animations_.Play("MainCharacter_hit");
				SetGodMode();
			}
			else
				Die ();
		}
	}
	
	
	public void Die ()
	{
		dash_controller_.SetControllerEnabled(false);
		animations_.Stop();
		animations_.Play("MainCharacter_die");
		animations_.animationCompleteDelegate = delegate {
			animations_.animationCompleteDelegate = null;
			EnableMove();
		};
		DisableMove();
		transform.Find("HitCollider").collider.enabled = false;
		Invoke("PlayDeathSound", 0.3F);
		
		iTween.MoveAdd (gameObject, 
						iTween.Hash ("x", -50.0f, 
									 "time", 1.0f,
									 "easetype", iTween.EaseType.linear));	
	}
	
	
	private void PlayDeathSound(){
		audio_manager_.PlayOnPlayer("22_Hero Death", 1.0F, 1.0F, false);
	}
	
	
	public void AddHeart()
	{
		float real_heart = initial_health_ * 0.2f;
		health_ += real_heart;
		
		if(health_ > initial_health_)
			health_ = initial_health_;
	}
	
	
	public void ResurrectCollider(){
		transform.Find("HitCollider").collider.enabled = true;
	}
	
	
	//God Mode power
	public void SetGodModePower()
	{
		invulnerability_back_sprite_.enabled = true;
		invulnerability_back_sprite_.GetComponent<tk2dSprite>().color = 
					new Color(1.0F, 1.0F, 1.0F, 1.0F);
		invulnerability_sprite_.enabled = true;
		invulnerability_sprite_.GetComponent<tk2dSprite>().color = 
					new Color(1.0F, 1.0F, 1.0F, 1.0F);
		
		god_mode_hit_counter_ = god_mode_hit_;
		god_mode_power_ = true;
	}
	
	public void EndGodModePower()
	{
		invulnerability_back_sprite_.enabled = false;
		invulnerability_sprite_.enabled = false;
		audio_manager_.PlayOnPlayer("30_Invulnerability Bubble Off", 1.0F, 1.0F, false);
		
		god_animation_.Stop();
		
		god_mode_power_ = false;
	}
	
	
	public void EndGodModeFlash()
	{
		end_invulnerability_.Play ("PlayerInvulnerability");
	}
	
	//God Mode
	public void SetGodMode()
	{
		god_animation_.Play("GodMode");
		Invoke("EndGodMode", hit_god_duration_);
		
		god_mode_ = true;
	}
	
	private void EndGodMode()
	{
		GetComponent<MeshRenderer>().enabled = true;
		god_animation_.Stop();
		
		god_mode_ = false;
	}
	
	//Hidden god mode
	public void SetHiddenGodMode()
	{
		god_mode_ = true;
	}
	
	public void EndHiddenGodMode()
	{
		god_mode_ = false;
	}
	
	//Magnetics
	public void ActivateMagnetics()
	{
		if(!magnetism_)
		{
			magnetism_ = true;
			
			magnetics_back_sprite_.enabled = true;
			magnetics_sprite_.enabled = true;
			
			Invoke("DisableMagnetics", magnetism_duration_);
			
			audio_manager_.Play("32_Magnet On Loop", this.transform, 1.0F, true);
		}
	}
	
	public void DisableMagnetics()
	{
		magnetics_back_sprite_.enabled = false;
		magnetics_sprite_.enabled = false;
		
		magnetism_ = false;
	}
	
	//Animations
	public void PlayAnimation(string clip_name)
	{
		animations_.Play(clip_name);
	}
	
	public void PlayAnimation(string clip_name, Vector3 local_position)
	{
		animations_.transform.localPosition = local_position;
		animations_.Play(clip_name);
	}
	
	public void ShowAnimations()
	{
		animations_.renderer.enabled = true;
	}
	
	public void HideAnimations()
	{
		animations_.renderer.enabled = false;
	}
	
	//Getter setter
	public bool GodMode()
	{
		return god_mode_;
	}
	
	public bool god_mode_power()
	{
		return god_mode_power_;
	}
	
	public bool magnetic()
	{
		return magnetism_;
	}
	
	public float health()
	{
		return health_;
	}
	
	public int big_shot_number()
	{
		return big_shot_number_;
	}
	
	public void SetBigShotNumber(int n)
	{
		big_shot_number_ = n;
	}
	
	public tk2dAnimatedSprite animations()
	{
		return animations_;
	}
	
	//Power Punch indicator
	public void ShowPowerPunchIndicator(float initial_value)
	{
		power_up_initial_value_ = initial_value;
		power_punch_indicator_.renderer.enabled = true;
		power_punch_indicator_bg_.renderer.enabled = true;
		UpdatePowerPunchIndicator(initial_value);
	}
	
	public void UpdatePowerPunchIndicator (float current_value)
	{
		power_punch_indicator_.scale = new Vector3 (
			current_value / power_up_initial_value_, 
			power_punch_indicator_.scale.y, 
			power_punch_indicator_.scale.z);
	}
	
	public void HidePowerPunchIndicator()
	{
		power_punch_indicator_.renderer.enabled = false;
		power_punch_indicator_bg_.renderer.enabled = false;
	}
	
	public void Pushed(float x_push)
	{
		DisableMove();
		
		iTween.MoveBy  (gameObject, 
						iTween.Hash ("x", x_push, 
									 "time", 0.4,
									 "onComplete", "PushedEnd",
									 "onCompleteTarget", gameObject, 
									 "easetype", iTween.EaseType.linear));	
		//this.transform.position -= new Vector3(75.0F, 0.0F, 0.0F);
	}
	
	public void Dash(string direction){
		DisableMove();
		
		float x_to_move = 0.0F;
		
		//Debug.Log(GameObject.Find("PlayerWalkableArea").GetComponent<PlayerWalkableArea>().current_player_walkable_area().bounds.size.x);
		
		float right_edge = 270F;
		float left_edge = 80F;
#if UNITY_IPHONE
		if ( Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone4)
		{
			right_edge = 270F;
			left_edge = 80F;
		}	
		else if ( Framework.Instance.device_type() == Framework.WMF_DeviceType.ipad)
		{
			right_edge = 290F;
			left_edge = 60F;
		}
		else if ( Framework.Instance.device_type() == Framework.WMF_DeviceType.iphone5)		
		{
			right_edge = 380F;
			left_edge = 95F;
		}
#elif UNITY_ANDROID
		BoxCollider box_collider = GameObject.Find("PlayerWalkableArea").GetComponent<PlayerWalkableArea>().current_player_walkable_area();
		right_edge = box_collider.gameObject.transform.position.x + (box_collider.size.x * 0.5F);
		left_edge = box_collider.gameObject.transform.position.x - (box_collider.size.x * 0.5F);
#endif
	
		if (direction == "forward"){
			x_to_move = right_edge - transform.position.x;
			//x_to_move = 10.0f;
		}
		else if (direction == "back"){
			x_to_move = -transform.position.x + left_edge;
			//x_to_move = 400.0f;
		}
		else if (direction == "auto"){
			if (transform.position.x >= 240){
			x_to_move = -transform.position.x + left_edge;
			}
			else if (transform.position.x < 240){
				x_to_move = right_edge - transform.position.x;
			}
		}
		
	
		iTween.MoveAdd (gameObject, 
						iTween.Hash ("x", x_to_move , 
									 "time", 0.15,
									 "onComplete", "PushedEnd",
									 "onCompleteTarget", gameObject, 
									 "easetype", iTween.EaseType.linear));
		
		//Tutorial
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("tutorial_in_game_done") == false)
			GameObject.Find("Game").GetComponent<InGameTutorialManager>().PerformDash();
	}
	
	public void PushedEnd()
	{
		Invoke("EnableMove", 0.3F);
	}
	
	public void EnableMove()
	{
		GetComponent<PlayerMoveController>().enabled = true;
	}
	
	public void DisableMove()
	{
		GetComponent<PlayerMoveController>().enabled = false;
	}
	
	public void Resurrect()
	{
		GameObject.Find("GUI").GetComponent<DashController>().SetControllerEnabled(true);
		health_ = initial_health_;
		animations_.Play("MainCharacter_resurrect");
		animations_.animationCompleteDelegate = delegate {
			animations_.animationCompleteDelegate = null;
			EnableMove();
		};
		DisableMove();
		audio_manager_.PlayOnPlayer("31_Resurrection Sound", 1.0f, 1.0f, false);
		Invoke("SetGodMode", 1.0F);
		Invoke("ResurrectCollider", 1.0F);
	}
	
	//Getter / Setter
	public PlayerDetails player_details()
	{
		return player_details_;
	}
	
	public void set_player_details(PlayerDetails player_details)
	{
		player_details_ = player_details;
		
		//reset lifes
		initial_health_ *= player_details_.GetStat("dd_hp_max");
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "life_boost"))
			initial_health_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("life_boost");
		health_ = initial_health_;
		
		defense_ = player_details_.GetStat("dd_def_level");
		
		float attack_ = player_details_.GetStat("dd_att_level");
		if(Framework.Instance.InstanceComponent<PlayerData>().GetBool("utilities_equipped", "att_boost"))
			attack_ *= Framework.Instance.InstanceComponent<Store>().GetCurrentPropertyEffectValueByName("att_boost");
		GameObject.Find("AttackSprite").GetComponent<EnemyInfo>().set_damage(attack_);
		GameObject.Find("AttackSprite").GetComponent<EnemyInfo>().set_xp(boss_xp_);
	}
	
	public float GetInitialHealth(){
		return initial_health_;
	}
	
	public float GetStat(string key)
	{
		return player_details_.GetStat(key);
	}
	
	public bool IsPlayerAlive()
	{
		bool return_value = true;
		if( health_ <= 0)
			return_value = false;
		return return_value;
	}
	
	public bool punch_enabled ()
	{
		return punch_enabled_;
	}
	
	private void EnablePunch()
	{
		punch_enabled_ = true;	
	}
	
	public void PushedByBoss(){
		if(health_ > 0.0f)
		{
			punch_enabled_ = false;
			animations_.Play("MainCharacter_hit2");
			audio_manager_.PlayOnPlayer("21_Hero Gets Hit by Boss push", 1.0F, 1.0F, false);
			Invoke("EnablePunch", 1.0F);
			GameObject obj = PoolManager.Spawn(pushed_boss_explosion_);
			obj.transform.position = transform.position;
		}
	}

	public float HitValueMult()
	{
#if UNITY_WEBPLAYER && UNITY_EDITOR
		return 1.5f;
#else
		return 1.0f;
#endif
	}
	
	public float AttackValueMult()
	{
#if UNITY_WEBPLAYER && UNITY_EDITOR
		return 1.0f;
#else
		return 1.0f;
#endif
	}

}