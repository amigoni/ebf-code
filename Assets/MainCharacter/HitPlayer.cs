using UnityEngine;
using System.Collections;

public class HitPlayer : MonoBehaviour {
	
	private GameObject player_;
	
	private float hit_size_y = 25.0F;
	private float chainsaw_hit_size_y = 40.0F;
	private float laser_size_y = 50.0F;
	bool chain_saw_hit_ = false;
	public GameObject hit_explosion_;
	
	private Boss boss_;
	private AudioManager audio_manager_;
	
	//Analitycs
	private Analitycs analitycs_;
	
	// Use this for initialization
	void Start () {
		player_ = GameObject.Find("Player");
		boss_ = GameObject.Find("BossPrefab").GetComponent<Boss>();
		
		audio_manager_ = Framework.Instance.audio_manager();
		
		//Analitycs
		analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerStay(Collider other) 
	{
		
		if(other.tag == "BossDamage" || 
			other.tag == "Unwalkable" )	
			return;
		
		if(other.name == "ColliderUp" || 
		   other.name == "ColliderDown" || 
	  	   other.name == "ColliderLeft" || 
		   other.name == "ColliderRight" ||
		   other.name == "ColliderLeftUp" || 
		   other.name == "ColliderLeftDown" || 
	  	   other.name == "ColliderRightUp" || 
		   other.name == "ColliderRightDown" )
			return;
		
		if(other.GetComponent<Enemy>() != null )
			if(other.GetComponent<Enemy>().on_boss_)
				return;
		
    	if(other.name.Contains("Tap") ||
		   other.name.Contains("Up") ||
		   other.name.Contains("LaserEnemy") ||
		   other.name.Contains("DamageArea") ||
		   other.name.Contains("RightSpawnDamageArea") || 
		   other.name.Contains("LeftSpawnDamageArea")  )
		{
			if(other.name.Contains("Up") )
			{
				if(other.GetComponent<UpMoveController>().IsOnGround())
				{
					
				}
			}
			
			float player_baseline = player_.GetComponent<ZOrderObject> ().GetBaseline ();
			float other_baseline;
			if(other.name.Contains("DamageArea") || other.name.Contains("ShotDamageArea"))
				other_baseline = boss_.GetComponent<ZOrderObject> ().GetBaseline ();
			else
				other_baseline = other.GetComponent<ZOrderObject> ().GetBaseline ();
			
			float hit_size = hit_size_y;
			
			if(other.name.Contains("DamageArea"))
			{
				hit_size = chainsaw_hit_size_y;
			} else if(other.name.Contains("LaserEnemy"))
			{
				hit_size = laser_size_y;
			}
			
			
			if ((other_baseline > player_baseline - hit_size / 2) && 
				(other_baseline < player_baseline + hit_size / 2)) 
			{
				bool pushed = false;
				if(other.name.Contains("Tap") || other.name.Contains("DamageArea"))
					pushed = true;
				
				GameObject.Find("Game").GetComponent<Game>().PlayerHitted( 
					other.GetComponent<EnemyInfo>().damage (),
					pushed,
					-30.0F);
				
				if(other.name.Contains("Tap") ||
				   other.name.Contains("Up") )
				{
					//Spawn explosion
					GameObject obj = PoolManager.Spawn(hit_explosion_);
					obj.transform.position = transform.position;
					
					PoolManager.Despawn(other.gameObject);
					
					//combo
					Enemy enemy = other.GetComponent<Enemy> ();
					boss_.InvalidateCombo (enemy.group_id (), enemy.id ());
				}
				else if(other.name.Contains("LaserEnemy"))
				{
					//Audio
					//audio_manager_.PlayOnPlayer("CollisionWithLaser", 1.0F, 1.0F, true);
				}
				
				if(	other.name.Contains("RightSpawnDamageArea") || 
		   			other.name.Contains("LeftSpawnDamageArea")   )
				{
					player_.GetComponent<Player>().Pushed(-90.0F);
				}
				
				
				//Send mission update
				if (other.name.Contains ("Tap2"))
					GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.be_hit_by_avoid, 1);
				else if (other.name.Contains ("Up2Enemy"))
					GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.be_hit_by_fast_rockets, 1);
				else if (other.name.Contains ("DamageArea") && chain_saw_hit_ == false){
					GameObject.Find ("Game").GetComponent<Game> ().mission_manager ().UpdateMission (MissionsManager.MissionType.be_hit_by_chainsaw, 1);
					GameObject.Find ("Game").GetComponent<Game> ().CancelChainSawNoHit();
					chain_saw_hit_ = true;
				}

				//Analitycs
				if (other.name.Contains ("Tap"))
				{
					analitycs_.GetCurrentRound().damage_received_from_missiles += other.GetComponent<EnemyInfo>().damage ();
					analitycs_.GetCurrentRound().missiles_hit += 1;
				}
				else if (other.name.Contains ("Up"))
				{
					analitycs_.GetCurrentRound().rockets_hit += 1;
				}
			}
		}
    }
	
	void OnTriggerExit(Collider other){
		if(other.name.Contains("DamageArea"))
			chain_saw_hit_ = false;
	}
}
