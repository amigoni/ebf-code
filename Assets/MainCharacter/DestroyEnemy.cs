using UnityEngine;
using System.Collections;

public class DestroyEnemy : MonoBehaviour
{
	
	private GameObject player_;
	private Game game_;
	private Boss boss_;
	private float hit_size_y = 36.0F;
	public GameObject explosion_;
	public PopUp pop_up_;
	private PlayerData player_data_;
	
	//Analitycs
	private Analitycs analitycs_;
	
	// Use this for initialization
	void Start ()
	{
		player_ = GameObject.Find ("Player");
		boss_ = GameObject.Find ("BossPrefab").GetComponent<Boss> ();	
		game_ = GameObject.Find ("Game").GetComponent<Game> ();	
		player_data_ = Framework.Instance.InstanceComponent<PlayerData>();
		//Analitycs
		analitycs_ = Framework.Instance.InstanceComponent<Analitycs>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnTriggerEnter (Collider other)
	{	
		if (other.tag == "Enemy" || other.tag == "Mole")
		{
			if(!other.GetComponent<Enemy>().on_boss_)
			{
				if(CollideWithPlayer(other))
				{
					//Tutorial
					if (player_data_.GetBool("tutorial_in_game_done")== false)
						game_.GetComponent<InGameTutorialManager>().HitMissile();
					
					if( other.name.Contains ("BouncingTapEnemy") )
					{
						other.GetComponent<TapBouncingEnemy> ().InvertSpeed();
						//Combo
						Enemy enemy_script = other.GetComponent<Enemy> ();
						boss_.CheckComboValue (enemy_script.group_id (), enemy_script.id ());
						//Add xp
						game_.AddXp( (int)other.GetComponent<EnemyInfo>().xp () );
					}
					else if (other.name.Contains ("TapEnemy") ||
					   other.name.Contains ("Up") ||
					   other.name.Contains ("Down") ) 
					{				
						if (other.name.Contains ("Up") && other.GetComponent<UpMoveController> ().IsOnGround ()) {
							if (other.GetComponent<UpMoveController> ().IsDamaged ()) 
							{
								other.GetComponent<Enemy>().SpawnBonus();
								DestroyObject (other.gameObject);	
							} 
							else 
								other.GetComponent<UpMoveController> ().Damaged ();
								
						} 
						else 
						{
							other.GetComponent<Enemy>().SpawnBonus();
							DestroyObject (other.gameObject);
						}
					} 
					else if( other.name.Contains ("MoleEnemy") &&
				      player_.GetComponent<PlayerAttackController> ().GetCurrentClip () == "MainCharacter_down")
					{
						if(CollideWithPlayer(other))
							DestroyObject (other.gameObject);
					}
					
					//Analitycs
					if(other.name.Contains("tap"))
						analitycs_.GetCurrentRound().missiles_punches += 1;
					else if(other.name.Contains("rockets_punches"))
						analitycs_.GetCurrentRound().rockets_punches += 1;
				}
			}
		}
		else if( other.name.Contains ("BoxSprite") )
		{
			other.transform.parent.gameObject.GetComponent<Crate>().DamageIt();
		}
	}
	
	bool CollideWithPlayer(Collider other)
	{
		float player_baseline = player_.GetComponent<ZOrderObject> ().GetBaseline (); //- 10;
		float other_baseline = other.GetComponent<ZOrderObject> ().GetBaseline ();
	
		if ((other_baseline > player_baseline - hit_size_y / 2) && 
			(other_baseline < player_baseline + hit_size_y / 2))
			return true;
		
		return false;
	}
	
	void DestroyObject (GameObject enemy)
	{
		//Destroy (enemy);
		PoolManager.Despawn(enemy);
		
		//Add xp
		game_.AddXp( (int)enemy.GetComponent<EnemyInfo>().xp () );
		
		Transform t = transform;
	  	t.Translate (0.0F, 0.0F, -0.5F);
		
		//Spawn explosion
		GameObject obj = PoolManager.Spawn(explosion_);
		obj.transform.position = t.position;
		/*
		//Spawn explosion
		GameObject obj = PoolManager.Spawn(explosion_);
		obj.transform.position = player_.GetComponent<PlayerAttackController>().GetCurrentColliderPosition();
		*/
		
		game_.AddScore (Game.ScoreValue.enemy);
		
		//Send mission update	
		if (enemy.name.Contains("TapEnemy") && enemy.name.Contains("Fast") == false && enemy.name.Contains("Chasing") == false)
			game_.mission_manager ().UpdateMission (MissionsManager.MissionType.hit_tap_enemy, 1);	
		else if (enemy.name.Contains ("Up"))
			game_.mission_manager ().UpdateMission (MissionsManager.MissionType.hit_up_enemy, 1);
		else if (enemy.name.Contains ("Down"))
			game_.mission_manager ().UpdateMission (MissionsManager.MissionType.hit_down_enemy, 1);
		else if (enemy.name.Contains ("FastTapEnemy"))
			game_.mission_manager ().UpdateMission (MissionsManager.MissionType.hit_fast_enemy, 1);
		else if (enemy.name.Contains ("ChasingTapEnemy"))
			game_.mission_manager ().UpdateMission (MissionsManager.MissionType.hit_chasing_enemy, 1);
		
		
		//Combo
		if(!enemy.name.Contains("MoleEnemy"))
		{
			Enemy enemy_script = enemy.GetComponent<Enemy> ();
			boss_.CheckComboValue (enemy_script.group_id (), enemy_script.id ());
		}
	}
}
