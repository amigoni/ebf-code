using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
	
public enum AttackType
{
	tap = 0,
	swipe_up = 1,
	swipe_down = 2,
	attack_count = 3
}


public struct HitBoxes
{
	public List<Vector2> positions_;
	public List<Vector2> sizes_;
	public List<bool>  collidables_;
};


public class PlayerAttackController : MonoBehaviour
{
	
	private bool controller_enabled_ = true;
	private tk2dAnimatedSprite animations_;
	private GameObject attack_sprite_;
	private HitBoxes[] hit_box_list_;
	
	public GameObject big_shot_prefab_;
	public Player player_script_;
	
	private AudioManager audio_manager_;
	private AudioClip audio_clip_;
	
	private PowerPunchManager power_punch_manager_;
	
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
	private const float anim_timer_ = 0.2f;
	private bool can_do_punch_ = true;
#endif
	
	// Use this for initialization
	void Start ()
	{
		InitHitBoxList();
		
		animations_ = GameObject.Find("AnimationSupport").GetComponent<tk2dAnimatedSprite> ();
		animations_.animationEventDelegate = this.OnAnimationPlaying;
		
		player_script_ = GetComponent<Player>();
		
		attack_sprite_ = GameObject.Find("AttackSprite");
		
		power_punch_manager_ = GameObject.Find("Game").GetComponent<PowerPunchManager>();
		
		//Audio
		audio_manager_ = Framework.Instance.audio_manager();
	}
	
	// Update is called once per frame
	void Update ()
	{		
		if(player_script_.IsPlayerAlive() == true && controller_enabled_ == true)
			GetPlayerInput ();
	}
	
	public void OnAnimationPlaying(tk2dAnimatedSprite sprite, tk2dSpriteAnimationClip clip, tk2dSpriteAnimationFrame frame, int frameNum)
	{
		AttackType attack_type = AttackType.attack_count;
		if(clip.name == "MainCharacter_tap")
		{
			attack_type = AttackType.tap;
		}
		else if(clip.name == "MainCharacter_up")
		{
			attack_type = AttackType.swipe_up;
		}
		else if(clip.name == "MainCharacter_down")
		{
			attack_type = AttackType.swipe_down;
		}
		
		if(attack_type != AttackType.attack_count)
		{
			attack_sprite_.transform.position = hit_box_list_[(int)attack_type].positions_[frameNum];
			attack_sprite_.transform.position += player_script_.transform.position;
			attack_sprite_.GetComponent<BoxCollider>().size = hit_box_list_[(int)attack_type].sizes_[frameNum];
			attack_sprite_.GetComponent<BoxCollider>().size += new Vector3(0.0F, 0.0F, 500.0F);
			attack_sprite_.GetComponent<BoxCollider>().enabled = hit_box_list_[(int)AttackType.tap].collidables_[frameNum];
		}
		else
		{
			attack_sprite_.GetComponent<BoxCollider>().enabled = false;
		}

	}
		
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
	private Vector2 GetPlayerInput()
	{
		Vector2 dir = new Vector2(0.0F, 0.0F);
		
		if(Input.GetKey(KeyCode.RightArrow))
			PlayAnimation( AttackType.tap );
		else if(Input.GetKeyUp(KeyCode.RightArrow))
			TouchEnded();
		
		if(Input.GetKey(KeyCode.UpArrow))
			PlayAnimation( AttackType.swipe_up );
		else if(Input.GetKeyUp(KeyCode.UpArrow))
			TouchEnded();
		
		if(Input.GetKey(KeyCode.DownArrow))
			PlayAnimation( AttackType.swipe_down );
		else if(Input.GetKeyUp(KeyCode.DownArrow))
			TouchEnded();
		
		if(Input.GetButtonDown("Button A"))
			PlayAnimation( AttackType.tap );
		else if(Input.GetButtonUp("Button A"))
			TouchEnded();
		
		if(Input.GetButtonDown("Button Y"))
			PlayAnimation( AttackType.swipe_up );
		else if(Input.GetButtonUp("Button Y"))
			TouchEnded();
		
		if(Input.GetButtonDown("Button B"))
			PlayAnimation( AttackType.swipe_down );
		else if(Input.GetButtonUp("Button B"))
			TouchEnded();
		
		return dir;
	}
#elif UNITY_IPHONE || UNITY_ANDROID
	private Vector2 GetPlayerInput()
	{
		Vector2 dir = new Vector2(0.0F, 0.0F);
		return dir;
	}
#else
	private Vector2 GetPlayerInput()
	{
		return new UnityEngine.Vector2();
	}
#endif
	
	public void DoPowerPunchIfNeeded()
	{
		power_punch_manager_.DoPowerPunch();
	}
	
	public void PlayAnimation (AttackType type)
	{
		//dt_timer_ += Time.deltaTime;
		
		if (player_script_.IsPlayerAlive() == false || 
			player_script_.punch_enabled() == false)
			return;
		
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN				
		if(!can_do_punch_)
			return;
		
		if(animations_.CurrentClip.name == "MainCharacter_hit" ||
		   power_punch_manager_.IsActive())
			return;
#endif
		if(!power_punch_manager_.DoPowerPunch())
		{
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_W
			can_do_punch_ = false;
#endif
			animations_.Stop();
			switch (type) {
			case AttackType.tap:
				animations_.Play ("MainCharacter_tap");
				break;
			case AttackType.swipe_up:
				animations_.Play ("MainCharacter_up");
				break;
			case AttackType.swipe_down:	
				animations_.Play ("MainCharacter_down");
				break;
			}
			//dt_timer_ = 0.0f;

			//Audio
			if (power_punch_manager_.IsHugePunchActive())
				audio_manager_.PlayOnPlayer("29_Huge Punch Misses", 1.0F, 1.0F, false);
			else
			{
				int random = Random.Range(0,2);
				if (random == 0)
					audio_manager_.PlayOnPlayer("20_Get Hero Punch Misses 1v", 1.0F, 1.0F, false);
				else if (random == 1)
					audio_manager_.PlayOnPlayer("20_Get Hero Punch Misses 2v", 1.0F, 1.0F, false);
			}
		}
	}
	
	public void TouchEnded()
	{
		//Debug.Log("TouchEndedTouchEndedTouchEndedTouchEndedTouchEnded");
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_W
		can_do_punch_ = true;
#endif
		power_punch_manager_.TouchEnded();
	}
	
	private void InitHitBoxList ()
	{	
		hit_box_list_ = new HitBoxes[(int)AttackType.attack_count];
		
		TextAsset xml_asset = Resources.Load ("Config/Hitbox") as TextAsset;
		string xml_text = xml_asset.text;
		XmlDocument xml_doc = new XmlDocument ();
		xml_doc.InnerXml = xml_text;
		XmlNodeList nodes = xml_doc.DocumentElement.ChildNodes;
		
		for (int i = 0; i < nodes.Count; ++i) {
			LoadFromChild (nodes [i], (AttackType)i);
		}
	}
	
	private void LoadFromChild (XmlNode child_node, AttackType type)
	{
		//Init lists
		hit_box_list_ [(int)type].positions_ = new List<Vector2>();
		hit_box_list_ [(int)type].sizes_ = new List<Vector2>();
		hit_box_list_ [(int)type].collidables_ = new List<bool>();
		
		XmlNodeList nodes = child_node.ChildNodes;
		
		for (int i = 0; i < nodes.Count; ++i) {
			string child_name = nodes [i].LocalName;
			if (child_name != "#comment") {
				Vector2 position;
				position.x = int.Parse (nodes [i].Attributes [0].Value);
				position.y = int.Parse (nodes [i].Attributes [1].Value);
				hit_box_list_ [(int)type].positions_.Add (position);
					
				Vector2 size;
				size.x = float.Parse (nodes [i].Attributes [2].Value);
				size.y = float.Parse (nodes [i].Attributes [3].Value);
				hit_box_list_ [(int)type].sizes_.Add (size);
					
				bool collidable = nodes [i].Attributes [4].Value == "true" ? true : false;
				hit_box_list_ [(int)type].collidables_.Add (collidable);
			}
		}
	}
	
	public void EndAttack()
	{
		attack_sprite_.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
		attack_sprite_.GetComponent<BoxCollider>().size = new Vector3(0.0F, 0.0F, 0.0F);
		attack_sprite_.GetComponent<BoxCollider>().enabled = false;
	}
	
	//getter
	public string GetCurrentClip()
	{
		return animations_.CurrentClip.name;
	}
	
	
	public void SetControllerEnabled ( bool enabled)
	{
		controller_enabled_ = enabled;
	}
	
	public Vector3 GetCurrentColliderPosition()
	{
		return attack_sprite_.transform.position;
	}
}
