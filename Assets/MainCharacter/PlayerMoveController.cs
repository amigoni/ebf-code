using UnityEngine;
using System.Collections;

public class PlayerMoveController : MonoBehaviour
{
	private tk2dAnimatedSprite animations_;
	private BoxCollider walkable_area_;
	private const float default_speed_x = 230.0F;
	private const float default_speed_y = 150.0F;
	private float speed_x = default_speed_x;
	private float speed_y = default_speed_y;
	private Player player_script_;
	
	private string environment_;
	private EnvironmentSelector environment_selector_;

	private InputController input_controller_;
	private GameObject instance_input_controller_ = null;
	private MoveOnShotType move_on_shot_ = MoveOnShotType.stop;
	
	private bool paused_ = false;
	
	private Vector2 past_direction_;
	private Vector2 current_direction_;
	
	private PowerPunchManager power_punch_manager_;
	
	//Audio
	private AudioSource audio_source_;
	private AudioClip step_left_clip_;
	private AudioClip step_right_clip_;
	
	private bool clamp_actived_ = false;
	
	// Use this for initialization
	void Start ()
	{
		//Determine current Environment
		environment_selector_ = GameObject.Find("BackgroundContainer").GetComponent<EnvironmentSelector>();
		
		player_script_ = GetComponent<Player>();
			
		animations_ = GameObject.Find("AnimationSupport").GetComponent<tk2dAnimatedSprite> ();
		
		//Z Order
		GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 34));
		
		power_punch_manager_ = GameObject.Find("Game").GetComponent<PowerPunchManager>();
		
		//Audio
		audio_source_ = GetComponent<AudioSource> ();
		audio_source_.volume = 0.5F;
		step_left_clip_ = Resources.Load ("Audio/step left") as AudioClip;
		step_right_clip_ = Resources.Load ("Audio/step right") as AudioClip;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (paused_)
			return;
		
		GetComponent<ZOrderObject> ().UpdateBaseline ((int)(- 34));
		
		if (!animations_.Playing && player_script_.IsPlayerAlive() == true)
			animations_.Play ("MainCharacter_idle");
		
		if (player_script_.IsPlayerAlive() == true){
			/*
			if (player_script_.transform.position.y >= walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F ||
			player_script_.transform.position.y <= walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F)
			{
			
			};
			
			if (player_script_.transform.position.x >= walkable_area_.transform.position.x + walkable_area_.size.x * 0.5F ||
			player_script_.transform.position.x <= walkable_area_.transform.position.x - walkable_area_.size.x * 0.5F)
			{
				
			};
			*/
			current_direction_ = GetPlayerInput();
			UpdatePosition (current_direction_);	
		}	
	}

#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
	private Vector2 GetPlayerInput()
	{				
		Vector2 dir = new Vector2(0.0F, 0.0F);
		
		if(Input.GetKey(KeyCode.A))
			dir.x -= 1.0F;
		if(Input.GetKey(KeyCode.D))
			dir.x += 1.0F;
		//TO-DO resolve this f*ck*ng problem, unity is a problem.
		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Z))
			dir.y += 1.0F;
		if(Input.GetKey(KeyCode.S))
			dir.y -= 1.0F;
		
		//xbox 
		//dir.x = Input.GetAxis("Horizontal");
		//dir.y = Input.GetAxis("Vertical");
		
		return dir;
	}
	/*
	private Vector2 GetPlayerInput()
	{
		//return virtual_pad_.GetDirection();
		return d_pad.GetDirection();
	}*/
	
#elif UNITY_IPHONE || UNITY_ANDROID
	private Vector2 GetPlayerInput()
	{
		return input_controller_.GetDirection();
	}
#else
	private Vector2 GetPlayerInput()
	{
		return new UnityEngine.Vector2();
	}
#endif
	
	public void UpdatePosition (Vector2 input_direction)
	{
		float delta_time = Time.deltaTime;
		
		Vector2 update_value = new Vector2(input_direction.x * speed_x, input_direction.y * speed_y) *delta_time ;
		
		
		//Correct for when you are on the edge
		if ((player_script_.transform.position.y >= walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F ||
			player_script_.transform.position.y <= walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F) &&
			Mathf.Abs(input_direction.x) > 0.2F){
			update_value.x = Mathf.Sign(input_direction.x)*speed_x*delta_time;
			//update_value.y = 0;
			//input_controller_.GetComponent<SwipeMotion>().ResetPosition("vertical");
		}
		
		if ((player_script_.transform.position.x >= walkable_area_.transform.position.x + walkable_area_.size.x * 0.5F ||
			player_script_.transform.position.x <= walkable_area_.transform.position.x - walkable_area_.size.x * 0.5F) &&
			Mathf.Abs(input_direction.y) > 0.2F){
			update_value.y = Mathf.Sign(input_direction.y)*speed_y*delta_time;
			//update_value.x = 0;
			//input_controller_.GetComponent<SwipeMotion>().ResetPosition("horizontal");
		}
		
			
		//Adjust for Environment
		if (environment_ == "ice")
		{
			
			float friction = 0.5F;
			float decay = 0.1F;
			
			update_value.x = (past_direction_.x + update_value.x*friction);
			
			if (update_value.x > speed_x*delta_time)
				update_value.x = speed_x*delta_time;
			else if (update_value.x < -speed_x*delta_time)
				update_value.x = -speed_x*delta_time;
			
			if (update_value.x > 0)
				update_value.x -= decay;
			else if (update_value.x < 0)
				update_value.x += decay;
		
			if (Mathf.Abs(update_value.x) < 0.1F)
				update_value.x = 0;
			
			update_value.y = (past_direction_.y + update_value.y*friction);
			if (update_value.y > speed_y*delta_time)
				update_value.y = speed_y*delta_time;
			else if (update_value.y < -speed_y*delta_time)
				update_value.y = -speed_y*delta_time;
			
			if (update_value.y > 0)
				update_value.y -= decay;
			else if (update_value.y < 0)
				update_value.y += decay;
			
			if (Mathf.Abs(update_value.y) < 0.1F)
				update_value.y = 0;
			
		
			update_value = new Vector2(update_value.x, update_value.y);
			past_direction_ = update_value;
		}
		else
			past_direction_ = new Vector2(0,0);
	
		
		if (animations_.CurrentClip.name == "MainCharacter_idle" ||
		 	animations_.CurrentClip.name == "MainCharacter_run_forward" ||
			animations_.CurrentClip.name == "MainCharacter_run_backward" ||
			power_punch_manager_.IsActive() ) 
		{
			
			//play animation
			if ((update_value.x > 0.0F) || (update_value.y > 0.0F)) {	
				if (animations_.CurrentClip.name != "MainCharacter_run_forward")
					animations_.Play ("MainCharacter_run_forward");
			} else if ((update_value.x < 0.0F) || (update_value.y < 0.0F)) {	
				if (animations_.CurrentClip.name != "MainCharacter_run_backward")
					animations_.Play ("MainCharacter_run_backward");
			}
			
			//Update position
			if(move_on_shot_ == MoveOnShotType.stop)
			{
				MoveAnimation(update_value);
			}
		}
		
		if(move_on_shot_ == MoveOnShotType.slow)
		{
			if (animations_.CurrentClip.name == "MainCharacter_tap" ||
		 		animations_.CurrentClip.name == "MainCharacter_up" ||
				animations_.CurrentClip.name == "MainCharacter_down") 
			{	
				update_value.Scale(new Vector2(0.5F, 0.5F));
				MoveAnimation(update_value);
			}
			else
				MoveAnimation(update_value);
		}
			
		//Update position
		if(move_on_shot_ == MoveOnShotType.move)
		{
			MoveAnimation(update_value);
		}
		
		if(clamp_actived_)
			ClampPosition ();
		else
			clamp_actived_ = true;
		
		//check for replay idle
		if ((update_value.x == 0.0F) && (update_value.y == 0.0F)) {
			if ((animations_.CurrentClip.name == "MainCharacter_run_forward" ||
			animations_.CurrentClip.name == "MainCharacter_run_backward")) {
				animations_.Play ("MainCharacter_idle");
			}
		} 
		else 
		{
			//Play Audio
			if (!audio_source_.isPlaying) {
				if (audio_source_.clip == step_right_clip_)
					audio_source_.clip = step_left_clip_;
				else
					audio_source_.clip = step_right_clip_;
				
				audio_source_.Play ();
			}
		}
	}
	
	void MoveAnimation(Vector2 translation)
	{ 
		player_script_.transform.Translate (translation);
	}
	
	void ClampPosition ()
	{
		//x
		if (player_script_.transform.position.x > walkable_area_.transform.position.x + walkable_area_.size.x * 0.5F)
			player_script_.transform.position = new Vector3 (walkable_area_.transform.position.x + walkable_area_.size.x * 0.5F, 
														 player_script_.transform.position.y,
														 player_script_.transform.position.z);
		if (player_script_.transform.position.x < walkable_area_.transform.position.x - walkable_area_.size.x * 0.5F)
			player_script_.transform.position = new Vector3 (walkable_area_.transform.position.x - walkable_area_.size.x * 0.5F, 
														 player_script_.transform.position.y,
														 player_script_.transform.position.z);
		//y
		if (player_script_.transform.position.y > walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F)
			player_script_.transform.position = new Vector3 (player_script_.transform.position.x,
														 walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F,
												      	 player_script_.transform.position.z);
		if (player_script_.transform.position.y < walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F)
			player_script_.transform.position = new Vector3 (player_script_.transform.position.x,
														 walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F, 
													     player_script_.transform.position.z);			
		//casting to int;
		int i_x = (int)player_script_.transform.position.x;
		int i_y = (int)player_script_.transform.position.y;
		
		player_script_.transform.position = new Vector3 (
			i_x,
			i_y,
			player_script_.transform.position.z);	
	}
	
	
	private bool CheckIfInWalkableArea(Vector2 position){
		bool return_value = false;
		
		if (position.x > walkable_area_.transform.position.x + walkable_area_.size.x * 0.5F &&
			position.x < walkable_area_.transform.position.x - walkable_area_.size.x * 0.5F &&
			position.y > walkable_area_.transform.position.y + walkable_area_.size.y * 0.5F &&
			position.y < walkable_area_.transform.position.y - walkable_area_.size.y * 0.5F)
			return_value = true;
		
		return return_value;
	}
	
	public void Pause ()
	{
		paused_ = true;
	
		animations_.Pause ();
	}
	
	public void Resume ()
	{
		paused_ = false;
		
		animations_.Resume ();
	}
	
	//Get Hit 
	public Vector2 GetHitPosition ()
	{
		Vector2 player_position = player_script_.transform.position;
		float player_baseline = GetComponent<ZOrderObject> ().GetBaseline ();
		return new Vector2 (player_position.x, player_baseline);
	}
	
	public void UpdateOptions(OptionsParams options_params)
	{		
#if UNITY_IPHONE || UNITY_ANDROID
		//controller
		if(input_controller_ == null || input_controller_.type() != options_params.input_controller_type)
		{
			DestroyObject(instance_input_controller_);
			GameObject prefab = null;
			
			if(options_params.input_controller_type == InputControllerType.d_pad)
				prefab = Resources.Load("GUI/DPad") as GameObject;
			else if(options_params.input_controller_type == InputControllerType.virtual_pad)
			{
				prefab = Resources.Load("GUI/VirtualPad") as GameObject;
				prefab.GetComponent<VirtualPad>().set_floating(options_params.floating);
			}
			else if(options_params.input_controller_type == InputControllerType.swipe_motion)
			{
				prefab = Resources.Load("GUI/SwipeMotion") as GameObject;
			}
			
			instance_input_controller_ = Instantiate(prefab) as GameObject;
			instance_input_controller_.transform.parent = GameObject.Find("GUI").transform;
			input_controller_ = instance_input_controller_.GetComponent<InputController>();
		}
		//Debug.Log("Move Controller is: "+options_params.input_controller_type);
#endif
	}
	
	public void set_walkable_area(BoxCollider walkable_area)
	{
		walkable_area_ = walkable_area;
	}
	
	public void InPuddle(){
		speed_x = default_speed_x*0.7F;
		speed_y = default_speed_y*0.7F; //Slows down a lot if below 0.5 only in vertical
	}
	
	public void OutOfPuddle(){
		speed_x = default_speed_x;
		speed_y = default_speed_y;
	}
	
	public void StopMove(){
		speed_x = 0;
		speed_y = 0;
	}
	
	public void UpdateEnvironment(string environment)
	{
		environment_ = environment;
		Debug.Log("Updated Environment in PlayerMoveController to: "+environment);
	}
	
	
	public Vector2 GetCurrentDirection(){
		return current_direction_;
	}
}
